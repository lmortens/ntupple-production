#ifndef MyAnalysis_MyTagAndProbeAnalysis_H
#define MyAnalysis_MyTagAndProbeAnalysis_H

#include "MyAnalysis/Helpers.h"
#include "MyAnalysis/particleWrapper.h"

#include <EventLoop/Algorithm.h>
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "JetResolution/JERTool.h"
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include <TH1.h>
#include <TTree.h>
#include <TRandom3.h>
#include "IsolationSelection/IsolationSelectionTool.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include "ElectronPhotonShowerShapeFudgeTool/ElectronPhotonShowerShapeFudgeTool.h"
#include "ElectronPhotonShowerShapeFudgeTool/TElectronMCShifterTool.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "TriggerMatchingTool/MatchingTool.h"
#include <MuonAnalysisInterfaces/IMuonSelectionTool.h>
#include "MuonAnalysisInterfaces/IMuonEfficiencyScaleFactors.h"
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"
#include <AsgTools/AnaToolHandle.h>
// #include "EgammaAnalysisInterfaces/IAsgPhotonIsEMSelector.h"
// #include "EgammaAnalysisInterfaces/IAsgElectronLikelihoodTool.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"
#include "ElectronPhotonSelectorTools/AsgElectronChargeIDSelectorTool.h"
#include "ElectronPhotonSelectorTools/AsgForwardElectronLikelihoodTool.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODCore/ShallowAuxContainer.h"
// GRL
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"
#include <tuple>

#include "MyAnalysis/LightGBMpredictor.h"
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
#include "ElectronEfficiencyCorrection/ElectronChargeEfficiencyCorrectionTool.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "EgammaAnalysisInterfaces/IAsgElectronEfficiencyCorrectionTool.h"

#include "JetCalibTools/JetCalibrationTool.h"

#include "METInterface/IMETMaker.h"

#include "IFFTruthClassifier/IFFTruthClassifier.h"


// Overlap removal tool, how to properly set-up (also read README)!
// /cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.29/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/AnalysisCommon/AssociationUtils/util/OverlapRemovalTester.cxx

// Last include
#include "MyAnalysis/Config.h"
#pragma GCC diagnostic push
MORE_DIAGNOSTICS()

typedef unsigned int uint;
typedef unsigned short int uint16;

template<typename T=float>
struct hist_bins {
    TString name;
    int nbins;
    float xmin, xmax;
    T* var;
    
    hist_bins( TString _name, int _nbins, float _xmin, float _xmax, T* _var ) : name(_name), nbins(_nbins), xmin(_xmin), xmax(_xmax), var(_var) {}
    hist_bins& operator= (const hist_bins&) = delete;
};


// Member variables have an identifier and underscore infront
// They are differently named to easily distinguish between them
// m_ is for regular member variables like those set by the run script, set in the initialization steps, or set in the beginning of execute. They're all used in multiple functions.
// hist_ is used by histograms
// count_ will be used by the counters that will be added to the histograms
// event_ is for event-level variables added to the output ntuple
// tracks_ is for the track collection added to the output ntuple
// BC_ is used for the bunch crossing collection added to the output ntuple
// Z_ is used for the Z candidate added to the output ntuple
// tag_ is used for the used tag added to the output ntuple
// p_ is used for the used probe added to the output ntuple
// p_cell_ is used for the cells of the probe added to the output ntuple
// pX_ is used for all the aux variables of the probe added to the output ntuple
// pFX_ is used for all forward-specific aux variables of the probe added to the output ntuple

class MyTagAndProbeAnalysis: public EL::Algorithm {
public:

    long int m_eventCounter; //!
    long int m_startTime; //!

    //============================================================================
    // Steering variables set by the run script
    //============================================================================

    std::string m_outputName; 

    bool Match(const std::vector<const xAOD::IParticle*>& recoObjects, const std::string& chain); //!


    //============================================================================
    // Settings defined by the configuration (read in initialize())
    //============================================================================

    // Non-physics
    int m_progressInterval; //!

    // Meta settings
    // Passed to the output trees
    // 0 = T&P (signal)
    // 1 = just dump reco e/gamma from egammaTruthParticles
    // 2 = background
    UChar_t m_analysisType; //!

    // Tag and probe
    double m_mZ; //!
    double m_mZLowerCutSignal; //!

    double m_tagPt; //!
    double m_tagPairPt; //!
    double m_probePt; //!
    double m_probeEta; //!

    // Background
    double m_backgroundPt; //!
    double m_backgroundmZVetoRange; //!
    double m_backgroundmWTransVeto; //!
    double m_backgroundEta; //!

    // Fill switches (save time by not saving stuff you don't need!^TM)
    bool m_fillpXVariables; //!
    bool m_fillBtags; //!
    bool m_fillTracks; //!
    bool m_fillPU; //!

    // Tool settings
    bool m_triggerMathcingUseTriggerMatching; //!
    double m_triggerMatchingdR; //!

    bool m_ShowerShapeFudgeApplyOnElectrons;  //!
    bool m_ShowerShapeFudgeApplyOnPhotons;  //!



    //============================================================================
    // Global variables
    //============================================================================

    // defining the output file name and tree that we will put in the output ntuple, also the one branch that will be in that tree
    TTree *m_el_tree; //!
    TTree *m_ga_tree; //!

    TRandom3 *m_rnd; //!

    // Containers used in several places
    const xAOD::EventInfo* m_eventInfo = nullptr; //!
    xAOD::ElectronContainer* m_electrons = nullptr; //!
    xAOD::MuonContainer* m_muons = nullptr; //!
    xAOD::PhotonContainer* m_photons = nullptr; //!
    // xAOD::TrackParticleContainer* m_gsfTracks = nullptr; //!
    std::unique_ptr< xAOD::TrackParticleContainer > m_indetTracks; //!
    std::unique_ptr< xAOD::ShallowAuxContainer > m_indetTracksAux; //!
    const xAOD::MissingET* m_METmaker = nullptr;//!
    xAOD::JetContainer* m_jets = nullptr; //!
    const xAOD::VertexContainer* m_PVs = nullptr; //!

    xAOD::MissingETContainer*    m_newMetContainer    = nullptr;//!
    xAOD::MissingETAuxContainer* m_newMetAuxContainer = nullptr; //!
    const xAOD::MissingETContainer* m_coreMet  = nullptr;//!
    const xAOD::MissingETAssociationMap* m_metMap = nullptr;//!
    const xAOD::TruthParticleContainer* m_truthContainer = nullptr; //!

    // Prescaling counter for analysisType=3 for particles with pt < 10 GeV; only every third is saved
    size_t prescalingCounter = 0; //!



    //============================================================================
    // Tools
    //============================================================================

    // GoodRunsListSelectionTool *m_grl; //!
    JetCalibrationTool* m_jetCalibrationTool = nullptr; //!
    asg::AnaToolHandle<IMETMaker> m_metutil; //!
        
    std::vector<CP::SystematicSet> m_sysList; //!
    Trig::TrigDecisionTool *m_trigDecisionTool = nullptr; //!
    TrigConf::xAODConfigTool *m_trigConfigTool = nullptr; //!
    ElectronPhotonShowerShapeFudgeTool* m_MCShifterTool; //!
    Trig::MatchingTool *m_tmt = nullptr; //!
    CP::IsolationSelectionTool *m_isolationSelectionToolLoose = nullptr; //!
    asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionMedium; //!
    CP::MuonCalibrationAndSmearingTool* m_muonCalibrationSmearingTool = nullptr; //!
    CP::MuonEfficiencyScaleFactors* m_muonSF = nullptr; //!
    CP::MuonEfficiencyScaleFactors* m_muonSFIso = nullptr; //!
    asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl; //!
    asg::AnaToolHandle<CP::IPileupReweightingTool> m_PileupReweighting; //!

    AsgElectronChargeIDSelectorTool* m_ECIDSToolLoose = nullptr; //!
    AsgPhotonIsEMSelector* m_photonTightIsEMSelector = nullptr; //!
    AsgPhotonIsEMSelector* m_photonLooseIsEMSelector = nullptr; //!
    AsgElectronLikelihoodTool* m_electronSelectionToolTight = nullptr; //!
    AsgElectronLikelihoodTool* m_electronSelectionToolMedium = nullptr; //!
    AsgElectronLikelihoodTool* m_electronSelectionToolLoose = nullptr; //!
    AsgElectronEfficiencyCorrectionTool* m_electronSF = nullptr; //!
    CP::EgammaCalibrationAndSmearingTool* m_electronCalibrationSmearingTool = nullptr; //!
    CP::EgammaCalibrationAndSmearingTool* m_electronCalibrationTool = nullptr; //!
    CP::ElectronChargeEfficiencyCorrectionTool* m_electronChargeIdSF = nullptr; //!
    IFFTruthClassifier* m_iffTruthClassifier = nullptr; //!
    
    LightGBMpredictor* m_lgbm_pdfcutvars = nullptr; //!
    LightGBMpredictor* m_lgbm_pdfvars = nullptr; //!
    LightGBMpredictor* m_lgbm_iso = nullptr; //!
    LightGBMpredictor* m_lgbm_isocalo = nullptr; //!
    LightGBMpredictor* m_lgbm_isotrack = nullptr; //!        
    
    
    std::vector<double> pdfcutvars_features; //!
    std::vector<double> pdfvars_features; //!
    std::vector<double> iso_features; //!
    std::vector<double> isocalo_features; //!
    std::vector<double> isotrack_features; //!
    

    //============================================================================
    // TODO: Output histograms and their counters
    //============================================================================

    std::vector<TH1D*> m_histCutAccum; //!
    std::vector<TH1D*> m_histCutIndiv; //!

    TH1D* m_sumOfWeights; //!
    size_t m_tagElectronCutHist; //!
    size_t m_preTagElectronCutHist; //!
    size_t m_probeElectronCutHist; //!
    size_t m_leptonPairCutHist; //!
    size_t m_muLeptonPairCutHist; //!
    size_t m_probeZElectronCutHist; //!
    TH2D* m_leptonPairGoodProbeCount; //!

    size_t m_backgroundCentralElectronsHist; //!
    TH1D* m_backgroundCentralElectronsmWTrans; //!
    TH1D* m_backgroundCentralElectronsmZ; //!

    size_t m_tagJpsiElectronCutHist; //!
    size_t m_probeJpsiElectronCutHist; //!
    size_t m_leptonJpsiPairCutHist;
    TH1D* m_JpsiMass; //!
    TH1D* m_JpsiLifetime; //!
    
    size_t m_tagMuonCutHist; //!
    
    
    std::vector< hist_bins<float>* > m_hist_bins_float; //!
    std::vector< hist_bins<double>* > m_hist_bins_double; //!
    std::vector< hist_bins<int>* > m_hist_bins_int; //!
    std::vector< hist_bins<UChar_t>* > m_hist_bins_UChart; //!
    std::vector< hist_bins<bool>* > m_hist_bins_bool; //!
    
    std::vector< std::map< TString, TH1* > > m_hists; //!


    void histadd(TString name, int nbins, float xmin, float xmax, float* var) {
        m_hist_bins_float.push_back( new hist_bins<float>(name, nbins, xmin, xmax, var));
    }    
    void histadd(TString name, int nbins, float xmin, float xmax, double* var) {
        m_hist_bins_double.push_back( new hist_bins<double>(name, nbins, xmin, xmax, var));
    }        
    void histadd(TString name, int nbins, float xmin, float xmax, int* var) {
        m_hist_bins_int.push_back( new hist_bins<int>(name, nbins, xmin, xmax, var));
    }    
    void histadd(TString name, int nbins, float xmin, float xmax, UChar_t* var) {
        m_hist_bins_UChart.push_back( new hist_bins<UChar_t>(name, nbins, xmin, xmax, var));
    }            
    void histadd(TString name, int nbins, float xmin, float xmax, bool* var) {
        m_hist_bins_bool.push_back( new hist_bins<bool>(name, nbins, xmin, xmax, var));
    }                
    
    void hists_for_cuts( TString suffix, long unsigned int i, bool signal); //!
    void fill_hists( TString suffix, long unsigned int i); //!
    void fill_hists_truth(TString suffix, long unsigned int initial_idx, long unsigned int diff_idx, bool Truth); //!


    std::map<int,float> m_crossSections;
    std::map<int,float> m_kFactors;
    std::map<int,float> m_filterEffs;    

    TTree* m_treeStatistics; //!
    int ntagElectrons; //!
    int ntagMuons; //!
    int nprobeCentralElectrons; //!
    int ntagJpsiElectrons; //!
    int nprobeJpsiElectrons; //!



    //============================================================================
    // Output tree branch variables
    //============================================================================

    double JpsiWeight; //!
    double bkgWeight; //!


    // Misc.
    std::string m_samplePath; //!

    std::vector<std::vector<float>> em_barrel_Lr0; //!
    std::vector<std::vector<float>> em_barrel_Lr1; //!
    std::vector<std::vector<float>> em_barrel_Lr2; //!
    std::vector<std::vector<float>> em_barrel_Lr3; //!
    std::vector<std::vector<float>> em_endcap_Lr0; //!
    std::vector<std::vector<float>> em_endcap_Lr1; //!
    std::vector<std::vector<float>> em_endcap_Lr2; //!
    std::vector<std::vector<float>> em_endcap_Lr3; //!
    std::vector<std::vector<float>> lar_endcap_Lr0; //!
    std::vector<std::vector<float>> lar_endcap_Lr1; //!
    std::vector<std::vector<float>> lar_endcap_Lr2; //!
    std::vector<std::vector<float>> lar_endcap_Lr3; //!
    std::vector<std::vector<float>> tile_barrel_Lr1; //!
    std::vector<std::vector<float>> tile_barrel_Lr2; //!
    std::vector<std::vector<float>> tile_barrel_Lr3; //!
    std::vector<std::vector<float>> tile_gap_Lr1; //!
    
    std::vector<std::vector<float>> time_em_barrel_Lr0; //!
    std::vector<std::vector<float>> time_em_barrel_Lr1; //!
    std::vector<std::vector<float>> time_em_barrel_Lr2; //!
    std::vector<std::vector<float>> time_em_barrel_Lr3; //!
    std::vector<std::vector<float>> time_em_endcap_Lr0; //!
    std::vector<std::vector<float>> time_em_endcap_Lr1; //!
    std::vector<std::vector<float>> time_em_endcap_Lr2; //!
    std::vector<std::vector<float>> time_em_endcap_Lr3; //!
    std::vector<std::vector<float>> time_lar_endcap_Lr0; //!
    std::vector<std::vector<float>> time_lar_endcap_Lr1; //!
    std::vector<std::vector<float>> time_lar_endcap_Lr2; //!
    std::vector<std::vector<float>> time_lar_endcap_Lr3; //!
    std::vector<std::vector<float>> time_tile_barrel_Lr1; //!
    std::vector<std::vector<float>> time_tile_barrel_Lr2; //!
    std::vector<std::vector<float>> time_tile_barrel_Lr3; //!
    std::vector<std::vector<float>> time_tile_gap_Lr1; //!

    std::vector<std::vector<int>> gain_em_barrel_Lr0; //!
    std::vector<std::vector<int>> gain_em_barrel_Lr1; //!
    std::vector<std::vector<int>> gain_em_barrel_Lr2; //!
    std::vector<std::vector<int>> gain_em_barrel_Lr3; //!
    std::vector<std::vector<int>> gain_em_endcap_Lr0; //!
    std::vector<std::vector<int>> gain_em_endcap_Lr1; //!
    std::vector<std::vector<int>> gain_em_endcap_Lr2; //!
    std::vector<std::vector<int>> gain_em_endcap_Lr3; //!
    std::vector<std::vector<int>> gain_lar_endcap_Lr0; //!
    std::vector<std::vector<int>> gain_lar_endcap_Lr1; //!
    std::vector<std::vector<int>> gain_lar_endcap_Lr2; //!
    std::vector<std::vector<int>> gain_lar_endcap_Lr3; //!
    std::vector<std::vector<int>> gain_tile_barrel_Lr1; //!
    std::vector<std::vector<int>> gain_tile_barrel_Lr2; //!
    std::vector<std::vector<int>> gain_tile_barrel_Lr3; //!
    std::vector<std::vector<int>> gain_tile_gap_Lr1; //!

    std::vector<std::vector<float>> noise_em_barrel_Lr0; //!
    std::vector<std::vector<float>> noise_em_barrel_Lr1; //!
    std::vector<std::vector<float>> noise_em_barrel_Lr2; //!
    std::vector<std::vector<float>> noise_em_barrel_Lr3; //!
    std::vector<std::vector<float>> noise_em_endcap_Lr0; //!
    std::vector<std::vector<float>> noise_em_endcap_Lr1; //!
    std::vector<std::vector<float>> noise_em_endcap_Lr2; //!
    std::vector<std::vector<float>> noise_em_endcap_Lr3; //!
    std::vector<std::vector<float>> noise_lar_endcap_Lr0; //!
    std::vector<std::vector<float>> noise_lar_endcap_Lr1; //!
    std::vector<std::vector<float>> noise_lar_endcap_Lr2; //!
    std::vector<std::vector<float>> noise_lar_endcap_Lr3; //!
    std::vector<std::vector<float>> noise_tile_barrel_Lr1; //!
    std::vector<std::vector<float>> noise_tile_barrel_Lr2; //!
    std::vector<std::vector<float>> noise_tile_barrel_Lr3; //!
    std::vector<std::vector<float>> noise_tile_gap_Lr1; //!
    
    bool m_gainNoiseImages; //!
    bool m_fine_eta; //!
    bool m_fine_phi; //!
    
    static const double m_eta_bins[8]; //!
    static const double m_fine_eta_bins[57]; //!
    static const double m_phi_bins[12]; //!
    static const double m_fine_phi_bins[56]; //!    
    const double m_phi_len = 2. * TMath::Pi() / 256.; //!
    const double m_fine_phi_len = 2. * TMath::Pi() / ( 256. * 5. ); //!    
    const double m_fine_eta_len = 0.025 / 8.; //!
    const double m_eta_len = 0.025; //!
    std::tuple< std::vector<float>, std::vector<float>, std::vector<float>, std::vector<float>, std::vector<int>, std::vector<float> > phi_upscale(std::vector<float> c_eta, std::vector<float> c_deta, std::vector<float> c_phi, std::vector<float> c_dphi, std::vector<float> c_energy, std::vector<int> c_sampling, std::vector<float> c_time); 
    std::tuple< std::vector<float>, std::vector<float>, std::vector<float>, std::vector<int>, std::vector<float> > eta_upscale(std::vector<float> c_eta, std::vector<float> c_deta, std::vector<float> c_phi, std::vector<float> c_energy, std::vector<int> c_sampling, double cl_eta, std::vector<float> c_time); 

    void fill_images(std::vector<std::vector<float>> &image, std::vector<std::vector<float>> &time_image, const float c_eta, const bool fine_eta, const bool fine_phi, const float c_phi, const float c_energy, const int c_sampling, const float c_time, const int layer); 
    void resize_images(std::vector<std::vector<float>> &image, std::vector<std::vector<float>> &time_image, const bool fine_eta, const bool fine_phi); 

    std::tuple< std::vector<float>, std::vector<float>, std::vector<float>, std::vector<float>, std::vector<int>, std::vector<float>, std::vector<int>, std::vector<float> > phi_upscale(std::vector<float> c_eta, std::vector<float> c_deta, std::vector<float> c_phi, std::vector<float> c_dphi, std::vector<float> c_energy, std::vector<int> c_sampling, std::vector<float> c_time, std::vector<int> c_gain, std::vector<float> c_noise); 
    std::tuple< std::vector<float>, std::vector<float>, std::vector<float>, std::vector<int>, std::vector<float>, std::vector<int>, std::vector<float> > eta_upscale(std::vector<float> c_eta, std::vector<float> c_deta, std::vector<float> c_phi, std::vector<float> c_energy, std::vector<int> c_sampling, double cl_eta, std::vector<float> c_time, std::vector<int> c_gain, std::vector<float> c_noise); 

    void fill_images(std::vector<std::vector<float>> &image, std::vector<std::vector<float>> &time_image, std::vector<std::vector<int>> &gain_image, std::vector<std::vector<float>> &noise_image, const float c_eta, const bool fine_eta, const bool fine_phi, const float c_phi, const float c_energy, const int c_sampling, const float c_time, const int c_gain, const float c_noise, const int layer); 
    void resize_images(std::vector<std::vector<float>> &image, std::vector<std::vector<float>> &time_image, std::vector<std::vector<int>> &gain_image, std::vector<std::vector<float>> &noise_image, const bool fine_eta, const bool fine_phi); 


    // Event info
    ULong64_t event_eventNumber; //!
    uint32_t event_mcChannelNumber; //!
    uint32_t event_runNumber; //!
    float event_actualInteractionsPerCrossing; //!
    float event_averageInteractionsPerCrossing; //!
    float event_correctedActualMu; //!
    float event_correctedAverageMu; //!
    float event_correctedScaledActualMu; //!
    float event_correctedScaledAverageMu; //!
    float event_pileupweight; //!
    float event_MCWeight; //!
    float event_totalWeight; //!
    float combinedWeight; //!

    // Information about event
    bool event_isMC; //!
    int event_NvtxReco; //! // Number of reconstructed vertices (Should be unsigned long!)

    std::vector<float> cell_energy; //!
    std::vector<float> cell_eta; //!
    std::vector<float> cell_deta; //!
    std::vector<float> cell_phi; //!
    std::vector<float> cell_dphi; //!
    std::vector<float> cell_time; //!
    std::vector<float> cell_noise; //!
    std::vector<int> cell_gain; //!
    std::vector<int> cell_sampling; //!


    // Inner detector tracks R=0.4 of probe (excl. probe and tag)
    std::vector<float> tracks_pt; //!
    std::vector<float> tracks_eta; //!
    std::vector<float> tracks_phi; //!
    std::vector<float> tracks_charge; //!
    std::vector<float> tracks_chi2; //!
    std::vector<float> tracks_ndof; //!
    std::vector<UChar_t> tracks_pixhits; //!
    std::vector<UChar_t> tracks_scthits; //!
    std::vector<UChar_t> tracks_trthits; //!
    std::vector<int> tracks_vertex; //!
    std::vector<float> tracks_z0; //!
    std::vector<float> tracks_d0; //!
    std::vector<float> tracks_dR; //!
    std::vector<float> tracks_theta; //!
    std::vector<float> tracks_sigmad0; //!


    // probe tracks
    std::vector<float> p_tracks_pt; //!
    std::vector<float> p_tracks_eta; //!
    std::vector<float> p_tracks_phi; //!
    std::vector<float> p_tracks_charge; //!
    std::vector<float> p_tracks_chi2; //!
    std::vector<float> p_tracks_ndof; //!
    std::vector<UChar_t> p_tracks_pixhits; //!
    std::vector<UChar_t> p_tracks_scthits; //!
    std::vector<UChar_t> p_tracks_trthits; //!
    std::vector<int> p_tracks_vertex; //!
    std::vector<float> p_tracks_z0; //!
    std::vector<float> p_tracks_d0; //!
    std::vector<float> p_tracks_theta; //!
    std::vector<float> p_tracks_sigmad0; //!

    int p_iffTruth; //!

    int settype; //!
    // Jet information (specifically b-jets)
    double jets_highestBtagMV2c10; //!
    double jets_highestBtagDL1_pb; //!
    double jets_highestBtagDL1_pc; //!
    double jets_highestBtagDL1_pu; //!

    // Bunch crossing information
    int BC_distanceFromFront; //!
    unsigned int BC_filledBunches; //!

    // Missing transverse energy (MET)
    float met_met; //!
    float met_phi; //!
    
    // ll particle for tag and probe
    float ll_m; //!
    float ll_eta; //!
    float ll_pt; //!
    float ll_phi; //!
    float ll_lifetime; //!

    // Tag misc.
    UChar_t tag_type; //!
    int tag_vertexIndex; //!
    float tag_SF; //!

    // Tag kinematics
    float tag_e; //!
    float tag_et_calo; //!
    float tag_pt_track; //!
    float tag_eta; //!
    float tag_phi; //!
    float tag_charge; //!
    float tag_z0; //!
    float tag_d0; //!
    float tag_sigmad0; //!

    // Tag truth
    int tag_TruthType; //!
    int tag_TruthOrigin; //!
    bool tag_truth_matched; //!
    float tag_truth_pt; //!
    float tag_truth_phi; //!
    float tag_truth_eta; //!
    float tag_truth_E; //!
    int tag_truth_pdgId; //!
    int tag_truth_parent_pdgId; //!
    int tag_firstEgMotherTruthType; //!
    int tag_firstEgMotherTruthOrigin; //!
    int tag_firstEgMotherPdgId; //!

    // Probe/background misc.
    UChar_t p_type; //!
    bool p_hasTrack; //!    // False = no trackParticle() (if false, below false as well then)
    int p_nTracks; //!
    int p_vertexIndex; //!
    double p_ECIDSResult; //!
    bool p_photonIsTightEM; //!
    bool p_photonIsLooseEM; //!

    bool p_unprescaledTriggers; //!
    bool p_prescaledTriggers; //!
    bool p_prescaledJpsiTriggers; //!

    // Probe/background kinematics
    float p_e; //!
    float p_calibratedE; //!
    float p_et_calo; //!
    float p_pt_track; //!
    float p_eta; //!
    float p_phi; //!
    float p_charge; //!
    float p_z0; //!
    float p_d0; //!
    float p_sigmad0; //!
    float p_d0Sig; //!
    float p_EptRatio; //!
    float p_qOverP; //!
    float p_dPOverP; //!
    float p_z0theta; //!
    float p_deltaR_tag; //!

    // Probe/background cluster info
    float p_etaCluster; //!
    float p_phiCluster; //!
    float p_eCluster; //!
    float p_etCluster; //!
    float p_RawEtaCluster; //!
    float p_RawPhiCluster; //!
    float p_RawECluster; //!
    float p_eClusterLr0; //!
    float p_eClusterLr1; //!
    float p_eClusterLr2; //!
    float p_eClusterLr3; //!
    float p_etaClusterLr1; //!
    float p_etaClusterLr2; //!
    float p_phiClusterLr2; //!
    float p_eAccCluster; //!
    float p_f0Cluster; //!
    double p_etaCalo; //!
    double p_phiCalo; //!
    float p_eTileGap3Cluster; //!
    float p_cellIndexCluster; //!
    float p_phiModCalo; //!
    float p_etaModCalo; //!
    float p_dPhiTH3; //!
    float p_R12; //!
    float p_fTG3; //!


    // Probe/background converted photons info
    int p_photonConversionType; //!
    float p_photonConversionRadius; //!
    float p_photonVertexPtConvDecor; //!
    float p_photonVertexPtConv; //!
    float p_photonVertexPt1; //!
    float p_photonVertexPt2; //!
    float p_photonVertexConvPtRatio; //!
    float p_photonVertexConvEtOverPt; //!
    float p_photonVertexRconv; //!
    float p_photonVertexzconv; //!
    UChar_t p_photonVertexPixHits1; //!
    UChar_t p_photonVertexSCTHits1; //!
    UChar_t p_photonVertexPixHits2; //!
    UChar_t p_photonVertexSCTHits2; //!

    // Probe/background calculated calorimeter values
    float p_weta2; //!
    float p_Reta; //!
    float p_Rphi; //!
    float p_Eratio; //!
    float p_f1; //!
    float p_f3; //!
    float p_Rhad; //!
    float p_Rhad1; //!
    float p_deltaEta1; //!
    float p_deltaPhiRescaled2; //!

    // Probe/background identification
    float p_eProbHT; //!
    float p_LHValue; //!

    // Probe+MET transverse mass (W hypothesis)
    float p_mTransW; //!

    // Probe/background isolation
    float p_topoetcone20; //!
    float p_topoetcone30; //!
    float p_topoetcone40; //!
    float p_etcone20; //!
    float p_etcone30; //!
    float p_etcone40; //!
    float p_etcone20ptCorrection; //!
    float p_etcone30ptCorrection; //!
    float p_etcone40ptCorrection; //!
    float p_ptcone20; //!
    float p_ptcone30; //!
    float p_ptcone40; //!
    float p_ptvarcone20; //!
    float p_ptvarcone30; //!
    float p_ptvarcone40; //!
    float p_topoetcone20ptCorrection; //!
    float p_topoetcone30ptCorrection; //!
    float p_topoetcone40ptCorrection; //!
    float p_ptPU10;   //!
    float p_ptPU20;   //!
    float p_ptPU30;   //!
    float p_ptPU40;   //!
    float p_ptcone20_TightTTVA_pt500; //!
    float p_ptcone30_TightTTVA_pt500; //!
    float p_ptcone40_TightTTVA_pt500; //!
    float p_ptvarcone20_TightTTVA_pt500; //!
    float p_ptvarcone30_TightTTVA_pt500; //!
    float p_ptvarcone40_TightTTVA_pt500; //!
    float p_ptcone20_TightTTVA_pt1000; //!
    float p_ptcone30_TightTTVA_pt1000; //!
    float p_ptcone40_TightTTVA_pt1000; //!
    float p_ptvarcone20_TightTTVA_pt1000; //!
    float p_ptvarcone30_TightTTVA_pt1000; //!
    float p_ptvarcone40_TightTTVA_pt1000; //!


    // Probe/background track and hits
    float p_TRTTrackOccupancy; //!
    UChar_t p_numberOfInnermostPixelHits; //!
    UChar_t p_numberOfPixelHits; //!
    UChar_t p_numberOfSCTHits; //!
    UChar_t p_numberOfTRTHits; //!
    UChar_t p_numberOfTRTXenonHits; //!

    float p_chi2; //!
    int p_ndof; //!

    bool p_SharedMuonTrack; //!

    // Probe/background truth
    int p_TruthType; //!
    int p_TruthOrigin; //!
    bool p_truth_matched; //!
    float p_truth_pt; //!
    float p_truth_phi; //!
    float p_truth_eta; //!
    float p_truth_E; //!
    int p_truth_pdgId; //!
    int p_truth_parent_pdgId; //!
    int p_trueConv; //!
    int p_firstEgMotherTruthType; //!
    int p_firstEgMotherTruthOrigin; //!
    int p_firstEgMotherPdgId; //!    
    bool Truth; //!
    bool TruthSelection; //!

    // Probe/background: ALL aux variables for electrons/photons (some are central electron only)
    float pX_E7x7_Lr2; //!
    float pX_E7x7_Lr3; //!
    float pX_E_Lr0_HiG; //!
    float pX_E_Lr0_LowG; //!
    float pX_E_Lr0_MedG; //!
    float pX_E_Lr1_HiG; //!
    float pX_E_Lr1_LowG; //!
    float pX_E_Lr1_MedG; //!
    float pX_E_Lr2_HiG; //!
    float pX_E_Lr2_LowG; //!
    float pX_E_Lr2_MedG; //!
    float pX_E_Lr3_HiG; //!
    float pX_E_Lr3_LowG; //!
    float pX_E_Lr3_MedG; //!
    char pX_LHLoose; //!
    char pX_LHMedium; //!
    char pX_LHTight; //!
    char pX_Loose; //!
    float pX_m; //!
    char pX_Medium; //!
    char pX_MultiLepton; //!
    unsigned int pX_OQ; //!
    char pX_Tight; //!
    unsigned char pX_ambiguityType; //!
    float pX_asy1; //!
    unsigned short pX_author; //!
    float pX_barys1; //!
    float pX_core57cellsEnergyCorrection; //!
    float pX_deltaEta0; //!
    float pX_deltaEta2; //!
    float pX_deltaEta3; //!
    float pX_deltaPhi0; //!
    float pX_deltaPhi1; //!
    float pX_deltaPhi2; //!
    float pX_deltaPhi3; //!
    float pX_deltaPhiFromLastMeasurement; //!
    float pX_deltaPhiRescaled0; //!
    float pX_deltaPhiRescaled1; //!
    float pX_deltaPhiRescaled3; //!
    float pX_e1152; //!
    float pX_e132; //!
    float pX_e235; //!
    float pX_e255; //!
    float pX_e2ts1; //!
    float pX_ecore; //!
    float pX_emins1; //!
    unsigned int pX_etconeCorrBitset; //!
    float pX_ethad; //!
    float pX_ethad1; //!
    float pX_f1core; //!
    float pX_f3core; //!
    float pX_maxEcell_energy; //!
    int pX_maxEcell_gain; //!
    float pX_maxEcell_time; //!
    float pX_maxEcell_x; //!
    float pX_maxEcell_y; //!
    float pX_maxEcell_z; //!
    unsigned char pX_nCells_Lr0_HiG; //!
    unsigned char pX_nCells_Lr0_LowG; //!
    unsigned char pX_nCells_Lr0_MedG; //!
    unsigned char pX_nCells_Lr1_HiG; //!
    unsigned char pX_nCells_Lr1_LowG; //!
    unsigned char pX_nCells_Lr1_MedG; //!
    unsigned char pX_nCells_Lr2_HiG; //!
    unsigned char pX_nCells_Lr2_LowG; //!
    unsigned char pX_nCells_Lr2_MedG; //!
    unsigned char pX_nCells_Lr3_HiG; //!
    unsigned char pX_nCells_Lr3_LowG; //!
    unsigned char pX_nCells_Lr3_MedG; //!
    float pX_neflowisol20; //!
    float pX_neflowisol20ptCorrection; //!
    float pX_neflowisol30; //!
    float pX_neflowisol30ptCorrection; //!
    float pX_neflowisol40; //!
    float pX_neflowisol40ptCorrection; //!
    unsigned int pX_neflowisolCorrBitset; //!
    float pX_neflowisolcoreConeEnergyCorrection; //!
    float pX_pos; //!
    float pX_pos7; //!
    float pX_poscs1; //!
    float pX_poscs2; //!
    unsigned int pX_ptconeCorrBitset; //!
    float pX_ptconecoreTrackPtrCorrection; //!
    float pX_r33over37allcalo; //!
    unsigned int pX_topoetconeCorrBitset; //!
    float pX_topoetconecoreConeEnergyCorrection; //!
    float pX_topoetconecoreConeSCEnergyCorrection; //!
    float pX_weta1; //!
    float pX_widths1; //!
    float pX_widths2; //!
    float pX_wtots1; //!
    float pX_e233; //!
    float pX_e237; //!
    float pX_e277; //!
    float pX_e2tsts1; //!
    float pX_ehad1; //!
    float pX_emaxs1; //!
    float pX_fracs1; //!
    float pX_DeltaE; //!
    float pX_E3x5_Lr0; //!
    float pX_E3x5_Lr1; //!
    float pX_E3x5_Lr2; //!
    float pX_E3x5_Lr3; //!
    float pX_E5x7_Lr0; //!
    float pX_E5x7_Lr1; //!
    float pX_E5x7_Lr2; //!
    float pX_E5x7_Lr3; //!
    float pX_E7x11_Lr0; //!
    float pX_E7x11_Lr1; //!
    float pX_E7x11_Lr2; //!
    float pX_E7x11_Lr3; //!
    float pX_E7x7_Lr0; //!
    float pX_E7x7_Lr1; //!

    // Photons only
    float pPX_convMatchDeltaEta1; //!
    float pPX_convMatchDeltaEta2; //!
    float pPX_convMatchDeltaPhi1; //!
    float pPX_convMatchDeltaPhi2; //!

    double pdf_cut_vars_score; //!
    double pdf_vars_score; //!
    double IsoCalo_score; //!
    double IsoTrack_score; //!
    double Iso_score; //!


    //============================================================================
    // Functions
    //============================================================================

    MyTagAndProbeAnalysis();
    virtual EL::StatusCode setupJob(EL::Job& job);
    virtual EL::StatusCode fileExecute();
    virtual EL::StatusCode histInitialize();
    //virtual EL::StatusCode changeInput( bool firstFile );
    virtual EL::StatusCode initialize();
    virtual EL::StatusCode execute();
    virtual EL::StatusCode postExecute();
    virtual EL::StatusCode finalize();
    virtual EL::StatusCode histFinalize();

    // Hists
    size_t createCutHist( TString title, std::vector<TString> cuts );

    // Selections and filling
    std::vector< particleWrapper > preSelectTagElectrons();
    std::vector< particleWrapper > selectTagElectrons( std::vector< particleWrapper > electrons_in );//, const xAOD::JetContainer* jet_obj );
    std::vector< particleWrapper > selectTagMuons();//, const xAOD::JetContainer* jet_obj );
    std::vector< particleWrapper > selectJpsiTagElectrons( std::vector< particleWrapper > electrons_in );//, const xAOD::JetContainer* jet_obj );
    std::vector< particleWrapper > selectProbeElectrons( xAOD::ElectronContainer* electronContainer, const bool isCentralElectrons );
    std::vector< particleWrapper > selectJpsiProbeElectrons( xAOD::ElectronContainer* electronContainer, const bool isCentralElectrons );
    inline std::vector< particleWrapper > selectProbeCentralElectrons() { return selectProbeElectrons( m_electrons, true ); }
    std::vector< particleWrapper > selectProbePhotons();
    std::vector< particleWrapper > selectZProbeElectrons( std::vector< particleWrapper >  electrons_in );    
    std::vector< particleWrapper > selectBackground( std::vector< particleWrapper >  electrons_in );
    std::tuple < std::vector< std::pair<particleWrapper, particleWrapper> > , std::vector< std::pair<particleWrapper, particleWrapper> > > selectLeptonPairs( const std::vector< particleWrapper >& tags, std::vector< particleWrapper > probes );
    std::vector< std::pair< particleWrapper, particleWrapper> > selectJpsiLeptonPairs( const std::vector< particleWrapper >& tags, std::vector< particleWrapper > probes );
    std::vector< particleWrapper > selectTruthElectrons();
    std::vector< particleWrapper > selectTruthPhotons();
    void fillNtuple( const particleWrapper* tag, const particleWrapper* probe );
    void resetEventInfoBranchVariables();
    void resetRestOfBranchVariables();

    void create_images( const particleWrapper* particle );

    // For the configuration
    Config m_config;
    inline void setConfig( const Config &config_in ) { m_config = config_in; }

    // this is needed to distribute the algorithm to the workers
    ClassDef(MyTagAndProbeAnalysis, 1)

    //const SG::AuxElement::ConstAccessor< int > Accessor_BC_distanceFromFront{ "BC_distanceFromFront" };
    //const SG::AuxElement::ConstAccessor< unsigned int > Accessor_BC_filledBunches{ "BC_filledBunches" };

    const SG::AuxElement::ConstAccessor< float > Accessor_TRTTrackOccupancy{ "TRTTrackOccupancy" };//!
    const SG::AuxElement::ConstAccessor< UChar_t > Accessor_numberOfTRTXenonHits{ "numberOfTRTXenonHits" };//!

    const SG::AuxElement::ConstAccessor< UChar_t > Accessor_numberOfInnermostPixelLayerHits{ "numberOfInnermostPixelLayerHits" };//!
    const SG::AuxElement::ConstAccessor< UChar_t > Accessor_numberOfPixelHits{ "numberOfPixelHits" };//!
    const SG::AuxElement::ConstAccessor< UChar_t > Accessor_numberOfPixelDeadSensors{ "numberOfPixelDeadSensors" };//!
    const SG::AuxElement::ConstAccessor< UChar_t > Accessor_numberOfSCTHits{ "numberOfSCTHits" };//!
    const SG::AuxElement::ConstAccessor< UChar_t > Accessor_numberOfSCTDeadSensors{ "numberOfSCTDeadSensors" };//!
    const SG::AuxElement::ConstAccessor< UChar_t > Accessor_numberOfTRTHits{ "numberOfTRTHits" };//!

    const SG::AuxElement::ConstAccessor< int > Accessor_truthType{ "truthType" };//!
    const SG::AuxElement::ConstAccessor< int > Accessor_truthOrigin{ "truthOrigin" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_truthE{ "truthE" };//!
    const SG::AuxElement::ConstAccessor< int > Accessor_trueConv{ "truthConv" };//!

    const SG::AuxElement::ConstAccessor< double > Accessor_calibratedE{ "calibratedE" };//!


    const SG::AuxElement::ConstAccessor< float > Accessor_E7x7_Lr2{ "E7x7_Lr2" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E7x7_Lr3{ "E7x7_Lr3" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E_Lr0_HiG{ "E_Lr0_HiG" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E_Lr0_LowG{ "E_Lr0_LowG" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E_Lr0_MedG{ "E_Lr0_MedG" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E_Lr1_HiG{ "E_Lr1_HiG" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E_Lr1_LowG{ "E_Lr1_LowG" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E_Lr1_MedG{ "E_Lr1_MedG" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E_Lr2_HiG{ "E_Lr2_HiG" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E_Lr2_LowG{ "E_Lr2_LowG" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E_Lr2_MedG{ "E_Lr2_MedG" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E_Lr3_HiG{ "E_Lr3_HiG" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E_Lr3_LowG{ "E_Lr3_LowG" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E_Lr3_MedG{ "E_Lr3_MedG" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_LHValue{ "LHValue" };//!
    const SG::AuxElement::ConstAccessor< char > Accessor_MultiLepton{ "MultiLepton" };//!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_ambiguityType{ "ambiguityType" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_asy1{ "asy1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_barys1{ "barys1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_core57cellsEnergyCorrection{ "core57cellsEnergyCorrection" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_e1152{ "e1152" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_e132{ "e132" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_e235{ "e235" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_e255{ "e255" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_e2ts1{ "e2ts1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ecore{ "ecore" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_emins1{ "emins1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_etcone20{ "etcone20" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_etcone20ptCorrection{ "etcone20ptCorrection" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_etcone30{ "etcone30" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_etcone30ptCorrection{ "etcone30ptCorrection" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_etcone40{ "etcone40" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_etcone40ptCorrection{ "etcone40ptCorrection" };//!
    const SG::AuxElement::ConstAccessor< unsigned int > Accessor_etconeCorrBitset{ "etconeCorrBitset" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ethad{ "ethad" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ethad1{ "ethad1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_maxEcell_energy{ "maxEcell_energy" };//!
    const SG::AuxElement::ConstAccessor< int > Accessor_maxEcell_gain{ "maxEcell_gain" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_maxEcell_time{ "maxEcell_time" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_maxEcell_x{ "maxEcell_x" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_maxEcell_y{ "maxEcell_y" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_maxEcell_z{ "maxEcell_z" };//!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_nCells_Lr0_HiG{ "nCells_Lr0_HiG" };//!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_nCells_Lr0_LowG{ "nCells_Lr0_LowG" };//!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_nCells_Lr0_MedG{ "nCells_Lr0_MedG" };//!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_nCells_Lr1_HiG{ "nCells_Lr1_HiG" };//!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_nCells_Lr1_LowG{ "nCells_Lr1_LowG" };//!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_nCells_Lr1_MedG{ "nCells_Lr1_MedG" };//!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_nCells_Lr2_HiG{ "nCells_Lr2_HiG" };//!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_nCells_Lr2_LowG{ "nCells_Lr2_LowG" };//!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_nCells_Lr2_MedG{ "nCells_Lr2_MedG" };//!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_nCells_Lr3_HiG{ "nCells_Lr3_HiG" };//!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_nCells_Lr3_LowG{ "nCells_Lr3_LowG" };//!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_nCells_Lr3_MedG{ "nCells_Lr3_MedG" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_neflowisol20{ "neflowisol20" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_neflowisol20ptCorrection{ "neflowisol20ptCorrection" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_neflowisol30{ "neflowisol30" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_neflowisol30ptCorrection{ "neflowisol30ptCorrection" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_neflowisol40{ "neflowisol40" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_neflowisol40ptCorrection{ "neflowisol40ptCorrection" };//!
    const SG::AuxElement::ConstAccessor< unsigned int > Accessor_neflowisolCorrBitset{ "neflowisolCorrBitset" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_neflowisolcoreConeEnergyCorrection{ "neflowisolcoreConeEnergyCorrection" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_pos{ "pos" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_pos7{ "pos7" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_poscs1{ "poscs1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_poscs2{ "poscs2" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptcone20{ "ptcone20" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptcone30{ "ptcone30" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptcone40{ "ptcone40" };//!
    const SG::AuxElement::ConstAccessor< unsigned int > Accessor_ptconeCorrBitset{ "ptconeCorrBitset" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptconecoreTrackPtrCorrection{ "ptconecoreTrackPtrCorrection" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone20{ "ptvarcone20" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone30{ "ptvarcone30" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone40{ "ptvarcone40" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptcone20_TightTTVA_pt500{ "ptcone20_TightTTVA_pt500" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptcone30_TightTTVA_pt500{ "ptcone30_TightTTVA_pt500" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptcone40_TightTTVA_pt500{ "ptcone40_TightTTVA_pt500" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone20_TightTTVA_pt500{ "ptvarcone20_TightTTVA_pt500" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone30_TightTTVA_pt500{ "ptvarcone30_TightTTVA_pt500" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone40_TightTTVA_pt500{ "ptvarcone40_TightTTVA_pt500" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptcone20_TightTTVA_pt1000{ "ptcone20_TightTTVA_pt1000" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptcone30_TightTTVA_pt1000{ "ptcone30_TightTTVA_pt1000" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptcone40_TightTTVA_pt1000{ "ptcone40_TightTTVA_pt1000" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone20_TightTTVA_pt1000{ "ptvarcone20_TightTTVA_pt1000" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone30_TightTTVA_pt1000{ "ptvarcone30_TightTTVA_pt1000" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone40_TightTTVA_pt1000{ "ptvarcone40_TightTTVA_pt1000" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_r33over37allcalo{ "r33over37allcalo" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_topoetcone20ptCorrection{ "topoetcone20ptCorrection" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_topoetcone30ptCorrection{ "topoetcone30ptCorrection" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_topoetcone40ptCorrection{ "topoetcone40ptCorrection" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_topoetconecoreConeSCEnergyCorrection{ "topoetconecoreConeSCEnergyCorrection" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_widths1{ "widths1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_widths2{ "widths2" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_e233{ "e233" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_e237{ "e237" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_e2tsts1{ "e2tsts1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_ehad1{ "ehad1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_emaxs1{ "emaxs1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E3x5_Lr0{ "E3x5_Lr0" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E3x5_Lr1{ "E3x5_Lr1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E3x5_Lr2{ "E3x5_Lr2" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E3x5_Lr3{ "E3x5_Lr3" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E5x7_Lr0{ "E5x7_Lr0" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E5x7_Lr1{ "E5x7_Lr1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E5x7_Lr2{ "E5x7_Lr2" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E5x7_Lr3{ "E5x7_Lr3" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E7x11_Lr0{ "E7x11_Lr0" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E7x11_Lr1{ "E7x11_Lr1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E7x11_Lr2{ "E7x11_Lr2" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E7x11_Lr3{ "E7x11_Lr3" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E7x7_Lr0{ "E7x7_Lr0" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_E7x7_Lr1{ "E7x7_Lr1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_Eratio{ "Eratio" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_pt{ "pt" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_eta{ "eta" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_phi{ "phi" };//!
    const SG::AuxElement::ConstAccessor< char > Accessor_Loose{ "Loose" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_m{ "m" };//!
    const SG::AuxElement::ConstAccessor< char > Accessor_Medium{ "Medium" };//!
    const SG::AuxElement::ConstAccessor< unsigned int > Accessor_OQ{ "OQ" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_Reta{ "Reta" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_Rhad{ "Rhad" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_Rhad1{ "Rhad1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_Rphi{ "Rphi" };//!
    const SG::AuxElement::ConstAccessor< char > Accessor_Tight{ "Tight" };//!
    const SG::AuxElement::ConstAccessor< unsigned short > Accessor_author{ "author" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltaEta0{ "deltaEta0" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltaEta1{ "deltaEta1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltaEta2{ "deltaEta2" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltaEta3{ "deltaEta3" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltaPhi0{ "deltaPhi0" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltaPhi1{ "deltaPhi1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltaPhi2{ "deltaPhi2" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltaPhi3{ "deltaPhi3" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltaPhiFromLastMeasurement{ "deltaPhiFromLastMeasurement" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltaPhiRescaled0{ "deltaPhiRescaled0" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltaPhiRescaled1{ "deltaPhiRescaled1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltaPhiRescaled2{ "deltaPhiRescaled2" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltaPhiRescaled3{ "deltaPhiRescaled3" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_f1{ "f1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_f1core{ "f1core" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_f3{ "f3" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_f3core{ "f3core" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_topoetcone20{ "topoetcone20" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_topoetcone30{ "topoetcone30" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_topoetcone40{ "topoetcone40" };//!
    const SG::AuxElement::ConstAccessor< unsigned int > Accessor_topoetconeCorrBitset{ "topoetconeCorrBitset" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_topoetconecoreConeEnergyCorrection{ "topoetconecoreConeEnergyCorrection" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_weta1{ "weta1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_wtots1{ "wtots1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_e277{ "e277" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_fracs1{ "fracs1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_weta2{ "weta2" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_DeltaE{ "DeltaE" };//!

    const SG::AuxElement::ConstAccessor< float > Accessor_convMatchDeltaEta1{ "convMatchDeltaEta1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_convMatchDeltaEta2{ "convMatchDeltaEta2" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_convMatchDeltaPhi1{ "convMatchDeltaPhi1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_convMatchDeltaPhi2{ "convMatchDeltaPhi2" };//!

    const SG::AuxElement::ConstAccessor< int > Accessor_firstEgMotherTruthType{ "firstEgMotherTruthType" };//!
    const SG::AuxElement::ConstAccessor< int > Accessor_firstEgMotherTruthOrigin{ "firstEgMotherTruthOrigin" };//!
	const SG::AuxElement::ConstAccessor< int > Accessor_firstEgMotherPdgId{ "firstEgMotherPdgId" };//!
    const SG::AuxElement::ConstAccessor< int > Accessor_lastEgMotherTruthType{ "lastEgMotherTruthType" };//!
    const SG::AuxElement::ConstAccessor< int > Accessor_lastEgMotherTruthOrigin{ "lastEgMotherTruthOrigin" };//!

    // photon vertex (conversions)
    const SG::AuxElement::ConstAccessor< float > Accessor_px{ "px" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_py{ "py" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_pt1{ "pt1" };//!
    const SG::AuxElement::ConstAccessor< float > Accessor_pt2{ "pt2" };//!

    const SG::AuxElement::ConstAccessor< std::vector<float> > Accessor_cell_time{ "cell_time" };//!
    const SG::AuxElement::ConstAccessor< std::vector<float> > Accessor_cell_energy{ "cell_energy" };//!
    const SG::AuxElement::ConstAccessor< std::vector<int> > Accessor_cell_gain{ "cell_gain" };//!
    const SG::AuxElement::ConstAccessor< std::vector<float> > Accessor_cell_eta{ "cell_eta" };//!
    const SG::AuxElement::ConstAccessor< std::vector<float> > Accessor_cell_deta{ "cell_deta" };//!
    const SG::AuxElement::ConstAccessor< std::vector<float> > Accessor_cell_phi{ "cell_phi" };//!
    const SG::AuxElement::ConstAccessor< std::vector<float> > Accessor_cell_dphi{ "cell_dphi" };//!
    const SG::AuxElement::ConstAccessor< std::vector<int> > Accessor_cell_sampling{ "cell_sampling" };//!
    const SG::AuxElement::ConstAccessor< std::vector<float> > Accessor_cell_totalnoise{ "cell_totalnoise" };//!
    
    const SG::AuxElement::ConstAccessor< bool > Accessor_DFLHBLLoose{ "DFCommonElectronsLHLooseBL" };//!
    
};

#pragma GCC diagnostic pop
#endif
