#ifndef TRTFramework_JobHelper_H
#define TRTFramework_JobHelper_H

#include "MyAnalysis/MyTagAndProbeAnalysis.h"

// Ignore warnings from the following includes
//#pragma GCC system_header

#include <SampleHandler/SampleLocal.h>
#include <EventLoop/CondorDriver.h>
#include <EventLoopGrid/GridDriver.h>
#include <EventLoopGrid/PrunDriver.h>
#include "xAODCore/tools/IOStats.h"
#include "xAODCore/tools/ReadStats.h"
#include <EventLoop/StatusCode.h>
#include "EventLoop/OutputStream.h"
#include <EventLoopGrid/GridDriver.h>
#include <EventLoopGrid/PrunDriver.h>
#include "EventLoopAlgs/NTupleSvc.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoop/Job.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "SampleHandler/DiskListLocal.h"
#include "SampleHandler/DiskListEOS.h"
#include "SampleHandler/ScanDir.h"

#include "xAODRootAccess/Init.h"
#include <TSystem.h>
#include <iostream>
#include <string>
#include <time.h>
#include <TDatime.h>

#include "MyAnalysis/Helpers.h"
#include "MyAnalysis/Config.h"
#pragma GCC diagnostic push
MORE_DIAGNOSTICS()


//! \name   A few general helper methods and definitions
//@{

StrV parseArguments(Config *conf, int argc, char **argv);
TString getHelp(TString progName);
void runJob(MyTagAndProbeAnalysis *alg, int argc, char** argv);

//@}

#pragma GCC diagnostic pop
#endif
