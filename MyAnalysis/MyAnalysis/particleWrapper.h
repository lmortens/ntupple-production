#ifndef MyAnalysis_particleWrapper_H
#define MyAnalysis_particleWrapper_H

#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#pragma GCC diagnostic push
MORE_DIAGNOSTICS()

class particleWrapper {
public:
    particleWrapper( const xAOD::Electron* el, const bool isCentralElectron );
    particleWrapper( const xAOD::Muon* mu );
    particleWrapper( const xAOD::Photon* ph );
    particleWrapper( const xAOD::Electron* fel );

    inline bool isCentralElectron() const { return type == 1; }
    inline bool isMuon() const { return type == 2; }
    inline bool isPhoton() const { return type == 3; }
    inline bool isForwardElectron() const { return  type == 4; }
    inline int getType() const { return type; }


    // True = forward electron/photon


    inline const xAOD::Electron* getCentralElectron() const { return electron; }
    inline const xAOD::Muon* getMuon() const { return muon; }
    inline const xAOD::Photon* getPhoton() const { return photon; }
    inline const xAOD::Electron* getForwardElectron() const { return forward_electron; }

    // bool operator== naaah...
    bool sameAs( const particleWrapper& rhs ) const;
    bool sameAs( const xAOD::Electron* el ) const;
    const TLorentzVector& p4() const;
    float charge() const;
    const xAOD::TrackParticle* trackParticle( std::size_t index = 0 ) const;
    const xAOD::CaloCluster* caloCluster() const;
    const xAOD::IParticle* getIParticle() const;
    const SG::AuxElement* getAuxElement() const;
    int nTrackParticles() const;

    template< class T >
    inline const T& auxdata( const std::string& name ) const {
        switch ( type ) {
            case 1:  return electron->auxdata<T>(name); break;
            case 2:  return muon->auxdata<T>(name); break;
            case 3:  return photon->auxdata<T>(name); break;
            case 4:  return forward_electron->auxdata<T>(name); break;
            default: using std::runtime_error; throw runtime_error("Type is bad value!"); break;
        }
    }

    // Would love to remove copying but apparently I cannot make it work
    // particleWrapper(const particleWrapper&) = delete;
    // particleWrapper& operator= (const particleWrapper&) = delete;
private:
    const xAOD::Electron* electron = nullptr;
    const xAOD::Muon* muon = nullptr;
    const xAOD::Photon* photon = nullptr;
    const xAOD::Electron* forward_electron = nullptr;
    // 0 = undefined
    // 1 = electron
    // 2 = muon
    // 3 = photon
    // 4 = forward electron
    UChar_t type = 0;
};

#pragma GCC diagnostic pop
#endif
