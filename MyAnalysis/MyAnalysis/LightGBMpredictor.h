#ifndef MyAnalysis_LightGBMpredictor_H
#define MyAnalysis_LightGBMpredictor_H

// A bunch of includes
#include <LightGBM/config.h>
#include <LightGBM/dataset_loader.h>
#include <LightGBM/boosting.h>
#include <LightGBM/prediction_early_stop.h>
#include <LightGBM/objective_function.h>
#include <LightGBM/metric.h>
#include <LightGBM/utils/common.h>
// #include "TagAndProbeFrame/EDMHelper.h"

#include <fstream>
#include <TSystem.h>
#include <array>
#include <string>

// Last include
#include "MyAnalysis/Config.h"
#pragma GCC diagnostic push
MORE_DIAGNOSTICS()

class LightGBMpredictor{
    public:
        LightGBMpredictor();
        ~LightGBMpredictor(){delete m_booster;}
        void initialize(std::string txtFile);
        
        double predict(std::vector<double> features);
        LightGBM::Boosting* m_booster = nullptr;


  // ClassDef(LightGBMpredictor, 1);
};

#pragma GCC diagnostic pop
#endif
