#ifndef TRTFramework_Config_H
#define TRTFramework_Config_H

/********************
 * Config:
 *   class to read settings from text files
 *   relies on root's TEnv
 *
 * Usage:
 *   Config settings("TRTConfig.cfg");
 *   TString muContainerName = settings.getStr("MuonContainer");
 *   TString elContainerName = settings.getStr("ElectronContainer");
 *   vector<TString> systShifts = settings.getStrV("Systematics");
 *
 */

// Ignore warnings from the following includes
//#pragma GCC system_header

#include <vector>
#include "TString.h"
#include "TEnv.h"
#include "THashList.h"
#include "PathResolver/PathResolver.h"
#include <TSystem.h>
#include <algorithm>
#include "MyAnalysis/Helpers.h"
#pragma GCC diagnostic push
MORE_DIAGNOSTICS()

// typedef for a vector of strings (to save some typing)
typedef std::vector<TString> StrV;
typedef std::vector<double>  NumV;

// Class that handles reading input from a configuration text file based on root's TEnv format, i.e. key-value pairs
class Config {
private:
	TEnv m_env; // TEnv objects holding the settings database
	StrV m_ignoreList; // Variables not to print
	StrV m_whiteList; // Variables that was requested are whitelisted
public:
	// Config constructor
	// fileName name of text file with user-specified settings
	Config(TString fileName);
	Config(const Config &config);
	Config();
	virtual ~Config() {}

	Config &operator=(const Config &rhs); // assignment operator

	// Access a string value from the config database. Exception thrown if no entry exists.
	TString getStr(TString key);

	// Access a string value from the config database. Adds to config if no entry exists.
	TString getStrDefault(TString key, TString dflt);

	// Access a vector of strings from the config database
	StrV  getStrV(TString key);

	// Access an integer from the config database. Exception thrown if no entry exists.
	int getInt(TString key);

	// Access an integer from the config database. Adds to config if no entry exists.
	int getIntDefault(TString key, int dflt);

	// Access a boolean from the config database. Exception thrown if no entry exists.
	bool getBool(TString key);

	// Access a boolean from the config database. Adds to config if no entry exists.
	bool getBoolDefault(TString key, bool dflt);

	// Access a real number from the config database. Exception thrown if no entry exists.
	double getNum(TString key);

	// Access a real number from the config database. Adds to config if no entry exists.
	double getNumDefault(TString key, double dflt);

	// Access a vector of integers from the config database
	// std::vector<int>    getIntV(TString key);

	// Access a vector of doubles from the config database
	NumV getNumV(TString key);

	// returns true if the key is defined
	bool isDefined(TString key);

	// Add more user specified settings to the
	void addFile(TString fileName);

	// Set value
	inline void setValue(TString key, TString value) { m_env.SetValue(key,value); }

	// accessor to the TEnv database
	inline const TEnv* getDB() { return &m_env; }

	// prints the TEnv database to screen
	void printDB();

	void addToIgnoreList( TString name ) { m_ignoreList.push_back( name ); }
	void addToWhiteList( TString name ) { m_whiteList.push_back( name ); }
	void addToWhiteList( StrV names ) { m_whiteList.insert(m_whiteList.end(), names.begin(), names.end()); }
	void checkWhitelist();

private:
	// ensures that there is a value in the database assocated with key. If not, abort with error message
	void ensureDefined(TString key);
	inline void copyTable(const TEnv &env);

	ClassDef(Config, 1)
};

inline Config& Config::operator=(const Config &rhs) {
	rhs.m_env.Copy(m_env);
	m_whiteList.insert(m_whiteList.end(), rhs.m_whiteList.begin(), rhs.m_whiteList.end());
	copyTable(rhs.m_env);
	return *this;
}

inline void Config::copyTable(const TEnv &env) {
	m_env.GetTable()->Delete();
	THashList* hl = nullptr;
	if ( (hl = env.GetTable()) ){
		TIter next(hl);
		TEnvRec* env_rec = nullptr;
		while ( (env_rec = dynamic_cast<TEnvRec*>(next.Next())) ){
			m_env.SetValue(env_rec->GetName(),env_rec->GetValue());
		}
	}
}

#pragma GCC diagnostic pop
#endif
