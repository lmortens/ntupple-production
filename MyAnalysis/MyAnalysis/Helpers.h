#ifndef MyAnalysis_Helpers_H
#define MyAnalysis_Helpers_H

#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include <xAODTruth/xAODTruthHelpers.h>

#if !defined(__clang__)
#define MORE_DIAGNOSTICS() \
    _Pragma("GCC diagnostic error \"-Wall\"") \
    _Pragma("GCC diagnostic error \"-Wextra\"") \
    _Pragma("GCC diagnostic error \"-Wshadow\"") \
    _Pragma("GCC diagnostic error \"-Wcast-align\"") \
    _Pragma("GCC diagnostic error \"-Wctor-dtor-privacy\"") \
    _Pragma("GCC diagnostic error \"-Wdisabled-optimization\"") \
    _Pragma("GCC diagnostic error \"-Wformat\"=2") \
    _Pragma("GCC diagnostic error \"-Winit-self\"") \
    _Pragma("GCC diagnostic error \"-Wlogical-op\"") \
    _Pragma("GCC diagnostic error \"-Wmissing-include-dirs\"") \
    _Pragma("GCC diagnostic error \"-Wnoexcept\"") \
    _Pragma("GCC diagnostic error \"-Woverloaded-virtual\"") \
    _Pragma("GCC diagnostic error \"-Wredundant-decls\"") \
    _Pragma("GCC diagnostic error \"-Wsign-conversion\"") \
    _Pragma("GCC diagnostic error \"-Wsign-promo\"") \
    _Pragma("GCC diagnostic error \"-Wstrict-null-sentinel\"") \
    _Pragma("GCC diagnostic error \"-Wstrict-overflow\"=5") \
    _Pragma("GCC diagnostic error \"-Wswitch-default\"") \
    _Pragma("GCC diagnostic error \"-Wundef\"") \
    _Pragma("GCC diagnostic error \"-Wfloat-equal\"") \
    _Pragma("GCC diagnostic error \"-Wpointer-arith\"") \
    _Pragma("GCC diagnostic error \"-Wwrite-strings\"") \
    _Pragma("GCC diagnostic error \"-Wswitch-enum\"") \
    _Pragma("GCC diagnostic error \"-Wunreachable-code\"") \
    _Pragma("GCC diagnostic error \"-Wdeprecated\"") \
    _Pragma("GCC diagnostic error \"-Wmisleading-indentation\"")
#else
#define MORE_DIAGNOSTICS() \
    _Pragma("clang diagnostic error \"-Wall\"") \
    _Pragma("clang diagnostic error \"-Wextra\"") \
    _Pragma("clang diagnostic error \"-Wshadow\"") \
    _Pragma("clang diagnostic error \"-Wcast-align\"") \
    _Pragma("clang diagnostic error \"-Wctor-dtor-privacy\"") \
    _Pragma("clang diagnostic error \"-Wdisabled-optimization\"") \
    _Pragma("clang diagnostic error \"-Winit-self\"") \
    _Pragma("clang diagnostic error \"-Wmissing-include-dirs\"") \
    _Pragma("clang diagnostic error \"-Woverloaded-virtual\"") \
    _Pragma("clang diagnostic error \"-Wredundant-decls\"") \
    _Pragma("clang diagnostic error \"-Wsign-conversion\"") \
    _Pragma("clang diagnostic error \"-Wsign-promo\"") \
    _Pragma("clang diagnostic error \"-Wswitch-default\"") \
    _Pragma("clang diagnostic error \"-Wundef\"") \
    _Pragma("clang diagnostic error \"-Wfloat-equal\"") \
    _Pragma("clang diagnostic error \"-Wpointer-arith\"") \
    _Pragma("clang diagnostic error \"-Wwrite-strings\"") \
    _Pragma("clang diagnostic error \"-Wswitch-enum\"") \
    _Pragma("clang diagnostic error \"-Wunreachable-code\"") \
    _Pragma("clang diagnostic error \"-Wdeprecated\"") \
    \
    _Pragma("clang diagnostic error \"-Wformat\"") \
    _Pragma("clang diagnostic error \"-Wunused-private-field\"") \
    _Pragma("clang diagnostic error \"-Weverything\"") \
    _Pragma("clang diagnostic ignored \"-Wc++98-compat\"") \
    _Pragma("clang diagnostic ignored \"-Wc++98-compat-pedantic\"") \
    _Pragma("clang diagnostic ignored \"-Wpadded\"") \
    _Pragma("clang diagnostic ignored \"-Wgnu-zero-variadic-macro-arguments\"")
    //_Pragma("GCC diagnostic error \"-Wlogical-op\"")
    //_Pragma("GCC diagnostic error \"-Wnoexcept\"")
    //_Pragma("GCC diagnostic error \"-Wstrict-null-sentinel\"")
    //_Pragma("GCC diagnostic error \"-Wstrict-overflow\"=5")
#endif

#define LESS_DIAGNOSTICS() \
    _Pragma("GCC diagnostic ignored \"-Wall\"") \
    _Pragma("GCC diagnostic ignored \"-Wextra\"") \
    _Pragma("GCC diagnostic ignored \"-Wshadow\"") \
    _Pragma("GCC diagnostic ignored \"-Wcast-align\"") \
    _Pragma("GCC diagnostic ignored \"-Wctor-dtor-privacy\"") \
    _Pragma("GCC diagnostic ignored \"-Wdisabled-optimization\"") \
    _Pragma("GCC diagnostic ignored \"-Wformat\"=2") \
    _Pragma("GCC diagnostic ignored \"-Winit-self\"") \
    _Pragma("GCC diagnostic ignored \"-Wlogical-op\"") \
    _Pragma("GCC diagnostic ignored \"-Wmissing-include-dirs\"") \
    _Pragma("GCC diagnostic ignored \"-Wnoexcept\"") \
    _Pragma("GCC diagnostic ignored \"-Woverloaded-virtual\"") \
    _Pragma("GCC diagnostic ignored \"-Wredundant-decls\"") \
    _Pragma("GCC diagnostic ignored \"-Wsign-conversion\"") \
    _Pragma("GCC diagnostic ignored \"-Wsign-promo\"") \
    _Pragma("GCC diagnostic ignored \"-Wstrict-null-sentinel\"") \
    _Pragma("GCC diagnostic ignored \"-Wstrict-overflow\"=5") \
    _Pragma("GCC diagnostic ignored \"-Wswitch-default\"") \
    _Pragma("GCC diagnostic ignored \"-Wundef\"") \
    _Pragma("GCC diagnostic ignored \"-Wfloat-equal\"") \
    _Pragma("GCC diagnostic ignored \"-Wpointer-arith\"") \
    _Pragma("GCC diagnostic ignored \"-Wwrite-strings\"") \
    _Pragma("GCC diagnostic ignored \"-Wswitch-enum\"") \
    _Pragma("GCC diagnostic ignored \"-Wunreachable-code\"") \
    _Pragma("GCC diagnostic ignored \"-Wdeprecated\"") \
    _Pragma("GCC diagnostic ignored \"-Wmisleading-indentation\"")

#pragma GCC diagnostic push
MORE_DIAGNOSTICS()



//============================================================================
// Generic helpful snippets used in the analysis
//============================================================================

#define FILENAME (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define fatal( format, ... ) _fatal( FILENAME, __LINE__, format, ##__VA_ARGS__ )

void _fatal( char const * const file, int line, char const * const format, ... );
void _fatal( char const * const file, int line, char const * const format );
void _fatal( char const * const format );



//============================================================================
// Assorted helpers
//============================================================================

// void ptSort(xAOD::IParticleContainer* c);
float deltaR( const float eta1, const float phi1, const float eta2, const float phi2 );
template <class T>
inline float deltaR( const T tp1, const T tp2 ) { return deltaR( tp1->eta(), tp1->phi(), tp2->eta(), tp2->phi() ); }



//============================================================================
// Truth extraction
//============================================================================

void fillTruth( const xAOD::TruthParticle* tp, bool& truth_matched, float& truth_pt, float& truth_phi, float& truth_eta, float& truth_e, int& truth_pdgId, int& truth_parent_pdgId );

void retrieveTruth( const xAOD::IParticle* particle, bool& truth_matched, float& truth_pt, float& truth_phi, float& truth_eta, float& truth_e, int& truth_pdgId, int& truth_parent_pdgId );

bool isTruePhoton(int p_type);
bool isTrueElectron(int p_type, int p_origin, int mother_origin);
bool isTrueElectronFromW(int p_type, int p_origin, int mother_origin);
bool isTrueElectronFromZ(int p_type, int p_origin, int mother_origin);
bool isTrueElectronFromPromptJpsi(int p_type, int p_origin);
bool isTrueElectronFromTtbar(int p_type, int p_origin, int mother_origin);



/*
void getEgMotherTruthParticles( const xAOD::IParticle& p, const xAOD::TruthParticle*& firstEgMother, const xAOD::TruthParticle*& lastEgMother );

void retrieveCentralElectronAdditionalTruth(
    const xAOD::IParticle* centralElectron,
    bool& firstEgMotherTruthMatched,
    float& firstEgMotherPt,
    float& firstEgMotherPhi,
    float& firstEgMotherEta,
    float& firstEgMotherE,
    int& firstEgMotherPdgId,
    int& firstEgMotherParentPdgId,
    bool& lastEgMotherTruthMatched,
    float& lastEgMotherPt,
    float& lastEgMotherPhi,
    float& lastEgMotherEta,
    float& lastEgMotherE,
    int& lastEgMotherPdgId,
    int& lastEgMotherParentPdgId
);
*/



//============================================================================
// For the configuration
//============================================================================

// typedef for a vector of doubles (to save some typing)
typedef std::vector<double>  NumV;
// typedef for a vector of ints (to save some typing)
typedef std::vector<int>     IntV;
// typedef for a vector of strings (to save some typing)
typedef std::vector<TString> StrV;

// Converts a text line to a vector of words
// str input string with words
// sep separator to define where a word ends or starts
StrV vectorize(TString str, TString sep=" ");

// Converts string of separated numbers to vector<double>
// str input string with numbers
// sep separator to define where a number ends or starts
NumV vectorizeNum(TString str, TString sep=" ");

bool fileExist(TString fn);

#pragma GCC diagnostic pop
#endif
