#include "MyAnalysis/MyTagAndProbeAnalysis.h"
#include "MyAnalysis/RunUtils.h"

MORE_DIAGNOSTICS()

int main( int argc, char *argv[] ) {
	// Set up the job for xAOD access
	xAOD::Init().ignore();

	// Create our algorithm
	MyTagAndProbeAnalysis *alg = new MyTagAndProbeAnalysis();

    // Use helper to start the job
	runJob(alg, argc, argv);

	return 0;
}
