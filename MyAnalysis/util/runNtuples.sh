#!/usr/bin/env bash



###########################################################################################
#   Variables modified by command line options
###########################################################################################

# running on grid instead of local
	run_grid=false

# if running on grid, which iteration of the ntuple production for the same DAOD file is it (number must be supplied on command line and be between 1 and 99)
output_version=0

# ignore already existing output and run only missing datasets
run_only_missing_datasets=false

# number of concurrent EventLoops
proc_max=10

# number of events to run over locally (-1 = all)
events_max=-1

# text file containing list of datasets and analysis types
fileoverdatasets="testdataset.txt"

# directory with datasets
search_dir="/lustre/hpc/hep/ehrke/storage/DAOD/"

# whether executing a dry run
dry_run=true

# whether to stop after checks and do no (dry-)running at all
no_running=false

# if not empty, will only keep the datasets with this analysis type (can only handle one)
filter_analysis_type=""

# do not check for unknown datasets in the dataset directory
ignore_unknown_datasets=false

# do not check for missing datasets in the dataset directory
ignore_missing_datasets=false

# debug sent to framework
debug=false

# default text for help screen
run_grid_txt="default local"
if [ "$run_grid" = true ]; then run_grid_txt="this is the default behavior"; fi
run_only_missing_datasets_txt="defaults to run over all and fails if any is found already"
if [ "$run_only_missing_datasets" = true ]; then run_only_missing_datasets_txt="this is the default behavior"; fi
dry_run_txt0="Dry-runs"
dry_run_txt1=" Needs -r/--run to actually execute."
dry_run_txt2=" (default is dry-run)"
if [ "$dry_run" = false ]; then dry_run_txt0="Executes"; dry_run_txt1=""; dry_run_txt2=" (default is executing; no dry-running)"; fi
debug_txt="false"
if [ "$debug" = true ]; then debug_txt="true, no normal running"; fi

# help screen
USAGE=$(cat <<-END
Usage: $(basename $0) [OPTION]...

$dry_run_txt0 the DAOD->ntuple EventLoop framework.${dry_run_txt1}
Fails if the output directory has any output from a dataset, unless -m/--missing-only is set.
Dataset names are re-used for the outgoing names with the _EXT0 ending changed to _NTUP#, where # is the analysisType #.

Mandatory arguments to long options are mandatory for short options too.
  -d, --debug                   set debug flag for framework (default $debug_txt)
  -e, --max-events=N            if running locally, change the maximum events to N with no effect on
                                grid and -1 being all (default $events_max)
  -g, --grid-run                execute on grid instead of locally ($run_grid_txt)
  -h, --help                    display this help and exit
  -l, --dataset-list=LIST       set dataset list to LIST, a text file containing a list of datasets and analyis type(s);
                                use the form "nfiles dsname 1 2" for dataset "dsname" running over analysis types
                                1 and 2 with nfiles number of files to be used when running on the grid;
                                comments only on start of line with '#' character
                                (default $fileoverdatasets)
  -m, --missing-only            run only over datasets not found in output directory
                                ($run_only_missing_datasets_txt)
  -p, --max-processes=N         change the number of concurrent executions to N (default $proc_max)
  -q, --quit                    stop after checks (when combined with -r/--run, does checks normally excluded
                                when dry-running but does not execute)
  -r, --run                     execute framework${dry_run_txt2}
  -s, --search-dir=DIR          set dataset directory to DIR; datasets must be directories containing .root
                                files and all directories must exist even if running on grid
                                (default $search_dir)
  -v, --grid-version=XX         if running on grid, specify ntuple version (1-99) for the DAOD datasets
                                (this is mandatory as there will several iterations for the same datasets,
                                and this ensures naming consistency)
      --filter-analysis-type=F  keep only datasets with analysis type F, and keep only F for the datasets that
                                have multiple analysis types
      --ignore-unknown-datasets do not check for unknown datasets in the dataset directory
      --ignore-missing-datasets do not check for missing datasets in the dataset directory
END
)

# parse options
OPTS=`getopt -o dgmp:e:l:s:rhqv: --long debug,grid-run,missing-only,max-processes:,max-events:,dataset-list:,search-dir:,run,help,quit,grid-version:,filter-analysis-type:,ignore-unknown-datasets,ignore-missing-datasets -n "$(basename $0)" -- "$@"`
if [ $? != 0 ]; then echo "Try \`$(basename $0) --help' for more information."; exit 2; fi
# echo "$OPTS"
eval set -- "$OPTS"

while true; do
  case "$1" in
    -d | --debug )         debug=true; shift ;;
    -h | --help )          echo "$USAGE"; exit 0; shift ;;
    -g | --grid-run )      run_grid=true; shift ;;
    -m | --missing-only )  run_only_missing_datasets=true; shift ;;
    -p | --max-processes ) proc_max="$2"; shift 2 ;;
    -e | --max-events )    events_max="$2"; shift 2 ;;
    -l | --dataset-list )  fileoverdatasets="$2"; shift 2 ;;
    -s | --search-dir )    search_dir="$2"; shift 2 ;;
    -r | --run )           dry_run=false; shift ;;
    -q | --quit )          no_running=true; shift ;;
    -v | --grid-version )  output_version="$2"; shift 2 ;;
    --filter-analysis-type ) filter_analysis_type="$2"; shift 2 ;;
    --ignore-unknown-datasets ) ignore_unknown_datasets=true; shift ;;
    --ignore-missing-datasets ) ignore_missing_datasets=true; shift ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done



###########################################################################################
#   Functions used by scripts
###########################################################################################

# removes unnecessary stuff from ds names
function trimFat {
	name=$1
	# double underscore names first
	name=${name/Pythia8B_A14_CTEQ6L1_/}
	name=${name/PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_/}
	name=${name/PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_/}
	name=${name/PowhegPythia8EvtGen_NNLOPS_nnlo_/}
	# then single underscore names
	name=${name/Sherpa_CT10_/}
	name=${name/PowhegPythia8EvtGen_AZNLOCTEQ6L1_/}
	name=${name/Pythia8EvtGen_A14NNPDF23LO_/}
    	name=${name/PhPy8EG_A14_/}
	name=${name/PowhegPythia8EvtGen_A14_/}
	echo $name
}

# kill a processes and ALL descendants (not just first child)
function kill_descendant_processes() {
	local pid="$1"
	local and_self="${2:-false}"
	if children="$(pgrep -P "$pid")"; then
		for child in $children; do
			kill_descendant_processes "$child" true
		done
	fi
	if [ "$and_self" = true ]; then
		kill -SIGINT "$pid"
	fi
}

# true if $1 is in $2
# $1 - string
# $2 - array passed EXACTLY as "${array[@]}" with quotes
elementIn () {
	local e match="$1"
	shift
	for e; do [[ "$e" == "$match" ]] && return 0; done
	return 1
}

function prepTest {
	fail=false
	printf "[ \e[37m....\e[0m ] $1... "
}

# if needing several writes to the same line
# accepts formatted strings (second argument is the variable list)
function updateTest {
	printf "\r\033[2K[ \e[37m....\e[0m ] $1... " $2
}

function failTest {
	if [ $fail = false ]; then
		printf "$1"
		echo -e "\r[\e[91mFAILED\e[0m]"
	fi
	fail=true
}

function finishTest {
	if [ $fail = true ]; then
		echo -e "\nExiting."
		exit_good=true
		exit 1
	fi
	echo -e "\r[  \e[32mOK\e[0m  ]"
}

# just an info line
function printInfo {
	if [ $# != 1 ]; then
		echo "printInfo must only have ONE parameter (use parentheses). First parameter is: $1"
		echo "Killing script."
		exit 1
	fi
	echo -e "\r[ \e[96mINFO\e[0m ] $1"
}

# if dry-running, echo the commands that would be executed
function runCMD {
	if [ $# != 1 ]; then
		echo "runCMD must only have ONE parameter (use parentheses). First parameter is: $1"
		echo "Killing script."
		exit 1
	fi
	if [ "$dry_run" = true ]; then
		echo -e "\r[\e[93mDRYRUN\e[0m] Would execute: $1"
	else
		eval $1
	fi
}

# even if not dry-running, echo the commands that execute
function runCMDVerbose {
	if [ "$dry_run" = false ]; then
		echo -e "\r[\e[93mVERBOS\e[0m] Executing: $1"
	fi
	runCMD "$@"
}

# This makes the output name based on dataset and analysis type
function makeOutputDSName {
	if [ $# != 2 ]; then
		echo "makeOutputDSName must have EXACTLY TWO parameters (use parentheses). First parameter is: $1"
		echo "Killing script."
		exit 1
	fi
	datasettrimmed=$(trimFat $1)
	analysisTypeinput=$2
	ds="${datasettrimmed/%_EXT0/_NTUP${analysisTypeinput}}_v${output_version}"
	# if input is made from a user, change user to this user
	ds=${ds/user.[a-z]*.mc/user.$RUCIO_ACCOUNT.mc}
	echo $ds
}



###########################################################################################
#   Start of script; apply some settings
###########################################################################################

echo -e "\n[[ Tool to run over all datasets ]]\n"

# convert string to integer
# done before set -e
output_version=$(expr $output_version - 0 2> /dev/null)
version_int_res=$?

# exit on any uncaught error
set -e

# empty matches return empty string
# helps when comparing dataset list with directory but screws with associative arrays
shopt -s nullglob

# warn on sudden exits
exit_good=false
function finish {
	if [ $exit_good != true ]; then
		echo -e "\n[[ UNEXPECTED EXIT ]]"
	fi
	printf "\n"
}
trap finish EXIT

# if grid-running, output is output_grid
output=outputDirs
if [ "$run_grid" = true ]; then
	output=outputDirs_grid
fi

# if debug is turned on, make string for the framework
debug_string=""
if [ "$debug" = true ]; then
	debug_string="Debug: YES"
fi



###########################################################################################
#   Pre-run checks; includes loading of sample list, etc.
###########################################################################################

# cannot be sourced since we cd around and exit on error and such
prepTest "Checking if not sourced"
if [ "$0" != "$BASH_SOURCE" ]; then
	printf "Being sourced - not allowed."
	echo -e "\r[\e[91mFAILED\e[0m]"
	# since it's sourced, we cannot exit and hence not use failTest
	return
fi
finishTest

# check if all environmental variables are correct, and that all arguments are well-defined, and that there are no missing directories
prepTest "Checking env. vars, options, and directories"
# ntuple version must exist and be between 1 and 99
if [ "$run_grid" = true ]; then
	if [ "$version_int_res" -ne 0 ] || [ $output_version -lt 1 ] || [ $output_version -gt 99 ]; then
		failTest "The following issues were found:"
		echo "Grid selected, but value for sample version (-v, --grid-version) is bad. Give a value between 1 and 99."
	else
		if [ $output_version -lt 10 ]; then
			output_version="0$output_version"
		fi
	fi
fi
# RUCIO_ACCOUNT must be set for grid access; it's the username we'll use
if [ "$run_grid" = true ] && [ -z $RUCIO_ACCOUNT ]; then
	failTest "The following issues were found:"
	echo "Grid selected, but \$RUCIO_ACCOUNT is unset. Set the environmental variable to your CERN/grid username."
fi
# Check if file over samples exists
if [ ! -f "$fileoverdatasets" ]; then
	failTest "The following issues were found:"
	echo "File containing list of samples not found. Please make or link the file."
fi
if [ ! -d "$output" ]; then
	failTest "The following issues were found:"
	echo "Output dir [$output] does not exist."
fi
finishTest

# Stop early if not setup (properly)
prepTest "Checking if EventLoop is set up"
if [[ "$UserAnalysis_DIR" != *"/NtupleProd/build/x"* ]] || [ -z $UserAnalysis_DIR ] || [ -z $TestArea ] || [[ "$UserAnalysis_DIR" != "$TestArea"* ]]; then
	failTest "EventLoop not set up to this project, or the project is not named 'NtupleProd'."
fi
finishTest

# Stop if multiple build sub-directories has been made (bug that happens on non-lxplus systems - either re-make the build directory and recompile or remove this test)
# prepTest "Checking for bugged build (multiple platforms)"
# if [ $(ls -d1 $TestArea/x*-*-*-*/ | wc -l) -ne 1 ]; then
# 	failTest "Not exactly one x* folder found in build directory."
# fi
# finishTest

# load datasets
declare -A datasets
declare -A datasetnfiles
# read dataset list from file
prepTest "Reading dataset list from file [$fileoverdatasets]"
while read -r line; do
	# skip lines that start with # (comments) and empty (at most only whitespace) lines
	if [ "${line::1}" = "#" ] || [[ "$line" =~ ^[[:space:]]*$ ]]; then
		continue
	fi
	# die if a comment characeter is found later
	if [[ "$line" =~ "#" ]]; then
		failTest "Bad line. Comments may only be at the start of a line:"
		echo "$line"
		break
	fi
	# must contain at least two spaces
	res="${line//[^ ]}"
	if [[ ${#res} -lt 2 ]]; then
		failTest "Bad line. Line must contain at least TWO spaces:"
		echo "$line"
		break
	fi
	# passed simple checks, let's make the strings and test them
	re="^user.[a-z]+\..*_(r|p).*\.[0-9]{2,3}\.ePID18_EXT0$"
	nfiles="${line%% *}" # everything until first space
	line2="${line#* }" # everything after first space is the new line
	ds="${line2%% *}" # everything until first space of the new line (so, between first and second space of the original line)
	at="${line2#* }" # everything after first space of the new line (so, after second space of the original line)
	arr=(${at})
	# check that dataset name is sorta correct (also catches if a space is missed between ds name and analysis types)
	# if [[ ! $ds =~ $re ]]; then
	# 	failTest "Bad line. Line does not conform to expected naming:"
	# 	echo "$ds"
	# 	break
	# fi
	# analysisType is bad
	if [ ${#arr[@]} -eq 0 ]; then
		failTest "Bad line. AnalysisType is empty on this line:"
		echo "$line"
		break
	fi
	# must not exist twice
	if [ ! -z "${datasets["$ds"]}" ]; then
		failTest "Bad line. Dataset already defined on a previous line:"
		echo "$ds"
		break
	fi
	# DAOD version must be the same for all
	arrds=(${ds//./ })
	if [ -z $daod_version ]; then
		daod_version=${arrds[7]}
	else
		if [ $daod_version != ${arrds[7]} ]; then
			failTest "Bad line. All datasets must be the same version; this one is something else:"
			echo $line
			break
		fi
	fi
	# if filter_analysis_type is not empty, keep only the datasets with analysis type equal to the filter; remove any dataset with wrong type, and remove other types from right datasets
	if [ "$filter_analysis_type" != "" ]; then
		if elementIn $filter_analysis_type "${arr[@]}"; then
			# filter found in the line's analysis type, and it has more types
			if [ ${#arr[@]} -gt 1 ]; then
				at=$filter_analysis_type
			fi
		else
			# filter not found, do not keep it
			continue
		fi
	fi
	# accepted
	datasets["${ds}"]="${at}"
	datasetnfiles["${ds}"]="${nfiles}"
done < $fileoverdatasets
finishTest

# echo
# echo "found:"
# for dataset in "${!datasets[@]}"
# do
# 	echo "$dataset   -   ${datasets[$dataset]}"
# done

# exit

# grid requested, do grid-related checks
if [ "$run_grid" = true ]; then
	# properly set-up?
	prepTest "Grid requested. Checking if properly set up"
	if [[ -z $ATLAS_LOCAL_PANDACLIENT_PATH ]] || [[ -z $PANDA_CONFIG_ROOT ]] || [[ -z $ALRB_gridType ]] || [[ -z $X509_USER_PROXY ]] || [[ -z $GLOBUS_LOCATION ]]; then
		failTest "Please setup grid (panda,rucio,voms-proxy-init) first."
	fi
	finishTest

	# grid proxy cannot run out in the middle of submission
	prepTest "Grid requested. Checking if at least 6 hours left"
	if ! voms-proxy-info -exists -valid 6:00 &> /dev/null; then
		failTest "Please run voms-proxy-init to renew proxy."
	fi
	finishTest

	# will any outgoing dataset name be too long?
	prepTest "Grid requested. Checking lengths of (trimmed) outgoing dataset names"
	for dataset in "${!datasets[@]}"; do
		# remember
		# outDS will be two characters longer due to something + "/"
		# max length 119 for outDS
		ds="$(makeOutputDSName $dataset "X")/"
		if [ ${#ds} -ge 118 ]; then
			failTest "The following dataset(s) have too long dataset names:"
			echo "Trimmed length: ${#ds}/118. Trimmed OutDS: $ds. InDS: $dataset"
		fi
	done
	finishTest

	# make sure any output dataset does not already exist
	if [ "$dry_run" = false ]; then
		prepTest "Grid requested. Checking for existing ntuples with same version on grid"
		found_ds=$(rucio ls "user.${RUCIO_ACCOUNT}.mc16_13TeV.*.*.EGAM*.${daod_version}.ePID18_NTUP*_v${output_version}_myOutput.root" --filter type=CONTAINER --short)
		found_ds=($found_ds)
		for dataset in "${!datasets[@]}"; do
			for analysisType in ${datasets[$dataset]}; do
				ds=$(makeOutputDSName $dataset $analysisType)  # outDS
				ds="user.${RUCIO_ACCOUNT}:${ds}_myOutput.root" # grid dataset name
				if elementIn $ds "${found_ds[@]}"; then
					failTest "The following dataset(s) already exist on the grid:"
					echo $ds
				fi
			done
		done
	else
		prepTest "Grid requested. Checking for existing ntuples with same version on grid (skipped for dry-runs)"
	fi
	finishTest
fi

# check directory for unknown datasets
cd $search_dir
if [ "$ignore_unknown_datasets" = false ]; then
	prepTest "Checking dataset dir for unknown datasets"
	for dir in */; do
		if [[ -z ${datasets["${dir::${#dir}-1}"]} ]]; then
			failTest "Found the following unknown dataset(s):"
			echo "$dir"
		fi
	done
else
	prepTest "Checking dataset dir for unknown datasets (skipped through command line option)"
fi
finishTest

# check directory for missing datasets
if [ "$ignore_missing_datasets" = false ]; then
	prepTest "Checking dataset dir for missing datasets"
	for dataset in "${!datasets[@]}"; do
		if [[ ! -d "$dataset"/ ]]; then
			failTest "The following expected dataset(s) is not found:"
			echo "$dataset"
		fi
	done
else
	prepTest "Checking dataset dir for missing datasets (skipped through command line option)"
fi
finishTest

# make sure the directories contain at least ONE .root* file so we know that the dataset downloaded successfully
if [ "$ignore_missing_datasets" = false ]; then
	prepTest "Checking datasets for missing files"
	for dataset in "${!datasets[@]}"; do
		if [ ! -d "$dataset" ]; then
			continue
		fi
		if [ $(ls -Ub1 $dataset/ | grep -P '\.root(\.[0-9])?$' | wc -l) -eq 0 ]; then
			failTest "The following expected dataset(s) contains no .root* files:"
			echo "$dataset"
		fi
	done
else
	prepTest "Checking datasets for missing files (skipped because previous was skipped)"
fi
finishTest

# check output dir for already existing datasets (not allowed)
cd - &> /dev/null
cd $output
if [ $run_only_missing_datasets = true ]; then
	prepTest "Checking output dir [$output] for already existing datasets (skipped through command line option)"
else
	prepTest "Checking output dir [$output] for already existing datasets"
fi
for out in */; do
	ds=${out::${#out}-7}_EXT0
	if [[ ! -z ${datasets[$ds]} ]]; then
		# if only running on datasets without already existing output, remove the found dataset's analysis type from list
		if [ $run_only_missing_datasets = true ]; then
			# removing analysis type for this dataset
			NTUP=${out:${#out}-1}
			datasets[$ds]="${datasets[$ds]//$NTUP/}"
			# trim (remove whitespace on sides)
			datasets[$ds]="$(echo -e "${datasets[$ds]}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"
			# if no more analysis types left, remove dataset from list
			if [[ "${datasets[$ds]}" = "" ]]; then
				# since nullglob is on, must unset this exact way
				unset -v 'datasets[$ds]'
			fi
			# go on to next directory (never fail)
			continue
		fi
		# output found, fail with directory name
		failTest "The following dataset(s) already has output (not allowed - remove):"
		echo "$output/${out}"
	fi
done
finishTest
cd ..

# check if compiled by starting compilation and killing it if it start compiling a file
# this is not done when dry-running as it takes a little while and is not needed for dry-runs
if [ "$dry_run" = false ]; then
	prepTest "Checking if compiled (timeout in 9.0 seconds)"
	cmake --build $TestArea &> .test_compilation_tmp &
	pid=$!
	start_time="$(date -u +%s.%N)"
	while sleep 0.5; do
		end_time="$(date -u +%s.%N)"
		elapsed="$( bc <<< "$end_time-$start_time" )"
		left="$( bc <<< "9-$elapsed" )"
		updateTest "Checking if compiled (timeout in %3.1f seconds)" "$left"
		if [ $( bc <<< "$elapsed>9" ) -eq 1 ]; then
			updateTest "Checking if compiled"
			failTest "Timed out"
			kill_descendant_processes $pid
			# ! suppresses exit on non-zero returns (set -e) so the script does not stop
			! rm .test_compilation_tmp &> /dev/null
			break
		fi
		if grep -q "Package build succeeded" .test_compilation_tmp; then
			updateTest "Checking if compiled"
			! rm .test_compilation_tmp &> /dev/null
			break
		fi
		if grep -q "Building CXX object " .test_compilation_tmp; then
			updateTest "Checking if compiled"
			failTest "Compilation is needed - please compile before running"
			kill_descendant_processes $pid
			! rm .test_compilation_tmp &> /dev/null
			break
		fi
		if ! kill -0 $pid &> /dev/null; then
			updateTest "Checking if compiled"
			failTest "Ended unexpectedly - please compile"
			! rm .test_compilation_tmp &> /dev/null
			break
		fi
	done
else
	prepTest "Checking if compiled (skipped for dry-runs)"
fi
finishTest
! rm .test_compilation_tmp &> /dev/null



###########################################################################################
#   Execution of framework
###########################################################################################

# all tests, completed
printInfo "All tests completed."
if [ "$no_running" = true ]; then
	exit_good=true
	exit 0
fi

# start everything
for dataset in "${!datasets[@]}"; do
	while [ $(pgrep -u $USER -f "runMyTagAndProbeAnalysis MyAnalysis/" | wc -l) -ge $proc_max ]; do
		sleep 5
	done
	for analysisType in ${datasets[$dataset]}; do
		ds=$(makeOutputDSName $dataset $analysisType)
		if [ "$run_grid" = true ]; then
			n_files=${datasetnfiles[$dataset]}
			runCMDVerbose "nohup runMyTagAndProbeAnalysis MyAnalysis/dnielsen-grid.cfg $debug_string OutputDir: $output/$ds OutputDS: ${ds} GridDS: $dataset nc_nFiles: $n_files analysisType: $analysisType PrintFileList: NO &> $output/$ds.txt & disown"
		else
			runCMDVerbose "nohup runMyTagAndProbeAnalysis MyAnalysis/dnielsen.cfg $debug_string OutputDir: $output/$ds InputDir: $search_dir/$dataset/ NumEvents: $events_max analysisType: $analysisType PrintFileList: NO &> $output/$ds.txt & disown"
		fi
	done
done

# done
printInfo "Finished successfully."
exit_good=true
