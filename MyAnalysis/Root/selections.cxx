#include <MyAnalysis/MyTagAndProbeAnalysis.h>

#define PREPARE_CUTTING( HIST_ID ) \
    size_t histCutID = HIST_ID; \
    bool allCutsPassedSoFar = true;

#define NEW_CUT_HIST( HIST_ID ) \
    histCutID = HIST_ID;

#define RESET_CUTTING() \
    allCutsPassedSoFar = true;

#define CUT_SUCCEEDED( CUT_ID, RESULT ) \
    if ( RESULT ) m_histCutIndiv[histCutID]->Fill( CUT_ID ); \
    allCutsPassedSoFar &= RESULT; \
    if ( allCutsPassedSoFar ) m_histCutAccum[histCutID]->Fill( CUT_ID );

#define CHECK_ALL_CUT_SUCCESS() \
    if ( !allCutsPassedSoFar ) continue;

MORE_DIAGNOSTICS()


bool MyTagAndProbeAnalysis::Match(const std::vector<const xAOD::IParticle*>& recoObjects, const std::string& chain){
  
  double eta_tag = recoObjects.at(0)->eta();
  double phi_tag = recoObjects.at(0)->phi();

  double eta_probe = recoObjects.at(1)->eta();
  double phi_probe = recoObjects.at(1)->phi();

  double dRcut = 0.07;

  auto chainGroup = m_trigDecisionTool->getChainGroup(chain);
  bool decision = chainGroup->isPassed();
  // MSG_DEBUG(chain << " is passed: " << decision);
  if(!decision) return false;
   
  auto featureContainer = chainGroup->features();
  auto combinations = featureContainer.getCombinations();
  // MSG_DEBUG("chain has: " << combinations.size() << " combos"); 

  bool resultTag = false;
  bool resultProbe = false;
  bool result = false;
  int iComb = 0; 
  int tagComb = -1;
  int probeComb = -1;
  int ntag = 0;
  int nprobe = 0;
  
  for(auto& comb : combinations){ 
    const auto vec = comb.containerFeature<xAOD::ElectronContainer>();
    const auto gvec = comb.containerFeature<xAOD::PhotonContainer>();
    bool active = comb.active();
    if(!active) continue;
    for(const auto feat: gvec){
      const xAOD::PhotonContainer   *gcont=feat.cptr();
      std::string trigLegName = Trig::getTEName(*feat.te());
      size_t pos = trigLegName.find("etcut");
      if(pos == std::string::npos) continue;
      for(const auto ph : *gcont){
    double trig_eta = ph->eta();
    double trig_phi = ph->phi();
    double tryDeltaR = deltaR(eta_probe, phi_probe, trig_eta, trig_phi);
    if(tryDeltaR < dRcut){
      resultProbe = true;
      probeComb = iComb;
      nprobe++;
    }
      }
    }
    
    for(const auto feat: vec){
      const xAOD::ElectronContainer *cont=feat.cptr();
      std::string trigLegName = Trig::getTEName(*feat.te());
      size_t posTag = trigLegName.find("tight");
      if(posTag != std::string::npos){
    for(const auto el : *cont){
      double trig_eta = el->eta();
      double trig_phi = el->phi();
      double tryDeltaR = deltaR(eta_tag, phi_tag, trig_eta, trig_phi);
      if(tryDeltaR < dRcut){
        resultTag = true;
        tagComb = iComb;
        ntag++;
      }
    }
      }

      size_t posProbe = trigLegName.find("etcut");
      if(posProbe != std::string::npos){
    for(const auto el : *cont){
      double trig_eta = el->eta();
      double trig_phi = el->phi();
      double tryDeltaR = deltaR(eta_probe, phi_probe, trig_eta, trig_phi);
      if(tryDeltaR < dRcut){
        resultProbe = true;
        probeComb = iComb;
        nprobe++;
      
      }
    }
      }
    }

    result = (resultTag && resultProbe && tagComb==probeComb);
    if(result) break;
    iComb++;
  }

  return result;
}

std::vector< particleWrapper > MyTagAndProbeAnalysis::selectTruthElectrons() {
    std::vector< particleWrapper > electrons_out;
    for ( const auto& el : *m_electrons ) {
        particleWrapper pw = particleWrapper(el, true);
        const SG::AuxElement& pwAuxElementRef = *(pw.getAuxElement());
        if ( isTrueElectron( Accessor_truthType( pwAuxElementRef ), Accessor_truthOrigin( pwAuxElementRef ), Accessor_lastEgMotherTruthOrigin( pwAuxElementRef ) ) ) {
            electrons_out.push_back(pw);
        }
    }
    return electrons_out;
}

std::vector< particleWrapper > MyTagAndProbeAnalysis::selectTruthPhotons() {
    std::vector< particleWrapper > photons_out;
    for ( const auto& ga : *m_photons ) {
        particleWrapper pw = particleWrapper(ga);
        const SG::AuxElement& pwAuxElementRef = *(pw.getAuxElement());
        if ( isTruePhoton( Accessor_truthType( pwAuxElementRef ) ) ) {
            photons_out.push_back(pw);
        }
    }
    return photons_out;
}


std::vector< particleWrapper > MyTagAndProbeAnalysis::selectTagElectrons( std::vector< particleWrapper > electrons_in ) {
    std::vector< particleWrapper > electrons_out;
    std::vector<const xAOD::IParticle*> myParticles;
    PREPARE_CUTTING( m_tagElectronCutHist )
    bool res;
    for ( const auto& pw : electrons_in ) {
        RESET_CUTTING()

        // Initial
        CUT_SUCCEEDED( 0, true )

        // pt cut
        res = pw.getCentralElectron()->pt() > m_tagPt;
        CUT_SUCCEEDED( 1, res )

        // Match trigger
        myParticles.clear();
        myParticles.push_back( pw.getCentralElectron() );
        res = m_tmt->match( myParticles, "HLT_e26_lhtight_nod0_ivarloose", m_triggerMatchingdR, false ) ||
                m_tmt->match( myParticles, "HLT_e60_lhmedium_nod0", m_triggerMatchingdR, false ) ||  // included for scale factors
                m_tmt->match( myParticles, "HLT_e140_lhloose_nod0", m_triggerMatchingdR, false ); // included for scale factors 
        ;
        CUT_SUCCEEDED( 2, res )

        // Add if all cuts have passed
        CHECK_ALL_CUT_SUCCESS()
        electrons_out.push_back( pw );
    }

    return electrons_out;
}


std::vector< particleWrapper > MyTagAndProbeAnalysis::selectTagMuons() {
    std::vector< particleWrapper > muons_out;
    std::vector<const xAOD::IParticle*> myParticles;
    PREPARE_CUTTING( m_tagMuonCutHist )
    bool res;
    for ( const auto& mu : *m_muons ) {
        RESET_CUTTING()
    
        // Initial
        CUT_SUCCEEDED( 0, true )        
        
        // Only combined muons
        res = mu->muonType() == xAOD::Muon::MuonType::Combined ;
        CUT_SUCCEEDED( 1, res )    

        // pt > 25 GeV
        res = mu->pt() > m_tagPt ;
        CUT_SUCCEEDED( 2, res )  
        
        // Make sure there's a track
        // Unfortunately, we cannot also require a vertex (dunno why)
        res = mu->primaryTrackParticle() != nullptr ;
        CUT_SUCCEEDED( 3, res )  
        
        // Loose isolation
        res = m_isolationSelectionToolLoose->accept( *mu ) ;
        CUT_SUCCEEDED( 4, res )  
        
        // Lowest unprescaled trigger
        myParticles.clear();
        myParticles.push_back( mu );
        res = m_tmt->match( myParticles, "HLT_mu26_ivarmedium", m_triggerMatchingdR, false ) ;
        CUT_SUCCEEDED( 5, res )  
        
        // Medium ID
        res = m_muonSelectionMedium->accept( *mu ) ;
        CUT_SUCCEEDED( 6, res )  
        
        CHECK_ALL_CUT_SUCCESS()
        muons_out.push_back( particleWrapper( mu ) );
    }

    return muons_out;
}


std::vector< particleWrapper > MyTagAndProbeAnalysis::preSelectTagElectrons() {
    std::vector< particleWrapper > electrons_out;
    std::vector<const xAOD::IParticle*> myParticles;
    PREPARE_CUTTING( m_preTagElectronCutHist )
    bool res;
    for ( const auto& el : *m_electrons ) {
        RESET_CUTTING()

        // Initial
        CUT_SUCCEEDED( 0, true )

        // pt cut
        res = el->pt() > 4500;
        CUT_SUCCEEDED( 1, res )

        // Eta crack veto
        const float eta_tag = fabs( el->eta() );
        res = eta_tag > 1.52 || 1.37 > eta_tag;
        CUT_SUCCEEDED( 2, res )

        // Tight ID
        res = m_electronSelectionToolTight->accept( el );
        CUT_SUCCEEDED( 3, res )

        // Loose iso
        res = m_isolationSelectionToolLoose->accept( *el );
        CUT_SUCCEEDED( 4, res )

        // Check Oject Quality
        res = el->isGoodOQ( xAOD::EgammaParameters::BADCLUSELECTRON );
        CUT_SUCCEEDED( 5, res )

        // Make sure there's a track and vertex
        res = el->trackParticle() != nullptr;
        CUT_SUCCEEDED( 6, res )
        res = res && el->trackParticle()->vertex() != nullptr;
        CUT_SUCCEEDED( 7, res )

        // Add if all cuts have passed
        CHECK_ALL_CUT_SUCCESS()
        electrons_out.push_back( particleWrapper( el, true ) );
    }

    return electrons_out;
}


std::vector< particleWrapper > MyTagAndProbeAnalysis::selectJpsiTagElectrons( std::vector< particleWrapper > electrons_in ) {
    std::vector< particleWrapper > electrons_out;
    std::vector<const xAOD::IParticle*> myParticles;
    PREPARE_CUTTING( m_tagJpsiElectronCutHist )
    bool res;
    for ( const auto& pw : electrons_in ) {
        RESET_CUTTING()

        // Initial
        CUT_SUCCEEDED( 0, true )

        // Match trigger
        myParticles.clear();
        myParticles.push_back( pw.getCentralElectron() );
        res =   m_tmt->match( myParticles, "HLT_e14_lhtight_nod0_e4_etcut_Jpsiee", m_triggerMatchingdR, false ) ||
                m_tmt->match( myParticles, "HLT_e5_lhtight_nod0_e4_etcut_Jpsiee", m_triggerMatchingdR, false ) ||
                m_tmt->match( myParticles, "HLT_e9_lhtight_nod0_e4_etcut_Jpsiee", m_triggerMatchingdR, false ) || 
                m_tmt->match( myParticles, "HLT_e5_lhtight_nod0_e9_etcut_Jpsiee", m_triggerMatchingdR, false ) || 
                m_tmt->match( myParticles, "HLT_e5_lhtight_nod0_e14_etcut_Jpsiee", m_triggerMatchingdR, false ); 
        CUT_SUCCEEDED( 1, res )


        // Add if all cuts have passed
        CHECK_ALL_CUT_SUCCESS()
        electrons_out.push_back( pw );
    }

    return electrons_out;
}


std::vector< particleWrapper> MyTagAndProbeAnalysis::selectProbeElectrons( xAOD::ElectronContainer* electronContainer, const bool isCentralElectrons ) {
    std::vector< particleWrapper > electrons_out;
    PREPARE_CUTTING( m_probeElectronCutHist )
    bool res;

    for ( const auto& el : *electronContainer ) {
        RESET_CUTTING()

        // Initial
        CUT_SUCCEEDED( 0, true )

        // pt cut (15 GeV)
        res = el->pt() > m_probePt;
        CUT_SUCCEEDED( 1, res )

        // Check Oject Quality
        res = el->isGoodOQ( xAOD::EgammaParameters::BADCLUSELECTRON );
        CUT_SUCCEEDED( 2, res )

        // Exclude very forward electrons (4.9)
        res = fabs( el->eta() ) < m_probeEta;
        CUT_SUCCEEDED( 3, res )
        
        // Make sure there's a track and vertex
        res = el->trackParticle() != nullptr;
        CUT_SUCCEEDED( 4, res )
        res = res && el->trackParticle()->vertex() != nullptr;
        CUT_SUCCEEDED( 5, res )
        
 
        CHECK_ALL_CUT_SUCCESS()
        electrons_out.push_back( particleWrapper( el, isCentralElectrons ) );
    }

    return electrons_out;
}

std::vector< particleWrapper> MyTagAndProbeAnalysis::selectZProbeElectrons( std::vector< particleWrapper> electrons_in ){
    std::vector< particleWrapper > electrons_out;
    PREPARE_CUTTING( m_probeZElectronCutHist )
    bool res;
    
    for ( const auto& pw : electrons_in ){
        RESET_CUTTING()

        // Initial
        CUT_SUCCEEDED( 0, true )

        // pt cut (15 GeV)
        res = pw.getCentralElectron()->pt() > 9500;
        CUT_SUCCEEDED( 1, res )
        
        CHECK_ALL_CUT_SUCCESS()
        electrons_out.push_back ( pw );
    }
    return electrons_out;
}


std::vector< particleWrapper> MyTagAndProbeAnalysis::selectJpsiProbeElectrons( xAOD::ElectronContainer* electronContainer, const bool isCentralElectrons ) {
    std::vector< particleWrapper > electrons_out;
    PREPARE_CUTTING( m_probeJpsiElectronCutHist )
    bool res;

    for ( const auto& el : *electronContainer ) {
        RESET_CUTTING()

        // Initial
        CUT_SUCCEEDED( 0, true )

        // pt cut (15 GeV)
        res = el->pt() > 4500;
        CUT_SUCCEEDED( 1, res )

        // Check Oject Quality
        res = el->isGoodOQ( xAOD::EgammaParameters::BADCLUSELECTRON );
        CUT_SUCCEEDED( 2, res )

        // Exclude very forward electrons (4.9)
        res = fabs( el->eta() ) < m_probeEta;
        CUT_SUCCEEDED( 3, res )

        // Make sure there's a track and vertex
        res = el->trackParticle() != nullptr;
        CUT_SUCCEEDED( 4, res )
        res = res && el->trackParticle()->vertex() != nullptr;
        CUT_SUCCEEDED( 5, res )
        

        CHECK_ALL_CUT_SUCCESS()
        electrons_out.push_back( particleWrapper( el, isCentralElectrons ) );
    }

    return electrons_out;
}



std::vector< particleWrapper > MyTagAndProbeAnalysis::selectBackground( std::vector< particleWrapper > electrons_in ) {
    std::vector< particleWrapper > backgroundOut;
    bool res;
    std::vector<const xAOD::IParticle*> myParticles;
    PREPARE_CUTTING( m_backgroundCentralElectronsHist )
    for ( const auto& pw : electrons_in ) {
        RESET_CUTTING()
        // number of tracks considered (what was this for?)

        // Initial
        CUT_SUCCEEDED( 0, true )

        // Match trigger
        myParticles.clear();
        myParticles.push_back( pw.getCentralElectron() );
        res = m_tmt->match( myParticles, "HLT_e5_etcut", m_triggerMatchingdR, false ) ||
                m_tmt->match( myParticles, "HLT_e10_etcut_L1EM7", m_triggerMatchingdR, false ) ||
                m_tmt->match( myParticles, "HLT_e15_etcut_L1EM7", m_triggerMatchingdR, false ) ||
                m_tmt->match( myParticles, "HLT_e20_etcut_L1EM12", m_triggerMatchingdR, false ) ||
                m_tmt->match( myParticles, "HLT_e25_etcut_L1EM15", m_triggerMatchingdR, false ) ||
                m_tmt->match( myParticles, "HLT_e30_etcut_L1EM15", m_triggerMatchingdR, false ) ||
                m_tmt->match( myParticles, "HLT_e40_etcut_L1EM15", m_triggerMatchingdR, false ) ||
                m_tmt->match( myParticles, "HLT_e50_etcut_L1EM15", m_triggerMatchingdR, false ) ||
                m_tmt->match( myParticles, "HLT_e60_etcut", m_triggerMatchingdR, false ) ||
                m_tmt->match( myParticles, "HLT_e80_etcut", m_triggerMatchingdR, false ) ||
                m_tmt->match( myParticles, "HLT_e100_etcut", m_triggerMatchingdR, false ) ||
                m_tmt->match( myParticles, "HLT_e120_etcut", m_triggerMatchingdR, false );
        CUT_SUCCEEDED( 1, res )

        CHECK_ALL_CUT_SUCCESS()
        backgroundOut.push_back( pw );
    }

    return backgroundOut;
}


std::tuple < std::vector< std::pair<particleWrapper, particleWrapper> > , std::vector< std::pair<particleWrapper, particleWrapper> > > MyTagAndProbeAnalysis::selectLeptonPairs( const std::vector< particleWrapper >& tags, std::vector< particleWrapper > probes ) {

    // Output
    std::vector< std::pair<particleWrapper, particleWrapper> > leptonPairs_out;
    std::vector< std::pair<particleWrapper, particleWrapper> > SS_leptonPairs_out;

    // How this works:
    // Tags and probes are paired to find their invariant masses, with these additional requirements:
    //  - If there is only one good central probe the central probe is used.
    //  - If there is more than one good central probe, the tag fails.

    PREPARE_CUTTING( ( tags[0].getType() == 1 ? m_leptonPairCutHist : m_muLeptonPairCutHist ) ) 
    
    bool res;

    for ( size_t i = 0; i < tags.size(); ++i ) {
        size_t jbest = 0;
        std::vector < size_t > ssj;
        bool foundProbe = false;
        size_t nFoundCentralProbes = 0;
        for ( size_t j=0; j < probes.size(); ++j ) {
            // Avoid pairing electrons with themselves
            if ( tags[i].sameAs( probes[j] ) ) continue;

            RESET_CUTTING()

            // Initial
            CUT_SUCCEEDED( 0, true )


            // If tag and probe is too close, skip
            res = tags[i].p4().DeltaR( probes[j].p4() ) > ( 0.4);
            CUT_SUCCEEDED( 1, res )

            // Close to Z mass
            const double temp = ( tags[i].p4() + probes[j].p4() ).M();
            res = temp > m_mZLowerCutSignal;
            CUT_SUCCEEDED( 2, res )

            CHECK_ALL_CUT_SUCCESS()

            // same charge
            res = tags[i].charge() * probes[j].charge() < 0;
            CUT_SUCCEEDED( 3, res )
            
            if ( res ){
                ++nFoundCentralProbes;
                jbest = j;
            }
            else{
                ssj.push_back( j );
            }
        
            foundProbe = true;
        }
        // A good probe was found for this tag, and
        // the probe was central and the only central one
        if ( foundProbe && nFoundCentralProbes==1 ) {
            leptonPairs_out.push_back( std::make_pair( tags[i], probes[jbest] ) );
        }
        if ( foundProbe && ssj.size() > 0 ){
            for ( const auto k : ssj ){
                SS_leptonPairs_out.push_back( std::make_pair( tags[i], probes[k] ) );
            }
        }
    }

    return { leptonPairs_out, SS_leptonPairs_out};
}



std::vector< std::pair<particleWrapper, particleWrapper> > MyTagAndProbeAnalysis::selectJpsiLeptonPairs( const std::vector< particleWrapper >& tags, std::vector< particleWrapper > probes ) {

    // Output
    std::vector< std::pair<particleWrapper, particleWrapper> > leptonPairs_out;

    // How this works:
    // Tags and probes are paired to find their invariant masses, with these additional requirements:
    //  - If there is only one good central probe the central probe is used.
    //  - If there is more than one good central probe, the tag fails.


    PREPARE_CUTTING( m_leptonJpsiPairCutHist )
    bool res;

    for ( size_t i = 0; i < tags.size(); ++i ) {
        size_t jbest = 0;
        bool foundProbe = false;
        size_t nFoundCentralProbes = 0;
        for ( size_t j=0; j < probes.size(); ++j ) {
            // Avoid pairing electrons with themselves
            if ( tags[i].sameAs( probes[j] ) ) continue;

            RESET_CUTTING()

            // Initial
            CUT_SUCCEEDED( 0, true )

            // if same charge, skip
            res = tags[i].charge() * probes[j].charge() < 0;
            CUT_SUCCEEDED( 1, res )

            // If tag and probe is too close, skip
            res = tags[i].p4().DeltaR( probes[j].p4() ) > ( 0.1 );
            CUT_SUCCEEDED( 2, res )

            // Close to Z mass
            const double temp = ( tags[i].p4() + probes[j].p4() ).M();
            m_JpsiMass->Fill( temp );
            res = temp > 1500 && temp < 5000;
            CUT_SUCCEEDED( 3, res )


            double probe_lifetime;


            float lxy = -9999;
            const TLorentzVector& tag = tags[i].p4();
            const TLorentzVector& probe = probes[j].p4();
            if ( fabs( tag.Phi() - probe.Phi() ) > 1e-6 ){
                const xAOD::TrackParticle* t = nullptr;
                const xAOD::TrackParticle* p = nullptr;
                t = tags[i].trackParticle();
                p = probes[j].trackParticle();


                if ( t != nullptr && p != nullptr ){
                    double simpleXv = ( - p->d0() * cos(tag.Phi()) + t->d0() * cos(probe.Phi())) / ( sin( probe.Phi() - tag.Phi()) ) ;
                    double simpleYv = ( - p->d0() * sin(tag.Phi()) + t->d0() * sin(probe.Phi()))/ ( sin( probe.Phi() - tag.Phi()) ) ;

                    double f1 = fabs( t->pt() )* cos( tag.Phi() ) + fabs( p->pt() )* cos( probe.Phi() ) ;
                    double f2 = fabs( t->pt() )* sin( tag.Phi() ) + fabs( p->pt() )* sin( probe.Phi() ) ;
                    double c = sqrt( f1*f1 + f2*f2 ) ;

                    if ( fabs( c ) >= 1e-6 ) {
                        double a = simpleXv * f1 ;
                        double b = simpleYv * f2 ;

                        lxy = ( a + b ) / c ;
                    }
                }
            }


            // Turn Lxy into lifetime
            float ptEE_probe = ( tags[i].p4() + probes[j].p4() ).Pt();
            // float ptEE_probe = m_EDM->getPtll(itag,iprobe);
            probe_lifetime       = lxy * 3096.916/(0.299792458 * ptEE_probe);
            m_JpsiLifetime->Fill( probe_lifetime );
            res = -10 < probe_lifetime && probe_lifetime < 2. ;
            CUT_SUCCEEDED( 4, res )


            CHECK_ALL_CUT_SUCCESS()

            ++nFoundCentralProbes;

            jbest = j;
            foundProbe = true;

        }
        // A good probe was found for this tag, and
        // the probe was central and the only central one
        if ( foundProbe && nFoundCentralProbes==1 ) {
            leptonPairs_out.push_back( std::make_pair( tags[i], probes[jbest] ) );
        }
    }

    return leptonPairs_out;
}
