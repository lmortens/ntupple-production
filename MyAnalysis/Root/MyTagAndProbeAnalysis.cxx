#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <MyAnalysis/MyTagAndProbeAnalysis.h>
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include <AsgTools/MessageCheck.h>
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include <TSystem.h>
#include "xAODEgamma/ElectronContainerFwd.h"
#include "PATInterfaces/CorrectionCode.h"
#include "xAODCore/ShallowCopy.h"
#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicsUtil.h"
#include <TFile.h>
#include "EventLoop/OutputStream.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODCore/AuxContainerBase.h"
#include "xAODCore/ShallowCopy.h"
#include "AsgTools/MsgStream.h"
#include "AsgTools/MsgStreamMacros.h"
#include <math.h>
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/TrackParticleAuxContainer.h"
#include "xAODTracking/TrackingPrimitives.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include  "xAODCaloEvent/CaloCluster.h"
#include  "xAODCaloEvent/CaloClusterContainer.h"
#include "TLeaf.h"
#ifndef ROOT_TVector3
#include "TVector3.h"
#endif
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include <xAODTruth/TruthParticle.h>
#include <xAODTruth/TruthVertex.h>
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/EgammaDefs.h"
#include "xAODRootAccess/tools/TFileAccessTracer.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODMetaData/FileMetaData.h"

#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

#include "METUtilities/METHelpers.h"

#include <MyAnalysis/LightGBMpredictor.h>



MORE_DIAGNOSTICS()

// this is needed to distribute the algorithm to the workers
ClassImp( MyTagAndProbeAnalysis )



MyTagAndProbeAnalysis::MyTagAndProbeAnalysis()
    :   m_metutil( "met::METMaker/metMaker", this),
        m_muonSelectionMedium( "CP::MuonSelectionTool", this ),
        m_grl ("GoodRunsListSelectionTool/grl", this),
        m_PileupReweighting ("CP::PileupReweightingTool/tool", this)
          {
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  Note that you should only put
    // the most basic initialization here, since this method will be
    // called on both the submission and the worker node.  Most of your
    // initialization code will go into histInitialize() and
    // initialize().
}


EL::StatusCode MyTagAndProbeAnalysis::setupJob( EL::Job& job ) {
    // Here you put code that sets up the job on the submission object
    // so that it is ready to work with your algorithm, e.g. you can
    // request the D3PDReader service or add output files.  Any code you
    // put here could instead also go into the submission script.  The
    // sole advantage of putting it here is that it gets automatically
    // activated/deactivated when you add/remove the algorithm from your
    // job, which may or may not be of value to you.

    #pragma GCC diagnostic push
    LESS_DIAGNOSTICS()
    ANA_CHECK_SET_TYPE( EL::StatusCode );
    #pragma GCC diagnostic pop

    setMsgLevel( MSG::INFO );

    ANA_MSG_DEBUG("::setupJob: job.useXAOD();");
    job.useXAOD();
    //job.options()->setString( EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_athena );
    ANA_MSG_DEBUG("::setupJob: ANA_CHECK( xAOD::Init() );");
    ANA_CHECK( xAOD::Init() );

    // Tell EventLoop about our output xAOD
    // EL::OutputStream out( "outputLabel", "xAOD" );
    // job.outputAdd( out );

    return EL::StatusCode::SUCCESS;
}

size_t MyTagAndProbeAnalysis::createCutHist( TString title, std::vector<TString> cuts ) {
    TH1D* accum = new TH1D( "hist_accum_"+title, title, cuts.size(), -0.5, cuts.size()-0.5 );
    TH1D* indiv = new TH1D( "hist_indiv_"+title, title, cuts.size(), -0.5, cuts.size()-0.5 );

    for ( TH1D* hist : { accum, indiv } ) {
        wk()->addOutput( hist );
        hist->GetXaxis()->SetAlphanumeric();
        for ( size_t i = 0; i < cuts.size(); ++i ) {
            hist->GetXaxis()->SetBinLabel( i+1, cuts[i] );
        }
    }

    m_histCutAccum.push_back( accum );
    m_histCutIndiv.push_back( indiv );

    return m_histCutAccum.size()-1;
}

EL::StatusCode MyTagAndProbeAnalysis::histInitialize() {
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.

    #pragma GCC diagnostic push
    LESS_DIAGNOSTICS()
    ANA_CHECK_SET_TYPE( EL::StatusCode );
    #pragma GCC diagnostic pop

    ANA_MSG_DEBUG("::histInitialize: TFile* outputFile = wk()->getOutputFile( m_outputName );");
    TFile* outputFile = wk()->getOutputFile( m_outputName );
    m_el_tree = new TTree( "el_tree", "el_tree" );
    ANA_MSG_DEBUG("::histInitialize: m_el_tree->SetDirectory( outputFile );");
    m_el_tree->SetDirectory( outputFile );
    m_el_tree->SetAutoFlush(500);
    m_ga_tree = new TTree( "ga_tree", "ga_tree" );
    ANA_MSG_DEBUG("::histInitialize: m_ga_tree->SetDirectory( outputFile );");
    m_ga_tree->SetDirectory( outputFile );
    m_ga_tree->SetAutoFlush(500);

    m_rnd = new TRandom3();

    // Statistics
    m_treeStatistics = new TTree( "treeStatistics", "treeStatistics" );
    m_treeStatistics->SetDirectory( outputFile );
    m_treeStatistics->Branch("ntagElectrons", &ntagElectrons );
    m_treeStatistics->Branch("ntagMuons", &ntagMuons );    
    m_treeStatistics->Branch("nprobeCentralElectrons", &nprobeCentralElectrons );
    m_treeStatistics->Branch("ntagJpsiElectrons", &ntagJpsiElectrons );
    m_treeStatistics->Branch("nprobeJpsiElectrons", &nprobeJpsiElectrons );

    m_sumOfWeights = new TH1D("SumOfWeights", "SumOfWeights", 6, 0, 6);
    wk()->addOutput( m_sumOfWeights );
    m_sumOfWeights->SetBinContent(1,0);
    m_sumOfWeights->SetBinContent(2,0);
    m_sumOfWeights->SetBinContent(3,0);
    m_sumOfWeights->SetBinContent(4,0);
    m_sumOfWeights->SetBinContent(5,0);
    m_sumOfWeights->SetBinContent(6,0);    
    //============================================================================
    // Output tree branch variables
    //============================================================================

    // Misc.
    m_el_tree->Branch( "samplePath", &m_samplePath );

    m_el_tree->Branch( "em_barrel_Lr0", &em_barrel_Lr0);
    m_el_tree->Branch( "em_barrel_Lr1", &em_barrel_Lr1);
    m_el_tree->Branch( "em_barrel_Lr2", &em_barrel_Lr2);
    m_el_tree->Branch( "em_barrel_Lr3", &em_barrel_Lr3);
    m_el_tree->Branch( "em_endcap_Lr0", &em_endcap_Lr0);
    m_el_tree->Branch( "em_endcap_Lr1", &em_endcap_Lr1);
    m_el_tree->Branch( "em_endcap_Lr2", &em_endcap_Lr2);
    m_el_tree->Branch( "em_endcap_Lr3", &em_endcap_Lr3);
    m_el_tree->Branch( "lar_endcap_Lr0", &lar_endcap_Lr0);
    m_el_tree->Branch( "lar_endcap_Lr1", &lar_endcap_Lr1);
    m_el_tree->Branch( "lar_endcap_Lr2", &lar_endcap_Lr2);
    m_el_tree->Branch( "lar_endcap_Lr3", &lar_endcap_Lr3);
    m_el_tree->Branch( "tile_barrel_Lr1", &tile_barrel_Lr1);
    m_el_tree->Branch( "tile_barrel_Lr2", &tile_barrel_Lr2);
    m_el_tree->Branch( "tile_barrel_Lr3", &tile_barrel_Lr3);
    m_el_tree->Branch( "tile_gap_Lr1", &tile_gap_Lr1);
    
    m_el_tree->Branch( "time_em_barrel_Lr0", &time_em_barrel_Lr0);
    m_el_tree->Branch( "time_em_barrel_Lr1", &time_em_barrel_Lr1);
    m_el_tree->Branch( "time_em_barrel_Lr2", &time_em_barrel_Lr2);
    m_el_tree->Branch( "time_em_barrel_Lr3", &time_em_barrel_Lr3);
    m_el_tree->Branch( "time_em_endcap_Lr0", &time_em_endcap_Lr0);
    m_el_tree->Branch( "time_em_endcap_Lr1", &time_em_endcap_Lr1);
    m_el_tree->Branch( "time_em_endcap_Lr2", &time_em_endcap_Lr2);
    m_el_tree->Branch( "time_em_endcap_Lr3", &time_em_endcap_Lr3);
    m_el_tree->Branch( "time_lar_endcap_Lr0", &time_lar_endcap_Lr0);
    m_el_tree->Branch( "time_lar_endcap_Lr1", &time_lar_endcap_Lr1);
    m_el_tree->Branch( "time_lar_endcap_Lr2", &time_lar_endcap_Lr2);
    m_el_tree->Branch( "time_lar_endcap_Lr3", &time_lar_endcap_Lr3);
    m_el_tree->Branch( "time_tile_barrel_Lr1", &time_tile_barrel_Lr1);
    m_el_tree->Branch( "time_tile_barrel_Lr2", &time_tile_barrel_Lr2);
    m_el_tree->Branch( "time_tile_barrel_Lr3", &time_tile_barrel_Lr3);
    m_el_tree->Branch( "time_tile_gap_Lr1", &time_tile_gap_Lr1);
    
    // Event info
    m_el_tree->Branch( "eventNumber", &event_eventNumber );
    m_el_tree->Branch( "mcChannelNumber", &event_mcChannelNumber );
    m_el_tree->Branch( "runNumber", &event_runNumber );
    m_el_tree->Branch( "actualInteractionsPerCrossing", &event_actualInteractionsPerCrossing );
    m_el_tree->Branch( "averageInteractionsPerCrossing", &event_averageInteractionsPerCrossing );
    
    m_el_tree->Branch( "correctedActualMu", &event_correctedActualMu );
    m_el_tree->Branch( "correctedAverageMu", &event_correctedAverageMu );
    m_el_tree->Branch( "correctedScaledActualMu", &event_correctedScaledActualMu );
    m_el_tree->Branch( "correctedScaledAverageMu", &event_correctedScaledAverageMu );
    m_el_tree->Branch( "PileupWeight", &event_pileupweight );
    m_el_tree->Branch( "MC_eventWeight", &event_MCWeight );
    m_el_tree->Branch( "combinedWeight", &combinedWeight );
    m_el_tree->Branch( "event_totalWeight", &event_totalWeight );
    
    // Information about event
    m_el_tree->Branch( "isMC", &event_isMC );
    m_el_tree->Branch( "analysisType", &m_analysisType );
    m_el_tree->Branch( "NvtxReco", &event_NvtxReco );
    m_el_tree->Branch( "settype", &settype);
    
    // Inner detector tracks R=0.4 of probe ( excl. probe and tag )
    m_el_tree->Branch( "tracks_pt", &tracks_pt );
    m_el_tree->Branch( "tracks_eta", &tracks_eta );
    m_el_tree->Branch( "tracks_phi", &tracks_phi );
    m_el_tree->Branch( "tracks_charge", &tracks_charge );
    m_el_tree->Branch( "tracks_chi2", &tracks_chi2 );
    m_el_tree->Branch( "tracks_ndof", &tracks_ndof );
    m_el_tree->Branch( "tracks_pixhits", &tracks_pixhits );
    m_el_tree->Branch( "tracks_scthits", &tracks_scthits );
    m_el_tree->Branch( "tracks_trthits", &tracks_trthits );
    m_el_tree->Branch( "tracks_vertex", &tracks_vertex );
    m_el_tree->Branch( "tracks_z0", &tracks_z0 );
    m_el_tree->Branch( "tracks_d0", &tracks_d0 );
    m_el_tree->Branch( "tracks_dR", &tracks_dR );
    m_el_tree->Branch( "tracks_theta", &tracks_theta );
    m_el_tree->Branch( "tracks_sigmad0", &tracks_sigmad0 );

    // probe tracks
    m_el_tree->Branch( "p_tracks_pt", &p_tracks_pt );
    m_el_tree->Branch( "p_tracks_eta", &p_tracks_eta );
    m_el_tree->Branch( "p_tracks_phi", &p_tracks_phi );
    m_el_tree->Branch( "p_tracks_charge", &p_tracks_charge );
    m_el_tree->Branch( "p_tracks_chi2", &p_tracks_chi2 );
    m_el_tree->Branch( "p_tracks_ndof", &p_tracks_ndof );
    m_el_tree->Branch( "p_tracks_pixhits", &p_tracks_pixhits );
    m_el_tree->Branch( "p_tracks_scthits", &p_tracks_scthits );
    m_el_tree->Branch( "p_tracks_trthits", &p_tracks_trthits );
    m_el_tree->Branch( "p_tracks_vertex", &p_tracks_vertex );
    m_el_tree->Branch( "p_tracks_z0", &p_tracks_z0 );
    m_el_tree->Branch( "p_tracks_d0", &p_tracks_d0 );
    m_el_tree->Branch( "p_tracks_theta", &p_tracks_theta );
    m_el_tree->Branch( "p_tracks_sigmad0", &p_tracks_sigmad0 );
    
    // Jet information (specifically b-jets)
    m_el_tree->Branch( "jets_highestBtagMV2c10", &jets_highestBtagMV2c10 );
    m_el_tree->Branch( "jets_highestBtagDL1_pb", &jets_highestBtagDL1_pb );
    m_el_tree->Branch( "jets_highestBtagDL1_pc", &jets_highestBtagDL1_pc );
    m_el_tree->Branch( "jets_highestBtagDL1_pu", &jets_highestBtagDL1_pu );
    
    // Bunch crossing information
    m_el_tree->Branch( "BC_distanceFromFront", &BC_distanceFromFront );
    m_el_tree->Branch( "BC_filledBunches", &BC_filledBunches );
    
    // Missing transverse energy (MET)
    m_el_tree->Branch( "met_met", &met_met );
    m_el_tree->Branch( "met_phi", &met_phi );
        
    // Z particle for tag and probe
    m_el_tree->Branch( "ll_m", &ll_m );
    m_el_tree->Branch( "ll_pt", &ll_pt );
    m_el_tree->Branch( "ll_eta", &ll_eta );
    m_el_tree->Branch( "ll_phi", &ll_phi );
    m_el_tree->Branch( "ll_lifetime", &ll_lifetime );
    
    // Tag misc.
    m_el_tree->Branch( "tag_type", &tag_type );
    m_el_tree->Branch( "tag_vertexIndex", &tag_vertexIndex );
    m_el_tree->Branch( "tag_SF", &tag_SF);
    
    // Tag kinematics
    m_el_tree->Branch( "tag_e", &tag_e );
    m_el_tree->Branch( "tag_et_calo", &tag_et_calo );
    m_el_tree->Branch( "tag_pt_track", &tag_pt_track );
    m_el_tree->Branch( "tag_eta", &tag_eta );
    m_el_tree->Branch( "tag_phi", &tag_phi );
    m_el_tree->Branch( "tag_charge", &tag_charge );
    m_el_tree->Branch( "tag_z0", &tag_z0 );
    m_el_tree->Branch( "tag_d0", &tag_d0 );
    m_el_tree->Branch( "tag_sigmad0", &tag_sigmad0 );
    
    // Tag truth
    m_el_tree->Branch( "tag_TruthType", &tag_TruthType ); 
    m_el_tree->Branch( "tag_TruthOrigin", &tag_TruthOrigin ); 
    m_el_tree->Branch( "tag_truth_matched", &tag_truth_matched ); 
    m_el_tree->Branch( "tag_truth_pt", &tag_truth_pt ); 
    m_el_tree->Branch( "tag_truth_phi", &tag_truth_phi ); 
    m_el_tree->Branch( "tag_truth_eta", &tag_truth_eta ); 
    m_el_tree->Branch( "tag_truth_E", &tag_truth_E ); 
    m_el_tree->Branch( "tag_truth_pdgId", &tag_truth_pdgId ); 
    m_el_tree->Branch( "tag_truth_parent_pdgId", &tag_truth_parent_pdgId ); 
    m_el_tree->Branch( "tag_firstEgMotherTruthType", &tag_firstEgMotherTruthType ); 
    m_el_tree->Branch( "tag_firstEgMotherTruthOrigin", &tag_firstEgMotherTruthOrigin ); 
    m_el_tree->Branch( "tag_firstEgMotherPdgId", &tag_firstEgMotherPdgId ); 
    
    
    // Probe/background misc.
    m_el_tree->Branch( "p_type", &p_type );
    m_el_tree->Branch( "p_hasTrack", &p_hasTrack );
    m_el_tree->Branch( "p_nTracks", &p_nTracks );
    m_el_tree->Branch( "p_vertexIndex", &p_vertexIndex );
    m_el_tree->Branch( "p_ECIDSResult", &p_ECIDSResult );
    m_el_tree->Branch( "p_unprescaledTriggers", &p_unprescaledTriggers );
    m_el_tree->Branch( "p_prescaledJpsiTriggers", &p_prescaledJpsiTriggers );
    m_el_tree->Branch( "p_prescaledTriggers", &p_prescaledTriggers );
        
    // Probe/background kinematics
    m_el_tree->Branch( "p_e", &p_e );
    m_el_tree->Branch( "p_calibratedE", &p_calibratedE );
    m_el_tree->Branch( "p_et_calo", &p_et_calo );
    m_el_tree->Branch( "p_pt_track", &p_pt_track );
    m_el_tree->Branch( "p_eta", &p_eta );
    m_el_tree->Branch( "p_phi", &p_phi );
    m_el_tree->Branch( "p_charge", &p_charge );
    m_el_tree->Branch( "p_qOverP", &p_qOverP );
    m_el_tree->Branch( "p_z0", &p_z0 );
    m_el_tree->Branch( "p_d0", &p_d0 );
    m_el_tree->Branch( "p_sigmad0", &p_sigmad0 );
    m_el_tree->Branch( "p_d0Sig", &p_d0Sig );
    m_el_tree->Branch( "p_EptRatio", &p_EptRatio );
    m_el_tree->Branch( "p_dPOverP", &p_dPOverP );
    m_el_tree->Branch( "p_z0theta", &p_z0theta );
    m_el_tree->Branch( "p_deltaR_tag", &p_deltaR_tag );
    
    // Probe/background cluster info
    m_el_tree->Branch( "p_etaCluster", &p_etaCluster );
    m_el_tree->Branch( "p_phiCluster", &p_phiCluster );
    m_el_tree->Branch( "p_eCluster", &p_eCluster );
    m_el_tree->Branch( "p_etCluster", &p_etCluster );
    m_el_tree->Branch( "p_rawEtaCluster", &p_RawEtaCluster );
    m_el_tree->Branch( "p_rawPhiCluster", &p_RawPhiCluster );
    m_el_tree->Branch( "p_rawECluster", &p_RawECluster );
    m_el_tree->Branch( "p_eClusterLr0", &p_eClusterLr0 );
    m_el_tree->Branch( "p_eClusterLr1", &p_eClusterLr1 );
    m_el_tree->Branch( "p_eClusterLr2", &p_eClusterLr2 );
    m_el_tree->Branch( "p_eClusterLr3", &p_eClusterLr3 );
    m_el_tree->Branch( "p_etaClusterLr1", &p_etaClusterLr1 );
    m_el_tree->Branch( "p_etaClusterLr2", &p_etaClusterLr2 );
    m_el_tree->Branch( "p_phiClusterLr2", &p_phiClusterLr2 );
    m_el_tree->Branch( "p_eAccCluster", &p_eAccCluster );
    m_el_tree->Branch( "p_f0Cluster", &p_f0Cluster );
    m_el_tree->Branch( "p_etaCalo", &p_etaCalo );
    m_el_tree->Branch( "p_phiCalo", &p_phiCalo );
    m_el_tree->Branch( "p_eTileGap3Cluster", &p_eTileGap3Cluster );
    m_el_tree->Branch( "p_cellIndexCluster", &p_cellIndexCluster );
    m_el_tree->Branch( "p_phiModCalo", &p_phiModCalo );
    m_el_tree->Branch( "p_etaModCalo", &p_etaModCalo );
    m_el_tree->Branch( "p_dPhiTH3", &p_dPhiTH3 );
    m_el_tree->Branch( "p_R12", &p_R12 );
    m_el_tree->Branch( "p_fTG3", &p_fTG3 );
    
    // Probe/background calculated calorimeter values
    m_el_tree->Branch( "p_weta2", &p_weta2 );
    m_el_tree->Branch( "p_Reta", &p_Reta );
    m_el_tree->Branch( "p_Rphi", &p_Rphi );
    m_el_tree->Branch( "p_Eratio", &p_Eratio );
    m_el_tree->Branch( "p_f1", &p_f1 );
    m_el_tree->Branch( "p_f3", &p_f3 );
    m_el_tree->Branch( "p_Rhad", &p_Rhad );
    m_el_tree->Branch( "p_Rhad1", &p_Rhad1 );
    m_el_tree->Branch( "p_deltaEta1", &p_deltaEta1 );
    m_el_tree->Branch( "p_deltaPhiRescaled2", &p_deltaPhiRescaled2 );
    
    // Probe/background identification
    m_el_tree->Branch( "p_TRTPID", &p_eProbHT );
    m_el_tree->Branch( "p_LHValue", &p_LHValue );
    
    // Probe+MET transverse mass (W hypothesis)
    m_el_tree->Branch( "p_mTransW", &p_mTransW );
    
    // Probe/background isolation
    
    m_el_tree->Branch( "p_topoetcone20", &p_topoetcone20 );
    m_el_tree->Branch( "p_topoetcone30", &p_topoetcone30 );
    m_el_tree->Branch( "p_topoetcone40", &p_topoetcone40 );
    m_el_tree->Branch( "p_etcone20", &p_etcone20 );
    m_el_tree->Branch( "p_etcone30", &p_etcone30 );
    m_el_tree->Branch( "p_etcone40", &p_etcone40 );
    m_el_tree->Branch( "p_etcone20ptCorrection", &p_etcone20ptCorrection );
    m_el_tree->Branch( "p_etcone30ptCorrection", &p_etcone30ptCorrection );
    m_el_tree->Branch( "p_etcone40ptCorrection", &p_etcone40ptCorrection );
    m_el_tree->Branch( "p_ptcone20", &p_ptcone20 );
    m_el_tree->Branch( "p_ptcone30", &p_ptcone30 );
    m_el_tree->Branch( "p_ptcone40", &p_ptcone40 );
    m_el_tree->Branch( "p_ptvarcone20", &p_ptvarcone20 );
    m_el_tree->Branch( "p_ptvarcone30", &p_ptvarcone30 );
    m_el_tree->Branch( "p_ptvarcone40", &p_ptvarcone40 );
    m_el_tree->Branch( "p_ptcone20_TightTTVA_pt500", &p_ptcone20_TightTTVA_pt500 );
    m_el_tree->Branch( "p_ptcone30_TightTTVA_pt500", &p_ptcone30_TightTTVA_pt500 );
    m_el_tree->Branch( "p_ptcone40_TightTTVA_pt500", &p_ptcone40_TightTTVA_pt500 );
    m_el_tree->Branch( "p_ptvarcone20_TightTTVA_pt500", &p_ptvarcone20_TightTTVA_pt500 );
    m_el_tree->Branch( "p_ptvarcone30_TightTTVA_pt500", &p_ptvarcone30_TightTTVA_pt500 );
    m_el_tree->Branch( "p_ptvarcone40_TightTTVA_pt500", &p_ptvarcone40_TightTTVA_pt500 );
    m_el_tree->Branch( "p_ptcone20_TightTTVA_pt1000", &p_ptcone20_TightTTVA_pt1000 );
    m_el_tree->Branch( "p_ptcone30_TightTTVA_pt1000", &p_ptcone30_TightTTVA_pt1000 );
    m_el_tree->Branch( "p_ptcone40_TightTTVA_pt1000", &p_ptcone40_TightTTVA_pt1000 );
    m_el_tree->Branch( "p_ptvarcone20_TightTTVA_pt1000", &p_ptvarcone20_TightTTVA_pt1000 );
    m_el_tree->Branch( "p_ptvarcone30_TightTTVA_pt1000", &p_ptvarcone30_TightTTVA_pt1000 );
    m_el_tree->Branch( "p_ptvarcone40_TightTTVA_pt1000", &p_ptvarcone40_TightTTVA_pt1000 );
    m_el_tree->Branch( "p_topoetcone20ptCorrection", &p_topoetcone20ptCorrection );
    m_el_tree->Branch( "p_topoetcone30ptCorrection", &p_topoetcone30ptCorrection );
    m_el_tree->Branch( "p_topoetcone40ptCorrection", &p_topoetcone40ptCorrection );
    m_el_tree->Branch( "p_ptPU10", &p_ptPU10 );
    m_el_tree->Branch( "p_ptPU20", &p_ptPU20 );
    m_el_tree->Branch( "p_ptPU30", &p_ptPU30 );
    m_el_tree->Branch( "p_ptPU40", &p_ptPU40 );
    
    // Probe/background track and hits
    m_el_tree->Branch( "p_TRTTrackOccupancy", &p_TRTTrackOccupancy );
    m_el_tree->Branch( "p_numberOfInnermostPixelHits", &p_numberOfInnermostPixelHits );
    m_el_tree->Branch( "p_numberOfPixelHits", &p_numberOfPixelHits );
    m_el_tree->Branch( "p_numberOfSCTHits", &p_numberOfSCTHits );
    m_el_tree->Branch( "p_numberOfTRTHits", &p_numberOfTRTHits );
    m_el_tree->Branch( "p_numberOfTRTXenonHits", &p_numberOfTRTXenonHits );
    m_el_tree->Branch( "p_chi2", &p_chi2 );
    m_el_tree->Branch( "p_ndof", &p_ndof );
    m_el_tree->Branch( "p_SharedMuonTrack", &p_SharedMuonTrack );
    
    // Probe/background truth
    m_el_tree->Branch( "p_TruthType", &p_TruthType ); 
    m_el_tree->Branch( "p_TruthOrigin", &p_TruthOrigin ); 
    m_el_tree->Branch( "p_truth_matched", &p_truth_matched ); 
    m_el_tree->Branch( "p_truth_pt", &p_truth_pt ); 
    m_el_tree->Branch( "p_truth_phi", &p_truth_phi ); 
    m_el_tree->Branch( "p_truth_eta", &p_truth_eta ); 
    m_el_tree->Branch( "p_truth_E", &p_truth_E ); 
    m_el_tree->Branch( "p_truth_pdgId", &p_truth_pdgId ); 
    m_el_tree->Branch( "p_truth_parent_pdgId", &p_truth_parent_pdgId ); 
    m_el_tree->Branch( "p_firstEgMotherTruthType", &p_firstEgMotherTruthType ); 
    m_el_tree->Branch( "p_firstEgMotherTruthOrigin", &p_firstEgMotherTruthOrigin ); 
    m_el_tree->Branch( "p_firstEgMotherPdgId", &p_firstEgMotherPdgId );     
    m_el_tree->Branch( "Truth", &Truth);
    m_el_tree->Branch( "TruthSelection", &TruthSelection);
    m_el_tree->Branch( "p_iffTruth", &p_iffTruth);

    
    // Probe/background: ALL aux variables for electrons/photons (some are central electron only)
    m_el_tree->Branch( "p_E7x7_Lr2", &pX_E7x7_Lr2 );
    m_el_tree->Branch( "p_E7x7_Lr3", &pX_E7x7_Lr3 );
    m_el_tree->Branch( "p_E_Lr0_HiG", &pX_E_Lr0_HiG );
    m_el_tree->Branch( "p_E_Lr0_LowG", &pX_E_Lr0_LowG );
    m_el_tree->Branch( "p_E_Lr0_MedG", &pX_E_Lr0_MedG );
    m_el_tree->Branch( "p_E_Lr1_HiG", &pX_E_Lr1_HiG );
    m_el_tree->Branch( "p_E_Lr1_LowG", &pX_E_Lr1_LowG );
    m_el_tree->Branch( "p_E_Lr1_MedG", &pX_E_Lr1_MedG );
    m_el_tree->Branch( "p_E_Lr2_HiG", &pX_E_Lr2_HiG );
    m_el_tree->Branch( "p_E_Lr2_LowG", &pX_E_Lr2_LowG );
    m_el_tree->Branch( "p_E_Lr2_MedG", &pX_E_Lr2_MedG );
    m_el_tree->Branch( "p_E_Lr3_HiG", &pX_E_Lr3_HiG );
    m_el_tree->Branch( "p_E_Lr3_LowG", &pX_E_Lr3_LowG );
    m_el_tree->Branch( "p_E_Lr3_MedG", &pX_E_Lr3_MedG );
    m_el_tree->Branch( "p_LHLoose", &pX_LHLoose );
    m_el_tree->Branch( "p_LHMedium", &pX_LHMedium );
    m_el_tree->Branch( "p_LHTight", &pX_LHTight );
    m_el_tree->Branch( "p_Loose", &pX_Loose );
    m_el_tree->Branch( "p_m", &pX_m );
    m_el_tree->Branch( "p_Medium", &pX_Medium );
    m_el_tree->Branch( "p_MultiLepton", &pX_MultiLepton );
    m_el_tree->Branch( "p_OQ", &pX_OQ );
    m_el_tree->Branch( "p_Tight", &pX_Tight );
    m_el_tree->Branch( "p_ambiguityType", &pX_ambiguityType );
    m_el_tree->Branch( "p_asy1", &pX_asy1 );
    m_el_tree->Branch( "p_author", &pX_author );
    m_el_tree->Branch( "p_barys1", &pX_barys1 );
    m_el_tree->Branch( "p_core57cellsEnergyCorrection", &pX_core57cellsEnergyCorrection );
    m_el_tree->Branch( "p_deltaEta0", &pX_deltaEta0 );
    m_el_tree->Branch( "p_deltaEta2", &pX_deltaEta2 );
    m_el_tree->Branch( "p_deltaEta3", &pX_deltaEta3 );
    m_el_tree->Branch( "p_deltaPhi0", &pX_deltaPhi0 );
    m_el_tree->Branch( "p_deltaPhi1", &pX_deltaPhi1 );
    m_el_tree->Branch( "p_deltaPhi2", &pX_deltaPhi2 );
    m_el_tree->Branch( "p_deltaPhi3", &pX_deltaPhi3 );
    m_el_tree->Branch( "p_deltaPhiFromLastMeasurement", &pX_deltaPhiFromLastMeasurement );
    m_el_tree->Branch( "p_deltaPhiRescaled0", &pX_deltaPhiRescaled0 );
    m_el_tree->Branch( "p_deltaPhiRescaled1", &pX_deltaPhiRescaled1 );
    m_el_tree->Branch( "p_deltaPhiRescaled3", &pX_deltaPhiRescaled3 );
    m_el_tree->Branch( "p_e1152", &pX_e1152 );
    m_el_tree->Branch( "p_e132", &pX_e132 );
    m_el_tree->Branch( "p_e235", &pX_e235 );
    m_el_tree->Branch( "p_e255", &pX_e255 );
    m_el_tree->Branch( "p_e2ts1", &pX_e2ts1 );
    m_el_tree->Branch( "p_ecore", &pX_ecore );
    m_el_tree->Branch( "p_emins1", &pX_emins1 );
    m_el_tree->Branch( "p_etconeCorrBitset", &pX_etconeCorrBitset );
    m_el_tree->Branch( "p_ethad", &pX_ethad );
    m_el_tree->Branch( "p_ethad1", &pX_ethad1 );
    m_el_tree->Branch( "p_f1core", &pX_f1core );
    m_el_tree->Branch( "p_f3core", &pX_f3core );
    m_el_tree->Branch( "p_maxEcell_energy", &pX_maxEcell_energy );
    m_el_tree->Branch( "p_maxEcell_gain", &pX_maxEcell_gain );
    m_el_tree->Branch( "p_maxEcell_time", &pX_maxEcell_time );
    m_el_tree->Branch( "p_maxEcell_x", &pX_maxEcell_x );
    m_el_tree->Branch( "p_maxEcell_y", &pX_maxEcell_y );
    m_el_tree->Branch( "p_maxEcell_z", &pX_maxEcell_z );
    m_el_tree->Branch( "p_nCells_Lr0_HiG", &pX_nCells_Lr0_HiG );
    m_el_tree->Branch( "p_nCells_Lr0_LowG", &pX_nCells_Lr0_LowG );
    m_el_tree->Branch( "p_nCells_Lr0_MedG", &pX_nCells_Lr0_MedG );
    m_el_tree->Branch( "p_nCells_Lr1_HiG", &pX_nCells_Lr1_HiG );
    m_el_tree->Branch( "p_nCells_Lr1_LowG", &pX_nCells_Lr1_LowG );
    m_el_tree->Branch( "p_nCells_Lr1_MedG", &pX_nCells_Lr1_MedG );
    m_el_tree->Branch( "p_nCells_Lr2_HiG", &pX_nCells_Lr2_HiG );
    m_el_tree->Branch( "p_nCells_Lr2_LowG", &pX_nCells_Lr2_LowG );
    m_el_tree->Branch( "p_nCells_Lr2_MedG", &pX_nCells_Lr2_MedG );
    m_el_tree->Branch( "p_nCells_Lr3_HiG", &pX_nCells_Lr3_HiG );
    m_el_tree->Branch( "p_nCells_Lr3_LowG", &pX_nCells_Lr3_LowG );
    m_el_tree->Branch( "p_nCells_Lr3_MedG", &pX_nCells_Lr3_MedG );
    m_el_tree->Branch( "p_neflowisol20", &pX_neflowisol20 );
    m_el_tree->Branch( "p_neflowisol20ptCorrection", &pX_neflowisol20ptCorrection );
    m_el_tree->Branch( "p_neflowisol30", &pX_neflowisol30 );
    m_el_tree->Branch( "p_neflowisol30ptCorrection", &pX_neflowisol30ptCorrection );
    m_el_tree->Branch( "p_neflowisol40", &pX_neflowisol40 );
    m_el_tree->Branch( "p_neflowisol40ptCorrection", &pX_neflowisol40ptCorrection );
    m_el_tree->Branch( "p_neflowisolCorrBitset", &pX_neflowisolCorrBitset );
    m_el_tree->Branch( "p_neflowisolcoreConeEnergyCorrection", &pX_neflowisolcoreConeEnergyCorrection );
    m_el_tree->Branch( "p_pos", &pX_pos );
    m_el_tree->Branch( "p_pos7", &pX_pos7 );
    m_el_tree->Branch( "p_poscs1", &pX_poscs1 );
    m_el_tree->Branch( "p_poscs2", &pX_poscs2 );
    m_el_tree->Branch( "p_ptconeCorrBitset", &pX_ptconeCorrBitset );
    m_el_tree->Branch( "p_ptconecoreTrackPtrCorrection", &pX_ptconecoreTrackPtrCorrection );
    m_el_tree->Branch( "p_r33over37allcalo", &pX_r33over37allcalo );
    m_el_tree->Branch( "p_topoetconeCorrBitset", &pX_topoetconeCorrBitset );
    m_el_tree->Branch( "p_topoetconecoreConeEnergyCorrection", &pX_topoetconecoreConeEnergyCorrection );
    m_el_tree->Branch( "p_topoetconecoreConeSCEnergyCorrection", &pX_topoetconecoreConeSCEnergyCorrection );
    m_el_tree->Branch( "p_weta1", &pX_weta1 );
    m_el_tree->Branch( "p_widths1", &pX_widths1 );
    m_el_tree->Branch( "p_widths2", &pX_widths2 );
    m_el_tree->Branch( "p_wtots1", &pX_wtots1 );
    m_el_tree->Branch( "p_e233", &pX_e233 );
    m_el_tree->Branch( "p_e237", &pX_e237 );
    m_el_tree->Branch( "p_e277", &pX_e277 );
    m_el_tree->Branch( "p_e2tsts1", &pX_e2tsts1 );
    m_el_tree->Branch( "p_ehad1", &pX_ehad1 );
    m_el_tree->Branch( "p_emaxs1", &pX_emaxs1 );
    m_el_tree->Branch( "p_fracs1", &pX_fracs1 );
    m_el_tree->Branch( "p_DeltaE", &pX_DeltaE );
    m_el_tree->Branch( "p_E3x5_Lr0", &pX_E3x5_Lr0 );
    m_el_tree->Branch( "p_E3x5_Lr1", &pX_E3x5_Lr1 );
    m_el_tree->Branch( "p_E3x5_Lr2", &pX_E3x5_Lr2 );
    m_el_tree->Branch( "p_E3x5_Lr3", &pX_E3x5_Lr3 );
    m_el_tree->Branch( "p_E5x7_Lr0", &pX_E5x7_Lr0 );
    m_el_tree->Branch( "p_E5x7_Lr1", &pX_E5x7_Lr1 );
    m_el_tree->Branch( "p_E5x7_Lr2", &pX_E5x7_Lr2 );
    m_el_tree->Branch( "p_E5x7_Lr3", &pX_E5x7_Lr3 );
    m_el_tree->Branch( "p_E7x11_Lr0", &pX_E7x11_Lr0 );
    m_el_tree->Branch( "p_E7x11_Lr1", &pX_E7x11_Lr1 );
    m_el_tree->Branch( "p_E7x11_Lr2", &pX_E7x11_Lr2 );
    m_el_tree->Branch( "p_E7x11_Lr3", &pX_E7x11_Lr3 );
    m_el_tree->Branch( "p_E7x7_Lr0", &pX_E7x7_Lr0 );
    m_el_tree->Branch( "p_E7x7_Lr1", &pX_E7x7_Lr1 );
    
    m_el_tree->Branch( "pdf_cut_vars_score", &pdf_cut_vars_score);
    m_el_tree->Branch( "pdf_vars_score", &pdf_vars_score);
    m_el_tree->Branch( "Iso_score", &Iso_score);
    m_el_tree->Branch( "IsoCalo_score", &IsoCalo_score);
    m_el_tree->Branch( "IsoTrack_score", &IsoTrack_score);

    // Photon tree

    // Misc.
    m_ga_tree->Branch( "samplePath", &m_samplePath );
    
    m_ga_tree->Branch( "em_barrel_Lr0", &em_barrel_Lr0);
    m_ga_tree->Branch( "em_barrel_Lr1", &em_barrel_Lr1);
    m_ga_tree->Branch( "em_barrel_Lr2", &em_barrel_Lr2);
    m_ga_tree->Branch( "em_barrel_Lr3", &em_barrel_Lr3);
    m_ga_tree->Branch( "em_endcap_Lr0", &em_endcap_Lr0);
    m_ga_tree->Branch( "em_endcap_Lr1", &em_endcap_Lr1);
    m_ga_tree->Branch( "em_endcap_Lr2", &em_endcap_Lr2);
    m_ga_tree->Branch( "em_endcap_Lr3", &em_endcap_Lr3);
    m_ga_tree->Branch( "lar_endcap_Lr0", &lar_endcap_Lr0);
    m_ga_tree->Branch( "lar_endcap_Lr1", &lar_endcap_Lr1);
    m_ga_tree->Branch( "lar_endcap_Lr2", &lar_endcap_Lr2);
    m_ga_tree->Branch( "lar_endcap_Lr3", &lar_endcap_Lr3);
    m_ga_tree->Branch( "tile_barrel_Lr1", &tile_barrel_Lr1);
    m_ga_tree->Branch( "tile_barrel_Lr2", &tile_barrel_Lr2);
    m_ga_tree->Branch( "tile_barrel_Lr3", &tile_barrel_Lr3);
    m_ga_tree->Branch( "tile_gap_Lr1", &tile_gap_Lr1);
    
    m_ga_tree->Branch( "time_em_barrel_Lr0", &time_em_barrel_Lr0);
    m_ga_tree->Branch( "time_em_barrel_Lr1", &time_em_barrel_Lr1);
    m_ga_tree->Branch( "time_em_barrel_Lr2", &time_em_barrel_Lr2);
    m_ga_tree->Branch( "time_em_barrel_Lr3", &time_em_barrel_Lr3);
    m_ga_tree->Branch( "time_em_endcap_Lr0", &time_em_endcap_Lr0);
    m_ga_tree->Branch( "time_em_endcap_Lr1", &time_em_endcap_Lr1);
    m_ga_tree->Branch( "time_em_endcap_Lr2", &time_em_endcap_Lr2);
    m_ga_tree->Branch( "time_em_endcap_Lr3", &time_em_endcap_Lr3);
    m_ga_tree->Branch( "time_lar_endcap_Lr0", &time_lar_endcap_Lr0);
    m_ga_tree->Branch( "time_lar_endcap_Lr1", &time_lar_endcap_Lr1);
    m_ga_tree->Branch( "time_lar_endcap_Lr2", &time_lar_endcap_Lr2);
    m_ga_tree->Branch( "time_lar_endcap_Lr3", &time_lar_endcap_Lr3);
    m_ga_tree->Branch( "time_tile_barrel_Lr1", &time_tile_barrel_Lr1);
    m_ga_tree->Branch( "time_tile_barrel_Lr2", &time_tile_barrel_Lr2);
    m_ga_tree->Branch( "time_tile_barrel_Lr3", &time_tile_barrel_Lr3);
    m_ga_tree->Branch( "time_tile_gap_Lr1", &time_tile_gap_Lr1);
    
    // Event info
    m_ga_tree->Branch( "eventNumber", &event_eventNumber );
    m_ga_tree->Branch( "mcChannelNumber", &event_mcChannelNumber );
    m_ga_tree->Branch( "runNumber", &event_runNumber );
    m_ga_tree->Branch( "actualInteractionsPerCrossing", &event_actualInteractionsPerCrossing );
    m_ga_tree->Branch( "averageInteractionsPerCrossing", &event_averageInteractionsPerCrossing );
    
    m_ga_tree->Branch( "correctedActualMu", &event_correctedActualMu );
    m_ga_tree->Branch( "correctedAverageMu", &event_correctedAverageMu );
    m_ga_tree->Branch( "correctedScaledActualMu", &event_correctedScaledActualMu );
    m_ga_tree->Branch( "correctedScaledAverageMu", &event_correctedScaledAverageMu );
    m_ga_tree->Branch( "PileupWeight", &event_pileupweight);
    
    // Information about event
    m_ga_tree->Branch( "isMC", &event_isMC );
    m_ga_tree->Branch( "analysisType", &m_analysisType );
    m_ga_tree->Branch( "NvtxReco", &event_NvtxReco );
    m_ga_tree->Branch( "settype", &settype);
    
    // Inner detector tracks R=0.4 of probe ( excl. probe and tag )
    m_ga_tree->Branch( "tracks_pt", &tracks_pt );
    m_ga_tree->Branch( "tracks_eta", &tracks_eta );
    m_ga_tree->Branch( "tracks_phi", &tracks_phi );
    m_ga_tree->Branch( "tracks_charge", &tracks_charge );
    m_ga_tree->Branch( "tracks_chi2", &tracks_chi2 );
    m_ga_tree->Branch( "tracks_ndof", &tracks_ndof );
    m_ga_tree->Branch( "tracks_pixhits", &tracks_pixhits );
    m_ga_tree->Branch( "tracks_scthits", &tracks_scthits );
    m_ga_tree->Branch( "tracks_trthits", &tracks_trthits );
    m_ga_tree->Branch( "tracks_vertex", &tracks_vertex );
    m_ga_tree->Branch( "tracks_z0", &tracks_z0 );
    m_ga_tree->Branch( "tracks_d0", &tracks_d0 );
    m_ga_tree->Branch( "tracks_dR", &tracks_dR );
    m_ga_tree->Branch( "tracks_theta", &tracks_theta );
    m_ga_tree->Branch( "tracks_sigmad0", &tracks_sigmad0 );
    
    // Jet information (specifically b-jets)
    m_ga_tree->Branch( "jets_highestBtagMV2c10", &jets_highestBtagMV2c10 );
    m_ga_tree->Branch( "jets_highestBtagDL1_pb", &jets_highestBtagDL1_pb );
    m_ga_tree->Branch( "jets_highestBtagDL1_pc", &jets_highestBtagDL1_pc );
    m_ga_tree->Branch( "jets_highestBtagDL1_pu", &jets_highestBtagDL1_pu );
    
    // Bunch crossing information
    m_ga_tree->Branch( "BC_distanceFromFront", &BC_distanceFromFront );
    m_ga_tree->Branch( "BC_filledBunches", &BC_filledBunches );
    
    // Missing transverse energy (MET)
    m_ga_tree->Branch( "met_met", &met_met );
    m_ga_tree->Branch( "met_phi", &met_phi );
    
    // Probe/background misc.
    m_ga_tree->Branch( "p_type", &p_type );
    m_ga_tree->Branch( "p_vertexIndex", &p_vertexIndex );
    m_ga_tree->Branch( "p_photonIsTightEM", &p_photonIsTightEM );
    m_ga_tree->Branch( "p_photonIsLooseEM", &p_photonIsLooseEM );
    
    
    // Probe/background kinematics
    m_ga_tree->Branch( "p_e", &p_e );
    m_ga_tree->Branch( "p_calibratedE", &p_calibratedE );
    m_ga_tree->Branch( "p_et_calo", &p_et_calo );
    m_ga_tree->Branch( "p_eta", &p_eta );
    m_ga_tree->Branch( "p_phi", &p_phi );
    
    
    // Probe/background cluster info
    m_ga_tree->Branch( "p_etaCluster", &p_etaCluster );
    m_ga_tree->Branch( "p_phiCluster", &p_phiCluster );
    m_ga_tree->Branch( "p_eCluster", &p_eCluster );
    m_ga_tree->Branch( "p_etCluster", &p_etCluster );
    m_ga_tree->Branch( "p_rawEtaCluster", &p_RawEtaCluster );
    m_ga_tree->Branch( "p_rawPhiCluster", &p_RawPhiCluster );
    m_ga_tree->Branch( "p_rawECluster", &p_RawECluster );
    m_ga_tree->Branch( "p_eClusterLr0", &p_eClusterLr0 );
    m_ga_tree->Branch( "p_eClusterLr1", &p_eClusterLr1 );
    m_ga_tree->Branch( "p_eClusterLr2", &p_eClusterLr2 );
    m_ga_tree->Branch( "p_eClusterLr3", &p_eClusterLr3 );
    m_ga_tree->Branch( "p_etaClusterLr1", &p_etaClusterLr1 );
    m_ga_tree->Branch( "p_etaClusterLr2", &p_etaClusterLr2 );
    m_ga_tree->Branch( "p_phiClusterLr2", &p_phiClusterLr2 );
    m_ga_tree->Branch( "p_eAccCluster", &p_eAccCluster );
    m_ga_tree->Branch( "p_f0Cluster", &p_f0Cluster );
    m_ga_tree->Branch( "p_etaCalo", &p_etaCalo );
    m_ga_tree->Branch( "p_phiCalo", &p_phiCalo );
    m_ga_tree->Branch( "p_eTileGap3Cluster", &p_eTileGap3Cluster );
    m_ga_tree->Branch( "p_cellIndexCluster", &p_cellIndexCluster );
    m_ga_tree->Branch( "p_phiModCalo", &p_phiModCalo );
    m_ga_tree->Branch( "p_etaModCalo", &p_etaModCalo );
    m_ga_tree->Branch( "p_dPhiTH3", &p_dPhiTH3 );
    m_ga_tree->Branch( "p_R12", &p_R12 );
    m_ga_tree->Branch( "p_fTG3", &p_fTG3 );
    
    // Probe/background converted photons info
    m_ga_tree->Branch( "p_photonConversionType", &p_photonConversionType );
    m_ga_tree->Branch( "p_photonConversionRadius", &p_photonConversionRadius );
    m_ga_tree->Branch( "p_photonVertexPtConvDecor", &p_photonVertexPtConvDecor );
    m_ga_tree->Branch( "p_photonVertexPtConv", &p_photonVertexPtConv );
    m_ga_tree->Branch( "p_photonVertexPt1", &p_photonVertexPt1 );
    m_ga_tree->Branch( "p_photonVertexPt2", &p_photonVertexPt2 );
    m_ga_tree->Branch( "p_photonVertexConvPtRatio", &p_photonVertexConvPtRatio );
    m_ga_tree->Branch( "p_photonVertexConvEtOverPt", &p_photonVertexConvEtOverPt );
    m_ga_tree->Branch( "p_photonVertexRconv", &p_photonVertexRconv );
    m_ga_tree->Branch( "p_photonVertexzconv", &p_photonVertexzconv );
    m_ga_tree->Branch( "p_photonVertexPixHits1", &p_photonVertexPixHits1 );
    m_ga_tree->Branch( "p_photonVertexSCTHits1", &p_photonVertexSCTHits1 );
    m_ga_tree->Branch( "p_photonVertexPixHits2", &p_photonVertexPixHits2 );
    m_ga_tree->Branch( "p_photonVertexSCTHits2", &p_photonVertexSCTHits2 );
    
    // Probe/background calculated calorimeter values
    m_ga_tree->Branch( "p_weta2", &p_weta2 );
    m_ga_tree->Branch( "p_Reta", &p_Reta );
    m_ga_tree->Branch( "p_Rphi", &p_Rphi );
    m_ga_tree->Branch( "p_Eratio", &p_Eratio );
    m_ga_tree->Branch( "p_f1", &p_f1 );
    m_ga_tree->Branch( "p_f3", &p_f3 );
    m_ga_tree->Branch( "p_Rhad", &p_Rhad );
    m_ga_tree->Branch( "p_Rhad1", &p_Rhad1 );
    
    // Probe+MET transverse mass (W hypothesis)
    m_ga_tree->Branch( "p_mTransW", &p_mTransW );
    
    // Probe/background isolation
    m_ga_tree->Branch( "p_topoetcone20", &p_topoetcone20 );
    m_ga_tree->Branch( "p_topoetcone30", &p_topoetcone30 );
    m_ga_tree->Branch( "p_topoetcone40", &p_topoetcone40 );
    m_ga_tree->Branch( "p_etcone20", &p_etcone20 );
    m_ga_tree->Branch( "p_etcone30", &p_etcone30 );
    m_ga_tree->Branch( "p_etcone40", &p_etcone40 );
    m_ga_tree->Branch( "p_etcone20ptCorrection", &p_etcone20ptCorrection );
    m_ga_tree->Branch( "p_etcone30ptCorrection", &p_etcone30ptCorrection );
    m_ga_tree->Branch( "p_etcone40ptCorrection", &p_etcone40ptCorrection );
    m_ga_tree->Branch( "p_ptcone20", &p_ptcone20 );
    m_ga_tree->Branch( "p_ptcone30", &p_ptcone30 );
    m_ga_tree->Branch( "p_ptcone40", &p_ptcone40 );
    m_ga_tree->Branch( "p_ptvarcone20", &p_ptvarcone20 );
    m_ga_tree->Branch( "p_ptvarcone30", &p_ptvarcone30 );
    m_ga_tree->Branch( "p_ptvarcone40", &p_ptvarcone40 );
    m_ga_tree->Branch( "p_topoetcone20ptCorrection", &p_topoetcone20ptCorrection );
    m_ga_tree->Branch( "p_topoetcone30ptCorrection", &p_topoetcone30ptCorrection );
    m_ga_tree->Branch( "p_topoetcone40ptCorrection", &p_topoetcone40ptCorrection );
    m_ga_tree->Branch( "p_ptPU10", &p_ptPU10 );
    m_ga_tree->Branch( "p_ptPU20", &p_ptPU20 );
    m_ga_tree->Branch( "p_ptPU30", &p_ptPU30 );
    m_ga_tree->Branch( "p_ptPU40", &p_ptPU40 );
    
    
    // Probe/background truth
    m_ga_tree->Branch( "p_TruthType", &p_TruthType ); 
    m_ga_tree->Branch( "p_TruthOrigin", &p_TruthOrigin ); 
    m_ga_tree->Branch( "p_truth_matched", &p_truth_matched ); 
    m_ga_tree->Branch( "p_truth_pt", &p_truth_pt ); 
    m_ga_tree->Branch( "p_truth_phi", &p_truth_phi ); 
    m_ga_tree->Branch( "p_truth_eta", &p_truth_eta ); 
    m_ga_tree->Branch( "p_truth_E", &p_truth_E ); 
    m_ga_tree->Branch( "p_truth_pdgId", &p_truth_pdgId ); 
    m_ga_tree->Branch( "p_truth_parent_pdgId", &p_truth_parent_pdgId ); 
    m_ga_tree->Branch( "p_trueConv", &p_trueConv );
    m_ga_tree->Branch( "p_firstEgMotherTruthType", &p_firstEgMotherTruthType ); 
    m_ga_tree->Branch( "p_firstEgMotherTruthOrigin", &p_firstEgMotherTruthOrigin ); 
    m_ga_tree->Branch( "p_firstEgMotherPdgId", &p_firstEgMotherPdgId );         
    m_ga_tree->Branch( "Truth", &Truth);
    
    // Probe/background: ALL aux variables for electrons/photons (some are central electron only)
    m_ga_tree->Branch( "p_E7x7_Lr2", &pX_E7x7_Lr2 );
    m_ga_tree->Branch( "p_E7x7_Lr3", &pX_E7x7_Lr3 );
    m_ga_tree->Branch( "p_E_Lr0_HiG", &pX_E_Lr0_HiG );
    m_ga_tree->Branch( "p_E_Lr0_LowG", &pX_E_Lr0_LowG );
    m_ga_tree->Branch( "p_E_Lr0_MedG", &pX_E_Lr0_MedG );
    m_ga_tree->Branch( "p_E_Lr1_HiG", &pX_E_Lr1_HiG );
    m_ga_tree->Branch( "p_E_Lr1_LowG", &pX_E_Lr1_LowG );
    m_ga_tree->Branch( "p_E_Lr1_MedG", &pX_E_Lr1_MedG );
    m_ga_tree->Branch( "p_E_Lr2_HiG", &pX_E_Lr2_HiG );
    m_ga_tree->Branch( "p_E_Lr2_LowG", &pX_E_Lr2_LowG );
    m_ga_tree->Branch( "p_E_Lr2_MedG", &pX_E_Lr2_MedG );
    m_ga_tree->Branch( "p_E_Lr3_HiG", &pX_E_Lr3_HiG );
    m_ga_tree->Branch( "p_E_Lr3_LowG", &pX_E_Lr3_LowG );
    m_ga_tree->Branch( "p_E_Lr3_MedG", &pX_E_Lr3_MedG );
    m_ga_tree->Branch( "p_Loose", &pX_Loose );
    m_ga_tree->Branch( "p_m", &pX_m );
    m_ga_tree->Branch( "p_Medium", &pX_Medium );
    m_ga_tree->Branch( "p_MultiLepton", &pX_MultiLepton );
    m_ga_tree->Branch( "p_OQ", &pX_OQ );
    m_ga_tree->Branch( "p_Tight", &pX_Tight );
    m_ga_tree->Branch( "p_ambiguityType", &pX_ambiguityType );
    m_ga_tree->Branch( "p_asy1", &pX_asy1 );
    m_ga_tree->Branch( "p_author", &pX_author );
    m_ga_tree->Branch( "p_barys1", &pX_barys1 );
    m_ga_tree->Branch( "p_core57cellsEnergyCorrection", &pX_core57cellsEnergyCorrection );
    m_ga_tree->Branch( "p_e1152", &pX_e1152 );
    m_ga_tree->Branch( "p_e132", &pX_e132 );
    m_ga_tree->Branch( "p_e235", &pX_e235 );
    m_ga_tree->Branch( "p_e255", &pX_e255 );
    m_ga_tree->Branch( "p_e2ts1", &pX_e2ts1 );
    m_ga_tree->Branch( "p_ecore", &pX_ecore );
    m_ga_tree->Branch( "p_emins1", &pX_emins1 );
    m_ga_tree->Branch( "p_etconeCorrBitset", &pX_etconeCorrBitset );
    m_ga_tree->Branch( "p_ethad", &pX_ethad );
    m_ga_tree->Branch( "p_ethad1", &pX_ethad1 );
    m_ga_tree->Branch( "p_f1core", &pX_f1core );
    m_ga_tree->Branch( "p_f3core", &pX_f3core );
    m_ga_tree->Branch( "p_maxEcell_energy", &pX_maxEcell_energy );
    m_ga_tree->Branch( "p_maxEcell_gain", &pX_maxEcell_gain );
    m_ga_tree->Branch( "p_maxEcell_time", &pX_maxEcell_time );
    m_ga_tree->Branch( "p_maxEcell_x", &pX_maxEcell_x );
    m_ga_tree->Branch( "p_maxEcell_y", &pX_maxEcell_y );
    m_ga_tree->Branch( "p_maxEcell_z", &pX_maxEcell_z );
    m_ga_tree->Branch( "p_nCells_Lr0_HiG", &pX_nCells_Lr0_HiG );
    m_ga_tree->Branch( "p_nCells_Lr0_LowG", &pX_nCells_Lr0_LowG );
    m_ga_tree->Branch( "p_nCells_Lr0_MedG", &pX_nCells_Lr0_MedG );
    m_ga_tree->Branch( "p_nCells_Lr1_HiG", &pX_nCells_Lr1_HiG );
    m_ga_tree->Branch( "p_nCells_Lr1_LowG", &pX_nCells_Lr1_LowG );
    m_ga_tree->Branch( "p_nCells_Lr1_MedG", &pX_nCells_Lr1_MedG );
    m_ga_tree->Branch( "p_nCells_Lr2_HiG", &pX_nCells_Lr2_HiG );
    m_ga_tree->Branch( "p_nCells_Lr2_LowG", &pX_nCells_Lr2_LowG );
    m_ga_tree->Branch( "p_nCells_Lr2_MedG", &pX_nCells_Lr2_MedG );
    m_ga_tree->Branch( "p_nCells_Lr3_HiG", &pX_nCells_Lr3_HiG );
    m_ga_tree->Branch( "p_nCells_Lr3_LowG", &pX_nCells_Lr3_LowG );
    m_ga_tree->Branch( "p_nCells_Lr3_MedG", &pX_nCells_Lr3_MedG );
    m_ga_tree->Branch( "p_neflowisol20", &pX_neflowisol20 );
    m_ga_tree->Branch( "p_neflowisol20ptCorrection", &pX_neflowisol20ptCorrection );
    m_ga_tree->Branch( "p_neflowisol30", &pX_neflowisol30 );
    m_ga_tree->Branch( "p_neflowisol30ptCorrection", &pX_neflowisol30ptCorrection );
    m_ga_tree->Branch( "p_neflowisol40", &pX_neflowisol40 );
    m_ga_tree->Branch( "p_neflowisol40ptCorrection", &pX_neflowisol40ptCorrection );
    m_ga_tree->Branch( "p_neflowisolCorrBitset", &pX_neflowisolCorrBitset );
    m_ga_tree->Branch( "p_neflowisolcoreConeEnergyCorrection", &pX_neflowisolcoreConeEnergyCorrection );
    m_ga_tree->Branch( "p_pos", &pX_pos );
    m_ga_tree->Branch( "p_pos7", &pX_pos7 );
    m_ga_tree->Branch( "p_poscs1", &pX_poscs1 );
    m_ga_tree->Branch( "p_poscs2", &pX_poscs2 );
    m_ga_tree->Branch( "p_ptconeCorrBitset", &pX_ptconeCorrBitset );
    m_ga_tree->Branch( "p_ptconecoreTrackPtrCorrection", &pX_ptconecoreTrackPtrCorrection );
    m_ga_tree->Branch( "p_r33over37allcalo", &pX_r33over37allcalo );
    m_ga_tree->Branch( "p_topoetconeCorrBitset", &pX_topoetconeCorrBitset );
    m_ga_tree->Branch( "p_topoetconecoreConeEnergyCorrection", &pX_topoetconecoreConeEnergyCorrection );
    m_ga_tree->Branch( "p_topoetconecoreConeSCEnergyCorrection", &pX_topoetconecoreConeSCEnergyCorrection );
    m_ga_tree->Branch( "p_weta1", &pX_weta1 );
    m_ga_tree->Branch( "p_widths1", &pX_widths1 );
    m_ga_tree->Branch( "p_widths2", &pX_widths2 );
    m_ga_tree->Branch( "p_wtots1", &pX_wtots1 );
    m_ga_tree->Branch( "p_e233", &pX_e233 );
    m_ga_tree->Branch( "p_e237", &pX_e237 );
    m_ga_tree->Branch( "p_e277", &pX_e277 );
    m_ga_tree->Branch( "p_e2tsts1", &pX_e2tsts1 );
    m_ga_tree->Branch( "p_ehad1", &pX_ehad1 );
    m_ga_tree->Branch( "p_emaxs1", &pX_emaxs1 );
    m_ga_tree->Branch( "p_fracs1", &pX_fracs1 );
    m_ga_tree->Branch( "p_DeltaE", &pX_DeltaE );
    m_ga_tree->Branch( "p_E3x5_Lr0", &pX_E3x5_Lr0 );
    m_ga_tree->Branch( "p_E3x5_Lr1", &pX_E3x5_Lr1 );
    m_ga_tree->Branch( "p_E3x5_Lr2", &pX_E3x5_Lr2 );
    m_ga_tree->Branch( "p_E3x5_Lr3", &pX_E3x5_Lr3 );
    m_ga_tree->Branch( "p_E5x7_Lr0", &pX_E5x7_Lr0 );
    m_ga_tree->Branch( "p_E5x7_Lr1", &pX_E5x7_Lr1 );
    m_ga_tree->Branch( "p_E5x7_Lr2", &pX_E5x7_Lr2 );
    m_ga_tree->Branch( "p_E5x7_Lr3", &pX_E5x7_Lr3 );
    m_ga_tree->Branch( "p_E7x11_Lr0", &pX_E7x11_Lr0 );
    m_ga_tree->Branch( "p_E7x11_Lr1", &pX_E7x11_Lr1 );
    m_ga_tree->Branch( "p_E7x11_Lr2", &pX_E7x11_Lr2 );
    m_ga_tree->Branch( "p_E7x11_Lr3", &pX_E7x11_Lr3 );
    m_ga_tree->Branch( "p_E7x7_Lr0", &pX_E7x7_Lr0 );
    m_ga_tree->Branch( "p_E7x7_Lr1", &pX_E7x7_Lr1 );
    
    // Photons only
    m_ga_tree->Branch( "p_convMatchDeltaEta1", &pPX_convMatchDeltaEta1 );
    m_ga_tree->Branch( "p_convMatchDeltaEta2", &pPX_convMatchDeltaEta2 );
    m_ga_tree->Branch( "p_convMatchDeltaPhi1", &pPX_convMatchDeltaPhi1 );
    m_ga_tree->Branch( "p_convMatchDeltaPhi2", &pPX_convMatchDeltaPhi2 );


    // initialize LGBM predictors
    m_lgbm_pdfcutvars = new LightGBMpredictor();
    m_lgbm_pdfcutvars->initialize("500_binary_auc_eval_0_TruthType_results_pdf_cut_vars_19.txt");
    m_lgbm_pdfvars = new LightGBMpredictor();
    m_lgbm_pdfvars->initialize("500_binary_auc_eval_0_TruthType_results_pdf_vars_14.txt");
    m_lgbm_iso = new LightGBMpredictor();
    m_lgbm_iso->initialize("500_binary_auc_eval_0_TruthType_results_Iso_16.txt");
    m_lgbm_isocalo = new LightGBMpredictor();
    m_lgbm_isocalo->initialize("500_binary_auc_eval_0_TruthType_results_IsoCalo_10.txt");
    m_lgbm_isotrack = new LightGBMpredictor();
    m_lgbm_isotrack->initialize("500_binary_auc_eval_0_TruthType_results_IsoTrack_6.txt");         


    // setup all the histograms that will be filled 
    histadd("Zmass", 100, 50, 150, &ll_m);
    histadd("Zlifetime", 100, -10, 10, &ll_lifetime);
    histadd("Jpsimass", 100, 1.5, 4.5, &ll_m);
    histadd("Jpsilifetime", 100, -10, 10, &ll_lifetime);   
    histadd("probeEt", 100, 4.5, 150, &p_et_calo);
    histadd("tagEt", 100, 4.5, 150, &tag_et_calo);
    histadd("etMiss", 100, 0.0, 100, &met_met);
    histadd("probeMt", 100, 0.0, 100, &p_mTransW);
    histadd("probeEta", 100, -2.47, 2.47, &p_eta);  
    histadd("avgMu", 91, 0, 90, &event_correctedScaledAverageMu);
    histadd("NvtxReco", 51, 0, 50, &event_NvtxReco);
    // histadd("BtagMV2c10", 100, -1, 1, &jets_highestBtagMV2c10); 
    // histadd("Btagpb", 100, -1, 1, &jets_highestBtagDL1_pb); 
    // histadd("Btagpc", 100, -1, 1, &jets_highestBtagDL1_pc); 
    // histadd("Btagpu", 100, -1, 1, &jets_highestBtagDL1_pu); 
    histadd("Ntracks", 12, 0, 12, &p_nTracks); 
    histadd("ptTrack", 100, 4.5, 150, &p_pt_track); 
    histadd("z0", 100, -2, 2, &p_z0); 
    histadd("d0", 100, -1, 1, &p_d0); 
    histadd("sigmad0", 100, 0, 0.5, &p_sigmad0); 
    histadd("d0Sig", 100, -10, 10, &p_d0Sig); 
    histadd("EptRatio", 100, 0, 40, &p_EptRatio); 
    histadd("qOverP", 100, -150, 150, &p_qOverP); 
    histadd("dpOverP", 100, -5, 5, &p_dPOverP); 
    histadd("z0Theta", 100, 0, 1, &p_z0theta); 
    histadd("deltaRtag", 100, 0, 6, &p_deltaR_tag); 
    histadd("etCluster", 100, 4.5, 150, &p_etCluster); 
    histadd("Weta2", 100, 0.0, 0.05, &p_weta2); 
    histadd("Reta", 100, 0, 1, &p_Reta); 
    histadd("Rphi", 100, 0, 1, &p_Rphi); 
    histadd("Eratio", 100, 0, 1, &p_Eratio); 
    histadd("f1", 100, -0.1, 1, &p_f1); 
    histadd("f3", 100, -0.1, 1, &p_f3); 
    histadd("Rhad", 100, -0.2, 2, &p_Rhad); 
    histadd("Rhad1", 100, -0.2, 2, &p_Rhad1); 
    histadd("deltaEta1", 100, -0.2, 0.2, &p_deltaEta1); 
    histadd("deltaPhiRescaled2", 100, -0.2, 0.2, &p_deltaPhiRescaled2); 
    histadd("TRTPID", 100, -1, 2, &p_eProbHT); 
    histadd("LHValue", 100, -5, 3, &p_LHValue); 
    histadd("numberOfInnermostPixelHits", 5, 0, 5, &p_numberOfInnermostPixelHits); 
    histadd("numberOfPixelHits", 10, 0, 10, &p_numberOfPixelHits); 
    histadd("numberOfSCTHits", 20, 0, 20, &p_numberOfSCTHits); 
    histadd("numberOfTRTHits", 50, 0, 50, &p_numberOfTRTHits); 
    histadd("numberOfTRTXenonHits", 50, 0, 50, &p_numberOfTRTXenonHits); 
    histadd("TruthType", 20, 0, 20, &p_TruthType); 
    histadd("TruthOrigin", 50, 0, 50, &p_TruthOrigin); 
    histadd("pdfVars", 100, 0, 1, &pdf_vars_score); 
    histadd("pdfCutVars", 100, 0, 1, &pdf_cut_vars_score); 
    histadd("IsoVars", 100, 0, 1, &Iso_score); 
    histadd("IsoCaloVars", 100, 0, 1, &IsoCalo_score); 
    histadd("IsoTrackVars", 100, 0, 1, &IsoTrack_score); 
    histadd("Truth", 2, 0, 2, &Truth);
    histadd("TruthSelection", 2, 0, 2, &TruthSelection);
    histadd("IFFTruth", 10, 0, 10, &p_iffTruth);
    histadd("ECIDS", 100, -1, 1, &p_ECIDSResult);
    
    
    m_hists.resize(60);
    hists_for_cuts("_sig", 0, true);
    hists_for_cuts("_sig_true_sig", 3, true);
    hists_for_cuts("_sig_true_bkg", 6, true);
    hists_for_cuts("_selection", 9, true);
    hists_for_cuts("_selection_true_sig", 12, true);
    hists_for_cuts("_selection_true_bkg", 15, true);
    hists_for_cuts("_bkg", 18, false);
    hists_for_cuts("_bkg_true_sig", 22, false);
    hists_for_cuts("_bkg_true_bkg", 26, false);
    
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyTagAndProbeAnalysis::fileExecute() {
    // Here you do everything that needs to be done exactly once for every
    // single file, e.g. collect a list of all lumi-blocks processed
    
    
    TString dirname = gSystem->Getenv("UserAnalysis_DIR"); 
    xAOD::TEvent* event = wk()->xaodEvent();
    
    // get the MetaData tree once a new file is opened, with
    TTree *MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
    if (!MetaData) {
        Error("fileExecute()", "MetaData not found! Exiting.");
        return EL::StatusCode::FAILURE;
    }
    
    MetaData->LoadTree(0);
        
    const xAOD::FileMetaData* fmld = new xAOD::FileMetaData();
    float mcProcID = -999;
    if( event->retrieveMetaInput(fmld, "FileMetaData").isSuccess()  ) {
        fmld->value( xAOD::FileMetaData::mcProcID, mcProcID);   
    }

    event_isMC = mcProcID > 1;
    Info( "initialize()", "mcProcID: %f", mcProcID );
    Info( "initialize()", "File is MC: %d", event_isMC);
    
    if ( event_isMC ){
        //check if file is from a DxAOD
        bool m_isDerivation = !MetaData->GetBranch("StreamAOD");
        
        if(m_isDerivation ){
            const xAOD::CutBookkeeperContainer* incompleteCBC = nullptr;
            if(!event->retrieveMetaInput(incompleteCBC, "IncompleteCutBookkeepers").isSuccess()){
                Error("initializeEvent()","Failed to retrieve IncompleteCutBookkeepers from MetaData! Exiting.");
                return EL::StatusCode::FAILURE;
            }
            // if ( incompleteCBC->size() != 0 ) {
            //   Error("initializeEvent()","Found incomplete Bookkeepers! Check file for corruption.");
            //   return EL::StatusCode::FAILURE;
            // }
            // Now, let's find the actual information
            const xAOD::CutBookkeeperContainer* completeCBC = 0;
            if(!event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess()){
                Error("initializeEvent()","Failed to retrieve CutBookkeepers from MetaData! Exiting.");
                return EL::StatusCode::FAILURE;
            }
            
            // First, let's find the smallest cycle number,
            // i.e., the original first processing step/cycle
            int minCycle = 10000;
            for ( auto cbk : *completeCBC ) {
                if ( ! cbk->name().empty()  && minCycle > cbk->cycle() ){ minCycle = cbk->cycle(); }
            }
            // Now, let's actually find the right one that contains all the needed info...
            const xAOD::CutBookkeeper* allEventsCBK=0;
            const xAOD::CutBookkeeper* DxAODEventsCBK=0;
            std::string derivationName = "EGAM1Kernel"; //need to replace by appropriate name EGAM1
            int maxCycle = -1;
            for (const auto& cbk: *completeCBC) {
                if (cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD") {
                    allEventsCBK = cbk;
                    maxCycle = cbk->cycle();
                }
                if ( cbk->name() == derivationName){
                    DxAODEventsCBK = cbk;
                }
            }
            uint64_t nEventsProcessed  = allEventsCBK->nAcceptedEvents() + m_sumOfWeights->GetBinContent(1);
            double sumOfWeights        = allEventsCBK->sumOfEventWeights() + m_sumOfWeights->GetBinContent(2);
            double sumOfWeightsSquared = allEventsCBK->sumOfEventWeightsSquared() + m_sumOfWeights->GetBinContent(3);
            
            uint64_t nEventsDxAOD           = DxAODEventsCBK->nAcceptedEvents() + m_sumOfWeights->GetBinContent(4);
            double sumOfWeightsDxAOD        = DxAODEventsCBK->sumOfEventWeights() + m_sumOfWeights->GetBinContent(5);
            double sumOfWeightsSquaredDxAOD = DxAODEventsCBK->sumOfEventWeightsSquared() + m_sumOfWeights->GetBinContent(6);
            
            
            
            
            m_sumOfWeights->SetBinContent(1,nEventsProcessed);
            m_sumOfWeights->SetBinContent(2,sumOfWeights);
            m_sumOfWeights->SetBinContent(3,sumOfWeightsSquared);
            m_sumOfWeights->SetBinContent(4,nEventsDxAOD);
            m_sumOfWeights->SetBinContent(5,sumOfWeightsDxAOD);
            m_sumOfWeights->SetBinContent(6,sumOfWeightsSquaredDxAOD);
            
        }
        
        

        //Example of loading in the crossSections into a map
        TTree cross_t; cross_t.ReadFile(dirname + "/data/MyAnalysis/my.metadata.txt"); 
        int mcchannel=0; double crossSection=0; double kFactor = 0; double filterEff = 0;
        cross_t.SetBranchAddress("dataset_number",&mcchannel);
        cross_t.SetBranchAddress("crossSection",&crossSection);
        cross_t.SetBranchAddress("kFactor",&kFactor);
        cross_t.SetBranchAddress("genFiltEff",&filterEff);
        for(int i=0;i<cross_t.GetEntries();i++) {
            cross_t.GetEntry(i);
            m_crossSections[mcchannel] = crossSection;
            m_kFactors[mcchannel] = kFactor;
            m_filterEffs[mcchannel] = filterEff;
        }
    }
    
    m_samplePath = wk()->inputFile()->GetPath();
 
   
    return EL::StatusCode::SUCCESS;
}


/*
EL::StatusCode MyTagAndProbeAnalysis::changeInput( bool firstFile ) {
    // Here you do everything you need to do when we change input files,
    // e.g. resetting branch addresses on trees.  If you are using
    // D3PDReader or a similar service this method is not needed.
    return EL::StatusCode::SUCCESS;
}
*/


EL::StatusCode MyTagAndProbeAnalysis::initialize() {
    // Here you do everything that you need to do after the first input
    // file has been connected and before the first event is processed,
    // e.g. create additional histograms based on which variables are
    // available in the input files.  You can also create all of your
    // histograms and trees in here, but be aware that this method
    // doesn't get called if no events are processed.  So any objects
    // you create here won't be available in the output if you have no
    // input events.

    #pragma GCC diagnostic push
    LESS_DIAGNOSTICS()
    ANA_CHECK_SET_TYPE( EL::StatusCode );
    #pragma GCC diagnostic pop

    ANA_MSG_DEBUG("::initialize: 4x::enableFailure");
    CP::SystematicCode::enableFailure();
    StatusCode::enableFailure();
    xAOD::TReturnCode::enableFailure();
    CP::CorrectionCode::enableFailure();

    ANA_MSG_DEBUG("::initialize: xAOD::TFileAccessTracer::enableDataSubmission(false);");
    xAOD::TFileAccessTracer::enableDataSubmission(false);

    xAOD::TEvent* event = wk()->xaodEvent();
    Info( "initialize()", "Number of events = %lli", event->getEntries() ); // print long long int
    
    TString dirname = gSystem->Getenv("UserAnalysis_DIR"); 
    
    // output xAOD
    TFile* file = wk()->getOutputFile( m_outputName );
    ANA_MSG_DEBUG("::initialize: ANA_CHECK( event->writeTo( file ) );");
    ANA_CHECK( event->writeTo( file ) );

    // Trigger tools
    m_trigConfigTool = new TrigConf::xAODConfigTool( "xAODConfigTool" ); // gives us access to the meta-data
    ANA_CHECK( m_trigConfigTool->initialize() );
    ToolHandle< TrigConf::ITrigConfigTool > trigConfigHandle( m_trigConfigTool );
    
    m_trigDecisionTool = new Trig::TrigDecisionTool( "TrigDecisionTool" );
    ANA_CHECK( m_trigDecisionTool->setProperty( "ConfigTool", trigConfigHandle ) ); // connect the TrigDecisionTool to the ConfigTool
    ANA_CHECK( m_trigDecisionTool->setProperty( "TrigDecisionKey", "xTrigDecision" ) );
    ANA_CHECK( m_trigDecisionTool->initialize() );


    // GRL
    const char* GRLFilePath = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml";
    const char* fullGRLFilePath = gSystem->ExpandPathName (GRLFilePath);
    std::vector<std::string> vecStringGRL;
    vecStringGRL.push_back(fullGRLFilePath);
    ANA_CHECK(m_grl.setProperty( "GoodRunsListVec", vecStringGRL));
    ANA_CHECK(m_grl.setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
    ANA_CHECK(m_grl.initialize());
    
    // PRW
    std::vector< TString > listOfLumicalcFiles = {"GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"};
    listOfLumicalcFiles.push_back( dirname + "/data/MyAnalysis/LumiCalc/ilumicalc_histograms_HLT_e5_etcut_325713-340453_OflLumi-13TeV-010.root:HLT_e5_etcut");
    listOfLumicalcFiles.push_back( dirname + "/data/MyAnalysis/LumiCalc/ilumicalc_histograms_HLT_e10_etcut_L1EM7_325713-340453_OflLumi-13TeV-010.root:HLT_e10_etcut");
    listOfLumicalcFiles.push_back( dirname + "/data/MyAnalysis/LumiCalc/ilumicalc_histograms_HLT_e15_etcut_L1EM7_325713-340453_OflLumi-13TeV-010.root:HLT_e15_etcut");
    listOfLumicalcFiles.push_back( dirname + "/data/MyAnalysis/LumiCalc/ilumicalc_histograms_HLT_e20_etcut_L1EM12_325713-340453_OflLumi-13TeV-010.root:HLT_e20_etcut");
    listOfLumicalcFiles.push_back( dirname + "/data/MyAnalysis/LumiCalc/ilumicalc_histograms_HLT_e25_etcut_L1EM15_325713-340453_OflLumi-13TeV-010.root:HLT_e25_etcut");
    listOfLumicalcFiles.push_back( dirname + "/data/MyAnalysis/LumiCalc/ilumicalc_histograms_HLT_e30_etcut_L1EM15_325713-340453_OflLumi-13TeV-010.root:HLT_e30_etcut");
    listOfLumicalcFiles.push_back( dirname + "/data/MyAnalysis/LumiCalc/ilumicalc_histograms_HLT_e40_etcut_L1EM15_325713-340453_OflLumi-13TeV-010.root:HLT_e40_etcut");
    listOfLumicalcFiles.push_back( dirname + "/data/MyAnalysis/LumiCalc/ilumicalc_histograms_HLT_e50_etcut_L1EM15_325713-340453_OflLumi-13TeV-010.root:HLT_e50_etcut");
    listOfLumicalcFiles.push_back( dirname + "/data/MyAnalysis/LumiCalc/ilumicalc_histograms_HLT_e60_etcut_325713-340453_OflLumi-13TeV-010.root:HLT_e60_etcut");
    listOfLumicalcFiles.push_back( dirname + "/data/MyAnalysis/LumiCalc/ilumicalc_histograms_HLT_e80_etcut_325713-340453_OflLumi-13TeV-010.root:HLT_e80_etcut");
    listOfLumicalcFiles.push_back( dirname + "/data/MyAnalysis/LumiCalc/ilumicalc_histograms_HLT_e100_etcut_325713-340453_OflLumi-13TeV-010.root:HLT_e100_etcut");
    listOfLumicalcFiles.push_back( dirname + "/data/MyAnalysis/LumiCalc/ilumicalc_histograms_HLT_e120_etcut_325713-340453_OflLumi-13TeV-010.root:HLT_e120_etcut");

    listOfLumicalcFiles.push_back( dirname + "/data/MyAnalysis/LumiCalc/ilumicalc_histograms_HLT_e5_lhtight_nod0_e4_etcut_Jpsiee_325713-340453_OflLumi-13TeV-010.root:HLT_e5_lhtight_nod0_e4_etcut_Jpsiee");
    listOfLumicalcFiles.push_back( dirname + "/data/MyAnalysis/LumiCalc/ilumicalc_histograms_HLT_e9_lhtight_nod0_e4_etcut_Jpsiee_325713-340453_OflLumi-13TeV-010.root:HLT_e9_lhtight_nod0_e4_etcut_Jpsiee");
    listOfLumicalcFiles.push_back( dirname + "/data/MyAnalysis/LumiCalc/ilumicalc_histograms_HLT_e14_lhtight_nod0_e4_etcut_Jpsiee_325713-340453_OflLumi-13TeV-010.root:HLT_e14_lhtight_nod0_e4_etcut_Jpsiee");
    listOfLumicalcFiles.push_back( dirname + "/data/MyAnalysis/LumiCalc/ilumicalc_histograms_HLT_e5_lhtight_nod0_e9_etcut_Jpsiee_325713-340453_OflLumi-13TeV-010.root:HLT_e5_lhtight_nod0_e9_etcut_Jpsiee");
    listOfLumicalcFiles.push_back( dirname + "/data/MyAnalysis/LumiCalc/ilumicalc_histograms_HLT_e5_lhtight_nod0_e14_etcut_Jpsiee_325713-340453_OflLumi-13TeV-010.root:HLT_e5_lhtight_nod0_e14_etcut_Jpsiee");
    
    // std::vector<std::string> listOfLumicalcFiles = {"/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root"};
    // std::vector<std::string> listOfLumicalcFiles = {"/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root"};
    std::vector< TString > PRWConfigFiles = { dirname + "/data/MyAnalysis/combined_prw.root", "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"};
    ANA_CHECK(m_PileupReweighting.setProperty("ConfigFiles",PRWConfigFiles));
    ANA_CHECK(m_PileupReweighting.setProperty("LumiCalcFiles",listOfLumicalcFiles));
    ANA_CHECK(m_PileupReweighting.retrieve());


    //============================================================================
    // Read configuration
    //============================================================================

    ANA_MSG_DEBUG("::initialize: Reading conf...");

    // Non-physics
    m_progressInterval      = m_config.getInt( "progressInterval" ); // 1'000

    // Meta settings
    m_analysisType          = m_config.getInt( "analysisType" ); // Set by user

    // Tag and probe
    m_mZ                    = 91'188.0; // Mass of Z doesn't change
    m_mZLowerCutSignal      = m_config.getNum( "TNP.mZLowerCutSignal" ); // 30'000.0

    m_tagPt                 = m_config.getNum( "TNP.tagPt" ); // 24'500.0

    m_tagPairPt             = m_config.getNum( "TNP.tagPairPt" ); // 14'500.0

    m_probePt               = m_config.getNum( "TNP.probePt" ); // 14'500.0
    m_probeEta              = m_config.getNum( "TNP.probeEta" ); // 4.9

    // Background
    m_backgroundPt          = m_config.getNum( "Background.pt" ); // 14'500.0
    m_backgroundmZVetoRange = m_config.getNum( "Background.mZVetoRange" ); // 20'000.0
    m_backgroundmWTransVeto = m_config.getNum( "Background.mWTransVeto" ); // 40'000.0
    m_backgroundEta         = m_config.getNum( "Background.eta"); //4.9

    // Fill switches (save time by not saving stuff you don't need!^TM)
    m_fillpXVariables       = m_config.getBool( "Fill.pXVariables" ); // YES
    m_fillBtags             = m_config.getBool( "Fill.Btags" ); // YES (NO for grid?!)
    m_fillTracks            = m_config.getBool( "Fill.Tracks" ); // YES
    m_fillPU                = m_config.getBool( "Fill.PU" ); // YES

    // Tool settings
    m_triggerMathcingUseTriggerMatching = m_config.getBool( "TriggerMatching.UseTriggerMatching" ); // YES
    m_triggerMatchingdR                 = m_config.getNum( "TriggerMatching.dR" ); // 0.07

    m_ShowerShapeFudgeApplyOnElectrons  = m_config.getBool( "ShowerShapeFudge.applyOnElectrons" ); // YES
    m_ShowerShapeFudgeApplyOnPhotons    = m_config.getBool( "ShowerShapeFudge.applyOnPhotons" ); // YES
    
    m_gainNoiseImages = m_config.getBool( "Fill.gainNoiseImages" );
    m_fine_eta        = m_config.getBool( "Use.fineEta" );
    m_fine_phi        = m_config.getBool( "Use.finePhi" );
    

    // Print configuration database, if requested
    if ( m_config.getBoolDefault("PrintConfig", true) ) {
        Info("initialize()", "Printing full configuration:");
        m_config.printDB();
    }

    // Check whether the settings are valid
    if ( m_mZLowerCutSignal < 1'000. ) _fatal( "TNP.mZLowerCutSignal is less than 1000 MeV. The units used are MeV." );
    if ( m_tagPt < 1'000. ) _fatal( "TNP.tagPt is less than 1000 MeV. The units used are MeV." );
    if ( m_tagPairPt < 1'000. ) _fatal( "TNP.tagPairPt is less than 1000 MeV. The units used are MeV." );
    if ( m_probePt < 1'000. ) _fatal( "TNP.probePt is less than 1000 MeV. The units used are MeV." );
    if ( m_probeEta > 5. ) _fatal( "TNP.probeEta is greater than 5. Did you mean that?" );
    if ( m_probeEta < 0. ) _fatal( "TNP.probeEta is less than 0. That makes no sense." );
    if ( m_backgroundPt < 1'000. ) _fatal( "TNP.pt is less than 1000 MeV. The units used are MeV." );
    if ( m_backgroundmZVetoRange < 1'000. ) _fatal( "TNP.mZVetoRange is less than 1000 MeV. The units used are MeV." );
    if ( m_backgroundmWTransVeto < 1'000. ) _fatal( "TNP.mWTransVeto is less than 1000 MeV. The units used are MeV." );
    if ( m_backgroundEta > 5. ) _fatal( "BackgroundEta is greater than 5. Did you mean that?" );
    if ( m_backgroundEta < 0. ) _fatal( "BackgroundEta is less than 0. That makes no sense." );
    if ( !m_triggerMathcingUseTriggerMatching ) _fatal( "Trigger matching cannot be turned off at the moment. Set TriggerMatching.UseTriggerMatching: YES." );
    if ( m_triggerMatchingdR < 0.0 || m_triggerMatchingdR > 3.0 ) fatal( "TriggerMatching.dR is outside range (0-3): %f", m_triggerMatchingdR );

    // Add config vars that should be whitelisted even if not always read (no additional vars need be added at the moment)
    // const std::vector< TString > configWhitelist = { };
    // m_config.addToWhiteList( configWhitelist );

    // If any variable not on the whitelist is found, print a fatal message
    // Any variable requested (e.g. config()->getNum() ) is automatically put on the whitelist
    // Therefore, read the configuration fully before this line!
    m_config.checkWhitelist();

    // include gain and noise images
    if ( m_gainNoiseImages ){
        m_el_tree->Branch( "gain_em_barrel_Lr0", &gain_em_barrel_Lr0);
        m_el_tree->Branch( "gain_em_barrel_Lr1", &gain_em_barrel_Lr1);
        m_el_tree->Branch( "gain_em_barrel_Lr2", &gain_em_barrel_Lr2);
        m_el_tree->Branch( "gain_em_barrel_Lr3", &gain_em_barrel_Lr3);
        m_el_tree->Branch( "gain_em_endcap_Lr0", &gain_em_endcap_Lr0);
        m_el_tree->Branch( "gain_em_endcap_Lr1", &gain_em_endcap_Lr1);
        m_el_tree->Branch( "gain_em_endcap_Lr2", &gain_em_endcap_Lr2);
        m_el_tree->Branch( "gain_em_endcap_Lr3", &gain_em_endcap_Lr3);
        m_el_tree->Branch( "gain_lar_endcap_Lr0", &gain_lar_endcap_Lr0);
        m_el_tree->Branch( "gain_lar_endcap_Lr1", &gain_lar_endcap_Lr1);
        m_el_tree->Branch( "gain_lar_endcap_Lr2", &gain_lar_endcap_Lr2);
        m_el_tree->Branch( "gain_lar_endcap_Lr3", &gain_lar_endcap_Lr3);
        m_el_tree->Branch( "gain_tile_barrel_Lr1", &gain_tile_barrel_Lr1);
        m_el_tree->Branch( "gain_tile_barrel_Lr2", &gain_tile_barrel_Lr2);
        m_el_tree->Branch( "gain_tile_barrel_Lr3", &gain_tile_barrel_Lr3);
        m_el_tree->Branch( "gain_tile_gap_Lr1", &gain_tile_gap_Lr1);
        
        m_el_tree->Branch( "noise_em_barrel_Lr0", &noise_em_barrel_Lr0);
        m_el_tree->Branch( "noise_em_barrel_Lr1", &noise_em_barrel_Lr1);
        m_el_tree->Branch( "noise_em_barrel_Lr2", &noise_em_barrel_Lr2);
        m_el_tree->Branch( "noise_em_barrel_Lr3", &noise_em_barrel_Lr3);
        m_el_tree->Branch( "noise_em_endcap_Lr0", &noise_em_endcap_Lr0);
        m_el_tree->Branch( "noise_em_endcap_Lr1", &noise_em_endcap_Lr1);
        m_el_tree->Branch( "noise_em_endcap_Lr2", &noise_em_endcap_Lr2);
        m_el_tree->Branch( "noise_em_endcap_Lr3", &noise_em_endcap_Lr3);
        m_el_tree->Branch( "noise_lar_endcap_Lr0", &noise_lar_endcap_Lr0);
        m_el_tree->Branch( "noise_lar_endcap_Lr1", &noise_lar_endcap_Lr1);
        m_el_tree->Branch( "noise_lar_endcap_Lr2", &noise_lar_endcap_Lr2);
        m_el_tree->Branch( "noise_lar_endcap_Lr3", &noise_lar_endcap_Lr3);
        m_el_tree->Branch( "noise_tile_barrel_Lr1", &noise_tile_barrel_Lr1);
        m_el_tree->Branch( "noise_tile_barrel_Lr2", &noise_tile_barrel_Lr2);
        m_el_tree->Branch( "noise_tile_barrel_Lr3", &noise_tile_barrel_Lr3);
        m_el_tree->Branch( "noise_tile_gap_Lr1", &noise_tile_gap_Lr1);        
                
        
        m_ga_tree->Branch( "gain_em_barrel_Lr0", &gain_em_barrel_Lr0);
        m_ga_tree->Branch( "gain_em_barrel_Lr1", &gain_em_barrel_Lr1);
        m_ga_tree->Branch( "gain_em_barrel_Lr2", &gain_em_barrel_Lr2);
        m_ga_tree->Branch( "gain_em_barrel_Lr3", &gain_em_barrel_Lr3);
        m_ga_tree->Branch( "gain_em_endcap_Lr0", &gain_em_endcap_Lr0);
        m_ga_tree->Branch( "gain_em_endcap_Lr1", &gain_em_endcap_Lr1);
        m_ga_tree->Branch( "gain_em_endcap_Lr2", &gain_em_endcap_Lr2);
        m_ga_tree->Branch( "gain_em_endcap_Lr3", &gain_em_endcap_Lr3);
        m_ga_tree->Branch( "gain_lar_endcap_Lr0", &gain_lar_endcap_Lr0);
        m_ga_tree->Branch( "gain_lar_endcap_Lr1", &gain_lar_endcap_Lr1);
        m_ga_tree->Branch( "gain_lar_endcap_Lr2", &gain_lar_endcap_Lr2);
        m_ga_tree->Branch( "gain_lar_endcap_Lr3", &gain_lar_endcap_Lr3);
        m_ga_tree->Branch( "gain_tile_barrel_Lr1", &gain_tile_barrel_Lr1);
        m_ga_tree->Branch( "gain_tile_barrel_Lr2", &gain_tile_barrel_Lr2);
        m_ga_tree->Branch( "gain_tile_barrel_Lr3", &gain_tile_barrel_Lr3);
        m_ga_tree->Branch( "gain_tile_gap_Lr1", &gain_tile_gap_Lr1);
        
        m_ga_tree->Branch( "noise_em_barrel_Lr0", &noise_em_barrel_Lr0);
        m_ga_tree->Branch( "noise_em_barrel_Lr1", &noise_em_barrel_Lr1);
        m_ga_tree->Branch( "noise_em_barrel_Lr2", &noise_em_barrel_Lr2);
        m_ga_tree->Branch( "noise_em_barrel_Lr3", &noise_em_barrel_Lr3);
        m_ga_tree->Branch( "noise_em_endcap_Lr0", &noise_em_endcap_Lr0);
        m_ga_tree->Branch( "noise_em_endcap_Lr1", &noise_em_endcap_Lr1);
        m_ga_tree->Branch( "noise_em_endcap_Lr2", &noise_em_endcap_Lr2);
        m_ga_tree->Branch( "noise_em_endcap_Lr3", &noise_em_endcap_Lr3);
        m_ga_tree->Branch( "noise_lar_endcap_Lr0", &noise_lar_endcap_Lr0);
        m_ga_tree->Branch( "noise_lar_endcap_Lr1", &noise_lar_endcap_Lr1);
        m_ga_tree->Branch( "noise_lar_endcap_Lr2", &noise_lar_endcap_Lr2);
        m_ga_tree->Branch( "noise_lar_endcap_Lr3", &noise_lar_endcap_Lr3);
        m_ga_tree->Branch( "noise_tile_barrel_Lr1", &noise_tile_barrel_Lr1);
        m_ga_tree->Branch( "noise_tile_barrel_Lr2", &noise_tile_barrel_Lr2);
        m_ga_tree->Branch( "noise_tile_barrel_Lr3", &noise_tile_barrel_Lr3);
        m_ga_tree->Branch( "noise_tile_gap_Lr1", &noise_tile_gap_Lr1);      
            
    }
    
    // Create some whitespace
    std::cout << std::endl;



    //============================================================================
    // Output histograms ( TODO )
    //============================================================================

    ANA_MSG_DEBUG("::initialize: Making hists...");

    m_preTagElectronCutHist = createCutHist( "preTagElectrons", {
        "Initial",
        "p_{T} > 4.5 GeV",
        "Crack veto",
        "Tight ID",
        "Loose Iso",
        "Object Quality",
        "Has track",
        "Has vertex",
    } );

    m_tagElectronCutHist = createCutHist( "tagElectrons", {
        "Initial",
        "p_{T} > "+TString::Format("%2.1f",m_tagPt/1e3)+" GeV",
        "Match trigger",
    } );

    m_tagMuonCutHist = createCutHist( "tagMuons", {
        "Initial",
        "Combined muon",
        "p_{T} > "+TString::Format("%2.1f",m_tagPt/1e3)+" GeV",
        "Has track",
        "Loose iso",
        "Match trigger",
        "Medium ID",
    } );


    m_probeElectronCutHist = createCutHist( "probeElectrons", {
        "Initial",
        "pt > "+TString::Format("%2.1f",m_probePt/1e3)+" GeV",
        "Pass OQ",
        "|eta| < "+TString::Format("%2.1f",m_probeEta),
        "Has track",
        "Has vertex"
    });

    m_probeZElectronCutHist = createCutHist( "ZProbeElectrons", {
        "Initial",
        "pt > 9.5 GeV",
    });

    m_leptonPairCutHist = createCutHist( "leptonPairs", {
        "Initial",
        "dR>0.4(0.2) for el(mu)",
        "Inv. mass at least "+TString::Format("%2.0f",m_mZLowerCutSignal/1e3)+" GeV"
        "Opposite Charge",
    });
    
    m_muLeptonPairCutHist = createCutHist( "muLeptonPairs", {
        "Initial",
        "dR>0.4(0.2) for el(mu)",
        "Inv. mass at least "+TString::Format("%2.0f",m_mZLowerCutSignal/1e3)+" GeV"
        "Opposite Charge",
    });
    
    m_leptonPairGoodProbeCount = new TH2D( "hist_leptonPairGoodProbeCount", "hist_leptonPairGoodProbeCount", 10, 0, 10, 10, 0, 10 );
    wk()->addOutput( m_leptonPairGoodProbeCount );


    m_backgroundCentralElectronsHist = createCutHist( "backgroundCentralElectrons", {
        "Initial",
        "Triggers",
    });
    m_backgroundCentralElectronsmWTrans = new TH1D( "m_backgroundCentralElectronsmWTrans", "m_backgroundCentralElectronsmWTrans", 75, 0, 150e3);
    wk()->addOutput( m_backgroundCentralElectronsmWTrans );
    m_backgroundCentralElectronsmZ = new TH1D( "m_backgroundCentralElectronsmZ", "m_backgroundCentralElectronsmZ", 75, 0, 150e3);
    wk()->addOutput( m_backgroundCentralElectronsmZ );


    m_tagJpsiElectronCutHist = createCutHist( "JpsiTagElectrons", {
        "Initial",
        "Trigger Match",
    });
    m_probeJpsiElectronCutHist = createCutHist( "JpsiProbeElectrons", {
        "Initial",
        "pt > 4.5 GeV",
        "Good OQ",
        "eta",
        "Has track",
        "Has vertex"
    });
    m_leptonJpsiPairCutHist = createCutHist( "JpsiTagAndProbe",{
        "Initial",
        "Opposite charge",
        "DeltaR > 0.1",
        "InvMass cut",
        "Lifetime cut"
    });
    m_JpsiMass = new TH1D( "m_JpsiMass", "m_JpsiMass", 75, 0, 5000);
    wk()->addOutput( m_JpsiMass );
    m_JpsiLifetime = new TH1D( "m_JpsiLifetime", "m_JpsiLifetime", 75, -2, 1);
    wk()->addOutput( m_JpsiLifetime );

    
    //============================================================================
    // Set-up tools
    //============================================================================

    ANA_MSG_DEBUG("::initialize: Setting up tools...");



    // Trigger matching tool
    m_tmt = new Trig::MatchingTool( "MatchingTool" );
    ANA_CHECK( m_tmt->initialize() );

    // Isolation tools
    m_isolationSelectionToolLoose = new CP::IsolationSelectionTool( "isolationselectiontool" );
    ANA_CHECK( m_isolationSelectionToolLoose->setProperty( "ElectronWP", "FCLoose" ));
    ANA_CHECK( m_isolationSelectionToolLoose->setProperty( "MuonWP", "FCLoose" ));
   // ANA_CHECK( m_isolationSelectionToolLoose->setProperty( "OutputLevel", 2 ));
    ANA_CHECK( m_isolationSelectionToolLoose->initialize() );

    // Muon selection tool
    ANA_CHECK( m_muonSelectionMedium.setProperty( "MaxEta", 2.5 ) );
    ANA_CHECK( m_muonSelectionMedium.setProperty( "MuQuality", 1 ) );
//    ANA_CHECK( m_muonSelectionMedium.setProperty( "OutputLevel", 2 ));
    ANA_CHECK( m_muonSelectionMedium.initialize() );
    
    // Electron Charge ID Selector Tool - https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ElectronChargeFlipTaggerTool
    m_ECIDSToolLoose = new AsgElectronChargeIDSelectorTool("ECIDS_loose");
    ANA_CHECK( m_ECIDSToolLoose->setProperty("TrainingFile","ElectronPhotonSelectorTools/ChargeID/ECIDS_20180731rel21Summer2018.root") );
//    ANA_CHECK( m_ECIDSToolLoose->setProperty( "OutputLevel", 2 ));    
    ANA_CHECK( m_ECIDSToolLoose->initialize() );
    
    // Photon selection tool (tight)
    m_photonTightIsEMSelector = new AsgPhotonIsEMSelector( "PhotonTightIsEMSelector" );
    ANA_CHECK( m_photonTightIsEMSelector->setProperty( "isEMMask", egammaPID::PhotonTight ) );
    ANA_CHECK( m_photonTightIsEMSelector->setProperty( "ConfigFile", "ElectronPhotonSelectorTools/offline/20180825/PhotonIsEMTightSelectorCutDefs.conf" ) ); // Rel 21
//    ANA_CHECK( m_photonTightIsEMSelector->setProperty( "OutputLevel", 2 ));
    ANA_CHECK( m_photonTightIsEMSelector->initialize() );
    
    // Photon selection tool (loose)
    m_photonLooseIsEMSelector = new AsgPhotonIsEMSelector( "PhotonLooseIsEMSelector" );
    ANA_CHECK( m_photonLooseIsEMSelector->setProperty( "isEMMask", egammaPID::PhotonLoose ) );
    ANA_CHECK( m_photonLooseIsEMSelector->setProperty( "ConfigFile", "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMLooseSelectorCutDefs.conf" ) ); // Rel 21
//    ANA_CHECK( m_photonLooseIsEMSelector->setProperty( "OutputLevel", 2 ));
    ANA_CHECK( m_photonLooseIsEMSelector->initialize() );
    
    // Electron selection tool (tight)
    m_electronSelectionToolTight = new AsgElectronLikelihoodTool("TightLH");
    ANA_CHECK( m_electronSelectionToolTight->setProperty("WorkingPoint", "TightLHElectron") );
//    ANA_CHECK( m_electronSelectionToolTight->setProperty( "OutputLevel", 2 ));
    ANA_CHECK( m_electronSelectionToolTight->initialize() );
    
    // Electron selection tool (medium)
    m_electronSelectionToolMedium = new AsgElectronLikelihoodTool("MediumLH");
    ANA_CHECK( m_electronSelectionToolMedium->setProperty("WorkingPoint", "MediumLHElectron") );
//    ANA_CHECK( m_electronSelectionToolMedium->setProperty( "OutputLevel", 2 ));
    ANA_CHECK( m_electronSelectionToolMedium->initialize() );
    
    // Electron selection tool (loose)
    m_electronSelectionToolLoose = new AsgElectronLikelihoodTool("LooseLH");
    ANA_CHECK( m_electronSelectionToolLoose->setProperty("WorkingPoint", "LooseLHElectron") );
//    ANA_CHECK( m_electronSelectionToolLoose->setProperty( "OutputLevel", 2 ));
    ANA_CHECK( m_electronSelectionToolLoose->initialize() );
    
    // Calibration and Smearing Tool for electrons and photons
    m_electronCalibrationSmearingTool = new CP::EgammaCalibrationAndSmearingTool("EgammaCalibrationAndSmearingTool");
    ANA_CHECK( m_electronCalibrationSmearingTool->setProperty("ESModel", "es2017_R21_v1") );
//    ANA_CHECK( m_electronCalibrationSmearingTool->setProperty( "OutputLevel", 2 ));
    ANA_CHECK( m_electronCalibrationSmearingTool->initialize() );
    
    // Calibration and Smearing Tool for muons
    m_muonCalibrationSmearingTool = new CP::MuonCalibrationAndSmearingTool("MuonCalibrationAndSmearingTool");
    ANA_CHECK( m_muonCalibrationSmearingTool->setProperty("Year", "Data17") );
//    ANA_CHECK( m_muonCalibrationSmearingTool->setProperty( "OutputLevel", 2 ));
    ANA_CHECK( m_muonCalibrationSmearingTool->initialize() );
    
    if ( event_isMC ){
        // Egamma shower shape fudge tool
        m_MCShifterTool = new ElectronPhotonShowerShapeFudgeTool( "MCShifterTool" );
        ANA_CHECK( m_MCShifterTool->setProperty( "Preselection", 22 ) ); // Rel 21
        ANA_CHECK( m_MCShifterTool->setProperty( "FFCalibFile", "ElectronPhotonShowerShapeFudgeTool/v2/PhotonFudgeFactors.root") ); // Rel 21
//        ANA_CHECK( m_MCShifterTool->setProperty( "OutputLevel", 2 ));
        ANA_CHECK( m_MCShifterTool->initialize() );

        // Electron scale factor tool for the tag electrons (LHTight, FCLoose)
        m_electronSF = new AsgElectronEfficiencyCorrectionTool("TightFCLoose");
        ANA_CHECK( m_electronSF->setProperty("IdKey", "Tight"));
        ANA_CHECK( m_electronSF->setProperty("IsoKey", "FCLoose"));
//        ANA_CHECK( m_electronSF->setProperty( "OutputLevel", 2 ));
        ANA_CHECK( m_electronSF->initialize());

        // Electron charge misID scale factor for the tag electrons (LHTight, FCLoose)
        m_electronChargeIdSF = new CP::ElectronChargeEfficiencyCorrectionTool("TightFCLooseCharge");
        ANA_CHECK( m_electronChargeIdSF->setProperty("CorrectionFileName", "ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/charge_misID/chargeEfficiencySF.TightLLH_d0z0_v13_FCLoose.root" ));
//        ANA_CHECK( m_electronChargeIdSF->setProperty( "OutputLevel", 2 ));
        ANA_CHECK( m_electronChargeIdSF->initialize() );
       
        // Calibration and Smearing Tool for electrons and photons with no smearing in MC to get the best calibrated energy   
        m_electronCalibrationTool = new CP::EgammaCalibrationAndSmearingTool("EgammaCalibrationTool");
        ANA_CHECK( m_electronCalibrationTool->setProperty("ESModel", "es2017_R21_v1") );
        ANA_CHECK( m_electronCalibrationTool->setProperty("doSmearing",  0) );
//        ANA_CHECK( m_electronCalibrationTool->setProperty( "OutputLevel", 2 ));
        ANA_CHECK( m_electronCalibrationTool->initialize() );

        // Muon scale factor tool for the tag electrons (Medium)
        m_muonSF = new CP::MuonEfficiencyScaleFactors("MuonSF");
        ANA_CHECK( m_muonSF->setProperty("WorkingPoint", "Medium") );
//        ANA_CHECK( m_muonSF->setProperty( "OutputLevel", 2 ));
        ANA_CHECK( m_muonSF->initialize() );
        
        // Muon scale factor tool for the tag electrons (LooseIso), apparently you need to have two different instances of this tool, one for ID and one for Iso
        m_muonSFIso = new CP::MuonEfficiencyScaleFactors("MuonSFIso");
        ANA_CHECK( m_muonSFIso->setProperty("WorkingPoint", "LooseIso") );
//        ANA_CHECK( m_muonSFIso->setProperty( "OutputLevel", 2 ));
        ANA_CHECK( m_muonSFIso->initialize() );    

        // IFFTruthClassifier
        m_iffTruthClassifier = new IFFTruthClassifier("IFFTruthClassifier");
 //       ANA_CHECK( m_iffTruthClassifier->setProperty( "OutputLevel", 2 ));
        ANA_CHECK( m_iffTruthClassifier->initialize() );

    }

    // Jet Calibration Tool 
    m_jetCalibrationTool = new JetCalibrationTool("JetCalibration");
    ANA_CHECK( m_jetCalibrationTool->setProperty("JetCollection", "AntiKt4EMPFlow") );
    ANA_CHECK( m_jetCalibrationTool->setProperty("ConfigFile", "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config") );
    ANA_CHECK( m_jetCalibrationTool->setProperty("CalibArea", "00-04-82") );
    if ( event_isMC ){
        ANA_CHECK( m_jetCalibrationTool->setProperty("CalibSequence", "JetArea_Residual_EtaJES_GSC_Smear") );
        ANA_CHECK( m_jetCalibrationTool->setProperty("IsData", false) );
    }
    else{
        ANA_CHECK( m_jetCalibrationTool->setProperty("CalibSequence", "JetArea_Residual_EtaJES_GSC_Insitu") );
        ANA_CHECK( m_jetCalibrationTool->setProperty("IsData", true) );   
    }
//    ANA_CHECK( m_jetCalibrationTool->setProperty( "OutputLevel", 2 ));
    ANA_CHECK( m_jetCalibrationTool->initialize() );


    // MET builder 
    ANA_CHECK( m_metutil.setProperty("DoMuonEloss", false) );
    ANA_CHECK( m_metutil.setProperty("DoRemoveMuonJets", true) );
    ANA_CHECK( m_metutil.setProperty("DoSetMuonJetEMScale", true) ); 
//    ANA_CHECK( m_metutil.setProperty( "OutputLevel", 2 ));
    ANA_CHECK( m_metutil.initialize() );


    // Make them shut up after initialization so they don't print with every new .root file
    m_trigConfigTool->msg().setLevel( MSG::WARNING );
    m_trigDecisionTool->msg().setLevel( MSG::WARNING );
    

    m_eventCounter = 0;
    Info( "initialize()", "Initialization done" ); // print long long int
    return EL::StatusCode::SUCCESS;
}


EL::StatusCode MyTagAndProbeAnalysis::execute() {
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.  This is where most of your actual analysis
    // code will go.

    #pragma GCC diagnostic push
    LESS_DIAGNOSTICS()
    ANA_CHECK_SET_TYPE( EL::StatusCode );
    #pragma GCC diagnostic pop
    xAOD::TEvent* event = wk()->xaodEvent();

    // Print processing rate at set interval, so we know where we are
    ++m_eventCounter;
    if ( m_eventCounter == 1) m_startTime = time(0);
    if ( m_eventCounter % m_progressInterval == 0 ) {
        Info("execute()","%li events processed so far  <<<==", m_eventCounter );
        Info("execute()","Processing rate = %f Hz", float(m_eventCounter)/float(time(0)-m_startTime));
    }
    ANA_MSG_DEBUG("::execute: Event " << m_eventCounter);

    
    
    //============================================================================
    // Retrieve containers
    //============================================================================
    
    ANA_MSG_DEBUG("::execute: Retrieving containers...");
    
    // Event info
    ANA_CHECK( event->retrieve( m_eventInfo, "EventInfo" ));
    
    // Electrons
    const xAOD::ElectronContainer* electrons_org = 0;
    ANA_CHECK( event->retrieve( electrons_org, "Electrons" ) );
    std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > electrons_shallowCopy = xAOD::shallowCopyContainer( *electrons_org );
    m_electrons = electrons_shallowCopy.first;
    
    // Muons
    const xAOD::MuonContainer* muons_org = 0;
    ANA_CHECK( event->retrieve( muons_org, "Muons" ) );
    std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer( *muons_org );
    m_muons = muons_shallowCopy.first;
    
    // Photons
    const xAOD::PhotonContainer* photons_org = 0;
    ANA_CHECK( event->retrieve( photons_org, "Photons" ) );
    std::pair< xAOD::PhotonContainer*, xAOD::ShallowAuxContainer* > photons_shallowCopy = xAOD::shallowCopyContainer( *photons_org );
    m_photons = photons_shallowCopy.first;
    std::unique_ptr< xAOD::PhotonContainer > photons_p( photons_shallowCopy.first );
    std::unique_ptr< xAOD::ShallowAuxContainer > photons_p2( photons_shallowCopy.second );
    
    // Indet tracks
    if ( m_fillTracks || m_fillPU ) {
        ANA_MSG_DEBUG("::execute: Retrieving tracks...");
        const xAOD::TrackParticleContainer* indet_tracks_org = 0;
        ANA_CHECK( event->retrieve( indet_tracks_org, "InDetTrackParticles" ) );
        std::pair< xAOD::TrackParticleContainer*, xAOD::ShallowAuxContainer* > indet_tracks_shallowCopy = xAOD::shallowCopyContainer( *indet_tracks_org );
        m_indetTracks.reset( indet_tracks_shallowCopy.first );
        m_indetTracksAux.reset( indet_tracks_shallowCopy.second );
    }
    
    // Jets ( for b-tags )
    ANA_MSG_DEBUG("::execute: Retrieving jets...");
    const xAOD::JetContainer* jets_org = 0;
    ANA_CHECK( event->retrieve( jets_org, "AntiKt4EMPFlowJets" ) );
    std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jets_shallowCopy = xAOD::shallowCopyContainer( *jets_org );
    m_jets = jets_shallowCopy.first;
    
    
    // Truth particles
    if ( event_isMC ) {
        ANA_CHECK( event->retrieve( m_truthContainer, "TruthParticles" ) );
    }
    
    // needed by METMaker
    met::addGhostMuonsToJets(*muons_org, *m_jets);
    
    
    
    // Primary vertices
    ANA_CHECK( event->retrieve( m_PVs, "PrimaryVertices" ) );
    
    
    //============================================================================
    // Event-level pre-selection
    //============================================================================
    
    ANA_MSG_DEBUG("::execute: Retrieving event-level cuts...");
    
    // Initial
    
    // Event cleaning
    
    
    // if data check if event passes GRL
    if (!event_isMC) { // it's data!
      if (!m_grl->passRunLB(*m_eventInfo)) {
        return StatusCode::SUCCESS; // go to next event
      }
    } // end if not MC
    
    
    // Apply event cleaning to remove events due to
    // problematic regions of the detector or incomplete events.
    // Apply to data    
    if (
           ( m_eventInfo->errorState( xAOD::EventInfo::LAr )  == xAOD::EventInfo::Error )
        || ( m_eventInfo->errorState( xAOD::EventInfo::Tile ) == xAOD::EventInfo::Error )
        || ( m_eventInfo->errorState( xAOD::EventInfo::SCT )  == xAOD::EventInfo::Error )
        || ( m_eventInfo->isEventFlagBitSet( xAOD::EventInfo::Core, 18 ) )
     )
        return EL::StatusCode::SUCCESS;
    
    // at least one primary vertex
    if ( m_PVs->size() == 0 ) {
        return EL::StatusCode::SUCCESS;
    }
    
    // jet event cleaning
    if ( m_eventInfo->auxdata<char>("DFCommonJets_eventClean_LooseBad") == 0 ){
        return EL::StatusCode::SUCCESS;
    }
    
    //============================================================================
    // Saving event information
    //============================================================================
    
    ANA_MSG_DEBUG("::execute: Saving event-level information...");
    
    // For event info, set default values to tree branch variables and clear vectors ( will catch cases where we for some reason don't fill a branch )
    resetEventInfoBranchVariables();
    
    // Fill the branches of our trees
    event_eventNumber                    = m_eventInfo->eventNumber();
    if ( event_isMC ){
        event_mcChannelNumber            = m_eventInfo->mcChannelNumber();
        event_MCWeight                   = m_eventInfo->mcEventWeight();
    }
    ANA_CHECK(m_PileupReweighting->apply(*m_eventInfo));
    event_pileupweight                   = m_eventInfo->auxdecor<float>("PileupWeight");
    event_runNumber                      = m_eventInfo->runNumber();
    event_actualInteractionsPerCrossing  = m_eventInfo->actualInteractionsPerCrossing();
    event_averageInteractionsPerCrossing = m_eventInfo->averageInteractionsPerCrossing();
    event_correctedAverageMu             = m_PileupReweighting->getCorrectedAverageInteractionsPerCrossing( *m_eventInfo );
    event_correctedScaledAverageMu       = m_PileupReweighting->getCorrectedAverageInteractionsPerCrossing( *m_eventInfo, true );
    event_correctedActualMu              = m_PileupReweighting->getCorrectedActualInteractionsPerCrossing( *m_eventInfo );
    event_correctedScaledActualMu        = m_PileupReweighting->getCorrectedActualInteractionsPerCrossing( *m_eventInfo, true );
    
    //============================================================================
    // Apply corrections
    //============================================================================
    ANA_MSG_DEBUG("::execute: Applying corrections...");
    // electron calibration
    for ( const auto& el : *m_electrons ) {        
        if ( event_isMC ) {
            if ( m_ShowerShapeFudgeApplyOnElectrons ) {
                // apply fudge factors to electrons
                if ( m_MCShifterTool->applyCorrection( *el ) != CP::CorrectionCode::Ok ) {
                    ANA_MSG_WARNING( "execute(): Electron correction failed on an object.");
                }
                // get unsmeared energy estimate of electrons in MC
                if ( m_electronCalibrationTool->applyCorrection( *el ) != CP::CorrectionCode::Ok ) {
                    ANA_MSG_WARNING( "execute(): Electron calibration failed on an object.");
                }
                el->auxdata<double>("calibratedE") = el->e();
            }
        }
        if ( m_electronCalibrationSmearingTool->applyCorrection( *el ) != CP::CorrectionCode::Ok ) {
            ANA_MSG_WARNING( "execute(): Electron calibration failed on an object.");
        }
    }
    
    // photon calibration
    for ( const auto& ph : *m_photons ) {
        if ( event_isMC ){
            if ( m_ShowerShapeFudgeApplyOnPhotons ) {
                // apply fudge factors to photons
                if ( m_MCShifterTool->applyCorrection( *ph ) != CP::CorrectionCode::Ok ) {
                    ANA_MSG_WARNING( "execute(): Photon correction failed on an object.");
                }
                // get unsmeared energy estimate of photons in MC                
                if ( m_electronCalibrationTool->applyCorrection( *ph ) != CP::CorrectionCode::Ok ) {
                    ANA_MSG_WARNING( "execute(): Electron calibration failed on an object.");
                }
                ph->auxdata<double>("calibratedE") = ph->e();
            }
        }
        if ( m_electronCalibrationSmearingTool->applyCorrection( *ph ) != CP::CorrectionCode::Ok ) {
            ANA_MSG_WARNING( "execute(): Photon calibration failed on an object.");
        }
    }
    
    // muon calibration
    for ( const auto& mu : *m_muons ) {
        if ( m_muonCalibrationSmearingTool->applyCorrection( *mu ) != CP::CorrectionCode::Ok ){
            ANA_MSG_WARNING( "execute(): Muon calibration failed on an object.");
        }
    }
    
    // Jet calibration 
    for ( const auto& jet : *m_jets ) {
        if ( m_jetCalibrationTool->applyCorrection( *jet ) != CP::CorrectionCode::Ok ){
            ANA_MSG_WARNING( "execute(): Jet calibration failed on an object.");
        }
    }
    
    
    ANA_CHECK( event->retrieve(m_coreMet, "MET_Core_AntiKt4EMPFlow") );
    
    if(!xAOD::setOriginalObjectLink(*jets_org, *m_jets)){//tell calib container what old container it matches
        std::cout << "Failed to set the original object links" << std::endl;
        return 1;
    }
    if(!xAOD::setOriginalObjectLink(*electrons_org, *m_electrons)){//tell calib container what old container it matches
        std::cout << "Failed to set the original object links" << std::endl;
        return 1;
    }
    if(!xAOD::setOriginalObjectLink(*muons_org, *m_muons)){//tell calib container what old container it matches
        std::cout << "Failed to set the original object links" << std::endl;
        return 1;
    }
    
    // save electrons, muons, and jets to event store for METMaker
    ANA_CHECK( event->record(m_jets, "CalibJets") );
    ANA_CHECK( event->record(m_muons, "CalibMuons") );
    ANA_CHECK( event->record(m_electrons, "CalibElectrons") );
    ANA_CHECK( event->record(jets_shallowCopy.second, "CalibJetsAux") );
    ANA_CHECK( event->record(muons_shallowCopy.second, "CalibMuonsAux") );
    ANA_CHECK( event->record(electrons_shallowCopy.second, "CalibElectronsAux") );    
    
    //retrieve the MET association map
    ANA_CHECK( event->retrieve(m_metMap, "METAssoc_AntiKt4EMPFlow") );
    m_metMap->resetObjSelectionFlags();
    
    
    // Create a MissingETContainer with its aux store for each systematic
    m_newMetContainer    = new xAOD::MissingETContainer();
    m_newMetAuxContainer = new xAOD::MissingETAuxContainer();
    m_newMetContainer->setStore(m_newMetAuxContainer);
    ANA_CHECK( event->record(m_newMetContainer, "NewMET") );
    ANA_CHECK( event->record(m_newMetAuxContainer, "NewMETAux") );        
    
    m_metMap->resetObjSelectionFlags();
    
    //Electrons
    ConstDataVector<xAOD::ElectronContainer> metElectrons(SG::VIEW_ELEMENTS);
    for(const auto& el : *m_electrons) {
        if(el->pt()>10e3 && el->eta()<2.47 && Accessor_DFLHBLLoose( *el ) ) metElectrons.push_back(el); //  m_electronSelectionToolLoose->accept( el)
    }
    ANA_CHECK( m_metutil->rebuildMET("RefEle",                   //name of metElectrons in metContainer
                            xAOD::Type::Electron,       //telling the rebuilder that this is electron met
                            m_newMetContainer,            //filling this met container
                            metElectrons.asDataVector(),//using these metElectrons that accepted our cuts
                            m_metMap)                     //and this association map
                            );
    
    //Muons
    ConstDataVector<xAOD::MuonContainer> metMuons(SG::VIEW_ELEMENTS);
    for(const auto& mu : *m_muons) {
        if(mu->pt()>10e3 && mu->eta()<2.7 && m_muonSelectionMedium->accept( *mu ) ) metMuons.push_back(mu);
    }
    
    ANA_CHECK( m_metutil->rebuildMET("RefMuon",
                                    xAOD::Type::Muon,
                                    m_newMetContainer,
                                    metMuons.asDataVector(),
                                    m_metMap)
                                    );
    
    
    //Now time to rebuild jetMet and get the soft term
    //This adds the necessary soft term for both CST and TST
    //these functions create an xAODMissingET object with the given names inside the container
    ANA_CHECK( m_metutil->rebuildJetMET("RefJet",        //name of jet met
                                   "SoftClus",      //name of soft cluster term met
                                   "PVSoftTrk",     //name of soft track term met
                                   m_newMetContainer, //adding to this new met container
                                   m_jets,          //using this jet collection to calculate jet met
                                   m_coreMet,         //core met container
                                   m_metMap,          //with this association map
                                   false            //don't apply jet jvt cut
                                   )
           );
    
    ANA_CHECK( m_metutil->buildMETSum("FinalClus", m_newMetContainer, MissingETBase::Source::LCTopo) );
    m_METmaker = ( *m_newMetContainer )["FinalClus"];
    met_met = m_METmaker->met();
    met_phi = m_METmaker->phi();    
    
    event_NvtxReco = m_PVs->size();
    
    // BC_distanceFromFront = Accessor_BC_distanceFromFront( *m_eventInfo );
    // BC_filledBunches     = Accessor_BC_filledBunches( *m_eventInfo );
    
    //============================================================================
    // Find the highest b-tag scores (MV2c10 and DL1)
    //============================================================================
    
    if ( m_fillBtags ) {
        ANA_MSG_DEBUG("::fillNtuple: Filling btag info...");
    
        const xAOD::BTagging* btag;
        double tagweight;
        jets_highestBtagMV2c10 = -999.0;
        jets_highestBtagDL1_pb = -999.0;
        jets_highestBtagDL1_pc = -999.0;
        jets_highestBtagDL1_pu = -999.0;
    
        for ( const xAOD::Jet* jet : *m_jets ) {
            if (!(btag = jet->btagging())) continue;
    
            if ( btag->MVx_discriminant( "MV2c10", tagweight ) && tagweight > jets_highestBtagMV2c10 ) {
                jets_highestBtagMV2c10 = tagweight;
            }
            if ( btag->pb( "DL1", tagweight ) && tagweight > jets_highestBtagDL1_pb ) {
                jets_highestBtagDL1_pb = tagweight;
            }
            if ( btag->pc( "DL1", tagweight ) && tagweight > jets_highestBtagDL1_pc ) {
                jets_highestBtagDL1_pc = tagweight;
            }
            if ( btag->pu( "DL1", tagweight ) && tagweight > jets_highestBtagDL1_pu ) {
                jets_highestBtagDL1_pu = tagweight;
            }
        }
    }
    
    ANA_MSG_DEBUG("::execute: Splitting train, test, val");     
    // get a random number which decides whether the particle ends up in the train (80%), validation (10%), test (10%) set
    float random_num = m_rnd->Rndm();
    if( random_num <= 0.8 ){
        settype = 0;
    }
    else if ( random_num <= 0.9 ){
        settype = 1;
    }
    else{
        settype = 2;
    }
    

    //============================================================================
    // Selections
    //============================================================================

    ANA_MSG_DEBUG("::execute: Peforming selections...");

    if ( m_analysisType == 0 ) {
        ANA_MSG_DEBUG("::execute: AnalysisType = 0");
        
        //============================================================================
        // Find tags
        //============================================================================
        
        auto preTagElectrons = preSelectTagElectrons();
        auto tagMuons = selectTagMuons();
        auto tagElectrons = selectTagElectrons( preTagElectrons );
        auto tagJpsiElectrons = selectJpsiTagElectrons( preTagElectrons );
        
        //============================================================================
        // Find probes
        //============================================================================
        
        // Probe electrons 
        auto probeJpsiElectrons = selectJpsiProbeElectrons(m_electrons, true);
        auto probeCentralElectrons = selectZProbeElectrons( probeJpsiElectrons );
        
        
        //============================================================================
        // Statistics
        //============================================================================
        
        ntagElectrons = tagElectrons.size();
        ntagMuons = tagMuons.size();
        ntagJpsiElectrons = tagJpsiElectrons.size();
        
        nprobeCentralElectrons = probeCentralElectrons.size();
        nprobeJpsiElectrons = probeJpsiElectrons.size();
        
        m_treeStatistics->Fill();
        
        bool jump_el = false;
        
        // if there is a tag muon and a tag electron ( either Z or Jpsi ) we shouldn't use that event for anything
        if ( ( ntagMuons > 0 ) && ( ( ntagElectrons > 0 ) || ( ntagJpsiElectrons > 0 ) ) ){
            jump_el = true;
        }
        
        
        std::vector< std::pair< particleWrapper, particleWrapper > > leptonPairs;
        std::vector< std::pair< particleWrapper, particleWrapper > > SS_leptonPairs;
        std::vector< std::pair< particleWrapper, particleWrapper > > muePairs;        
        std::vector< std::pair< particleWrapper, particleWrapper > > SS_muePairs;        
        std::vector< std::pair< particleWrapper, particleWrapper > > leptonJpsiPairs;
        
        
        //============================================================================
        // Get prescale weights
        //============================================================================         
        ANA_MSG_DEBUG("::execute: Get Prescale weights");
        if ( event_isMC ){        
            JpsiWeight = m_PileupReweighting->getCombinedWeight( *m_eventInfo, "HLT_e5_lhtight_nod0_e4_etcut_Jpsiee||HLT_e9_lhtight_nod0_e4_etcut_Jpsiee||HLT_e14_lhtight_nod0_e4_etcut_Jpsiee||HLT_e5_lhtight_nod0_e9_etcut_Jpsiee||HLT_e5_lhtight_nod0_e14_etcut_Jpsiee"); // 
            bkgWeight = m_PileupReweighting->getCombinedWeight( *m_eventInfo, "HLT_e5_etcut||HLT_e10_etcut||HLT_e15_etcut||HLT_e20_etcut||HLT_e25_etcut||HLT_e30_etcut||HLT_e40_etcut||HLT_e50_etcut||HLT_e60_etcut||HLT_e80_etcut||HLT_e100_etcut||HLT_e120_etcut");                
        }
        combinedWeight = event_pileupweight;
        
        //============================================================================
        // Z->ee tag and probe
        //============================================================================    
        ANA_MSG_DEBUG("::execute: Z ee tag and probe");
        if ( ( !jump_el ) && (ntagElectrons > 0) && ( nprobeCentralElectrons > 0 ) ) {
            // Combination of tag and probe electrons
            std::tie( leptonPairs, SS_leptonPairs ) = selectLeptonPairs( tagElectrons, probeCentralElectrons );
        
        
            // Exactly one pair is required ( where the label switch is allowed )
            if (
                ( leptonPairs.size() > 2 ) ||
                ( leptonPairs.size() == 2 && !(leptonPairs[0].first.sameAs( leptonPairs[1].second ) && leptonPairs[1].first.sameAs( leptonPairs[0].second )) )
             ) {
                jump_el = true;
             }
        
             // fill the lepton pairs into the tree
             else if( leptonPairs.size() > 0) {
                for ( const auto& Z : leptonPairs ) {
                    resetRestOfBranchVariables();
                    // veto JF events where either the tag or the probe are truth electrons to get rid of the Z/W decays in JF
                    if ( event_isMC ){
                        if ( event_mcChannelNumber == 423300 || event_mcChannelNumber == 423301 || event_mcChannelNumber == 423302 || event_mcChannelNumber == 423303 ) {
                            const SG::AuxElement& tagAuxElementRef = *( ( &( Z.first ) )->getAuxElement());
                            bool truth_tag = isTrueElectron(Accessor_truthType( tagAuxElementRef ), Accessor_truthOrigin( tagAuxElementRef ), Accessor_firstEgMotherTruthOrigin( tagAuxElementRef ));
                            const SG::AuxElement& probeAuxElementRef = *( ( &( Z.second ) )->getAuxElement());
                            bool truth_probe = isTrueElectron(Accessor_truthType( probeAuxElementRef ), Accessor_truthOrigin( probeAuxElementRef ), Accessor_firstEgMotherTruthOrigin( probeAuxElementRef ));
                            if ( truth_tag || truth_probe ) continue;
                        }
                    }
                    fillNtuple( &( Z.first ), &( Z.second ) );
                    m_el_tree->Fill();
                    jump_el = true;
                }
            }
        
            // fill same sign bkg when no OS T&P pair is found
            else{
                if ( SS_leptonPairs.size() > 0 ){
                    for ( const auto& Z : SS_leptonPairs ) {
                        resetRestOfBranchVariables();
                        // veto JF events where either the tag or the probe are truth electrons to get rid of the Z/W decays in JF
                        if ( event_isMC ){
                            if ( event_mcChannelNumber == 423300 || event_mcChannelNumber == 423301 || event_mcChannelNumber == 423302 || event_mcChannelNumber == 423303 ) {
                                const SG::AuxElement& tagAuxElementRef = *( ( &( Z.first ) )->getAuxElement());
                                bool truth_tag = isTrueElectron(Accessor_truthType( tagAuxElementRef ), Accessor_truthOrigin( tagAuxElementRef ), Accessor_firstEgMotherTruthOrigin( tagAuxElementRef ));
                                const SG::AuxElement& probeAuxElementRef = *( ( &( Z.second ) )->getAuxElement());
                                bool truth_probe = isTrueElectron(Accessor_truthType( probeAuxElementRef ), Accessor_truthOrigin( probeAuxElementRef ), Accessor_firstEgMotherTruthOrigin( probeAuxElementRef ));
                                if ( truth_tag || truth_probe ) continue;
                            }
                        }
                        fillNtuple( &( Z.first ), &( Z.second ) );
                        m_el_tree->Fill();
                    }                
                }
            }
        }
        
        
        //============================================================================
        // Z->mue tag and probe
        //============================================================================    
        ANA_MSG_DEBUG("::execute: Z mue tag and probe");        
        if ( ( !jump_el ) && (tagMuons.size() > 0) && ( probeCentralElectrons.size() > 0 ) ) {
            // Combination of tag and probe electrons
            std::tie( muePairs, SS_muePairs ) = selectLeptonPairs( tagMuons, probeCentralElectrons );
        
            // doesn't matter how many pairs 
            // OS
            for ( const auto& Z :  muePairs ) {
                resetRestOfBranchVariables();
                // veto JF events where either the tag or the probe are truth electrons to get rid of the Z/W decays in JF
                if ( event_isMC ){
                    if ( event_mcChannelNumber == 423300 || event_mcChannelNumber == 423301 || event_mcChannelNumber == 423302 || event_mcChannelNumber == 423303 ) {
                        const SG::AuxElement& tagAuxElementRef = *( Z.first.getAuxElement() );
                        // bool truth_tag = isTrueElectron(Accessor_truthType( tagAuxElementRef ), Accessor_truthOrigin( tagAuxElementRef ), Accessor_firstEgMotherTruthOrigin( tagAuxElementRef ));
                        bool truth_tag = Accessor_truthType( tagAuxElementRef ) == 6;
                        const SG::AuxElement& probeAuxElementRef = *( Z.second.getAuxElement() );
                        bool truth_probe = isTrueElectron(Accessor_truthType( probeAuxElementRef ), Accessor_truthOrigin( probeAuxElementRef ), Accessor_firstEgMotherTruthOrigin( probeAuxElementRef ));
                        if ( truth_tag || truth_probe ) continue;
                    }
                }
                fillNtuple( &( Z.first ), &( Z.second ) );
                m_el_tree->Fill();
            }
            // SS
            for ( const auto& Z :  SS_muePairs ) {
                resetRestOfBranchVariables();
                // veto JF events where either the tag or the probe are truth electrons to get rid of the Z/W decays in JF                
                if ( event_isMC ){
                    if ( event_mcChannelNumber == 423300 || event_mcChannelNumber == 423301 || event_mcChannelNumber == 423302 || event_mcChannelNumber == 423303 ) {
                        const SG::AuxElement& tagAuxElementRef = *( Z.first.getAuxElement() );
                        // bool truth_tag = isTrueElectron(Accessor_truthType( tagAuxElementRef ), Accessor_truthOrigin( tagAuxElementRef ), Accessor_firstEgMotherTruthOrigin( tagAuxElementRef ));
                        bool truth_tag = Accessor_truthType( tagAuxElementRef ) == 6;
                        const SG::AuxElement& probeAuxElementRef = *( Z.second.getAuxElement() );
                        bool truth_probe = isTrueElectron(Accessor_truthType( probeAuxElementRef ), Accessor_truthOrigin( probeAuxElementRef ), Accessor_firstEgMotherTruthOrigin( probeAuxElementRef ));
                        if ( truth_tag || truth_probe ) continue;
                    }
                }
                fillNtuple( &( Z.first ), &( Z.second ) );
                m_el_tree->Fill();
            }
        }        
        
        
        //============================================================================
        // Jpsi->ee tag and probe
        //============================================================================
        ANA_MSG_DEBUG("::execute: Jpsi ee tag and probe");        
        if ( !jump_el && tagJpsiElectrons.size() > 0 && probeJpsiElectrons.size() > 0 ){
            leptonJpsiPairs = selectJpsiLeptonPairs( tagJpsiElectrons, probeJpsiElectrons );
                if (
                    ( leptonJpsiPairs.size() > 2 ) ||
                    ( leptonJpsiPairs.size() == 2 && !(leptonJpsiPairs[0].first.sameAs( leptonJpsiPairs[1].second ) && leptonJpsiPairs[1].first.sameAs( leptonJpsiPairs[0].second )) )
                 ) {
                     jump_el = true;
                 }
            }
        
            if ( leptonJpsiPairs.size() > 0 ){
                if ( event_isMC ){
                    combinedWeight = JpsiWeight;
                }
                for ( const auto& Jpsi : leptonJpsiPairs ) {
                    resetRestOfBranchVariables();
                    // veto JF events where either the tag or the probe are truth electrons to get rid of the Z/W decays in JF                    
                     if ( event_isMC ){
                         if ( event_mcChannelNumber == 423300 || event_mcChannelNumber == 423301 || event_mcChannelNumber == 423302 || event_mcChannelNumber == 423303 ) {
                             const SG::AuxElement& tagAuxElementRef = *( Jpsi.first.getAuxElement() );
                             bool truth_tag = isTrueElectron(Accessor_truthType( tagAuxElementRef ), Accessor_truthOrigin( tagAuxElementRef ), Accessor_firstEgMotherTruthOrigin( tagAuxElementRef ));
                             const SG::AuxElement& probeAuxElementRef = *( Jpsi.second.getAuxElement() );
                             bool truth_probe = isTrueElectron(Accessor_truthType( probeAuxElementRef ), Accessor_truthOrigin( probeAuxElementRef ), Accessor_firstEgMotherTruthOrigin( probeAuxElementRef ));
                             if ( truth_tag || truth_probe ) continue;
                         }
                     }
                    fillNtuple( &( Jpsi.first ), &( Jpsi.second ) );
                    m_el_tree->Fill();
                    jump_el = true;
                }
            }
        
        //============================================================================
        // Background selection
        //============================================================================
        ANA_MSG_DEBUG("::execute: Bkg selection");        
        if ( !jump_el && met_met > 25000.0 ) {
                jump_el = true;
        }
            // Still need Z ee veto and W ev veto
        if ( !jump_el ){
            for ( const auto& el : *m_electrons ) {
                const float mTrans = sqrt( 2.0 * met_met * el->pt() * ( 1.0 - cos( TVector2::Phi_mpi_pi( el->phi() - met_phi ) ) ) );
                if ( mTrans > 50e3 && mTrans < 100e3 ) jump_el = true;  // W veto
        
                for ( const auto& el_other : *m_electrons ) {
                    if ( el == el_other ) continue;
                    if ( el_other == nullptr ) continue;
                    if ( !m_electronSelectionToolMedium->accept( el_other ) ) continue;
                    const float invariant_m = ( el->p4()+el_other->p4() ).M();
                    if ( invariant_m < 110e3 && invariant_m > 70e3 ) jump_el = true; // Z veto
                }
            }
        }
        
        if ( !jump_el ){
            ANA_MSG_DEBUG("::execute: CentralElectrons = "<<m_electrons->size()<<", photons = "<<m_photons->size() );
            auto backgroundParticles = selectBackground( probeJpsiElectrons );
            ANA_MSG_DEBUG("::execute: backgroundParticles found = "<<backgroundParticles.size() );
        
            // for some reason the prescale seems to not always work for high eta. For now if there is no prescale applied, don't save that event
            if ( event_isMC ){
                if ( abs(bkgWeight - event_pileupweight) > 1e-3 ) {
                    combinedWeight = bkgWeight;
                }
                else{
                    jump_el = true;
                }
            }
            if ( !jump_el){
                for ( const auto& el : backgroundParticles ) {
                    resetRestOfBranchVariables();
                    // veto JF events where either the tag or the probe are truth electrons to get rid of the Z/W decays in JF                    
                    if ( event_isMC ){
                        if ( event_mcChannelNumber == 423300 || event_mcChannelNumber == 423301 || event_mcChannelNumber == 423302 || event_mcChannelNumber == 423303 ) {
                            const SG::AuxElement& probeAuxElementRef = *( el.getAuxElement() );
                            bool truth_probe = isTrueElectron(Accessor_truthType( probeAuxElementRef ), Accessor_truthOrigin( probeAuxElementRef ), Accessor_firstEgMotherTruthOrigin( probeAuxElementRef ));
                            if ( truth_probe ) continue;
                        }
                    }
                    fillNtuple( nullptr, &el );
                    m_el_tree->Fill();
                }
            }
        }
        
        //============================================================================
        // Photon selection
        //============================================================================
        ANA_MSG_DEBUG("::execute: Photon dumping");        
        if ( event_isMC ){
            for ( const auto& ph : *m_photons ) {
                const auto pw = particleWrapper( ph );
                const SG::AuxElement& pwAuxElementRef = *(pw.getAuxElement());
                // Always keep true photons
                if ( !( Accessor_truthType ( pwAuxElementRef ) == 14 || Accessor_truthType ( pwAuxElementRef ) == 15 || Accessor_truthType ( pwAuxElementRef ) == 16 ) )  {
                    // If not true electrons or photons, keep every third junk particle except for the very, very low pt ones
                    if ( ph->pt() < 4.5e3 ) continue;
                    if ( prescalingCounter++ % 3 != 0 ) continue;
                }
                resetRestOfBranchVariables();
                fillNtuple( nullptr, &pw );
                m_ga_tree->Fill();
            }
        }          

    } else if ( m_analysisType == 1 ) {
        ANA_MSG_DEBUG("::execute: AnalysisType = 1");
        //============================================================================
        // Dumping reco objects from egamma truth container
        //============================================================================

        static SG::AuxElement::ConstAccessor< ElementLink< xAOD::ElectronContainer > > accessorel( "recoElectronLink" );
        static SG::AuxElement::ConstAccessor< ElementLink< xAOD::PhotonContainer > > accessorph( "recoPhotonLink" );
        // const xAOD::Electron* el_link = nullptr;
        const xAOD::Photon* ph_link = nullptr;
        const xAOD::TruthParticleContainer* m_egammaTruthContainer = nullptr;
        ANA_CHECK( event->retrieve( m_egammaTruthContainer, "egammaTruthParticles" ) );
        for ( const xAOD::TruthParticle* tp : *m_egammaTruthContainer ) {
            if ( tp->absPdgId() == 11 || tp->absPdgId() == 22 ) {
                // if ( accessorel.isAvailable( *tp ) ) {
                //     const ElementLink< xAOD::ElectronContainer >& link = accessorel( *tp );
                //     if ( link.isValid() ) {
                //         el_link = *link;
                //         resetRestOfBranchVariables();
                //         const auto pw = particleWrapper( el_link, !(el_link->author(xAOD::EgammaParameters::AuthorFwdElectron)) );
                //         fillNtuple( nullptr, &pw );
                //         m_el_tree->Fill();
                //     }
                // }
                if ( accessorph.isAvailable( *tp ) ) {
                    const ElementLink< xAOD::PhotonContainer >& link = accessorph( *tp );
                    if ( link.isValid() ) {
                        ph_link = *link;
                        resetRestOfBranchVariables();
                        const auto pw = particleWrapper( m_photons->get(ph_link->index()) );
                        fillNtuple( nullptr, &pw );
                        m_ga_tree->Fill();
                    }
                }
            }
        }



    } else if ( m_analysisType == 2 ) {
        ANA_MSG_DEBUG("::execute: AnalysisType = 2");
        //============================================================================
        // Find background
        //============================================================================

        ANA_MSG_DEBUG("::execute: CentralElectrons = "<<m_electrons->size()<<", photons = "<<m_photons->size() );
        // auto backgroundParticles = selectBackground();
        // ANA_MSG_DEBUG("::execute: backgroundParticles found = "<<backgroundParticles.size() );
        // 
        // for ( const auto& el : backgroundParticles ) {
        //     resetRestOfBranchVariables();
        //     fillNtuple( nullptr, &el );
        //     m_el_tree->Fill();
        // }



    } else if ( m_analysisType == 3 ) {
        ANA_MSG_DEBUG("::execute: AnalysisType = 3");
        //============================================================================
        // Dumping reco objects with no selections applied
        //============================================================================

        for ( const auto& el :  *m_electrons  ) {
            const xAOD::TruthParticle* tp = xAOD::TruthHelpers::getTruthParticle( *static_cast<const xAOD::IParticle*>(el) );
            // Always keep true electrons and photons
            if ( tp == nullptr || ( tp->absPdgId() != 11 && tp->pdgId() != 22 ) ) {
                // If not true electrons or photons, keep every third junk particle except for the very, very low pt ones
                if ( el->pt() < 3.5e3 ) continue;
                if ( prescalingCounter++ % 3 != 0 ) continue;
            }
            resetRestOfBranchVariables();
            const auto pw = particleWrapper( el, !(el->author(xAOD::EgammaParameters::AuthorFwdElectron)) );
            fillNtuple( nullptr, &pw );
            m_el_tree->Fill();
        }

        for ( const auto& ph : *m_photons ) {
            const xAOD::TruthParticle* tp = xAOD::TruthHelpers::getTruthParticle( *static_cast<const xAOD::IParticle*>(ph) );
            // Always keep true electrons and photons
            if ( tp == nullptr || ( tp->absPdgId() != 11 && tp->pdgId() != 22 ) ) {
                // If not true electrons or photons, keep every third junk particle except for the very, very low pt ones
                if ( ph->pt() < 4.5e3 ) continue;
                if ( prescalingCounter++ % 10 != 0 ) continue;
            }
            resetRestOfBranchVariables();
            const auto pw = particleWrapper( ph );
            fillNtuple( nullptr, &pw );
            m_ga_tree->Fill();
        }


        
    } else if ( m_analysisType == 4 ) {
        std::vector< particleWrapper> truthEl = selectTruthElectrons();
        for ( const auto pw : truthEl){
            resetRestOfBranchVariables();
            fillNtuple( nullptr, &pw );
            m_el_tree->Fill();
        }

        std::vector< particleWrapper> truthGa = selectTruthPhotons();
        for ( const auto pw : truthGa){
            resetRestOfBranchVariables();
            fillNtuple( nullptr, &pw );
            m_ga_tree->Fill();
        }        
        

    } else {
        ANA_MSG_FATAL("::execute: AnalysisType = ?");
        //============================================================================
        // Bad analysisType was selected
        //============================================================================
        return EL::StatusCode::FAILURE;
    }

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyTagAndProbeAnalysis::postExecute() {
    // Here you do everything that needs to be done after the main event
    // processing.  This is typically very rare, particularly in user
    // code.  It is mainly used in implementing the NTupleSvc.
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyTagAndProbeAnalysis::finalize() {
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.  This is different from histFinalize() in that it only
    // gets called on worker nodes that processed input events.

    #pragma GCC diagnostic push
    LESS_DIAGNOSTICS()
    ANA_CHECK_SET_TYPE( EL::StatusCode );
    #pragma GCC diagnostic pop

    xAOD::TEvent* event = wk()->xaodEvent();

    // Finalize and close our output xAOD file
    TFile* file = wk()->getOutputFile( m_outputName );
    ANA_CHECK( event->finishWritingTo( file ) );

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyTagAndProbeAnalysis::histFinalize() {
    // This method is the mirror image of histInitialize(), meaning it
    // gets called after the last event has been processed on the worker
    // node and allows you to finish up any objects you created in
    // histInitialize() before they are written to disk.  This is
    // actually fairly rare, since this happens separately for each
    // worker node.  Most of the time you want to do your
    // post-processing on the submission node after all your histogram
    // outputs have been merged.  This is different from finalize() in
    // that it gets called on all worker nodes regardless of whether
    // they processed input events.


    return EL::StatusCode::SUCCESS;
}
