#include <MyAnalysis/MyTagAndProbeAnalysis.h>
#include <EventLoop/Worker.h>

// Creates all the histograms that should be filled during the selection, very specific to this analysis
void MyTagAndProbeAnalysis::hists_for_cuts ( TString suffix, long unsigned int i, bool signal){
    std::vector < TString > suffices;
    // for signal fill histograms with all signal selected, signal selected which are selected using the Z tag and probe, and signal selected with J/psi tag and probe
    if ( signal ){
        suffices.push_back(suffix);
        suffices.push_back("_Z" + suffix);
        suffices.push_back("_Jpsi" + suffix);
    }
    // for background fill histograms with all bkg, bkg where no T&P is applied, bkg from the mue selection, bkg from Zee selection with same sign
    else{
        suffices.push_back(suffix);
        suffices.push_back("_noTP" + suffix);
        suffices.push_back("_mue" + suffix); 
        suffices.push_back("_SS" + suffix);                
    }
    // loop over different selections ( all, only window around ll_mass, ...)
    for ( const auto& suff : suffices ){
        // loop over all variables which are floats
        for ( const hist_bins<float>* bins : m_hist_bins_float ){
            m_hists[i][bins->name + suff] = new TH1F(suff + "/" + bins->name + suff, bins->name + suff, bins->nbins, bins->xmin, bins->xmax);
            wk()->addOutput( m_hists[i][bins->name + suff]);
            // fill also histograms where particles with negative weights are removed. In the reweighting and ML training neg weights is probably not good, so these are filled to see if they look similar to the ones with neg weights
            m_hists[i+30][bins->name + suff + "_noNegWeights"] = new TH1F( suff + "/noNegWeight/" + bins->name + suff + "_noNegWeights" , bins->name + suff + "_noNegWeights", bins->nbins, bins->xmin, bins->xmax);
            wk()->addOutput( m_hists[i+30][bins->name + suff + "_noNegWeights"]);
        }

        // loop over all variables which are doubles
        for ( const hist_bins<double>* bins : m_hist_bins_double ){
            m_hists[i][bins->name + suff] = new TH1D(suff + "/" + bins->name + suff, bins->name + suff, bins->nbins, bins->xmin, bins->xmax);
            wk()->addOutput( m_hists[i][bins->name + suff]);
            m_hists[i+30][bins->name + suff + "_noNegWeights"] = new TH1D( suff + "/noNegWeight/" + bins->name + suff + "_noNegWeights", bins->name + suff + "_noNegWeights", bins->nbins, bins->xmin, bins->xmax);
            wk()->addOutput( m_hists[i+30][bins->name + suff + "_noNegWeights"]);
        }

        // loop over all variables which are ints
        for ( const hist_bins<int>* bins : m_hist_bins_int ){
            m_hists[i][bins->name + suff] = new TH1I(suff + "/" + bins->name + suff, bins->name + suff, bins->nbins, bins->xmin, bins->xmax);
            wk()->addOutput( m_hists[i][bins->name + suff]);
            m_hists[i+30][bins->name + suff + "_noNegWeights"] = new TH1I( suff + "/noNegWeight/" + bins->name + suff + "_noNegWeights", bins->name + suff + "_noNegWeights", bins->nbins, bins->xmin, bins->xmax);
            wk()->addOutput( m_hists[i+30][bins->name + suff + "_noNegWeights"]);
        }

        // loop over all variables which are UChar_ts
        for ( const hist_bins<UChar_t>* bins : m_hist_bins_UChart ){
            m_hists[i][bins->name + suff] = new TH1I(suff + "/" + bins->name + suff, bins->name + suff, bins->nbins, bins->xmin, bins->xmax);
            wk()->addOutput( m_hists[i][bins->name + suff]);
            m_hists[i+30][bins->name + suff + "_noNegWeights"] = new TH1I( suff + "/noNegWeight/" + bins->name + suff + "_noNegWeights", bins->name + suff + "_noNegWeights", bins->nbins, bins->xmin, bins->xmax);
            wk()->addOutput( m_hists[i+30][bins->name + suff + "_noNegWeights"]);
        }

        // loop over all variables which are bools
        for ( const hist_bins<bool>* bins : m_hist_bins_bool ){
            m_hists[i][bins->name + suff] = new TH1I(suff + "/" + bins->name + suff, bins->name + suff, bins->nbins, bins->xmin, bins->xmax);
            wk()->addOutput( m_hists[i][bins->name + suff]);
            m_hists[i+30][bins->name + suff + "_noNegWeights"] = new TH1I( suff + "/noNegWeight/" + bins->name + suff + "_noNegWeights", bins->name + suff + "_noNegWeights", bins->nbins, bins->xmin, bins->xmax);
            wk()->addOutput( m_hists[i+30][bins->name + suff + "_noNegWeights"]);
        }      
        i++;
    }
}


void MyTagAndProbeAnalysis::fill_hists( TString suffix, long unsigned int i ){
    // loop over variables which are floats
    for ( const auto& hist_stuff : m_hist_bins_float ){
        // fill histogram with the total weight
        m_hists[i][hist_stuff->name + suffix]->Fill(*(hist_stuff->var), event_totalWeight);
        // fill histogram with the total weight if it is larger than 0    
        if ( event_totalWeight > 0 ){
            m_hists[i+30][hist_stuff->name + suffix + "_noNegWeights" ]->Fill(*(hist_stuff->var), event_totalWeight);
        } 
    }
    for ( const auto& hist_stuff : m_hist_bins_double ){
        m_hists[i][hist_stuff->name + suffix]->Fill(*(hist_stuff->var), event_totalWeight);
        if ( event_totalWeight > 0 ){
                m_hists[i+30][hist_stuff->name + suffix + "_noNegWeights" ]->Fill(*(hist_stuff->var), event_totalWeight);
        } 
    }
    for ( const auto& hist_stuff : m_hist_bins_int ){
        m_hists[i][hist_stuff->name + suffix]->Fill(*(hist_stuff->var), event_totalWeight);
        if ( event_totalWeight > 0 ){
                m_hists[i+30][hist_stuff->name + suffix + "_noNegWeights" ]->Fill(*(hist_stuff->var), event_totalWeight);
        } 
    }
    for ( const auto& hist_stuff : m_hist_bins_UChart ){
        m_hists[i][hist_stuff->name + suffix]->Fill(*(hist_stuff->var), event_totalWeight);
        if ( event_totalWeight > 0 ){
                m_hists[i+30][hist_stuff->name + suffix + "_noNegWeights" ]->Fill(*(hist_stuff->var), event_totalWeight);
        } 
    }   
    for ( const auto& hist_stuff : m_hist_bins_bool ){
        m_hists[i][hist_stuff->name + suffix]->Fill(int(*(hist_stuff->var)), event_totalWeight);
        if ( event_totalWeight > 0 ){
                m_hists[i+30][hist_stuff->name + suffix + "_noNegWeights" ]->Fill(int(*(hist_stuff->var)), event_totalWeight);
        } 
    } 
}

// for MC fill histograms and additionally histograms where only true signal or true bkg is filled
void MyTagAndProbeAnalysis::fill_hists_truth(TString suffix, long unsigned int initial_idx, long unsigned int diff_idx, bool truth){
    fill_hists(suffix, initial_idx);
    if ( truth ){
    fill_hists(suffix + "_true_sig", initial_idx + diff_idx);
    }
    else{
        fill_hists(suffix + "_true_bkg", initial_idx + 2 * diff_idx);
    }        
}