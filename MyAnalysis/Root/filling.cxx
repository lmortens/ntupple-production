#include <MyAnalysis/MyTagAndProbeAnalysis.h>
#include "xAODEgamma/EgammaxAODHelpers.h"
#include <type_traits>

MORE_DIAGNOSTICS()


float getPtAtFirstMeasurement( const xAOD::TrackParticle* tp ) {
    if ( tp == nullptr ) return 0.0f;
    unsigned int index;
    if ( tp->indexOfParameterAtPosition( index, xAOD::FirstMeasurement ) )
        return std::hypot(tp->parameterPX(index), tp->parameterPY(index)); // hypotenuse with overflow safety
    return tp->pt();
}


void MyTagAndProbeAnalysis::fillNtuple( const particleWrapper* tag, const particleWrapper* probe ) {
    ANA_MSG_DEBUG("::fillNtuple: Start fillNtuple");

    // scale missing et to gev 
    met_met /= 1000. ;
    
    //============================================================================
    // Tags
    //============================================================================
 
    const SG::AuxElement& probeAuxElementRef = *(probe->getAuxElement());

    ANA_MSG_DEBUG("::fillNtuple: Cell variables with Accessors");
    // Get all the necessary cell values
    cell_energy                               = Accessor_cell_energy( probeAuxElementRef );
    cell_eta                                  = Accessor_cell_eta( probeAuxElementRef );
    cell_deta                                 = Accessor_cell_deta( probeAuxElementRef );
    cell_phi                                  = Accessor_cell_phi( probeAuxElementRef );
    cell_dphi                                 = Accessor_cell_dphi( probeAuxElementRef );
    cell_sampling                             = Accessor_cell_sampling( probeAuxElementRef );
    cell_time                                 = Accessor_cell_time( probeAuxElementRef );

    
    ANA_MSG_DEBUG("::fillNtuple: Creating images");    
    // create all the images for the particle (functions see above)
    create_images( probe );
    
    
    const xAOD::TrackParticle* t = nullptr;
    if ( tag != nullptr ) {
        ANA_MSG_DEBUG("::fillNtuple: Filling tag...");
        const TLorentzVector& tag_p4 = tag->p4();
        TLorentzVector ll = probe->p4() + tag_p4;
    
        tag_type = tag->getType();
    
        if (event_isMC ) {
            if ( tag_type == 1 ){
                double sf = 1;
                if(!m_electronSF->getEfficiencyScaleFactor(*(tag->getCentralElectron()),sf)){
                    ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
                }
                double charge_sf = 1;
                if(!m_electronChargeIdSF->getEfficiencyScaleFactor(*(tag->getCentralElectron()),charge_sf)){
                    ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
                }
    
                tag_SF = sf * charge_sf;
            }
            else if ( tag_type == 2 ){
                float id_sf = 1;
                if(!m_muonSF->getEfficiencyScaleFactor( *(tag->getMuon() ), id_sf))  ATH_MSG_WARNING( "Couldn't get muon scale factor!" );
                if(!m_muonSFIso->getEfficiencyScaleFactor( *(tag->getMuon() ), tag_SF))  ATH_MSG_WARNING( "Couldn't get muon scale factor!" );
                tag_SF *= id_sf;
            }
        }
    
        tag_e       = tag_p4.E() / 1000.;
        tag_et_calo = tag_p4.Pt() / 1000.;
        tag_eta     = tag_p4.Eta();
        tag_phi     = tag_p4.Phi();
        tag_charge  = tag->charge();
    
        t = tag->trackParticle();
        if ( t ) {
            tag_pt_track    = t->pt() / 1000.;
            tag_z0          = tag_type == 1 ? t->z0() + t->vz() - t->vertex()->z() : t->z0();
            tag_d0          = t->d0();
            tag_sigmad0     = sqrt( ( t->definingParametersCovMatrix() )( 0, 0 ));
            p_deltaR_tag    = probe->p4().DeltaR( tag->p4() );
            if ( t->vertex() ) {
                tag_vertexIndex = t->vertex()->index();
            }
        }
    
        // The ll candidate
        ll_pt             = ll.Pt() / 1000.;
        ll_eta            = ll.Eta();
        ll_phi            = ll.Phi();
        ll_m              = ll.M() / 1000.;
    
        const TLorentzVector& probe_p4 = probe->p4();
        if ( fabs( tag_phi - probe_p4.Phi() ) > 1e-6 ){
            const xAOD::TrackParticle* probe_track = probe->trackParticle();
    
    
            if ( t != nullptr && probe_track != nullptr ){
                double simpleXv = ( - probe_track->d0() * cos(tag_phi) + tag_d0 * cos(probe_p4.Phi())) / ( sin( probe_p4.Phi() - tag_phi) ) ;
                double simpleYv = ( - probe_track->d0() * sin(tag_phi) + tag_d0 * sin(probe_p4.Phi()))/ ( sin( probe_p4.Phi() - tag_phi) ) ;
    
                double f1 = fabs( tag_p4.Pt() )* cos( tag_phi ) + fabs( probe_p4.Pt() )* cos( probe_p4.Phi() ) ;
                double f2 = fabs( tag_p4.Pt() )* sin( tag_phi ) + fabs( probe_p4.Pt() )* sin( probe_p4.Phi() ) ;
                double c = sqrt( f1*f1 + f2*f2 ) ;
    
                if ( fabs( c ) >= 1e-6 ) {
                    double a = ( simpleXv ) * f1 ;
                    double b = ( simpleYv ) * f2 ;
    
                    double lxy = ( a + b ) / c ;
                    if ( ll_m > 50000 ){
                        ll_lifetime       = lxy * m_mZ /(0.299792458 * ll_pt * 1000 );
                    }
                    else{
                        ll_lifetime       = lxy * 3096.916/(0.299792458 * ll_pt * 1000 );
                    }
                }
            }
        }
    }
    
    
    //============================================================================
    // Probe kinematics, standard variables, and iso
    //============================================================================
    
    ANA_MSG_DEBUG("::fillNtuple: Filling probe info...");
    
    // Four vector and ref for aux elements
    const TLorentzVector& probe_p4 = probe->p4();
//    const SG::AuxElement& probeAuxElementRef = *(probe->getAuxElement());
    
    // Meta
    p_type              = probe->getType();
    
    // Kinematics
    p_e                 = probe_p4.E() / 1000. ;
    p_et_calo           = probe_p4.Pt() / 1000. ;
    p_eta               = probe_p4.Eta();
    p_phi               = probe_p4.Phi();
    p_charge            = probe->charge();
    
    if ( event_isMC ){
        p_calibratedE = Accessor_calibratedE( probeAuxElementRef ) / 1000. ;
    }
    
    // Standard PID variables
    p_weta2             = Accessor_weta2( probeAuxElementRef );
    p_Reta              = Accessor_Reta( probeAuxElementRef );
    p_Rphi              = Accessor_Rphi( probeAuxElementRef );
    p_Eratio            = Accessor_Eratio( probeAuxElementRef );
    p_f1                = Accessor_f1( probeAuxElementRef );
    p_f3                = Accessor_f3( probeAuxElementRef );
    p_Rhad              = Accessor_Rhad( probeAuxElementRef );
    p_Rhad1             = Accessor_Rhad1( probeAuxElementRef );
    p_deltaEta1         = ( probe->isCentralElectron() ) ? Accessor_deltaEta1( probeAuxElementRef ) : -1000.0f;
    p_deltaPhiRescaled2 = ( probe->isCentralElectron() ) ? Accessor_deltaPhiRescaled2( probeAuxElementRef ) : -1000.0f;
    p_LHValue           = ( probe->isCentralElectron() )  ? Accessor_LHValue( probeAuxElementRef ) : -1000.0f;
    
    // Iso for all probes
    p_topoetcone20                 = Accessor_topoetcone20( probeAuxElementRef ) / 1000. ;
    p_topoetcone30                 = Accessor_topoetcone30( probeAuxElementRef ) / 1000. ;
    p_topoetcone40                 = Accessor_topoetcone40( probeAuxElementRef ) / 1000. ;
    
    p_etcone20                     = Accessor_etcone20( probeAuxElementRef ) / 1000. ;
    p_etcone30                     = Accessor_etcone30( probeAuxElementRef ) / 1000. ;
    p_etcone40                     = Accessor_etcone40( probeAuxElementRef ) / 1000. ;
    p_etcone20ptCorrection         = Accessor_etcone20ptCorrection( probeAuxElementRef ) / 1000. ;
    p_etcone30ptCorrection         = Accessor_etcone30ptCorrection( probeAuxElementRef ) / 1000. ;
    p_etcone40ptCorrection         = Accessor_etcone40ptCorrection( probeAuxElementRef ) / 1000. ;
    p_ptcone20                     = Accessor_ptcone20( probeAuxElementRef ) / 1000. ;
    p_ptcone30                     = Accessor_ptcone30( probeAuxElementRef ) / 1000. ;
    p_ptcone40                     = Accessor_ptcone40( probeAuxElementRef ) / 1000. ;
    
    p_ptvarcone20                  = Accessor_ptvarcone20( probeAuxElementRef ) / 1000. ;
    p_ptvarcone30                  = Accessor_ptvarcone30( probeAuxElementRef ) / 1000. ;
    p_ptvarcone40                  = Accessor_ptvarcone40( probeAuxElementRef ) / 1000. ;
    p_ptcone20_TightTTVA_pt500     = Accessor_ptcone20_TightTTVA_pt500( probeAuxElementRef ) / 1000. ;
    p_ptcone30_TightTTVA_pt500     = Accessor_ptcone30_TightTTVA_pt500( probeAuxElementRef ) / 1000. ;
    p_ptcone40_TightTTVA_pt500     = Accessor_ptcone40_TightTTVA_pt500( probeAuxElementRef ) / 1000. ;
    p_ptvarcone20_TightTTVA_pt500  = Accessor_ptvarcone20_TightTTVA_pt500( probeAuxElementRef ) / 1000. ;
    p_ptvarcone30_TightTTVA_pt500  = Accessor_ptvarcone30_TightTTVA_pt500( probeAuxElementRef ) / 1000. ;
    p_ptvarcone40_TightTTVA_pt500  = Accessor_ptvarcone40_TightTTVA_pt500( probeAuxElementRef ) / 1000. ;
    p_ptcone20_TightTTVA_pt1000    = Accessor_ptcone20_TightTTVA_pt1000( probeAuxElementRef ) / 1000. ;
    p_ptcone30_TightTTVA_pt1000    = Accessor_ptcone30_TightTTVA_pt1000( probeAuxElementRef ) / 1000. ;
    p_ptcone40_TightTTVA_pt1000    = Accessor_ptcone40_TightTTVA_pt1000( probeAuxElementRef ) / 1000. ;
    p_ptvarcone20_TightTTVA_pt1000 = Accessor_ptvarcone20_TightTTVA_pt1000( probeAuxElementRef ) / 1000. ;
    p_ptvarcone30_TightTTVA_pt1000 = Accessor_ptvarcone30_TightTTVA_pt1000( probeAuxElementRef ) / 1000. ;
    p_ptvarcone40_TightTTVA_pt1000 = Accessor_ptvarcone40_TightTTVA_pt1000( probeAuxElementRef ) / 1000. ;
    p_topoetcone20ptCorrection     = Accessor_topoetcone20ptCorrection( probeAuxElementRef ) / 1000. ;
    p_topoetcone30ptCorrection     = Accessor_topoetcone30ptCorrection( probeAuxElementRef ) / 1000. ;
    p_topoetcone40ptCorrection     = Accessor_topoetcone40ptCorrection( probeAuxElementRef ) / 1000. ;
    
    
    
    //============================================================================
    // Fill probe track variables
    //============================================================================
    
    ANA_MSG_DEBUG("::fillNtuple: Filling probe track info...");
    
    p_nTracks = probe->nTrackParticles();
    
    const xAOD::TrackParticle* tt = probe->trackParticle();
    if ( tt ) {
        p_hasTrack = true;
        p_pt_track = tt->pt() / 1000. ;
    
        if ( tt->vertex() != nullptr )
            p_vertexIndex = tt->vertex()->index();
    
        p_TRTTrackOccupancy          = Accessor_TRTTrackOccupancy( *tt );
        p_numberOfTRTXenonHits       = Accessor_numberOfTRTXenonHits( *tt );
    
        p_numberOfInnermostPixelHits = Accessor_numberOfInnermostPixelLayerHits( *tt );
        p_numberOfPixelHits          = Accessor_numberOfPixelHits( *tt ) + Accessor_numberOfPixelDeadSensors( *tt );
        p_numberOfSCTHits            = Accessor_numberOfSCTHits( *tt ) + Accessor_numberOfSCTDeadSensors( *tt );
        p_numberOfTRTHits            = Accessor_numberOfTRTHits( *tt );
    
        p_z0       = tt->z0() + tt->vz() - tt->vertex()->z();
        p_d0       = tt->d0();
        p_sigmad0  = sqrt( ( tt->definingParametersCovMatrix() )( 0, 0 ) );
        p_d0Sig    = ( p_sigmad0 > 0.0f ) ? p_d0 / p_sigmad0 : -999.f;
        p_EptRatio = ( p_pt_track > 0.0f ) ? p_et_calo  / p_pt_track : -999.f;
        p_qOverP   = tt->qOverP();
        p_chi2     = tt->chiSquared();
        p_ndof     = tt->numberDoF();
        p_z0theta  = abs(p_z0 * sin(tt->theta()));
    
        unsigned int index;
        if ( tt->indexOfParameterAtPosition( index, xAOD::LastMeasurement ) ) {
            const double refittedTrack_LMqoverp = double( p_charge ) / sqrt( std::pow( tt->parameterPX( index ), 2 ) +
                    std::pow( tt->parameterPY( index ), 2 ) +
                    std::pow( tt->parameterPZ( index ), 2 ));
            p_dPOverP = 1.0 - p_qOverP/refittedTrack_LMqoverp;
            if ( p_dPOverP < -4.0 ) p_dPOverP = -4.0;
        } else {
            p_dPOverP = -999.f;
        }
        
        // p_qOverP /= 1000.0 ;
    
        bool transTRTPID = true;
        if ( transTRTPID == true ) {
            constexpr double tau = 15.0;
            constexpr double fEpsilon = 1.0e-30;  // to avoid zero division
            //Transform the TRT PID output for use in the LH tool.
            tt->summaryValue( p_eProbHT, ( xAOD::SummaryType )48 ); // type 48 = eProbabilityHT
            double pid_tmp = p_eProbHT;
            if ( pid_tmp >= 1.0 ) pid_tmp = 1.0 - 1.0e-15;  //this number comes from TMVA
            else if ( pid_tmp <= fEpsilon ) pid_tmp = fEpsilon;
            p_eProbHT = -log( 1.0/pid_tmp - 1.0 )*( 1./double( tau ));
        }
        else {
            tt->summaryValue( p_eProbHT, ( xAOD::SummaryType )48 ); // type 48 = eProbabilityHT
        }
    }
    
    
    //============================================================================
    // Fill misc. probe variables, cluster stuff, from tools, etc.
    //============================================================================
    
    ANA_MSG_DEBUG("::fillNtuple: Filling misc. info...");
    
    p_mTransW = sqrt( 2.0 * met_met * p_et_calo * ( 1.0 - cos( TVector2::Phi_mpi_pi( p_phi - met_phi ) ) ) );
    
    ANA_MSG_DEBUG("::fillNtuple: Filling probe cluster info...");
    // Cluster vars taken from https://gitlab.cern.ch/atlas/athena/blob/master/Reconstruction/egamma/egammaMVACalib/egammaMVACalib/egammaMVAFunctions.h (and its source file)
    
    const xAOD::CaloCluster* cluster = probe->caloCluster();
    if ( cluster ) {
        p_etaCluster       = cluster->eta();
        p_phiCluster       = cluster->phi();
        p_eCluster         = cluster->e() / 1000. ;
        p_etCluster        = p_eCluster / cosh(p_eta);
        p_RawEtaCluster    = cluster->rawEta();
        p_RawPhiCluster    = cluster->rawPhi();
        p_RawECluster      = cluster->rawE() / 1000. ;
        p_eClusterLr0      = cluster->energyBE(0) / 1000. ;
        p_eClusterLr1      = cluster->energyBE(1) / 1000. ;
        p_eClusterLr2      = cluster->energyBE(2) / 1000. ;
        p_eClusterLr3      = cluster->energyBE(3) / 1000. ;
        p_etaClusterLr1    = cluster->etaBE(1);
        p_etaClusterLr2    = cluster->etaBE(2);
        p_phiClusterLr2    = cluster->phiBE(2);
        p_eAccCluster      = p_eClusterLr1 + p_eClusterLr2 + p_eClusterLr3;
        p_f0Cluster        = (std::abs(p_eAccCluster) >= 0.0001) ? p_eClusterLr0 / p_eAccCluster : -999.0f;
        if ( !probe->isForwardElectron() ) {
            if ( !cluster->retrieveMoment( xAOD::CaloCluster::ETACALOFRAME, p_etaCalo ) )
                ANA_MSG_WARNING( "cannot find etaCalo" );
            if ( !cluster->retrieveMoment( xAOD::CaloCluster::PHICALOFRAME, p_phiCalo ) )
                ANA_MSG_WARNING( "cannot find phiCalo" );
        }
        p_eTileGap3Cluster = cluster->eSample(CaloSampling::TileGap3) / 1000. ; // float?
        p_cellIndexCluster = std::floor( std::abs( p_etaCalo/0.025 ) );
        p_phiModCalo       = ((std::abs(p_etaCalo) < 1.425) ?
            std::fmod(p_phiCalo, TMath::Pi()/512) :
            std::fmod(p_phiCalo, TMath::Pi()/384)); // float
        p_etaModCalo       = std::fmod(std::abs(p_etaCalo), 0.025); // float
        p_dPhiTH3          = std::fmod(2.*TMath::Pi()+p_phiCalo,TMath::Pi()/32.)-TMath::Pi()/64.0; // float
        p_R12              = (std::abs(p_eClusterLr2) >= 0.0001) ? p_eClusterLr1 / p_eClusterLr2 : -999.0f; // float
        p_fTG3             = (std::abs(p_eAccCluster) >= 0.0001) ? p_eTileGap3Cluster/p_eAccCluster : -999.0f; // float
    
    }
    
    ANA_MSG_DEBUG("::fillNtuple: Filling probe ECIDS info...");
    
    if ( probe->isCentralElectron()  && tt != nullptr ) {
        p_ECIDSResult = m_ECIDSToolLoose->calculate( probe->getCentralElectron() );
    }
    
    
    // Converted photon vars taken from https://gitlab.cern.ch/atlas/athena/blob/master/Reconstruction/egamma/egammaMVACalib/egammaMVACalib/egammaMVAFunctions.h (and its source file)
    if ( probe->isPhoton() ) {
        const xAOD::Photon* ph = probe->getPhoton();
        ANA_MSG_DEBUG("::fillNtuple: Filling probe photon EM selector info...");
        p_photonIsTightEM = m_photonTightIsEMSelector->accept( ph );
        p_photonIsLooseEM = m_photonLooseIsEMSelector->accept( ph );
    
        ANA_MSG_DEBUG("::fillNtuple: Filling misc. probe photon info...");
        // http://acode-browser2.usatlas.bnl.gov/lxr/source/r21/atlas/Event/xAOD/xAODEgamma/xAODEgamma/EgammaEnums.h#0238
        // unconverted = 0
        // singleSi = 1
        // singleTRT = 2
        // doubleSi = 3
        // doubleTRT = 4
        // doubleSiTRT = 5
        p_photonConversionType = ph->conversionType(); // int
        p_photonConversionRadius = ph->conversionRadius(); // float
    
        ANA_MSG_DEBUG("::fillNtuple: Filling probe photon vertex info...");
        const xAOD::Vertex* pvx = ph->vertex();
        if ( pvx != nullptr ) {
            // index
            p_vertexIndex = pvx->index();
    
            // vertex pt from vertex decoration
            p_photonVertexPtConvDecor = std::hypot(Accessor_px(*pvx), Accessor_py(*pvx)); // float
    
            // alternative vertex pt
            if ( pvx->nTrackParticles() == 1 ) {
                p_photonVertexPtConv = pvx->trackParticle(0)->p4().Perp(); // float
            } else if ( pvx->nTrackParticles() == 2 ) {
                p_photonVertexPtConv = (pvx->trackParticle(0)->p4()+pvx->trackParticle(1)->p4()).Perp(); // float
            }
    
            // Pt of electrons from photon conversion
            p_photonVertexPt1 = (Accessor_pt1.isAvailable(*pvx)) ? Accessor_pt1(*pvx) : getPtAtFirstMeasurement(pvx->trackParticle(0)); // float
            p_photonVertexPt2 = (Accessor_pt2.isAvailable(*pvx)) ? Accessor_pt2(*pvx) : getPtAtFirstMeasurement(pvx->trackParticle(1)); // float
    
            if (xAOD::EgammaHelpers::numberOfSiTracks(ph) == 2) {
                // pt ratio between electrons from converted photon
                p_photonVertexConvPtRatio = std::max(p_photonVertexPt1, p_photonVertexPt2)/(p_photonVertexPt1+p_photonVertexPt2); // float
    
                // et over pt for conversions
                p_photonVertexConvEtOverPt = std::min( std::max( 0.0f, p_eAccCluster/(std::cosh(p_etaCluster)*p_photonVertexPtConv) ), 2.0f ); // float
            }
    
            // Misc. conversions variables
            p_photonVertexRconv = std::hypot( pvx->position().x(), pvx->position().y() ); // float
            p_photonVertexzconv = pvx->position().z(); // float
            if ( pvx->trackParticle(0) != nullptr ) {
                pvx->trackParticle(0)->summaryValue( p_photonVertexPixHits1, xAOD::numberOfPixelHits ); // uint8_t
                pvx->trackParticle(0)->summaryValue( p_photonVertexSCTHits1, xAOD::numberOfSCTHits ); // uint8_t
            }
            if ( pvx->trackParticle(1) != nullptr ) {
                pvx->trackParticle(1)->summaryValue( p_photonVertexPixHits2, xAOD::numberOfPixelHits ); // uint8_t
                pvx->trackParticle(1)->summaryValue( p_photonVertexSCTHits2, xAOD::numberOfSCTHits ); // uint8_t
            }
        }
    }
    
    
    //============================================================================
    // Fill all probe aux variables
    //============================================================================
    
    if ( m_fillpXVariables ) {
        ANA_MSG_DEBUG("::fillNtuple: Filling probe extra info: Photons or central electrons...");
        // Photons or central electrons
        pX_E7x7_Lr2                             = Accessor_E7x7_Lr2( probeAuxElementRef ) / 1000. ;
        pX_E7x7_Lr3                             = Accessor_E7x7_Lr3( probeAuxElementRef ) / 1000. ;
        pX_E_Lr0_HiG                            = Accessor_E_Lr0_HiG( probeAuxElementRef ) / 1000. ;
        pX_E_Lr0_LowG                           = Accessor_E_Lr0_LowG( probeAuxElementRef ) / 1000. ;
        pX_E_Lr0_MedG                           = Accessor_E_Lr0_MedG( probeAuxElementRef ) / 1000. ;
        pX_E_Lr1_HiG                            = Accessor_E_Lr1_HiG( probeAuxElementRef ) / 1000. ;
        pX_E_Lr1_LowG                           = Accessor_E_Lr1_LowG( probeAuxElementRef ) / 1000. ;
        pX_E_Lr1_MedG                           = Accessor_E_Lr1_MedG( probeAuxElementRef ) / 1000. ;
        pX_E_Lr2_HiG                            = Accessor_E_Lr2_HiG( probeAuxElementRef ) / 1000. ;
        pX_E_Lr2_LowG                           = Accessor_E_Lr2_LowG( probeAuxElementRef ) / 1000. ;
        pX_E_Lr2_MedG                           = Accessor_E_Lr2_MedG( probeAuxElementRef ) / 1000. ;
        pX_E_Lr3_HiG                            = Accessor_E_Lr3_HiG( probeAuxElementRef ) / 1000. ;
        pX_E_Lr3_LowG                           = Accessor_E_Lr3_LowG( probeAuxElementRef ) / 1000. ;
        pX_E_Lr3_MedG                           = Accessor_E_Lr3_MedG( probeAuxElementRef ) / 1000. ;
        pX_ambiguityType                        = Accessor_ambiguityType( probeAuxElementRef );
        pX_asy1                                 = Accessor_asy1( probeAuxElementRef );
        pX_barys1                               = Accessor_barys1( probeAuxElementRef );
        pX_core57cellsEnergyCorrection          = Accessor_core57cellsEnergyCorrection( probeAuxElementRef );
        pX_e1152                                = Accessor_e1152( probeAuxElementRef ) / 1000. ;
        pX_e132                                 = Accessor_e132( probeAuxElementRef ) / 1000. ;
        pX_e235                                 = Accessor_e235( probeAuxElementRef ) / 1000. ;
        pX_e255                                 = Accessor_e255( probeAuxElementRef ) / 1000. ;
        pX_e2ts1                                = Accessor_e2ts1( probeAuxElementRef ) / 1000. ;
        pX_ecore                                = Accessor_ecore( probeAuxElementRef ) / 1000. ;
        pX_emins1                               = Accessor_emins1( probeAuxElementRef );
        pX_etconeCorrBitset                     = Accessor_etconeCorrBitset( probeAuxElementRef );
        pX_ethad                                = Accessor_ethad( probeAuxElementRef ) / 1000. ;
        pX_ethad1                               = Accessor_ethad1( probeAuxElementRef ) / 1000. ;
        pX_maxEcell_energy                      = Accessor_maxEcell_energy( probeAuxElementRef ) / 1000. ;
        pX_maxEcell_gain                        = Accessor_maxEcell_gain( probeAuxElementRef );
        pX_maxEcell_time                        = Accessor_maxEcell_time( probeAuxElementRef );
        pX_maxEcell_x                           = Accessor_maxEcell_x( probeAuxElementRef );
        pX_maxEcell_y                           = Accessor_maxEcell_y( probeAuxElementRef );
        pX_maxEcell_z                           = Accessor_maxEcell_z( probeAuxElementRef );
        pX_nCells_Lr0_HiG                       = Accessor_nCells_Lr0_HiG( probeAuxElementRef );
        pX_nCells_Lr0_LowG                      = Accessor_nCells_Lr0_LowG( probeAuxElementRef );
        pX_nCells_Lr0_MedG                      = Accessor_nCells_Lr0_MedG( probeAuxElementRef );
        pX_nCells_Lr1_HiG                       = Accessor_nCells_Lr1_HiG( probeAuxElementRef );
        pX_nCells_Lr1_LowG                      = Accessor_nCells_Lr1_LowG( probeAuxElementRef );
        pX_nCells_Lr1_MedG                      = Accessor_nCells_Lr1_MedG( probeAuxElementRef );
        pX_nCells_Lr2_HiG                       = Accessor_nCells_Lr2_HiG( probeAuxElementRef );
        pX_nCells_Lr2_LowG                      = Accessor_nCells_Lr2_LowG( probeAuxElementRef );
        pX_nCells_Lr2_MedG                      = Accessor_nCells_Lr2_MedG( probeAuxElementRef );
        pX_nCells_Lr3_HiG                       = Accessor_nCells_Lr3_HiG( probeAuxElementRef );
        pX_nCells_Lr3_LowG                      = Accessor_nCells_Lr3_LowG( probeAuxElementRef );
        pX_nCells_Lr3_MedG                      = Accessor_nCells_Lr3_MedG( probeAuxElementRef );
        pX_neflowisol20                         = Accessor_neflowisol20( probeAuxElementRef );
        pX_neflowisol20ptCorrection             = Accessor_neflowisol20ptCorrection( probeAuxElementRef );
        pX_neflowisol30                         = Accessor_neflowisol30( probeAuxElementRef );
        pX_neflowisol30ptCorrection             = Accessor_neflowisol30ptCorrection( probeAuxElementRef );
        pX_neflowisol40                         = Accessor_neflowisol40( probeAuxElementRef );
        pX_neflowisol40ptCorrection             = Accessor_neflowisol40ptCorrection( probeAuxElementRef );
        pX_neflowisolCorrBitset                 = Accessor_neflowisolCorrBitset( probeAuxElementRef );
        pX_neflowisolcoreConeEnergyCorrection   = Accessor_neflowisolcoreConeEnergyCorrection( probeAuxElementRef );
        pX_pos                                  = Accessor_pos( probeAuxElementRef );
        pX_pos7                                 = Accessor_pos7( probeAuxElementRef );
        pX_poscs1                               = Accessor_poscs1( probeAuxElementRef );
        pX_poscs2                               = Accessor_poscs2( probeAuxElementRef );
        pX_ptconeCorrBitset                     = Accessor_ptconeCorrBitset( probeAuxElementRef );
        pX_ptconecoreTrackPtrCorrection         = Accessor_ptconecoreTrackPtrCorrection( probeAuxElementRef );
        pX_r33over37allcalo                     = Accessor_r33over37allcalo( probeAuxElementRef );
        pX_topoetconecoreConeSCEnergyCorrection = Accessor_topoetconecoreConeSCEnergyCorrection( probeAuxElementRef );
        pX_widths1                              = Accessor_widths1( probeAuxElementRef );
        pX_widths2                              = Accessor_widths2( probeAuxElementRef );
        pX_e233                                 = Accessor_e233( probeAuxElementRef ) / 1000. ;
        pX_e237                                 = Accessor_e237( probeAuxElementRef ) / 1000. ;
        pX_e2tsts1                              = Accessor_e2tsts1( probeAuxElementRef );
        pX_ehad1                                = Accessor_ehad1( probeAuxElementRef ) / 1000. ;
        pX_emaxs1                               = Accessor_emaxs1( probeAuxElementRef );
        pX_E3x5_Lr0                             = Accessor_E3x5_Lr0( probeAuxElementRef ) / 1000. ;
        pX_E3x5_Lr1                             = Accessor_E3x5_Lr1( probeAuxElementRef ) / 1000. ;
        pX_E3x5_Lr2                             = Accessor_E3x5_Lr2( probeAuxElementRef ) / 1000. ;
        pX_E3x5_Lr3                             = Accessor_E3x5_Lr3( probeAuxElementRef ) / 1000. ;
        pX_E5x7_Lr0                             = Accessor_E5x7_Lr0( probeAuxElementRef ) / 1000. ;
        pX_E5x7_Lr1                             = Accessor_E5x7_Lr1( probeAuxElementRef ) / 1000. ;
        pX_E5x7_Lr2                             = Accessor_E5x7_Lr2( probeAuxElementRef ) / 1000. ;
        pX_E5x7_Lr3                             = Accessor_E5x7_Lr3( probeAuxElementRef ) / 1000. ;
        pX_E7x11_Lr0                            = Accessor_E7x11_Lr0( probeAuxElementRef ) / 1000. ;
        pX_E7x11_Lr1                            = Accessor_E7x11_Lr1( probeAuxElementRef ) / 1000. ;
        pX_E7x11_Lr2                            = Accessor_E7x11_Lr2( probeAuxElementRef ) / 1000. ;
        pX_E7x11_Lr3                            = Accessor_E7x11_Lr3( probeAuxElementRef ) / 1000. ;
        pX_E7x7_Lr0                             = Accessor_E7x7_Lr0( probeAuxElementRef ) / 1000. ;
        pX_E7x7_Lr1                             = Accessor_E7x7_Lr1( probeAuxElementRef ) / 1000. ;
    
        pX_Loose                                = Accessor_Loose( probeAuxElementRef );
        // FOR SOME REASON, THIS CRASHES WITH THE FOLLOWING SUPER-CRYPTIC ERROR ONLY ON THE GRID:
        // SG::ExcBadAuxVar: Attempt to retrieve nonexistent aux data item `::SV1_TrackParticles' (22).
        // pX_m                                 = Accessor_m( probeAuxElementRef );
        pX_OQ                                   = Accessor_OQ( probeAuxElementRef );
        pX_Tight                                = Accessor_Tight( probeAuxElementRef );
        pX_author                               = Accessor_author( probeAuxElementRef );
        pX_f1core                               = Accessor_f1core( probeAuxElementRef );
        pX_f3core                               = Accessor_f3core( probeAuxElementRef );
        pX_topoetconeCorrBitset                 = Accessor_topoetconeCorrBitset( probeAuxElementRef );
        pX_topoetconecoreConeEnergyCorrection   = Accessor_topoetconecoreConeEnergyCorrection( probeAuxElementRef );
        pX_weta1                                = Accessor_weta1( probeAuxElementRef );
        pX_wtots1                               = Accessor_wtots1( probeAuxElementRef );
        pX_e277                                 = Accessor_e277( probeAuxElementRef ) / 1000. ;
        pX_fracs1                               = Accessor_fracs1( probeAuxElementRef );
        pX_DeltaE                               = Accessor_DeltaE( probeAuxElementRef );
    
    
        if ( probe->isCentralElectron() ) {
            ANA_MSG_DEBUG("::fillNtuple: Filling probe extra info: Central electrons only...");
            // Central electrons only
            pX_LHLoose                     = m_electronSelectionToolLoose->accept( probe->getCentralElectron() );
            pX_LHMedium                    = m_electronSelectionToolMedium->accept( probe->getCentralElectron() );
            pX_LHTight                     = m_electronSelectionToolTight->accept( probe->getCentralElectron() );
            pX_MultiLepton                 = Accessor_MultiLepton( probeAuxElementRef );
            pX_Medium                      = Accessor_Medium( probeAuxElementRef );
            pX_deltaEta0                   = Accessor_deltaEta0( probeAuxElementRef );
            pX_deltaEta2                   = Accessor_deltaEta2( probeAuxElementRef );
            pX_deltaEta3                   = Accessor_deltaEta3( probeAuxElementRef );
            pX_deltaPhi0                   = Accessor_deltaPhi0( probeAuxElementRef );
            pX_deltaPhi1                   = Accessor_deltaPhi1( probeAuxElementRef );
            pX_deltaPhi2                   = Accessor_deltaPhi2( probeAuxElementRef );
            pX_deltaPhi3                   = Accessor_deltaPhi3( probeAuxElementRef );
            pX_deltaPhiFromLastMeasurement = Accessor_deltaPhiFromLastMeasurement( probeAuxElementRef );
            pX_deltaPhiRescaled0           = Accessor_deltaPhiRescaled0( probeAuxElementRef );
            pX_deltaPhiRescaled1           = Accessor_deltaPhiRescaled1( probeAuxElementRef );
            pX_deltaPhiRescaled3           = Accessor_deltaPhiRescaled3( probeAuxElementRef );
    
            // Trigger information
            std::vector<const xAOD::IParticle*> myParticles;
            myParticles.clear();
            myParticles.push_back( probe->getCentralElectron() );
            p_unprescaledTriggers = m_tmt->match( myParticles, "HLT_e26_lhtight_nod0_ivarloose", m_triggerMatchingdR, false );
            p_prescaledJpsiTriggers = m_tmt->match( myParticles, "HLT_e14_lhtight_nod0_e4_etcut_Jpsiee", m_triggerMatchingdR, false ) ||
                                    m_tmt->match( myParticles, "HLT_e5_lhtight_nod0_e4_etcut_Jpsiee", m_triggerMatchingdR, false ) ||
                                    m_tmt->match( myParticles, "HLT_e9_lhtight_nod0_e4_etcut_Jpsiee", m_triggerMatchingdR, false ) ||
                                    m_tmt->match( myParticles, "HLT_e5_lhtight_nod0_e9_etcut_Jpsiee", m_triggerMatchingdR, false ) || 
                                    m_tmt->match( myParticles, "HLT_e5_lhtight_nod0_e14_etcut_Jpsiee", m_triggerMatchingdR, false ); 
            p_prescaledTriggers =   m_tmt->match( myParticles, "HLT_e5_etcut", m_triggerMatchingdR, false ) ||
                                    m_tmt->match( myParticles, "HLT_e10_etcut_L1EM7", m_triggerMatchingdR, false ) ||
                                    m_tmt->match( myParticles, "HLT_e15_etcut_L1EM7", m_triggerMatchingdR, false ) ||
                                    m_tmt->match( myParticles, "HLT_e20_etcut_L1EM12", m_triggerMatchingdR, false ) ||
                                    m_tmt->match( myParticles, "HLT_e25_etcut_L1EM15", m_triggerMatchingdR, false ) ||
                                    m_tmt->match( myParticles, "HLT_e30_etcut_L1EM15", m_triggerMatchingdR, false ) ||
                                    m_tmt->match( myParticles, "HLT_e40_etcut_L1EM15", m_triggerMatchingdR, false ) ||
                                    m_tmt->match( myParticles, "HLT_e50_etcut_L1EM15", m_triggerMatchingdR, false ) ||
                                    m_tmt->match( myParticles, "HLT_e60_etcut", m_triggerMatchingdR, false ) ||
                                    m_tmt->match( myParticles, "HLT_e80_etcut", m_triggerMatchingdR, false ) ||
                                    m_tmt->match( myParticles, "HLT_e100_etcut", m_triggerMatchingdR, false ) ||
                                    m_tmt->match( myParticles, "HLT_e120_etcut", m_triggerMatchingdR, false ); // ||
    
        } else if( probe->isPhoton() ) {
            ANA_MSG_DEBUG("::fillNtuple: Filling probe extra info: Photons only...");
            // Photons only
            pPX_convMatchDeltaEta1 = Accessor_convMatchDeltaEta1( probeAuxElementRef );
            pPX_convMatchDeltaEta2 = Accessor_convMatchDeltaEta2( probeAuxElementRef );
            pPX_convMatchDeltaPhi1 = Accessor_convMatchDeltaPhi1( probeAuxElementRef );
            pPX_convMatchDeltaPhi2 = Accessor_convMatchDeltaPhi2( probeAuxElementRef );
        }
    
    }
    
    
    //============================================================================
    // Truth-information
    //============================================================================
    
    if ( event_isMC ) {
        ANA_MSG_DEBUG("::fillNtuple: Filling truth info...");
        if ( tag  ) {
            const SG::AuxElement& tagAuxElementRef = *(tag->getAuxElement());
            tag_TruthType                = Accessor_truthType( tagAuxElementRef );
            tag_TruthOrigin              = Accessor_truthOrigin( tagAuxElementRef );
            retrieveTruth( tag->getIParticle(), tag_truth_matched, tag_truth_pt, tag_truth_phi, tag_truth_eta, tag_truth_E, tag_truth_pdgId, tag_truth_parent_pdgId );
            tag_truth_E                  = tag_truth_E < 0 ? -999. : tag_truth_E / 1000. ;
            tag_truth_pt                 = tag_truth_pt < 0 ? -999. : tag_truth_pt / 1000. ;
            if ( tag->isCentralElectron() ){
                tag_firstEgMotherTruthType   = Accessor_firstEgMotherTruthType( tagAuxElementRef );
                tag_firstEgMotherTruthOrigin = Accessor_firstEgMotherTruthOrigin( tagAuxElementRef );
                tag_firstEgMotherPdgId       = Accessor_firstEgMotherPdgId( tagAuxElementRef );
            }
        }
    
        p_TruthType                = Accessor_truthType( probeAuxElementRef );
        p_TruthOrigin              = Accessor_truthOrigin( probeAuxElementRef );
        retrieveTruth( probe->getIParticle(), p_truth_matched, p_truth_pt, p_truth_phi, p_truth_eta, p_truth_E, p_truth_pdgId, p_truth_parent_pdgId );
        p_truth_E                  = p_truth_E < 0 ? -999. : p_truth_E / 1000. ;
        p_truth_pt                 = p_truth_pt < 0 ? -999. : p_truth_pt / 1000. ;
        if ( probe->isCentralElectron() ){
            p_firstEgMotherTruthType   = Accessor_firstEgMotherTruthType( probeAuxElementRef );
            p_firstEgMotherTruthOrigin = Accessor_firstEgMotherTruthOrigin( probeAuxElementRef );
            p_firstEgMotherPdgId       = Accessor_firstEgMotherPdgId( probeAuxElementRef ); 
        }
    
        if ( probe->isPhoton()){
            // p_trueConv = Accessor_trueConv( probeAuxElementRef );
            Truth = isTruePhoton(p_TruthType);
        }
    
        // fill Truth based on IFFTruthClassifier output
        if ( probe->isCentralElectron()){
            // IFFTruthClassifier information
            // enum class Type {
            //   0 Unknown,  
            //   1 KnownUnknown,
            //   2 IsoElectron,
            //   3 ChargeFlipIsoElectron,
            //   4 PromptMuon,
            //   5 PromptPhotonConversion,
            //   6 ElectronFromMuon,
            //   7 TauDecay,
            //   8 BHadronDecay,
            //   9 CHadronDecay,
            //   10 LightFlavorDecay,
            // };
            p_iffTruth = static_cast<std::underlying_type_t<IFF::Type>>(m_iffTruthClassifier->classify( *(probe->getCentralElectron()) ) ) ;

            if ( p_iffTruth == 2 || p_iffTruth == 3){
                Truth = 1;
            }
            else{
                Truth = 0;
            }
        }
    }
    
    
    
    //============================================================================
    // Find all tracks within dR<0.4 of the probe (except TRT seeded tracks), find their vertex index, and dump their info
    // Also dumps information about all tracks associated to the probe except TRT seeded tracks
    // Also calculates PU30, PU20, PU10
    //============================================================================
    if ( m_fillTracks || m_fillPU ) {
        ANA_MSG_DEBUG("::fillNtuple: Filling track/PU info...");
    
        // Tag1 track
        if ( t != nullptr && tag->isCentralElectron() ) {
            t = xAOD::EgammaHelpers::getOriginalTrackParticleFromGSF( t );
        }
    
        // Set to true if the electron track is also used by a muon which is not calo tagged
        p_SharedMuonTrack = 0;
        if ( tt != nullptr ) {
            if ( probe->isCentralElectron() ) {
                tt = xAOD::EgammaHelpers::getOriginalTrackParticleFromGSF( tt );
                for ( const auto& mu : *m_muons ) {
                    if (mu->muonType() == xAOD::Muon::MuonType::CaloTagged) continue;
                    if (mu->primaryTrackParticle() == tt){
                        p_SharedMuonTrack = 1;
                        continue;
                    }
                }
            }
        }
    
        // For PU calculation
        p_ptPU40 = 0;
        p_ptPU30 = 0;
        p_ptPU20 = 0;
        p_ptPU10 = 0;
        const xAOD::Vertex* probeVertex = ( tt ) ? tt->vertex() : nullptr;
    
        for ( const xAOD::TrackParticle* track : *m_indetTracks ) {
            // Make sure track exists
            if ( track == nullptr ) continue;
    
            // Track cannot be tags' nor probe's original tracks ( does this comparison work? )
            if ( t  != nullptr ) if ( track->index() == t ->index() ) continue;
            if ( tt != nullptr ) if ( track->index() == tt->index() ) continue;
    
            const float dR = deltaR( track->eta(), track->phi(), p_eta, p_phi );
            // Exclude tracks outside of dR=0.4
            if ( dR > 0.4 ) continue;
    
            int vertex_index = -999;
            if ( track->vertex() != nullptr ) vertex_index = track->vertex()->index();
    
            if ( m_fillTracks ) {
                UChar_t tmp_pixhits = Accessor_numberOfPixelHits( *track ) + Accessor_numberOfPixelDeadSensors( *track );
                UChar_t sct_pixhits = Accessor_numberOfSCTHits( *track ) + Accessor_numberOfSCTDeadSensors( *track );
                UChar_t trt_pixhits = Accessor_numberOfTRTHits( *track );
    
                if ( tmp_pixhits + sct_pixhits > 0 ){    
                    tracks_pt     .push_back( track->pt() / 1000. );
                    tracks_eta    .push_back( track->eta()        );
                    tracks_phi    .push_back( track->phi()        );
                    tracks_charge .push_back( track->charge()     );
                    tracks_chi2   .push_back( track->chiSquared() );
                    tracks_ndof   .push_back( track->numberDoF()  );
                    tracks_pixhits.push_back( tmp_pixhits         );
                    tracks_scthits.push_back( sct_pixhits         );
                    tracks_trthits.push_back( trt_pixhits         );
                    tracks_vertex .push_back( vertex_index        );
                    if ( tt ){
                        tracks_z0.push_back( track->z0() + track->vz() - tt->vertex()->z() );
                    }
                    else{
                        tracks_z0.push_back( track->z0() );
                    }
                    tracks_d0     .push_back( track->d0()         );
                    tracks_dR     .push_back( track->p4().DeltaR( probe_p4 ));
                    tracks_theta  .push_back( track->theta()      );
                    tracks_sigmad0.push_back( track->definingParametersCovMatrixVec()[0] );
                }
            }
    
            if ( m_fillPU ) {
                // Calculate PU40, PU30, PU20, PU10 for probe/background
                if ( probeVertex != nullptr ) {
                    if ( vertex_index == p_vertexIndex ) continue;
                }
                if ( dR < 0.4 ) {
                    p_ptPU40 += track->pt() / 1000. ;
                    if ( dR < 0.3 ) {
                        p_ptPU30 += track->pt() / 1000. ;
                        if ( dR < 0.2 ) {
                            p_ptPU20 += track->pt() / 1000. ;
                            if ( dR < 0.1 ) {
                                p_ptPU10 += track->pt() / 1000. ;
                            }
                        }
                    }
                }
            }
        }
    
        if ( m_fillPU ) {
            // For PU calculation
            p_ptPU40 = p_ptPU40 / p_et_calo;
            p_ptPU30 = p_ptPU30 / p_et_calo;
            p_ptPU20 = p_ptPU20 / p_et_calo;
            p_ptPU10 = p_ptPU10 / p_et_calo;
        }
    }
    
    
    // Tracks associated with the probe
    for ( int trk_idx = 0; trk_idx < p_nTracks; trk_idx++) {
        const xAOD::TrackParticle* track = probe->trackParticle(size_t(trk_idx));
        // Make sure track exists
        if ( track == nullptr ) continue;


        int vertex_index = -999;
        if ( track->vertex() != nullptr ) vertex_index = track->vertex()->index();

        if ( m_fillTracks ) {
            UChar_t tmp_pixhits = Accessor_numberOfPixelHits( *track ) + Accessor_numberOfPixelDeadSensors( *track );
            UChar_t sct_pixhits = Accessor_numberOfSCTHits( *track ) + Accessor_numberOfSCTDeadSensors( *track );
            UChar_t trt_pixhits = Accessor_numberOfTRTHits( *track );

            if ( tmp_pixhits + sct_pixhits > 0 ){
                p_tracks_pt     .push_back( track->pt() / 1000. );
                p_tracks_eta    .push_back( track->eta()        );
                p_tracks_phi    .push_back( track->phi()        );
                p_tracks_charge .push_back( track->charge()     );
                p_tracks_chi2   .push_back( track->chiSquared() );
                p_tracks_ndof   .push_back( track->numberDoF()  );
                p_tracks_pixhits.push_back( tmp_pixhits         );
                p_tracks_scthits.push_back( sct_pixhits         );
                p_tracks_trthits.push_back( trt_pixhits         );
                p_tracks_vertex .push_back( vertex_index        );
                if ( vertex_index != -999 ){
                    p_tracks_z0.push_back( track->z0() + track->vz() - track->vertex()->z() );
                }
                else{
                    p_tracks_z0.push_back( track->z0() );
                }
                p_tracks_d0     .push_back( track->d0()         );
                p_tracks_theta  .push_back( track->theta()      );
                p_tracks_sigmad0.push_back( track->definingParametersCovMatrixVec()[0] );
            }
        }
    }    
    
    //============================================================================
    // LightGBM model predictions
    //============================================================================
    // get all  the features
    if ( probe->isCentralElectron() ){
        pdfcutvars_features = {double(p_Rhad1), double(p_Rhad), double(p_f3), double(p_weta2), double(p_Rphi), double(p_Reta),
                               double(p_Eratio), double(p_f1), double(p_numberOfInnermostPixelHits), double(p_numberOfPixelHits),
                               double(p_numberOfSCTHits), double(p_d0), double(p_d0Sig), double(p_dPOverP),
                               double(p_deltaEta1), double(p_deltaPhiRescaled2), double(p_EptRatio), double(p_eProbHT), double(pX_wtots1)};
    
        // predict with the given features
        pdf_cut_vars_score = m_lgbm_pdfcutvars->predict(pdfcutvars_features);
        // clear the features ( probably not necessary)
        pdfcutvars_features.clear();
    
    
        pdfvars_features = {double(p_Rhad1), double(p_Rhad), double(p_f3), double(p_weta2), double(p_Rphi), double(p_Reta),
                            double(p_Eratio), double(p_f1), double(p_d0), double(p_d0Sig), double(p_dPOverP),
                            double(p_deltaEta1), double(p_deltaPhiRescaled2), double(p_eProbHT)};
    
        pdf_vars_score = m_lgbm_pdfvars->predict(pdfvars_features);
        pdfvars_features.clear();
    
    
        iso_features = {double(p_etcone20*1000), double(p_etcone30*1000), double(p_etcone40*1000),
                        double(p_topoetcone20*1000), double(p_topoetcone30*1000),
                        double(p_topoetcone40*1000), double(p_topoetcone20ptCorrection*1000),
                        double(p_topoetcone30ptCorrection*1000), double(p_topoetcone40ptCorrection*1000),  double(p_ptPU30*1000),
                        double(p_ptcone20*1000), double(p_ptcone30*1000), double(p_ptcone40*1000), double(p_ptvarcone20*1000),
                        double(p_ptvarcone30*1000), double(p_ptvarcone40*1000)};
    
        Iso_score = m_lgbm_iso->predict(iso_features);
        iso_features.clear();
    
    
        isocalo_features = {double(p_etcone20*1000), double(p_etcone30*1000), double(p_etcone40*1000),
                            double(p_topoetcone20*1000), double(p_topoetcone30*1000),
                            double(p_topoetcone40*1000), double(p_topoetcone20ptCorrection*1000),
                            double(p_topoetcone30ptCorrection*1000), double(p_topoetcone40ptCorrection*1000),  double(p_ptPU30*1000)};
    
        IsoCalo_score = m_lgbm_isocalo->predict(isocalo_features);
        isocalo_features.clear();
    
    
        isotrack_features = {double(p_ptcone20*1000), double(p_ptcone30*1000), double(p_ptcone40*1000), double(p_ptvarcone20*1000),
                             double(p_ptvarcone30*1000), double(p_ptvarcone40*1000)};
    
        IsoTrack_score = m_lgbm_isotrack->predict(isotrack_features);                
        isotrack_features.clear();
    }

    
    // weights
    if ( event_isMC ) {
        // Prescale/PileupWeight, MC event generator weight, cross Section of the sample, kFactor of the sample, and Filter efficiency of the sample
        event_totalWeight = combinedWeight * event_MCWeight * m_crossSections[int(event_mcChannelNumber)] * m_kFactors[int(event_mcChannelNumber)] * m_filterEffs[int(event_mcChannelNumber)];
        if ( tag  ){
            // if there is a tag add the SF
            event_totalWeight *= tag_SF;
        }
    }
    
    //============================================================================
    // Filling of several histograms
    //============================================================================
    
    if ( probe->isCentralElectron()){
        // if tag is an electron and the T&P is OS, save it as true selected
        if ( tag_type == 1 && ( abs(tag_charge * p_charge + 1) < 0.1 ) ){
            TruthSelection = true;
        }
        else {
            TruthSelection = false;
        }
    
        // for MC histograms are filled with all particles, only true bkg particles, and only true signal particles
        if ( event_isMC ){
            if ( TruthSelection ){
                // fill histograms with all signal selected particles
                fill_hists_truth("_sig", 0, 3, Truth);
                // only where T&P for J/psi
                if ( 0 < ll_m && ll_m < 10 ){
                    fill_hists_truth("_Jpsi_sig", 2, 3, Truth);
                }
                // only where T&P for Z
                else if ( ll_m > 50){
                    fill_hists_truth("_Z_sig", 1, 3, Truth);
                }
    
                // fill histograms where a further selection has been applied, which is probably the selection used before training on data
                if ( ( 80< ll_m && ll_m < 100 ) || ( 2.8 < ll_m && ll_m < 3.3 && -1 < ll_lifetime && ll_lifetime < 0.2 )){
                    // again no matter if J/psi or Z
                    fill_hists_truth("_selection", 9, 3, Truth);
    
                    // only J/psi
                    if ( 0 < ll_m && ll_m < 10 ){
                        fill_hists_truth("_Jpsi_selection", 11, 3, Truth);
                    }   
    
                    // only Z
                    else if ( ll_m > 50){
                        fill_hists_truth("_Z_selection", 10, 3, Truth);
                    }   
                }
            }
            else{
                // all kinds of background
                fill_hists_truth("_bkg", 18, 4, Truth);  
                // background with no T&P, so the basic bkg selection   
                if ( tag_type == 0 ){
                    fill_hists_truth("_noTP_bkg", 19, 4, Truth);                        
                }               
                // bkg from tag mu + probe e
                if ( tag_type == 2 ){
                    fill_hists_truth("_mue_bkg", 20, 4, Truth);    
                }
                // bkg from SS tag e + probe e
                if ( tag_type == 1 ){
                    fill_hists_truth("_SS_bkg", 21, 4, Truth);    
                }                
            }
        }
    
        // real Data, histograms are only filled with all particles (ince no truth information)
        else{
            if ( TruthSelection ){
                // fill histograms with all signal selected particles                
                fill_hists("_sig", 0);      
    
                // only where T&P for J/psi
                if ( 0 < ll_m && ll_m < 10 ){
                    fill_hists("_Jpsi_sig", 2);
                }   
    
                // only where T&P for Z
                else if ( ll_m > 50){
                    fill_hists("_Z_sig", 1);
                }   
    
                // fill histograms where a further selection has been applied, which is probably the selection used before training on data    
                if ( ( 80 < ll_m && ll_m < 100 ) || ( 2.8 < ll_m && ll_m < 3.3 && -1 < ll_lifetime && ll_lifetime < 0.2 )){
                    // again no matter if J/psi or Z                    
                    fill_hists("_selection", 9);
    
                    // only J/psi
                    if ( 0 < ll_m && ll_m < 10 ){
                        fill_hists("_Jpsi_selection", 11);   
                    }    
    
                    // only Z                         
                    else if ( ll_m > 50){
                        fill_hists("_Z_selection", 10);
                    }                       
                }    
            }  
            else{
                // all kinds of background                
                fill_hists("_bkg", 18 );    
                // background with no T&P, so the basic bkg selection                    
                if ( tag_type == 0 ){
                    fill_hists("_noTP_bkg", 19 );                        
                }      
                // bkg from tag mu + probe e                         
                if ( tag_type == 2 ){
                    fill_hists("_mue_bkg", 20 );    
                }
                // bkg from SS tag e + probe e                
                if ( tag_type == 1 ){
                    fill_hists("_SS_bkg", 21 );    
                }    
            }        
        }
    }

}



void MyTagAndProbeAnalysis::resetEventInfoBranchVariables() {
    event_eventNumber                    = 0;
    event_MCWeight                       = 1;
    event_mcChannelNumber                = 0;
    event_runNumber                      = 0;
    event_pileupweight                   = 1;
    event_actualInteractionsPerCrossing  = -999.0f;
    event_averageInteractionsPerCrossing = -999.0f;
    event_correctedActualMu              = -999.0f;
    event_correctedAverageMu             = -999.0f;
    event_correctedScaledActualMu        = -999.0f;
    event_correctedScaledAverageMu       = -999.0f;
    event_NvtxReco                       = -999;
    BC_distanceFromFront                 = -999;
    BC_filledBunches                     = 0;
    jets_highestBtagMV2c10               = -999.0;
    jets_highestBtagDL1_pb               = -999.0;
    jets_highestBtagDL1_pc               = -999.0;
    jets_highestBtagDL1_pu               = -999.0;
    combinedWeight                       = 1;
    settype                                 = 0;
}

// set all variables to a default value and clear the vectors, so one particle does not end up with the value from the previous one 
// e.g. a photon could enp up with a number of pixel hits if the previous particle was an electron and had some
void MyTagAndProbeAnalysis::resetRestOfBranchVariables() {
    cell_energy.clear();
    cell_eta.clear();
    cell_deta.clear();
    cell_phi.clear();
    cell_dphi.clear();
    cell_time.clear();
    cell_noise.clear();
    cell_gain.clear();
    cell_sampling.clear();

    event_totalWeight                       = 1;
    ll_m                                    = -999.0f;
    ll_eta                                  = -999.0f;
    ll_pt                                   = -999.0f;
    ll_phi                                  = -999.0f;
    ll_lifetime                             = -999.0f;
    tag_type                                = 0;
    tag_vertexIndex                         = -999;
    tag_SF                                  = 1;
    tag_e                                   = -999.0f;
    tag_et_calo                             = -999.0f;
    tag_pt_track                            = -999.0f;
    tag_eta                                 = -999.0f;
    tag_phi                                 = -999.0f;
    tag_charge                              = -999.0f;
    tag_z0                                  = -999.0f;
    tag_d0                                  = -999.0f;
    tag_sigmad0                             = -999.0f;
    tag_TruthType                           = 0;
    tag_TruthOrigin                         = 0;
    tag_truth_matched                       = 0;
    tag_truth_pt                            = -999;
    tag_truth_phi                           = -999;
    tag_truth_eta                           = -999;
    tag_truth_E                             = -999;
    tag_truth_pdgId                         = 0;
    tag_truth_parent_pdgId                  = 0;
    p_type                                  = 0;
    p_hasTrack                              = false;
    p_nTracks                               = 0;
    p_vertexIndex                           = -999;
    p_e                                     = -999.0f;
    p_calibratedE                           = -999.0f;
    p_et_calo                               = -999.0f;
    p_pt_track                              = -999.0f;
    p_eta                                   = -999.0f;
    p_phi                                   = -999.0f;
    p_charge                                = -999.0f;
    p_z0                                    = -999.0f;
    p_d0                                    = -999.0f;
    p_sigmad0                               = -999.0f;
    p_d0Sig                                 = -999.0f;
    p_EptRatio                              = -999.0f;
    p_qOverP                                = -999.0f;
    p_dPOverP                               = -999.0f;
    p_z0theta                               = -999.0f;
    p_deltaR_tag                            = -999.0f;
    p_etaCluster                            = -999.0f;
    p_phiCluster                            = -999.0f;
    p_eCluster                              = -999.0f;
    p_etCluster                             = -999.0f;
    p_RawEtaCluster                         = -999.0f;
    p_RawPhiCluster                         = -999.0f;
    p_RawECluster                           = -999.0f;
    p_eClusterLr0                           = -999.0f;
    p_eClusterLr1                           = -999.0f;
    p_eClusterLr2                           = -999.0f;
    p_eClusterLr3                           = -999.0f;
    p_etaClusterLr1                         = -999.0f;
    p_etaClusterLr2                         = -999.0f;
    p_phiClusterLr2                         = -999.0f;
    p_eAccCluster                           = -999.0f;
    p_f0Cluster                             = -999.0f;
    p_etaCalo                               = -999.0;
    p_phiCalo                               = -999.0;
    p_eTileGap3Cluster                      = -999.0f;
    p_cellIndexCluster                      = -999.0f;
    p_phiModCalo                            = -999.0f;
    p_etaModCalo                            = -999.0f;
    p_dPhiTH3                               = -999.0f;
    p_R12                                   = -999.0f;
    p_fTG3                                  = -999.0f;
    p_photonConversionType                  = -999;
    p_photonConversionRadius                = -999.0f;
    p_photonVertexPtConvDecor               = -999.0f;
    p_photonVertexPtConv                    = -999.0f;
    p_photonVertexPt1                       = -999.0f;
    p_photonVertexPt2                       = -999.0f;
    p_photonVertexConvPtRatio               = -999.0f;
    p_photonVertexConvEtOverPt              = -999.0f;
    p_photonVertexRconv                     = -999.0f;
    p_photonVertexzconv                     = -999.0f;
    p_photonVertexPixHits1                  = 0;
    p_photonVertexSCTHits1                  = 0;
    p_photonVertexPixHits2                  = 0;
    p_photonVertexSCTHits2                  = 0;
    p_ECIDSResult                           = -999.0;
    p_photonIsTightEM                       = false;
    p_photonIsLooseEM                       = false;
    p_weta2                                 = -999.0f;
    p_Reta                                  = -999.0f;
    p_Rphi                                  = -999.0f;
    p_Eratio                                = -999.0f;
    p_f1                                    = -999.0f;
    p_f3                                    = -999.0f;
    p_Rhad                                  = -999.0f;
    p_Rhad1                                 = -999.0f;
    p_deltaEta1                             = -999.0f;
    p_deltaPhiRescaled2                     = -999.0f;
    p_eProbHT                               = -999.0f;
    p_LHValue                               = -999.0f;
    p_etcone20                              = -999.0f;
    p_etcone30                              = -999.0f;
    p_etcone40                              = -999.0f;
    p_topoetcone20                          = -999.0f;
    p_topoetcone30                          = -999.0f;
    p_topoetcone40                          = -999.0f;
    p_etcone20                              = -999.0f;
    p_etcone30                              = -999.0f;
    p_etcone40                              = -999.0f;
    p_etcone20ptCorrection                  = -999.0f;
    p_etcone30ptCorrection                  = -999.0f;
    p_etcone40ptCorrection                  = -999.0f;
    p_ptcone20                              = -999.0f;
    p_ptcone30                              = -999.0f;
    p_ptcone40                              = -999.0f;
    p_ptvarcone20                           = -999.0f;
    p_ptvarcone30                           = -999.0f;
    p_ptvarcone40                           = -999.0f;
    p_ptcone20_TightTTVA_pt500              = -999.0f;
    p_ptcone30_TightTTVA_pt500              = -999.0f;
    p_ptcone40_TightTTVA_pt500              = -999.0f;
    p_ptvarcone20_TightTTVA_pt500           = -999.0f;
    p_ptvarcone30_TightTTVA_pt500           = -999.0f;
    p_ptvarcone40_TightTTVA_pt500           = -999.0f;
    p_ptcone20_TightTTVA_pt1000             = -999.0f;
    p_ptcone30_TightTTVA_pt1000             = -999.0f;
    p_ptcone40_TightTTVA_pt1000             = -999.0f;
    p_ptvarcone20_TightTTVA_pt1000          = -999.0f;
    p_ptvarcone30_TightTTVA_pt1000          = -999.0f;
    p_ptvarcone40_TightTTVA_pt1000          = -999.0f;
    p_topoetcone20ptCorrection              = -999.0f;
    p_topoetcone30ptCorrection              = -999.0f;
    p_topoetcone40ptCorrection              = -999.0f;
    p_ptPU10                                = -999.0f;
    p_ptPU20                                = -999.0f;
    p_ptPU30                                = -999.0f;
    p_ptPU40                                = -999.0f;
    p_TRTTrackOccupancy                     = -999.0f;
    p_numberOfInnermostPixelHits            = 0;
    p_numberOfPixelHits                     = 0;
    p_numberOfSCTHits                       = 0;
    p_numberOfTRTHits                       = 0;
    p_numberOfTRTXenonHits                  = 0;
    p_chi2                                  = 0;
    p_ndof                                  = 0;
    p_SharedMuonTrack                       = 0;
    p_TruthType                             = 0;
    p_TruthOrigin                           = 0;
    p_truth_E                               = -999.0f;
    p_truth_matched                         = 0;
    p_truth_pt                              = -999;
    p_truth_phi                             = -999;
    p_truth_eta                             = -999;
    p_truth_E                               = -999;
    p_truth_pdgId                           = 0;
    p_truth_parent_pdgId                    = 0; 
    p_iffTruth                              = 0;
    pX_E7x7_Lr2                             = -999.0f;
    pX_E7x7_Lr3                             = -999.0f;
    pX_E_Lr0_HiG                            = -999.0f;
    pX_E_Lr0_LowG                           = -999.0f;
    pX_E_Lr0_MedG                           = -999.0f;
    pX_E_Lr1_HiG                            = -999.0f;
    pX_E_Lr1_LowG                           = -999.0f;
    pX_E_Lr1_MedG                           = -999.0f;
    pX_E_Lr2_HiG                            = -999.0f;
    pX_E_Lr2_LowG                           = -999.0f;
    pX_E_Lr2_MedG                           = -999.0f;
    pX_E_Lr3_HiG                            = -999.0f;
    pX_E_Lr3_LowG                           = -999.0f;
    pX_E_Lr3_MedG                           = -999.0f;
    pX_LHLoose                              = std::numeric_limits<char>::min();
    pX_LHMedium                             = std::numeric_limits<char>::min();
    pX_LHTight                              = std::numeric_limits<char>::min();
    pX_Loose                                = std::numeric_limits<char>::min();
    pX_m                                    = -999.0f;
    pX_Medium                               = std::numeric_limits<char>::min();
    pX_MultiLepton                          = std::numeric_limits<char>::min();
    pX_OQ                                   = 0;
    pX_Tight                                = std::numeric_limits<char>::min();
    pX_ambiguityType                        = 0;
    pX_asy1                                 = -999.0f;
    pX_author                               = 0;
    pX_barys1                               = -999.0f;
    pX_core57cellsEnergyCorrection          = -999.0f;
    pX_deltaEta0                            = -999.0f;
    pX_deltaEta2                            = -999.0f;
    pX_deltaEta3                            = -999.0f;
    pX_deltaPhi0                            = -999.0f;
    pX_deltaPhi1                            = -999.0f;
    pX_deltaPhi2                            = -999.0f;
    pX_deltaPhi3                            = -999.0f;
    pX_deltaPhiFromLastMeasurement          = -999.0f;
    pX_deltaPhiRescaled0                    = -999.0f;
    pX_deltaPhiRescaled1                    = -999.0f;
    pX_deltaPhiRescaled3                    = -999.0f;
    pX_e1152                                = -999.0f;
    pX_e132                                 = -999.0f;
    pX_e235                                 = -999.0f;
    pX_e255                                 = -999.0f;
    pX_e2ts1                                = -999.0f;
    pX_ecore                                = -999.0f;
    pX_emins1                               = -999.0f;

    pX_etconeCorrBitset                     = 0;
    pX_ethad                                = -999.0f;
    pX_ethad1                               = -999.0f;
    pX_f1core                               = -999.0f;
    pX_f3core                               = -999.0f;
    pX_maxEcell_energy                      = -999.0f;
    pX_maxEcell_gain                        = -999;
    pX_maxEcell_time                        = -999.0f;
    pX_maxEcell_x                           = -999.0f;
    pX_maxEcell_y                           = -999.0f;
    pX_maxEcell_z                           = -999.0f;
    pX_nCells_Lr0_HiG                       = 0;
    pX_nCells_Lr0_LowG                      = 0;
    pX_nCells_Lr0_MedG                      = 0;
    pX_nCells_Lr1_HiG                       = 0;
    pX_nCells_Lr1_LowG                      = 0;
    pX_nCells_Lr1_MedG                      = 0;
    pX_nCells_Lr2_HiG                       = 0;
    pX_nCells_Lr2_LowG                      = 0;
    pX_nCells_Lr2_MedG                      = 0;
    pX_nCells_Lr3_HiG                       = 0;
    pX_nCells_Lr3_LowG                      = 0;
    pX_nCells_Lr3_MedG                      = 0;
    pX_neflowisol20                         = -999.0f;
    pX_neflowisol20ptCorrection             = -999.0f;
    pX_neflowisol30                         = -999.0f;
    pX_neflowisol30ptCorrection             = -999.0f;
    pX_neflowisol40                         = -999.0f;
    pX_neflowisol40ptCorrection             = -999.0f;
    pX_neflowisolCorrBitset                 = 0;
    pX_neflowisolcoreConeEnergyCorrection   = -999.0f;
    pX_pos                                  = -999.0f;
    pX_pos7                                 = -999.0f;
    pX_poscs1                               = -999.0f;
    pX_poscs2                               = -999.0f;
    pX_ptconeCorrBitset                     = 0;
    pX_ptconecoreTrackPtrCorrection         = -999.0f;
    pX_r33over37allcalo                     = -999.0f;
    pX_topoetconeCorrBitset                 = 0;
    pX_topoetconecoreConeEnergyCorrection   = -999.0f;
    pX_topoetconecoreConeSCEnergyCorrection = -999.0f;
    pX_weta1                                = -999.0f;
    pX_widths1                              = -999.0f;
    pX_widths2                              = -999.0f;
    pX_wtots1                               = -999.0f;
    pX_e233                                 = -999.0f;
    pX_e237                                 = -999.0f;
    pX_e277                                 = -999.0f;
    pX_e2tsts1                              = -999.0f;
    pX_ehad1                                = -999.0f;
    pX_emaxs1                               = -999.0f;
    pX_fracs1                               = -999.0f;
    pX_DeltaE                               = -999.0f;
    pX_E3x5_Lr0                             = -999.0f;
    pX_E3x5_Lr1                             = -999.0f;
    pX_E3x5_Lr2                             = -999.0f;
    pX_E3x5_Lr3                             = -999.0f;
    pX_E5x7_Lr0                             = -999.0f;
    pX_E5x7_Lr1                             = -999.0f;
    pX_E5x7_Lr2                             = -999.0f;
    pX_E5x7_Lr3                             = -999.0f;
    pX_E7x11_Lr0                            = -999.0f;
    pX_E7x11_Lr1                            = -999.0f;
    pX_E7x11_Lr2                            = -999.0f;
    pX_E7x11_Lr3                            = -999.0f;
    pX_E7x7_Lr0                             = -999.0f;
    pX_E7x7_Lr1                             = -999.0f;
    pPX_convMatchDeltaEta1                  = -999.0f;
    pPX_convMatchDeltaEta2                  = -999.0f;
    pPX_convMatchDeltaPhi1                  = -999.0f;
    pPX_convMatchDeltaPhi2                  = -999.0f;

    tag_firstEgMotherTruthType              = 0;
    tag_firstEgMotherTruthOrigin            = 0;
    tag_firstEgMotherPdgId                  = 0;
    p_firstEgMotherTruthType                = 0;
    p_firstEgMotherTruthOrigin              = 0;
    p_firstEgMotherPdgId                    = 0;

    p_trueConv                              = 0;
    Truth                                   = 0;
    TruthSelection                          = 0;

    pdf_cut_vars_score                      = 0;
    pdf_vars_score                          = 0;
    Iso_score                               = 0;
    IsoCalo_score                           = 0;
    IsoTrack_score                          = 0;
    
    
    tracks_pt.clear();
    tracks_eta.clear();
    tracks_phi.clear();
    tracks_charge.clear();
    tracks_chi2.clear();
    tracks_ndof.clear();
    tracks_pixhits.clear();
    tracks_scthits.clear();
    tracks_trthits.clear();
    tracks_vertex.clear();
    tracks_z0.clear();
    tracks_d0.clear();
    tracks_dR.clear();
    tracks_theta.clear();
    tracks_sigmad0.clear();

    p_tracks_pt.clear();
    p_tracks_eta.clear();
    p_tracks_phi.clear();
    p_tracks_charge.clear();
    p_tracks_chi2.clear();
    p_tracks_ndof.clear();
    p_tracks_pixhits.clear();
    p_tracks_scthits.clear();
    p_tracks_trthits.clear();
    p_tracks_vertex.clear();
    p_tracks_z0.clear();
    p_tracks_d0.clear();
    p_tracks_theta.clear();
    p_tracks_sigmad0.clear();

    em_barrel_Lr0.clear();
    em_barrel_Lr1.clear();
    em_barrel_Lr2.clear();
    em_barrel_Lr3.clear();
    em_endcap_Lr0.clear();
    em_endcap_Lr1.clear();
    em_endcap_Lr2.clear();
    em_endcap_Lr3.clear();
    lar_endcap_Lr0.clear();
    lar_endcap_Lr1.clear();
    lar_endcap_Lr2.clear();
    lar_endcap_Lr3.clear();
    tile_barrel_Lr1.clear();
    tile_barrel_Lr2.clear();
    tile_barrel_Lr3.clear();
    tile_gap_Lr1.clear();
    
    time_em_barrel_Lr0.clear();
    time_em_barrel_Lr1.clear();
    time_em_barrel_Lr2.clear();
    time_em_barrel_Lr3.clear();
    time_em_endcap_Lr0.clear();
    time_em_endcap_Lr1.clear();
    time_em_endcap_Lr2.clear();
    time_em_endcap_Lr3.clear();
    time_lar_endcap_Lr0.clear();
    time_lar_endcap_Lr1.clear();
    time_lar_endcap_Lr2.clear();
    time_lar_endcap_Lr3.clear();
    time_tile_barrel_Lr1.clear();
    time_tile_barrel_Lr2.clear();
    time_tile_barrel_Lr3.clear();
    time_tile_gap_Lr1.clear();
    
    
}
