#include "MyAnalysis/RunUtils.h"
MORE_DIAGNOSTICS()

// See header file for documentation

TString getHelp( TString progName ) {
    TString help="\n  HELP\n    "+progName+" [CONFIG-FILES] [root files] [KEY: VALUE]\n    (order is irrelevant)\n\n";
    help+="    [CONFIG-FILES] TEnv text files with settings used both by algortihm and job submission.\n";
    help+="    [root files] if argument is of the form *.root* these will be used as input\n";
    help+="    Some basic config KEYs, which follow the same format as the CONFIG-FILE, are:\n";
    help+="      InputDir:      specifies a directory in which ROOT files may be found\n";
    help+="      InputFile:     specifies an input ROOT file. Can be used multiple times\n";
    help+="      InputFileList: specifies a text file containing a list of ROOT files to run over\n";
    help+="      GridDS:        specifies a grid data sample to run over\n";
    help+="      OutputDir:     specifies ouput directory. DATE is replaced by date+time [default: "+progName+"_DATE]\n";
    help+="      SampleName:    specifies sample name [default: sample]\n";
    help+="      BaseConfig:    overrides the default base configuration file (calibration smearing etc)\n";
    help+="\n  EXAMPLE:\n";
    help+="    "+progName+" MyAnalysis/dnielsen.cfg InputDir: /groups/hep/dnielsen/storage/ElectronPID2018/GridTestFromDAOD/user.dnielsen.mc16_13TeV.361101.Wplusmunu.e3601_s3126_r9781_r9778.EGAM1.013.ePID18_EXT0/ NumEvents: 1000 analysisType: 0 PrintFileList: NO OutputDir: outputDirs/Wpmunu\n";
    help+="  or\n    "+progName+" MyAnalysis/dnielsen.cfg\n";
    help+="  in the latter case, the above options can all be specified in the config file (MyAnalysis/dnielsen.cfg).\n";
    return help;
}

StrV parseArguments( Config *conf, int argc, char **argv ) {
    StrV files;
    for (int argi=1; argi<argc; ++argi) {
        TString arg=argv[argi];

        // 1. Check if argument is a configuration file. If so add it!
        if ( arg.EndsWith(".cfg") || arg.EndsWith(".config" )) {
            conf->addFile(arg); continue;
        }

        // 2. If the argument contains ".root", i.e. *.root*, add it to files to run over
        if (arg.Contains(".root")) {
            files.push_back(arg); continue;
        }

        // 3. If the arguemnt ends with colon, add a new argument
        if (arg.EndsWith(":")) {
            TString key(arg); key.ReplaceAll(":","");
            conf->setValue(key,argv[++argi]); continue;
        }

        // If we get here, the arguemnt cannot be understood
        fatal("Cannot interpret argument: '%s'! If you are trying to set variables, you must use ':' without a leading space but with a trailing space. See below.%s", arg.Data(), getHelp(argv[0]).Data());
    }
    return files;
}

void runJob( MyTagAndProbeAnalysis *alg, int argc, char** argv ) {
    TString help = getHelp(argv[0]);

    // Read config file
    Config conf;
    StrV files = parseArguments(&conf, argc, argv);

    TString progName = argv[0];
    TString sampleName = conf.getStrDefault("SampleName", "sample");
    TString submitDir  = conf.getStrDefault("OutputDir", progName+"_DATE");
    if (submitDir.Contains("DATE")) {
        TDatime now = TDatime();
        submitDir.ReplaceAll(" ","");
        submitDir.ReplaceAll("DATE",Form("%d.%.2d.%.2d_%.2d.%.2d.%.2d",
        now.GetYear(),now.GetMonth(),now.GetDay(),
        now.GetHour(),now.GetMinute(),now.GetSecond()));
        conf.setValue("OutputDir", submitDir.Data());
    }

    printf("\nOutput directory: %s\n",submitDir.Data());

    if ( fileExist(submitDir) )
    fatal("Output directory %s already exists.\n  Rerun after deleting it, or specifying a new one, like below.\n    OutputDir: NEWDIR", submitDir.Data() );

    if (!conf.isDefined("BaseConfig")) {
        fatal( "You must specify a base configuration file, option: BaseConfig. Exiting.%s", getHelp(argv[0]).Data() );
    } else {
        conf.addFile( conf.getStr("BaseConfig") );
    }

    if ( !conf.isDefined("analysisType") ) {
        _fatal( "You must specify analysis type. Use analysisType: 0 for T&P (signal), 1 for dumping reco from truth, 2 for background. Exiting." );
    }

    const std::vector< TString > runutilswhitelist = {
        "Debug", "InputDir", "SamplePattern", "InputFile", "InputFileList", "PrintFileList", "InputFaxDS", "GridDS", "nc_grid_filter", "xAODAthenaAccessMode", "xAODBranchAccessMode", "OutputDS", "UserName", "NFilesPerJob", "NoScout", "nc_nFiles", "nc_nFilesPerJob", "nc_nJobs", "nc_excludedSite", "nc_EventLoop_SubmitFlags", "nc_mergeOutput", "SubmitAndDownload", "NumEvents"
    };
    conf.addToWhiteList(runutilswhitelist);

    // Set debug level as early as possible
    if ( conf.getBoolDefault("Debug", false) )
        alg->setMsgLevel( MSG::DEBUG );

    // Add the confiuration to the algorithm!
    alg->setConfig(conf);

    // create a new sample handler to describe the data files we use
    SH::SampleHandler sh;

    if (conf.isDefined("InputDir")) {
        TString inputDir = conf.getStr("InputDir");
        SH::ScanDir()
            // .samplePattern(conf.getStrDefault("SamplePattern", "*.root*").Data())
            .scan(sh, inputDir.Data());
        if ( conf.getBoolDefault("PrintFileList", true) ) sh.print();
    } else if (conf.isDefined("InputFile")) {
        for (auto file : conf.getStrV("InputFile")) files.push_back(file);
        std::auto_ptr<SH::SampleLocal> sample (new SH::SampleLocal (sampleName.Data()));
        for ( TString file : files ) {
            sample->add (file.Data());
        }
        sh.add(sample.release());
        if ( conf.getBoolDefault("PrintFileList", true) ) sh.print();
    } else if (conf.isDefined("InputFileList")) {
        TString fileList=conf.getStr("InputFileList");
        if (!fileExist(fileList))
            fileList = gSystem->ExpandPathName(("$ROOTCOREBIN/data/"+fileList).Data());
        if (!fileExist(fileList))
            fatal("The input file-list specified: %s doesn't exist.",conf.getStr("InputFileList").Data());
        SH::readFileList(sh, sampleName.Data(), fileList.Data());
        if ( conf.getBoolDefault("PrintFileList", true) ) sh.print();
    } else if (conf.isDefined("InputFaxDS")) {
        SH::addGrid(sh, conf.getStr("InputFaxDS").Data());
    } else if (conf.isDefined("GridDS")) {
        for (TString sample: conf.getStrV("GridDS"))
            SH::scanRucio(sh, sample.Data());
    } else {
        fatal("No input specified!%s",help.Data());
    }

    sh.setMetaString("nc_tree", "CollectionTree");
    if (conf.isDefined("nc_grid_filter"))
        sh.setMetaString("nc_grid_filter", conf.getStrDefault("nc_grid_filter", "*.root*").Data());

    EL::Job job;
    job.sampleHandler(sh);

    std::string m_outputFileName = "myOutput";
    EL::OutputStream output("myOutput");
    job.outputAdd(output);
    EL::NTupleSvc *ntuple = new EL::NTupleSvc("myOutput");

    alg->m_outputName = m_outputFileName;
    job.algsAdd(ntuple);

    if ( conf.getBoolDefault("xAODAthenaAccessMode", true) )
        job.options()->setString(EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_athena);
    else if ( conf.getBoolDefault("xAODBranchAccessMode", true) )
        job.options()->setString(EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_branch);
    else
        job.options()->setString(EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class);

    job.options()->setDouble (EL::Job::optCacheSize, 384*1024*1024);
    job.options()->setDouble (EL::Job::optCacheLearnEntries, 1);

    if ( conf.getBoolDefault("Verbose", false) ){
        job.options()->setDouble (EL::Job::optXAODPerfStats, 1);
        job.options()->setDouble (EL::Job::optPrintPerFileStats, 1);
    }

    job.algsAdd(alg);

    if (conf.isDefined("GridDS")) {
        TString outDS;
        EL::PrunDriver driver;
        if (conf.isDefined("OutputDS")){
            outDS = conf.getStr("OutputDS");
        } else if (conf.isDefined("UserName")) {

            TString userName = conf.getStr("UserName");
            TString gridTag  = conf.getStrDefault("GridTag", "DATE");
            outDS = "user." + userName;
            outDS += ".%in:name[4]%.%in:name[7]%.";
            outDS += gridTag;
        } else {
            _fatal("You must specify grid user name (UserName) or OutputDS");
        }
        if (outDS.Contains("DATE")) {
            TDatime now = TDatime();
            outDS.ReplaceAll("DATE",Form("%d.%.2d.%.2d_%.2d.%.2d",
            now.GetYear(),now.GetMonth(),now.GetDay(),
            now.GetHour(),now.GetMinute()));
        }

        driver.options()->setString("nc_outputSampleName", outDS.Data());

        if (conf.isDefined("NFilesPerJob"))
        job.options()->setDouble(EL::Job::optGridNFilesPerJob,conf.getInt("NFilesPerJob"));

        if ( conf.getBoolDefault("NoScout", false) ){
            job.options()->setString (EL::Job::optSubmitFlags, "--skipScout");
        }

        for (auto opt : {"nc_nFiles", "nc_nFilesPerJob", "nc_nJobs"})
            if (conf.isDefined(opt))
                driver.options()->setDouble(opt, conf.getInt(opt));

        for (auto opt : {"nc_excludedSite", "nc_EventLoop_SubmitFlags", "nc_mergeOutput"})
            if (conf.isDefined(opt))
                driver.options()->setString(opt, conf.getStr(opt).Data());

        if ( conf.getBoolDefault("SubmitAndDownload", false) ) {
            driver.submit(job, submitDir.Data());
        } else {
            driver.submitOnly(job, submitDir.Data());
        }

        return;
    }

    if (conf.isDefined("NumEvents"))
        job.options()->setDouble(EL::Job::optMaxEvents,conf.getInt("NumEvents"));

    EL::DirectDriver driver;
    driver.submit(job, submitDir.Data());

    // xAOD::IOStats::instance().stats().printSmartSlimmingBranchList();

    printf("\nOutput directory: %s\n",submitDir.Data());

}
