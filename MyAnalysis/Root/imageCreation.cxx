#include <MyAnalysis/MyTagAndProbeAnalysis.h>

MORE_DIAGNOSTICS()

std::tuple< std::vector<float>, std::vector<float>, std::vector<float>, std::vector<float>, std::vector<int>, std::vector<float> > MyTagAndProbeAnalysis::phi_upscale(std::vector<float> c_eta, std::vector<float> c_deta, std::vector<float> c_phi, std::vector<float> c_dphi, std::vector<float> c_energy, std::vector<int> c_sampling, std::vector<float> c_time){
    // define the upscaled vectors
    std::vector<float> c_energy_up, c_eta_up, c_deta_up, c_phi_up, c_time_up;
    std::vector<int> c_sampling_up;

    // loop over all the cells that are given
    for( size_t i = 0; i < c_dphi.size(); i++ ){
        size_t size_cell;
        // for all ecalo layers consider upscaling
        if ( c_sampling[i] < 8 ){
            // get the size of the cell based on the binning you want ( how often does the fine binned cell fit in the original cell ) and round it to integers
            if ( m_fine_phi ){
                size_cell = std::round(c_dphi[i] / m_fine_phi_len);
            }
            else{
                size_cell = std::round(c_dphi[i] / m_phi_len);
            }
            // loop over the number of new cells 
            for (size_t j = 0; j < size_cell; j++ ){
                // the new cells should all have the same eta, deta, sampling and time value. the energy is divided by the number of cells it is split into 
                c_eta_up.push_back(c_eta[i]);
                c_deta_up.push_back(c_deta[i]);
                c_energy_up.push_back(c_energy[i] / size_cell);
                // put the phi coordinates evenly spaces around the original coordinate
                if ( m_fine_phi ){
                    c_phi_up.push_back( c_phi[i] + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_fine_phi_len ) );
                }
                else{
                    c_phi_up.push_back( c_phi[i] + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_phi_len ) );
                }
                c_sampling_up.push_back( c_sampling[i]);
                c_time_up.push_back( c_time[i] );
            }
        }
        else{
            // loop over the number of new cells 
            size_cell = std::round(c_dphi[i] / m_phi_len);
            for (size_t j = 0; j < size_cell; j++ ){
                // the new cells should all have the same eta, deta, sampling and time value. the energy is divided by the number of cells it is split into 
                c_eta_up.push_back(c_eta[i]);
                c_deta_up.push_back(c_deta[i]);
                c_energy_up.push_back(c_energy[i] / size_cell);
                // put the phi coordinates evenly spaces around the original coordinate
                c_phi_up.push_back( c_phi[i] + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_phi_len ) );
                c_sampling_up.push_back( c_sampling[i]);
                c_time_up.push_back( c_time[i] );
            }            
        }
    }
    // return all te upscaled values
    return {c_eta_up, c_deta_up, c_phi_up, c_energy_up, c_sampling_up, c_time_up};
}


std::tuple< std::vector<float>, std::vector<float>, std::vector<float>, std::vector<int>, std::vector<float> > MyTagAndProbeAnalysis::eta_upscale(std::vector<float> c_eta, std::vector<float> c_deta, std::vector<float> c_phi, std::vector<float> c_energy, std::vector<int> c_sampling, double cl_eta, std::vector<float> c_time){
    // define upscaled vectors
    std::vector<float> c_energy_up, c_eta_up, c_phi_up, c_time_up;
    std::vector<int> c_sampling_up;

    // loop over all cells
    for( size_t i = 0; i < c_eta.size(); i++ ){
        // ecalo cells are considered for the fine binning
        if( c_sampling[i] < 8 ){
            // for layer 1 in the em endcap there is a region which has a resolution of 0.025/6 which is not integer divisible (is that a word??) by the fine resolution 0.025/8 therefore this has to be considered seperately
            if( c_sampling[i] == 5 ){
                // do not round the cell size immediately to integers to catch the mentioned region
                double size_cell_d;
                if ( m_fine_eta ){
                    size_cell_d = c_deta[i] / m_fine_eta_len;
                }
                else{
                    size_cell_d = c_deta[i] / m_eta_len;
                }
                // in the mentioned region the cell size should be 4/3
                if( std::abs(size_cell_d - 1.33333) < 0.1 ){
                    // get the number of cells that are in the region in front of the considered cell (the region starts at 1.8)
                    int rounded_value = std::round( (std::abs(c_eta[i] + cl_eta) - 1.8) / m_fine_eta_len * 4. / 3. );
                    // depending on which cell was hit the energy split has to be different ( each subcell gets the energy based on the area in the eta phi space )
                    int position = rounded_value % 3;
                    if( position == 0 ){
                        // if the position is zero 3/4 of the cell belongs to the first upscaled cell and 1/4 to the second one
                        // std::signbit gives 0 for positive values and 1 for negative values
                        // the first cell is put 5/6 (1/6) to the left ( if eta runs from left to right ) of the eta coordinate for positive (negative) eta  
                        c_eta_up.push_back( c_eta[i] - ( 1. + 4. * std::signbit(cl_eta) ) * m_fine_eta_len / 6. );
                        c_eta_up.push_back( c_eta[i] + ( 5. - 4. * std::signbit(cl_eta) ) * m_fine_eta_len / 6. );
                        c_energy_up.push_back( c_energy[i] * ( 3. - 2. * std::signbit(cl_eta) ) / 4.);
                        c_energy_up.push_back( c_energy[i] * ( 1. + 2. * std::signbit(cl_eta) ) / 4.);
                        c_time_up.push_back( c_time[i] ); // attempt to give the subcell only the time of the cell with the largest area covered
                        c_time_up.push_back( 0. );
                    }
                    else if( position == 1 ){
                        // if the position is one both cells have 1/2
                        // one cell is 4/8 to the left and one 3/6 to the right of the original eta coordinate
                        c_eta_up.push_back( c_eta[i] - 3. * m_fine_eta_len / 6. );
                        c_eta_up.push_back( c_eta[i] + 3 * m_fine_eta_len / 6. );
                        // both get half of the energy
                        c_energy_up.push_back( c_energy[i] * 2. / 4.);
                        c_energy_up.push_back( c_energy[i] * 2. / 4.);
                        c_time_up.push_back( c_time[i] ); // attempt to give the subcell only the time of the cell with the largest area covered
                        c_time_up.push_back( c_time[i] );
                    }
                    else{
                        // if the position is two 3/4 of the cell belongs to the second upscaled cell and 1/4 to the first one
                        // the first cell is put 1/6 (5/6) to the left ( if eta runs from left to right ) of the eta coordinate for positive (negative) eta  
                        c_eta_up.push_back( c_eta[i] - ( 5. + 4. * std::signbit(cl_eta) ) * m_fine_eta_len / 6. );
                        c_eta_up.push_back( c_eta[i] + ( 1. - 4. * std::signbit(cl_eta) ) * m_fine_eta_len / 6. );
                        c_energy_up.push_back( c_energy[i] * ( 1. - 2. * std::signbit(cl_eta) ) / 4.);
                        c_energy_up.push_back( c_energy[i] * ( 3. + 2. * std::signbit(cl_eta) ) / 4.);
                        c_time_up.push_back( 0. ); // attempt to give the subcell only the time of the cell with the largest area covered
                        c_time_up.push_back( c_time[i] );
                    }
                    // push back all other values twice for the two cells
                    c_phi_up.push_back( c_phi[i] );
                    c_phi_up.push_back( c_phi[i] );
                    c_sampling_up.push_back( c_sampling[i] );
                    c_sampling_up.push_back( c_sampling[i] );                    
                }
                else{
                    // if the cell is outside of the bad region the "standard" upscaling can be used
                    size_t size_cell = std::round(size_cell_d);
                    // if the cells are smaller than the desired binning (only if 7 is asked) just keep the cell by setting size_cell to 1
                    if (size_cell == 0) size_cell = 1;
                    for (size_t j = 0; j < size_cell; j++ ){
                        // the new cells should all have the same phi, sampling and time value. the energy is divided by the number of cells it is split into 
                        // put the eta coordinates evenly spaces around the original coordinate                        
                        if ( m_fine_eta ){
                            c_eta_up.push_back(c_eta[i]  + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_fine_eta_len ));
                        }
                        else{
                            c_eta_up.push_back(c_eta[i]  + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_eta_len ));
                        }                        
                        c_energy_up.push_back(c_energy[i] / size_cell);
                        c_phi_up.push_back( c_phi[i] );
                        c_sampling_up.push_back( c_sampling[i]);
                        c_time_up.push_back( c_time[i] );
                    }
                }
            }
            else{
                // if the cell is not in the em encap layer 1 the "standard" upscaling can be used
                size_t size_cell;
                if ( m_fine_eta ){
                     size_cell = std::round(c_deta[i] / m_fine_eta_len);
                 }
                 else{
                     // if the cells are smaller than the desired binning (only if 7 is asked) just keep the cell by setting size_cell to 1
                     size_cell = std::round(c_deta[i] / m_eta_len);
                     if (size_cell == 0) size_cell = 1;
                 }
                for (size_t j = 0; j < size_cell; j++ ){
                    // the new cells should all have the same phi, sampling and time value. the energy is divided by the number of cells it is split into 
                    // put the eta coordinates evenly spaces around the original coordinate   
                    if ( m_fine_eta ){
                        c_eta_up.push_back(c_eta[i]  + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_fine_eta_len ));
                    }
                    else{
                        c_eta_up.push_back(c_eta[i]  + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_eta_len ));
                    }                    
                    c_energy_up.push_back(c_energy[i] / size_cell);
                    c_phi_up.push_back( c_phi[i] );
                    c_sampling_up.push_back( c_sampling[i]);
                    c_time_up.push_back( c_time[i] );
                }
            }
        }
        // had calo always not fine binning
        else{
            size_t size_cell = std::round(c_deta[i] / m_eta_len);
            // loop over the number of new cells 
            for (size_t j = 0; j < size_cell; j++ ){
                // the new cells should all have the same phi, sampling and time value. the energy is divided by the number of cells it is split into 
                // put the eta coordinates evenly spaces around the original coordinate
                c_eta_up.push_back(c_eta[i]  + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_eta_len ));
                c_energy_up.push_back(c_energy[i] / size_cell);
                c_phi_up.push_back( c_phi[i] );
                c_sampling_up.push_back( c_sampling[i]);
                c_time_up.push_back( c_time[i] );
            }
        }
    }

    return {c_eta_up, c_phi_up, c_energy_up, c_sampling_up, c_time_up};
}


// for comments see the other phi upscale
std::tuple< std::vector<float>, std::vector<float>, std::vector<float>, std::vector<float>, std::vector<int>, std::vector<float>, std::vector<int>, std::vector<float> > MyTagAndProbeAnalysis::phi_upscale(std::vector<float> c_eta, std::vector<float> c_deta, std::vector<float> c_phi, std::vector<float> c_dphi, std::vector<float> c_energy, std::vector<int> c_sampling, std::vector<float> c_time, std::vector<int> c_gain, std::vector<float> c_noise){
    std::vector<float> c_energy_up, c_eta_up, c_deta_up, c_phi_up, c_time_up, c_noise_up;
    std::vector<int> c_sampling_up, c_gain_up;

    for( size_t i = 0; i < c_dphi.size(); i++ ){
        size_t size_cell;
        if ( c_sampling[i] < 8 ){
            if ( m_fine_phi ){
                size_cell = std::round(c_dphi[i] / m_fine_phi_len);
            }
            else{
                size_cell = std::round(c_dphi[i] / m_phi_len);
            }
            for (size_t j = 0; j < size_cell; j++ ){
                c_eta_up.push_back(c_eta[i]);
                c_deta_up.push_back(c_deta[i]);
                c_energy_up.push_back(c_energy[i] / size_cell);
                if ( m_fine_phi ){
                    c_phi_up.push_back( c_phi[i] + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_fine_phi_len ) );
                }
                else{
                    c_phi_up.push_back( c_phi[i] + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_phi_len ) );
                }
                c_sampling_up.push_back( c_sampling[i]);
                c_time_up.push_back( c_time[i] );
                c_gain_up.push_back( c_gain[i] );
                c_noise_up.push_back( c_noise[i] );
            }
        }
        else{
            size_cell = std::round(c_dphi[i] / m_phi_len);
            for (size_t j = 0; j < size_cell; j++ ){
                c_eta_up.push_back(c_eta[i]);
                c_deta_up.push_back(c_deta[i]);
                c_energy_up.push_back(c_energy[i] / size_cell);
                c_phi_up.push_back( c_phi[i] + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_phi_len ) );
                c_sampling_up.push_back( c_sampling[i]);
                c_time_up.push_back( c_time[i] );
                c_gain_up.push_back( c_gain[i] );
                c_noise_up.push_back( c_noise[i] );
            }            
        }
    }
    return {c_eta_up, c_deta_up, c_phi_up, c_energy_up, c_sampling_up, c_time_up, c_gain_up, c_noise_up};
}


// for comments see the other eta upscale
std::tuple< std::vector<float>, std::vector<float>, std::vector<float>, std::vector<int>, std::vector<float>, std::vector<int>, std::vector<float> > MyTagAndProbeAnalysis::eta_upscale(std::vector<float> c_eta, std::vector<float> c_deta, std::vector<float> c_phi, std::vector<float> c_energy, std::vector<int> c_sampling, double cl_eta, std::vector<float> c_time, std::vector<int> c_gain, std::vector<float> c_noise){
    std::vector<float> c_energy_up, c_eta_up, c_phi_up, c_time_up, c_noise_up;
    std::vector<int> c_sampling_up, c_gain_up;

    for( size_t i = 0; i < c_eta.size(); i++ ){
        if( c_sampling[i] < 8 ){
            if( c_sampling[i] == 5 ){
                double size_cell_d;
                if ( m_fine_eta ){
                    size_cell_d = c_deta[i] / m_fine_eta_len;
                }
                else{
                    size_cell_d = c_deta[i] / m_eta_len;
                }
                if( std::abs(size_cell_d - 1.33333) < 0.1 ){
                    int rounded_value = std::round( (std::abs(c_eta[i] + cl_eta) - 1.8) / m_fine_eta_len * 4. / 3. );
                    int position = rounded_value % 3;
                    if( position == 0 ){
                        c_eta_up.push_back( c_eta[i] - ( 1. + 4. * std::signbit(cl_eta) ) * m_fine_eta_len / 8. );
                        c_eta_up.push_back( c_eta[i] + ( 5. - 4. * std::signbit(cl_eta) ) * m_fine_eta_len / 8. );
                        c_energy_up.push_back( c_energy[i] * ( 3. - 2. * std::signbit(cl_eta) ) / 4.);
                        c_energy_up.push_back( c_energy[i] * ( 1. + 2. * std::signbit(cl_eta) ) / 4.);
                        c_time_up.push_back( c_time[i] ); // attempt to give the subcell only the time of the cell with the largest area covered
                        c_time_up.push_back( 0. );
                    }
                    else if( position == 1 ){
                        c_eta_up.push_back( c_eta[i] - 4. * m_fine_eta_len / 8. );
                        c_eta_up.push_back( c_eta[i] + 4. * m_fine_eta_len / 8. );
                        c_energy_up.push_back( c_energy[i] * 2. / 4.);
                        c_energy_up.push_back( c_energy[i] * 2. / 4.);
                        c_time_up.push_back( c_time[i] ); // attempt to give the subcell only the time of the cell with the largest area covered
                        c_time_up.push_back( c_time[i] );
                    }
                    else{
                        c_eta_up.push_back( c_eta[i] - ( 5. + 4. * std::signbit(cl_eta) ) * m_fine_eta_len / 8. );
                        c_eta_up.push_back( c_eta[i] + ( 1. - 4. * std::signbit(cl_eta) ) * m_fine_eta_len / 8. );
                        c_energy_up.push_back( c_energy[i] * ( 1. - 2. * std::signbit(cl_eta) ) / 4.);
                        c_energy_up.push_back( c_energy[i] * ( 3. + 2. * std::signbit(cl_eta) ) / 4.);
                        c_time_up.push_back( 0. ); // attempt to give the subcell only the time of the cell with the largest area covered
                        c_time_up.push_back( c_time[i] );
                    }
                    c_phi_up.push_back( c_phi[i] );
                    c_phi_up.push_back( c_phi[i] );
                    c_sampling_up.push_back( c_sampling[i]);
                    c_sampling_up.push_back( c_sampling[i]);

                    c_gain_up.push_back( c_gain[i] );
                    c_gain_up.push_back( c_gain[i] );
                    c_noise_up.push_back( c_noise[i] );
                    c_noise_up.push_back( c_noise[i] );                    
                }
                else{
                    size_t size_cell = std::round(size_cell_d);
                    if (size_cell == 0) size_cell = 1;
                    for (size_t j = 0; j < size_cell; j++ ){
                        if ( m_fine_eta ){
                            c_eta_up.push_back(c_eta[i]  + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_fine_eta_len ));
                        }
                        else{
                            c_eta_up.push_back(c_eta[i]  + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_eta_len ));
                        }
                        c_energy_up.push_back(c_energy[i] / size_cell);
                        c_phi_up.push_back( c_phi[i] );
                        c_sampling_up.push_back( c_sampling[i]);
                        c_time_up.push_back( c_time[i] );
                        c_gain_up.push_back( c_gain[i] );
                        c_noise_up.push_back( c_noise[i] );
                    }
                }
            }
            else{
                size_t size_cell;
                if ( m_fine_eta ){
                     size_cell = std::round(c_deta[i] / m_fine_eta_len);
                 }
                 else{
                     size_cell = std::round(c_deta[i] / m_eta_len);
                     if (size_cell == 0) size_cell = 1;
                 }
                for (size_t j = 0; j < size_cell; j++ ){
                    if ( m_fine_eta ){
                        c_eta_up.push_back(c_eta[i]  + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_fine_eta_len ));
                    }
                    else{
                        c_eta_up.push_back(c_eta[i]  + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_eta_len ));
                    }
                    c_energy_up.push_back(c_energy[i] / size_cell);
                    c_phi_up.push_back( c_phi[i] );
                    c_sampling_up.push_back( c_sampling[i]);
                    c_time_up.push_back( c_time[i] );
                    c_gain_up.push_back( c_gain[i] );
                    c_noise_up.push_back( c_noise[i] );
                }
            }
        }
        else{
            size_t size_cell = std::round(c_deta[i] / m_eta_len);
            for (size_t j = 0; j < size_cell; j++ ){
                c_eta_up.push_back(c_eta[i]  + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_eta_len ));
                c_energy_up.push_back(c_energy[i] / size_cell);
                c_phi_up.push_back( c_phi[i] );
                c_sampling_up.push_back( c_sampling[i]);
                c_time_up.push_back( c_time[i] );
                c_gain_up.push_back( c_gain[i] );
                c_noise_up.push_back( c_noise[i] );
            }
        }
    }

    return {c_eta_up, c_phi_up, c_energy_up, c_sampling_up, c_time_up, c_gain_up, c_noise_up};
}


void MyTagAndProbeAnalysis::create_images(const particleWrapper* particle){
    ANA_MSG_DEBUG("::createImages: retrieving variables");
    const SG::AuxElement& probeAuxElementRef = *( particle->getAuxElement());

    ANA_MSG_DEBUG("::createImages: Cell variables with Accessors");
    // Get all the necessary cell values
/*    cell_energy                               = Accessor_cell_energy( probeAuxElementRef );
    cell_eta                                  = Accessor_cell_eta( probeAuxElementRef );
    cell_deta                                 = Accessor_cell_deta( probeAuxElementRef );
    cell_phi                                  = Accessor_cell_phi( probeAuxElementRef );
    cell_dphi                                 = Accessor_cell_dphi( probeAuxElementRef );
    cell_sampling                             = Accessor_cell_sampling( probeAuxElementRef );
    cell_time                                 = Accessor_cell_time( probeAuxElementRef );
*/

    ANA_MSG_DEBUG("::createImages: get CLuster of the particle");
    // get the cluster
    const xAOD::CaloCluster* cluster = particle->caloCluster();

    ANA_MSG_DEBUG("::createImages:: Cluster eta and phi");
    // get the eta/phi coordinate of the cluster
    double cl_eta = cluster->eta();
    double cl_phi = cluster->phi();


    ANA_MSG_DEBUG("::createImages: phi wrapping");
    // if the phi coordinate is larger than 2 or smaller than -2, the cluster could go over pi/-pi and the cell coordinates will not be continuuos, but will be broken
    // therefore all phi cell coordinates which are on the "wrong" side are put to the other side
    if ( cl_phi > 2 ){
        for( size_t i = 0; i < cell_phi.size(); i++ ){
            if (cell_phi[i] < 0){
                cell_phi[i] += 2 * TMath::Pi();
            }
        }
    }
    else if ( cl_phi < -2 ){
        for( size_t i = 0; i < cell_phi.size(); i++ ){
            if (cell_phi[i] > 0){
                cell_phi[i] += -2 * TMath::Pi();
            }
        }
    }

    ANA_MSG_DEBUG("::createImages: center cell coordinates");

    // center the cell coordinates around the cluster coordinates
    std::transform(cell_eta.begin(), cell_eta.end(), cell_eta.begin(),
          bind2nd(std::plus<double>(), -cl_eta));
    std::transform(cell_phi.begin(), cell_phi.end(), cell_phi.begin(),
          bind2nd(std::plus<double>(), -cl_phi));
          
          
    // if gain/noise images are wanted
    if ( m_gainNoiseImages ){
        // get gain and noise variables
        cell_gain                                   = Accessor_cell_gain( probeAuxElementRef );    
        cell_noise                                = Accessor_cell_totalnoise( probeAuxElementRef );
        
        // scale up in eta and phi
        std::tie(cell_eta, cell_deta, cell_phi, cell_energy, cell_sampling, cell_time, cell_gain, cell_noise) = phi_upscale(cell_eta, cell_deta, cell_phi, cell_dphi, cell_energy, cell_sampling, cell_time, cell_gain, cell_noise);
        std::tie(cell_eta, cell_phi, cell_energy, cell_sampling, cell_time, cell_gain, cell_noise) = eta_upscale(cell_eta, cell_deta, cell_phi, cell_energy, cell_sampling, cl_eta, cell_time, cell_gain, cell_noise);

        // resize the vectors to be 56x11 or whatever resolution is wanted
        resize_images(em_barrel_Lr0, time_em_barrel_Lr0, gain_em_barrel_Lr0, noise_em_barrel_Lr0, m_fine_eta, m_fine_phi);
        resize_images(em_barrel_Lr1, time_em_barrel_Lr1, gain_em_barrel_Lr1, noise_em_barrel_Lr1, m_fine_eta, m_fine_phi);
        resize_images(em_barrel_Lr2, time_em_barrel_Lr2, gain_em_barrel_Lr2, noise_em_barrel_Lr2, m_fine_eta, m_fine_phi);
        resize_images(em_barrel_Lr3, time_em_barrel_Lr3, gain_em_barrel_Lr3, noise_em_barrel_Lr3, m_fine_eta, m_fine_phi);
        resize_images(em_endcap_Lr0, time_em_endcap_Lr0, gain_em_endcap_Lr0, noise_em_endcap_Lr0, m_fine_eta, m_fine_phi);
        resize_images(em_endcap_Lr1, time_em_endcap_Lr1, gain_em_endcap_Lr1, noise_em_endcap_Lr1, m_fine_eta, m_fine_phi);
        resize_images(em_endcap_Lr2, time_em_endcap_Lr2, gain_em_endcap_Lr2, noise_em_endcap_Lr2, m_fine_eta, m_fine_phi);
        resize_images(em_endcap_Lr3, time_em_endcap_Lr3, gain_em_endcap_Lr3, noise_em_endcap_Lr3, m_fine_eta, m_fine_phi);
        resize_images(lar_endcap_Lr0, time_lar_endcap_Lr0, gain_lar_endcap_Lr0, noise_lar_endcap_Lr0, false, false );
        resize_images(lar_endcap_Lr1, time_lar_endcap_Lr1, gain_lar_endcap_Lr1, noise_lar_endcap_Lr1, false, false );
        resize_images(lar_endcap_Lr2, time_lar_endcap_Lr2, gain_lar_endcap_Lr2, noise_lar_endcap_Lr2, false, false );
        resize_images(lar_endcap_Lr3, time_lar_endcap_Lr3, gain_lar_endcap_Lr3, noise_lar_endcap_Lr3, false, false );
        resize_images(tile_barrel_Lr1, time_tile_barrel_Lr1, gain_tile_barrel_Lr1, noise_tile_barrel_Lr1, false, false );
        resize_images(tile_barrel_Lr2, time_tile_barrel_Lr2, gain_tile_barrel_Lr2, noise_tile_barrel_Lr2, false, false );
        resize_images(tile_barrel_Lr3, time_tile_barrel_Lr3, gain_tile_barrel_Lr3, noise_tile_barrel_Lr3, false, false );
        resize_images(tile_gap_Lr1, time_tile_gap_Lr1, gain_tile_gap_Lr1, noise_tile_gap_Lr1, false, false );

        // loop over all cells and fill the images 
        for( size_t j = 0; j < cell_sampling.size(); j++){
            // had calo images are by default always 7x11
            // whenever images can be combined, because the cells do not overlap that is done (all tile_barrel and tile_ext_barrel layers are combined, all gap scintillators are combined)
            fill_images(em_barrel_Lr0, time_em_barrel_Lr0, gain_em_barrel_Lr0, noise_em_barrel_Lr0, cell_eta[j], m_fine_eta, m_fine_phi, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 0);
            fill_images(em_barrel_Lr1, time_em_barrel_Lr1, gain_em_barrel_Lr1, noise_em_barrel_Lr1, cell_eta[j], m_fine_eta, m_fine_phi, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 1);
            fill_images(em_barrel_Lr2, time_em_barrel_Lr2, gain_em_barrel_Lr2, noise_em_barrel_Lr2, cell_eta[j], m_fine_eta, m_fine_phi, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 2);
            fill_images(em_barrel_Lr3, time_em_barrel_Lr3, gain_em_barrel_Lr3, noise_em_barrel_Lr3, cell_eta[j], m_fine_eta, m_fine_phi, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 3);
            fill_images(em_endcap_Lr0, time_em_endcap_Lr0, gain_em_endcap_Lr0, noise_em_endcap_Lr0, cell_eta[j], m_fine_eta, m_fine_phi, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 4);
            fill_images(em_endcap_Lr1, time_em_endcap_Lr1, gain_em_endcap_Lr1, noise_em_endcap_Lr1, cell_eta[j], m_fine_eta, m_fine_phi, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 5);
            fill_images(em_endcap_Lr2, time_em_endcap_Lr2, gain_em_endcap_Lr2, noise_em_endcap_Lr2, cell_eta[j], m_fine_eta, m_fine_phi, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 6);
            fill_images(em_endcap_Lr3, time_em_endcap_Lr3, gain_em_endcap_Lr3, noise_em_endcap_Lr3, cell_eta[j], m_fine_eta, m_fine_phi, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 7);
            fill_images(lar_endcap_Lr0, time_lar_endcap_Lr0, gain_lar_endcap_Lr0, noise_lar_endcap_Lr0, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 8);
            fill_images(lar_endcap_Lr1, time_lar_endcap_Lr1, gain_lar_endcap_Lr1, noise_lar_endcap_Lr1, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 9);
            fill_images(lar_endcap_Lr2, time_lar_endcap_Lr2, gain_lar_endcap_Lr2, noise_lar_endcap_Lr2, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 10);
            fill_images(lar_endcap_Lr3, time_lar_endcap_Lr3, gain_lar_endcap_Lr3, noise_lar_endcap_Lr3, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 11);
            fill_images(tile_barrel_Lr1, time_tile_barrel_Lr1, gain_tile_barrel_Lr1, noise_tile_barrel_Lr1, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 12);
            fill_images(tile_barrel_Lr1, time_tile_barrel_Lr1, gain_tile_barrel_Lr1, noise_tile_barrel_Lr1, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 18);
            fill_images(tile_barrel_Lr2, time_tile_barrel_Lr2, gain_tile_barrel_Lr2, noise_tile_barrel_Lr2, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 13);
            fill_images(tile_barrel_Lr2, time_tile_barrel_Lr2, gain_tile_barrel_Lr2, noise_tile_barrel_Lr2, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 19);
            fill_images(tile_barrel_Lr3, time_tile_barrel_Lr3, gain_tile_barrel_Lr3, noise_tile_barrel_Lr3, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 14);
            fill_images(tile_barrel_Lr3, time_tile_barrel_Lr3, gain_tile_barrel_Lr3, noise_tile_barrel_Lr3, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 20);
            fill_images(tile_gap_Lr1, time_tile_gap_Lr1, gain_tile_gap_Lr1, noise_tile_gap_Lr1, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 15);
            fill_images(tile_gap_Lr1, time_tile_gap_Lr1, gain_tile_gap_Lr1, noise_tile_gap_Lr1, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 16);
            fill_images(tile_gap_Lr1, time_tile_gap_Lr1, gain_tile_gap_Lr1, noise_tile_gap_Lr1, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], cell_gain[j], cell_noise[j], 17);

        }
    }
    else{
        // scale up in eta and phi
        ANA_MSG_DEBUG("::createImages: phi upscaling");
        std::tie(cell_eta, cell_deta, cell_phi, cell_energy, cell_sampling, cell_time) = phi_upscale(cell_eta, cell_deta, cell_phi, cell_dphi, cell_energy, cell_sampling, cell_time);
        ANA_MSG_DEBUG("::createImages: eta upscaling");
        std::tie(cell_eta, cell_phi, cell_energy, cell_sampling, cell_time) = eta_upscale(cell_eta, cell_deta, cell_phi, cell_energy, cell_sampling, cl_eta, cell_time);

        // resize the vectors to be 56x11 or whatever resolution is wanted
        ANA_MSG_DEBUG("::createImages: resize images");
        resize_images(em_barrel_Lr0, time_em_barrel_Lr0, m_fine_eta, m_fine_phi);
        resize_images(em_barrel_Lr1, time_em_barrel_Lr1, m_fine_eta, m_fine_phi);
        resize_images(em_barrel_Lr2, time_em_barrel_Lr2, m_fine_eta, m_fine_phi);
        resize_images(em_barrel_Lr3, time_em_barrel_Lr3, m_fine_eta, m_fine_phi);
        resize_images(em_endcap_Lr0, time_em_endcap_Lr0, m_fine_eta, m_fine_phi);
        resize_images(em_endcap_Lr1, time_em_endcap_Lr1, m_fine_eta, m_fine_phi);
        resize_images(em_endcap_Lr2, time_em_endcap_Lr2, m_fine_eta, m_fine_phi);
        resize_images(em_endcap_Lr3, time_em_endcap_Lr3, m_fine_eta, m_fine_phi);
        resize_images(lar_endcap_Lr0, time_lar_endcap_Lr0, false, false );
        resize_images(lar_endcap_Lr1, time_lar_endcap_Lr1, false, false );
        resize_images(lar_endcap_Lr2, time_lar_endcap_Lr2, false, false );
        resize_images(lar_endcap_Lr3, time_lar_endcap_Lr3, false, false );
        resize_images(tile_barrel_Lr1, time_tile_barrel_Lr1, false, false );
        resize_images(tile_barrel_Lr2, time_tile_barrel_Lr2, false, false );
        resize_images(tile_barrel_Lr3, time_tile_barrel_Lr3, false, false );
        resize_images(tile_gap_Lr1, time_tile_gap_Lr1, false, false );

        // loop over all cells and fill the images 
        ANA_MSG_DEBUG("::createImages: fill images");
        for( size_t j = 0; j < cell_sampling.size(); j++){
            // had calo images are by default always 7x11
            // whenever images can be combined, because the cells do not overlap that is done (all tile_barrel and tile_ext_barrel layers are combined, all gap scintillators are combined)
            fill_images(em_barrel_Lr0, time_em_barrel_Lr0, cell_eta[j], m_fine_eta, m_fine_phi, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 0);
            fill_images(em_barrel_Lr1, time_em_barrel_Lr1, cell_eta[j], m_fine_eta, m_fine_phi, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 1);
            fill_images(em_barrel_Lr2, time_em_barrel_Lr2, cell_eta[j], m_fine_eta, m_fine_phi, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 2);
            fill_images(em_barrel_Lr3, time_em_barrel_Lr3, cell_eta[j], m_fine_eta, m_fine_phi, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 3);
            fill_images(em_endcap_Lr0, time_em_endcap_Lr0, cell_eta[j], m_fine_eta, m_fine_phi, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 4);
            fill_images(em_endcap_Lr1, time_em_endcap_Lr1, cell_eta[j], m_fine_eta, m_fine_phi, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 5);
            fill_images(em_endcap_Lr2, time_em_endcap_Lr2, cell_eta[j], m_fine_eta, m_fine_phi, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 6);
            fill_images(em_endcap_Lr3, time_em_endcap_Lr3, cell_eta[j], m_fine_eta, m_fine_phi, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 7);
            fill_images(lar_endcap_Lr0, time_lar_endcap_Lr0, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 8);
            fill_images(lar_endcap_Lr1, time_lar_endcap_Lr1, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 9);
            fill_images(lar_endcap_Lr2, time_lar_endcap_Lr2, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 10);
            fill_images(lar_endcap_Lr3, time_lar_endcap_Lr3, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 11);
            fill_images(tile_barrel_Lr1, time_tile_barrel_Lr1, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 12);
            fill_images(tile_barrel_Lr1, time_tile_barrel_Lr1, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 18);
            fill_images(tile_barrel_Lr2, time_tile_barrel_Lr2, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 13);
            fill_images(tile_barrel_Lr2, time_tile_barrel_Lr2, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 19);
            fill_images(tile_barrel_Lr3, time_tile_barrel_Lr3, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 14);
            fill_images(tile_barrel_Lr3, time_tile_barrel_Lr3, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 20);
            fill_images(tile_gap_Lr1, time_tile_gap_Lr1, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 15);
            fill_images(tile_gap_Lr1, time_tile_gap_Lr1, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 16);
            fill_images(tile_gap_Lr1, time_tile_gap_Lr1, cell_eta[j], false, false, cell_phi[j], cell_energy[j], cell_sampling[j], cell_time[j], 17);
        }        
    }
    return;
}


void MyTagAndProbeAnalysis::resize_images(std::vector<std::vector<float>> &image, std::vector<std::vector<float>> &time_image, const bool fine_eta, const bool fine_phi){
    // smallest size in eta is 7 cells, fine binning gives 56 cells
    size_t eta_size = 7;
    if( fine_eta ){
        eta_size = 56;
    }
    // smallest size in phi is 11 cells, fine binning gives 55 cells    
    size_t phi_size = 11;
    if ( fine_phi ){
        phi_size = 55;
    }
    // resize all the wanted images to be the desired size e.g. 56x11 and fill every value with 0
    image.resize(eta_size);
    time_image.resize(eta_size);
    for( size_t i = 0; i< eta_size; i++ ){
        image[i].resize(phi_size);
        time_image[i].resize(phi_size);
        for( size_t j = 0; j < phi_size; j++){
            image[i][j] = 0.;
            time_image[i][j] = 0.;
        }
    }
}


void MyTagAndProbeAnalysis::fill_images(std::vector<std::vector<float>> &image, std::vector<std::vector<float>> &time_image, const float c_eta, const bool fine_eta, const bool fine_phi, const float c_phi, const float c_energy, const int c_sampling, const float c_time, const int layer){
    // Fill the image if the cell is in the right layer for the image
    if( c_sampling == layer ){
        // set a initial value which is larger than the highest index
        size_t eta_index = 60;
        // if fine biining in eta is wanted
        if( fine_eta ){
            // Loop over the bin edges and check if the cell eta value is in the bin and save the bin
            for( size_t k = 0; k < 56; k++ ){
                if ( m_fine_eta_bins[k] <= c_eta && m_fine_eta_bins[k+1] > c_eta ){
                    eta_index = k;
                }
            }
        }
        // if no fine binning in eta is wanted, had calo images will always use the not fine binning
        else{
            // Loop over the bin edges and check if the cell eta value is in the bin and save the bin
            for( size_t k = 0; k < 7; k++ ){
                if ( m_eta_bins[k] <= c_eta && m_eta_bins[k+1] > c_eta ){
                    eta_index = k;
                }
            }
        }
        // set a initial value which is larger than the highest index
        size_t phi_index = 60;
        // if fine biining in phi is wanted
        if( fine_phi ){
            // Loop over the bin edges and check if the cell eta value is in the bin and save the bin
            for( size_t k = 0; k < 56; k++ ){
                if ( m_fine_phi_bins[k] <= c_phi && m_fine_phi_bins[k+1] > c_phi ){
                    phi_index = k;
                }
            }
        }
        // if no fine binning in phi is wanted, had calo images will always use the not fine binning
        else{
            // Loop over the bin edges and check if the cell eta value is in the bin and save the bin
            for( size_t k = 0; k < 11; k++ ){
                if ( m_phi_bins[k] <= c_phi && m_phi_bins[k+1] > c_phi ){
                    phi_index = k;
                }
            }            
        }
        
        // if the initial eta or phi index is not updated something went wrong
        if( eta_index >= 60 ) return;
        if( phi_index >= 60 ) return;

        // Fill all the images. Add the value to the according bin. NOTE if there are more than one cell that contriutes to one bin, the energy image is fine, the others probably not
        image[eta_index][phi_index] += c_energy / 1000.; // scaled to GeV
        time_image[eta_index][phi_index] += c_time;
        
        return;
    }
    else{
        return;
    }
}


void MyTagAndProbeAnalysis::resize_images(std::vector<std::vector<float>> &image, std::vector<std::vector<float>> &time_image, std::vector<std::vector<int>> &gain_image, std::vector<std::vector<float>> &noise_image, const bool fine_eta, const bool fine_phi){
    // smallest size in eta is 7 cells, fine binning gives 56 cells
    size_t eta_size = 7;
    if( fine_eta ){
        eta_size = 56;
    }
    // smallest size in phi is 11 cells, fine binning gives 55 cells    
    size_t phi_size = 11;
    if ( fine_phi ){
        phi_size = 55;
    }
    // resize all the wanted images to be the desired size e.g. 56x11 and fill every value with 0
    image.resize(eta_size);
    time_image.resize(eta_size);
    gain_image.resize(eta_size);
    noise_image.resize(eta_size);
    for( size_t i = 0; i< eta_size; i++ ){
        image[i].resize(phi_size);
        time_image[i].resize(phi_size);
        gain_image[i].resize(phi_size);
        noise_image[i].resize(phi_size);
        for( size_t j = 0; j < phi_size; j++){
            image[i][j] = 0.;
            time_image[i][j] = 0.;
            gain_image[i][j] = 0;
            noise_image[i][j] = 0.;
        }
    }
}


void MyTagAndProbeAnalysis::fill_images(std::vector<std::vector<float>> &image, std::vector<std::vector<float>> &time_image, std::vector<std::vector<int>> &gain_image, std::vector<std::vector<float>> &noise_image, const float c_eta, const bool fine_eta, const bool fine_phi, const float c_phi, const float c_energy, const int c_sampling, const float c_time, const int c_gain, const float c_noise, const int layer){
    // Fill the image if the cell is in the right layer for the image
    if( c_sampling == layer ){
        // set a initial value which is larger than the highest index
        size_t eta_index = 60;
        // if fine biining in eta is wanted
        if( fine_eta ){
            // Loop over the bin edges and check if the cell eta value is in the bin and save the bin
            for( size_t k = 0; k < 56; k++ ){
                if ( m_fine_eta_bins[k] <= c_eta && m_fine_eta_bins[k+1] > c_eta ){
                    eta_index = k;
                }
            }
        }
        // if no fine binning in eta is wanted, had calo images will always use the not fine binning
        else{
            // Loop over the bin edges and check if the cell eta value is in the bin and save the bin
            for( size_t k = 0; k < 7; k++ ){
                if ( m_eta_bins[k] <= c_eta && m_eta_bins[k+1] > c_eta ){
                    eta_index = k;
                }
            }
        }
        // set a initial value which is larger than the highest index
        size_t phi_index = 60;
        // if fine biining in phi is wanted
        if( fine_phi ){
            // Loop over the bin edges and check if the cell eta value is in the bin and save the bin
            for( size_t k = 0; k < 56; k++ ){
                if ( m_fine_phi_bins[k] <= c_phi && m_fine_phi_bins[k+1] > c_phi ){
                    phi_index = k;
                }
            }
        }
        // if no fine binning in phi is wanted, had calo images will always use the not fine binning
        else{
            // Loop over the bin edges and check if the cell eta value is in the bin and save the bin
            for( size_t k = 0; k < 11; k++ ){
                if ( m_phi_bins[k] <= c_phi && m_phi_bins[k+1] > c_phi ){
                    phi_index = k;
                }
            }            
        }
        
        // if the initial eta or phi index is not updated something went wrong
        if( eta_index >= 60 ) return;
        if( phi_index >= 60 ) return;

        // Fill all the images. Add the value to the according bin. NOTE if there are more than one cell that contriutes to one bin, the energy image is fine, the others probably not
        image[eta_index][phi_index] += c_energy / 1000.; // scaled to GeV
        time_image[eta_index][phi_index] += c_time;
        gain_image[eta_index][phi_index] += c_gain;
        noise_image[eta_index][phi_index] += c_noise;
        
        return;
    }
    else{
        return;
    }
}


const double MyTagAndProbeAnalysis::m_eta_bins[8] = {-0.0875, -0.0625, -0.0375, -0.0125,  0.0125,  0.0375,  0.0625,
        0.0875 };

const double MyTagAndProbeAnalysis::m_fine_eta_bins[57] = { -0.0875  , -0.084375, -0.08125 , -0.078125, -0.075   , -0.071875,
       -0.06875 , -0.065625, -0.0625  , -0.059375, -0.05625 , -0.053125,
       -0.05    , -0.046875, -0.04375 , -0.040625, -0.0375  , -0.034375,
       -0.03125 , -0.028125, -0.025   , -0.021875, -0.01875 , -0.015625,
       -0.0125  , -0.009375, -0.00625 , -0.003125,  0.      ,  0.003125,
        0.00625 ,  0.009375,  0.0125  ,  0.015625,  0.01875 ,  0.021875,
        0.025   ,  0.028125,  0.03125 ,  0.034375,  0.0375  ,  0.040625,
        0.04375 ,  0.046875,  0.05    ,  0.053125,  0.05625 ,  0.059375,
        0.0625  ,  0.065625,  0.06875 ,  0.071875,  0.075   ,  0.078125,
        0.08125 ,  0.084375,  0.0875  };

const double MyTagAndProbeAnalysis::m_phi_bins[12] = {-0.13499031, -0.11044662, -0.08590292, -0.06135923, -0.03681554,
       -0.01227185,  0.01227185,  0.03681554,  0.06135923,  0.08590292,
        0.11044662,  0.13499031};

const double MyTagAndProbeAnalysis::m_fine_phi_bins[56] = {-0.13499031, -0.13008157, -0.12517283, -0.12026409, -0.11535536,
       -0.11044662, -0.10553788, -0.10062914, -0.0957204 , -0.09081166,
       -0.08590292, -0.08099419, -0.07608545, -0.07117671, -0.06626797,
       -0.06135923, -0.05645049, -0.05154175, -0.04663302, -0.04172428,
       -0.03681554, -0.0319068 , -0.02699806, -0.02208932, -0.01718058,
       -0.01227185, -0.00736311, -0.00245437,  0.00245437,  0.00736311,
        0.01227185,  0.01718058,  0.02208932,  0.02699806,  0.0319068 ,
        0.03681554,  0.04172428,  0.04663302,  0.05154175,  0.05645049,
        0.06135923,  0.06626797,  0.07117671,  0.07608545,  0.08099419,
        0.08590292,  0.09081166,  0.0957204 ,  0.10062914,  0.10553788,
        0.11044662,  0.11535536,  0.12026409,  0.12517283,  0.13008157,
        0.13499031};
