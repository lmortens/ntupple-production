#include <MyAnalysis/LightGBMpredictor.h>

MORE_DIAGNOSTICS()

LightGBMpredictor::LightGBMpredictor(){
    // Create a gbdt and not from file (since that apparently freezes)
    m_booster = LightGBM::Boosting::CreateBoosting("gbdt", nullptr);
}

void LightGBMpredictor::initialize(std::string txtFile){
    // Make an input file object
    std::ifstream input_file;

    // Open your trained model
    std::string filename = gSystem->Getenv("UserAnalysis_DIR"); // 
    filename += "/data/MyAnalysis/";
    filename += txtFile ;
    input_file.open( filename, std::ios::in | std::ios::binary );
    // Make an std::string from the file
    std::string model_str((std::istreambuf_iterator<char>(input_file)), std::istreambuf_iterator<char>());
    
    // Get length as well since that is needed when passing the raw string
    size_t len = std::strlen(model_str.data());
    
    // Load model from the string from your model file
    m_booster->LoadModelFromString(model_str.data(), len);
    
    // close input file ( not necessary, but to be explicit )
    input_file.close();
    
    return;
}


double LightGBMpredictor::predict(std::vector<double> features){
    // Make score variable
    double score;

    static LightGBM::PredictionEarlyStopInstance early_stop = CreatePredictionEarlyStopInstance("none", LightGBM::PredictionEarlyStopConfig());

    // Predict on features, save in score
    m_booster->Predict(&features[0], &score, &early_stop);
    return score;
}

