#include "MyAnalysis/Helpers.h"

#include "xAODEgamma/EgammaxAODHelpers.h"

#include <TObjArray.h>
#include <TCollection.h>
#include <TObjString.h>
#include <TSystem.h>
MORE_DIAGNOSTICS()



//============================================================================
// Generic helpful snippets used in the analysis
//============================================================================

void _fatal( char const * const file, int line, char const * const format, ... ) {
	char out[1000];
	sprintf( out, "\n  FATAL: %s (%s:%d)\n", format, file, line );
	va_list args;
	va_start( args, format );
	vprintf( out, args );
	va_end( args );
	abort();
}

void _fatal( char const * const file, int line, char const * const format ) {
	char out[1000];
	sprintf( out, "\n  FATAL: %s (%s:%d)\n", format, file, line );
	printf( out );
	abort();
}

void _fatal( char const * const format ) {
	char out[1000];
	sprintf( out, "\n  FATAL: %s\n", format );
	printf( out );
	abort();
}



//============================================================================
// Assorted helpers
//============================================================================

float deltaR( const float eta1, const float phi1, const float eta2, const float phi2 ) {
	const float deta = fabs(eta1 - eta2);

	float dphi;
	if ( fabs(phi1 - phi2 - 2.0*3.14159265359) < fabs(phi1 - phi2) ) {
		dphi = fabs(phi1 - phi2 - 2.0*3.14159265359) ;
	} else {
		dphi = fabs(phi1 - phi2);
	}

	return sqrt( deta*deta + dphi*dphi );
}



//============================================================================
// Truth extraction
//============================================================================

void fillTruth( const xAOD::TruthParticle* tp, bool& truth_matched, float& truth_pt, float& truth_phi, float& truth_eta, float& truth_e, int& truth_pdgId, int& truth_parent_pdgId ) {
	truth_matched = true;
	truth_pt = tp->pt();
	truth_phi = tp->phi();
	truth_eta = tp->eta();
	truth_e = tp->e();
	truth_pdgId = tp->pdgId();

	const xAOD::TruthParticle* true_parent = tp->parent();
	if ( true_parent )
		truth_parent_pdgId = true_parent->pdgId();
	else
		truth_parent_pdgId = 0;
}

void retrieveTruth( const xAOD::IParticle* particle, bool& truth_matched, float& truth_pt, float& truth_phi, float& truth_eta, float& truth_e, int& truth_pdgId, int& truth_parent_pdgId ) {
	const xAOD::TruthParticle* tp = xAOD::TruthHelpers::getTruthParticle( *particle );
	if ( !tp ) return;
	fillTruth( tp, truth_matched, truth_pt, truth_phi, truth_eta, truth_e, truth_pdgId, truth_parent_pdgId );

}


bool isTruePhoton(int p_type){
    return (p_type == 14);
}


// Following fucntions taken from Tag and Probe Frame ( more or less )
bool isTrueElectron(int p_type, int p_origin,  int mother_origin){

    bool w = isTrueElectronFromW( p_type, p_origin, mother_origin);
    bool z = isTrueElectronFromZ( p_type, p_origin, mother_origin);
    bool jpsi = isTrueElectronFromPromptJpsi( p_type, p_origin);
    // bool ttbar = isTrueElectronFromTtbar( p_type, p_origin, mother_origin);

    return w || z || jpsi || ( p_type == 2 );
}

bool isTrueElectronFromW(int p_type, int p_origin, int mother_origin){
    bool isPromptElectron = (p_type == 2 && p_origin == 12);

    bool isFSRorBrem = false;
    if ( (p_type == 4 && mother_origin == 12) ) isFSRorBrem = true;

    return ( isPromptElectron || isFSRorBrem );

}

bool isTrueElectronFromZ(int p_type, int p_origin, int mother_origin){
    bool isPromptElectron = (p_type == 2 && p_origin == 13);

    bool isFSRorBrem = false;
    if ( (p_type == 4 && mother_origin == 13) ) isFSRorBrem = true;

    return ( isPromptElectron || isFSRorBrem );
}

bool isTrueElectronFromPromptJpsi(int p_type, int p_origin){
    return (p_type == 2 && (p_origin == 27 || p_origin == 28));
}

bool isTrueElectronFromTtbar(int p_type, int p_origin, int mother_origin){
    bool isPromptElectron    = false;//
    if (p_type == 2 && p_origin == 10) isPromptElectron=true;
    if (p_type == 3 && p_origin == 12) isPromptElectron=true;

    bool isFSRorBrem = false;

    if (p_type == 4 && (mother_origin == 10 || mother_origin == 40) ) isFSRorBrem = true;

    return (isPromptElectron || isFSRorBrem);
}


//============================================================================
// For the configuration
//============================================================================

StrV vectorize(TString str, TString sep) {
	StrV result;
	TObjArray *strings = str.Tokenize(sep.Data());
	if (strings->GetEntries()==0) { delete strings; return result; }
	TIter istr(strings);
	while (TObjString* os=(TObjString*)istr()) {
		// the number sign and everything after is treated as a comment
		if (os->GetString()[0]=='#') break;
		result.push_back(os->GetString());
	}
	delete strings;
	return result;
}

// convert a text line containing a list of numbers to a vector<double>
NumV vectorizeNum(TString str, TString sep) {
	NumV result; StrV vecS = vectorize(str,sep);
	for (uint i=0;i<vecS.size();++i)
	result.push_back(atof(vecS[i]));
	return result;
}

// checks if a given file or directory exist
bool fileExist(TString fn) {
	return !(gSystem->AccessPathName(fn.Data()));
}
