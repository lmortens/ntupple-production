#include "MyAnalysis/Helpers.h"
#include "MyAnalysis/particleWrapper.h"
MORE_DIAGNOSTICS()

using std::runtime_error;

// 0 = undefined
// 1 = electron
// 2 = muon
// 3 = photon
// 4 = forward electron

particleWrapper::particleWrapper( const xAOD::Electron* el, const bool isCentralElectron ){
    if (isCentralElectron){
        electron = el;
        type = 1;
    }
    else{
        forward_electron = el;
        type = 4;
    }
}

particleWrapper::particleWrapper( const xAOD::Muon* mu )
    : muon(mu), type(2) {
    }
particleWrapper::particleWrapper( const xAOD::Photon* ph )
    : photon(ph), type(3) {
}


bool particleWrapper::sameAs( const particleWrapper& rhs ) const {
    if ( type != rhs.type ) return false;
    switch ( type ) {
        case 1:  return electron == rhs.electron; break;
        case 2:  return muon == rhs.muon; break;
        case 3:  return photon == rhs.photon; break;
        case 4:  return forward_electron == rhs.forward_electron; break;
        default: throw runtime_error("Type is bad value!"); break;
    }
}

bool particleWrapper::sameAs( const xAOD::Electron* el ) const {
    return ( type == 1 && el != nullptr && electron == el );
}

const TLorentzVector& particleWrapper::p4() const {
    switch ( type ) {
        case 1:  return electron->p4(); break;
        case 2:  return muon->p4(); break;
        case 3:  return photon->p4(); break;
        case 4:  return forward_electron->p4(); break;
        default: throw runtime_error("Type is bad value!"); break;
    }
}

float particleWrapper::charge() const {
    switch ( type ) {
        case 1:  return electron->charge(); break;
        case 2:  return muon->charge(); break;
        case 3:  return 0.0f; break;
        case 4:  return forward_electron->charge(); break;
        default: throw runtime_error("Type is bad value!"); break;
    }
}

const xAOD::TrackParticle* particleWrapper::trackParticle( std::size_t index ) const {
    switch ( type ) {
        case 1:  return electron->trackParticle( index ); break;
        case 2:  return muon->primaryTrackParticle(); break;
        case 3:  return nullptr; break;
        case 4:  return forward_electron->trackParticle( index ); break;
        default: throw runtime_error("Type is bad value!"); break;
    }
}

const xAOD::CaloCluster* particleWrapper::caloCluster() const {
    switch ( type ) {
        case 1:  return electron->caloCluster(); break;
        case 2:  return nullptr; break;
        case 3:  return photon->caloCluster(); break;
        case 4:  return forward_electron->caloCluster(); break;
        default: throw runtime_error("Type is bad value!"); break;
    }
}

const xAOD::IParticle* particleWrapper::getIParticle() const {
    switch ( type ) {
        case 1:  return static_cast<const xAOD::IParticle*>(electron); break;
        case 2:  return static_cast<const xAOD::IParticle*>(muon); break;
        case 3:  return static_cast<const xAOD::IParticle*>(photon); break;
        case 4:  return static_cast<const xAOD::IParticle*>(forward_electron); break;
        default: throw runtime_error("Type is bad value!"); break;
    }
}

const SG::AuxElement* particleWrapper::getAuxElement() const {
    switch ( type ) {
        case 1:  return static_cast<const SG::AuxElement*>(electron); break;
        case 2:  return static_cast<const SG::AuxElement*>(muon); break;
        case 3:  return static_cast<const SG::AuxElement*>(photon); break;
        case 4:  return static_cast<const SG::AuxElement*>(forward_electron); break;
        default: throw runtime_error("Type is bad value!"); break;
    }
}

int particleWrapper::nTrackParticles() const {
    switch ( type ) {
        case 1:  return electron->nTrackParticles(); break;
        case 2:  return 0; break;
        case 3:  return 0; break;
        case 4:  return forward_electron->nTrackParticles(); break;
        default: throw runtime_error("Type is bad value!"); break;
    }
}
