#include "MyAnalysis/Config.h"
#include "MyAnalysis/Helpers.h"
MORE_DIAGNOSTICS()

Config::Config()
	: m_env("env") {
	// Must have no pointer initialization, for CINT
	m_env.IgnoreDuplicates(true);
}

Config::Config(const Config &config)
	: Config() {
	config.m_env.Copy(m_env);
	copyTable(config.m_env);
}

Config::Config(TString fileName)
	: Config() {
	addFile(fileName);
}

void Config::ensureDefined(TString key) {
	if (!isDefined(key)) fatal("Config: No value found for: %s\n  The code requested a setting which was not found the config file. Default hard-coded values are not adviced, so always fill up the base config.",key.Data());
	addToWhiteList(key);
}

bool Config::isDefined(TString key) {
	return m_env.Defined(key);
}

TString Config::getStr(TString key) {
	ensureDefined(key);
	return gSystem->ExpandPathName(m_env.GetValue(key,""));
}

TString Config::getStrDefault(TString key, TString dflt) {
	if (!isDefined(key)) m_env.SetValue(key,dflt);
	addToWhiteList(key);
	return gSystem->ExpandPathName(m_env.GetValue(key, dflt));
}

int Config::getInt(TString key) {
	ensureDefined(key);
	return m_env.GetValue(key,-99);
}

int Config::getIntDefault(TString key, int dflt) {
	if (!isDefined(key)) m_env.SetValue(key,dflt);
	addToWhiteList(key);
	return m_env.GetValue(key, dflt);
}

bool Config::getBool(TString key) {
	ensureDefined(key);
	return m_env.GetValue(key,false);
}

bool Config::getBoolDefault(TString key, bool dflt) {
	if (!isDefined(key)) m_env.SetValue(key,dflt);
	addToWhiteList(key);
	return m_env.GetValue(key, dflt);
}

double Config::getNum(TString key) {
	ensureDefined(key);
	return m_env.GetValue(key,-99.0);
}

double Config::getNumDefault(TString key, double dflt) {
	if (!isDefined(key)) m_env.SetValue(key,dflt);
	addToWhiteList(key);
	return m_env.GetValue(key,dflt);
}

StrV Config::getStrV(TString key) {
	ensureDefined(key);
	return vectorize(m_env.GetValue(key,"")," \t");
}

NumV Config::getNumV(TString key) {
	ensureDefined(key);
	return vectorizeNum(m_env.GetValue(key,"")," \t");
}

void Config::printDB() {
	TIter next(m_env.GetTable());
	while (TEnvRec *er = (TEnvRec*) next()) {
		bool cont = false;
		for ( const auto& s : m_ignoreList ) if ( TString(er->GetName()).BeginsWith(s) && er->GetName() != s+".Use"+s ) {cont=true;break;}
		if ( cont ) continue;
		printf("    %-50s%s\n",Form("%s:",er->GetName()),er->GetValue());
	}
	printf("\n\n");
}

void Config::addFile(TString fileName) {
	TString path(fileName);
	if (!fileExist(path))
		path = PathResolverFindCalibFile(fileName.Data());
	if (path == "")
		fatal("Config: Cannot find settings file %s\n  also searched in %s", fileName.Data(), path.Data());

	// settings read in by files should not overwrite values set by setValue()
	TEnv env;
	int status = env.ReadFile(path.Data(),EEnvLevel(0));
	if (status!=0)
		fatal("Config: Cannot read settings file %s",fileName);
	TIter next(env.GetTable());
	while (TEnvRec *er = (TEnvRec*) next())
	if (!isDefined(er->GetName()))
		setValue(er->GetName(),er->GetValue());
}

void Config::checkWhitelist() {
	TIter next(m_env.GetTable());
	while (TEnvRec *er = (TEnvRec*) next()) {
		const TString& str = er->GetName();
		if ( std::find_if( m_whiteList.begin(), m_whiteList.end(), [&]( const TString s ) { return s.EqualTo(str); } ) == m_whiteList.end() )
			fatal( "Config: The variable in your config file or command line '%s' is unknown! Did you misspell it?", str.Data() );
	}
}
