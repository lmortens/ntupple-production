# About
There are 2 versions, a very simple one which reads all electron container and dump the information to root ntuple and another more sophisticated that applies a tag and probe selection for signal and a dump for background.

# Note about acm
When compiling code, `acm` is now used to do the `cmake`, `make`, and `source */setup.sh` for us. This saves us a few commands, and the code compiles correctly on any system (slc6, centos 6/7). If you've used `cmake` before, you must re-create the `build` directory.

# Setup
First, a few words of caution. Run these from a fresh terminal where you have NOT used `lsetup root` nor `lsetup python`, and make sure that you do not have miniconda prepended to your path (append instead if you have).

Run the commands below

1. setupATLAS && lsetup git
2. mkdir NtupleProd && cd NtupleProd
3. mkdir -p build run/outputDirs source
4. cd source
5. git clone ssh://git@gitlab.cern.ch:7999/EgammaMachineLearning/NtupleProd.git . # note the dot!
6. git checkout -b master-lukas origin/master-lukas
7. git pull
8. cd ../build
9. acmSetup AnalysisBase,21.2.66 # you might have to do ",here" by the end
10. cd ../source
11. git clone ssh://git@gitlab.cern.ch:7999/ATLAS-IFF/IFFTruthClassifier.git

Add LightGBM following the description on https://gitlab.cern.ch/dnielsen/_lightgbm . You do not need to do Steps 2 and 3, but you should add the GIT_TAG stable as described after Step 4.


12. acm compile
13. cd ../run
14. ln -s ../source/MyAnalysis/util/runNtuples.sh
15. ln -s ../source/MyAnalysis/data/testdataset.txt

Additionally you need LumiCalc files to prescale MC for the prescaled triggers and the model files for the LightGBM models. These can be found at /eos/user/l/lehrke/Data/NtupleProd_data/ . The content of this file needs to be copied into the folder MyAnalysis/data .

# On every login
Go to the build folder and restore:
1. cd NtupleProd/build
2. acmSetup

# To compile
Run the command below from *any* directory:
1. acm compile

If you have added files/packages, you need to run the following beforehand:
1. acm find_packages

# To run
Use the run script. In the run folder, run `./runNtuples.sh -h` to get the help screen. By default, it will dry-run if the `-r` (run) switch is not supplied.

# To run without the run script
`cd run` and use `runMyTagAndProbeAnalysis`. It has a help menu. Most importantly is the `analysisType: 0` which is a T&P based signal selection and a background selection for electrons, while for photons all truth matched photons are saved and every tenth other particle.

Look into MyAnalysis/data/AnalysisBase.cfg to see all the settings used. As settings are moved to the configuration, their default values will be found in that file.
